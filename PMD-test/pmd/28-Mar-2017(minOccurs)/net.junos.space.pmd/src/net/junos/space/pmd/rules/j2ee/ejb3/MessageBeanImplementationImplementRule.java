package net.junos.space.pmd.rules.j2ee.ejb3;

import java.util.ArrayList;

import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;
import net.sourceforge.pmd.RuleContext;

public class MessageBeanImplementationImplementRule extends AbstractMessageListenerMethodRule {
	
	@Override
	public String getMessage() {
		return "The message-driven bean class must implement, directly or indirectly, "
			+"the message listener interface required by the messaging type that it supports or the methods of the message listener interface. "
			+"In the case of JMS, this is the javax.jms.MessageListener interface.";
	}
	
	@Override
	public void doEnd(RuleContext ctx) {
		for( IClassOutline outline : getOutlineContainer().getAllOutlines() ){
			
			IJavaTypeDeclaration typeDeclaration = outline.getTypeDeclaration();
			String qualifiedname = outline.getQualifiedNameString() ;
			if(!isMessageDrivenBeanEJB3(qualifiedname) )
				continue ;
			
			if(!outline.hasAllSuperclassesDefined(getOutlineContainer()))
				continue ;
			
			ArrayList<IJavaTypeDeclaration> messageListenerInterfaces
					= getMessageListenerInterfaces(typeDeclaration) ;
			
			if(messageListenerInterfaces.isEmpty())
				reportError(ctx, outline, typeDeclaration) ;
		}
		super.doEnd(ctx);
	}
}
