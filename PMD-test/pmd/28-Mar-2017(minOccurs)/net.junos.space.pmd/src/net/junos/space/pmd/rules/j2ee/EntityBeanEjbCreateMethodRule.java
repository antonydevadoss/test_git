package net.junos.space.pmd.rules.j2ee;

import net.junos.space.pmd.utils.outline.IClassOutline;

public class EntityBeanEjbCreateMethodRule extends AbstractCreateMethodExistenceRule {

	@Override
	public boolean isProperBean(IClassOutline outline) {
		return isEntityBeanEJB2(outline.getTypeDeclaration());
	}
	
	@Override
	public String getMessage() {
		return "Declare a ejbCreate method public for an Entity Bean";
	}
}
