package net.junos.space.pmd.rules.rest;

import java.util.List;

import net.juniper.common.client.nativejson.JSONArray;
import net.juniper.common.client.nativejson.JSONObject;

public class RBACInfo {
		private List<String> crudTypes;
		private List<String>  capabilities;
		private List<String> tasks;
		
		public List<String> getCrudTypes() {
			return crudTypes;
		}
		public void setCrudTypes(List<String> crudTypes) {
			this.crudTypes = crudTypes;
		}
		public List<String> getCapabilities() {
			return capabilities;
		}
		public void setCapabilities(List<String> capabilities) {
			this.capabilities = capabilities;
		}
		public List<String> getTasks() {
			return tasks;
		}
		public void setTasks(List<String> tasks) {
			this.tasks = tasks;
		}
		
		@Override
		public String toString() {
			StringBuffer br = new StringBuffer();
			if(crudTypes != null && !crudTypes.isEmpty()) {
				br.append("CRUD=[").append(join(crudTypes)).append("],");
			}
			if(capabilities != null && !capabilities.isEmpty()) {
				br.append("Capabilities=[").append(join(capabilities)).append("],");
			}
			if(tasks != null && !tasks.isEmpty()) {
				br.append("Tasks=[").append(join(tasks)).append("],");
			}
			if(br.length() > 0) {
				br.deleteCharAt(br.length()-1);
			}
			return br.toString();
		}
		
		private String join(List<String> stringList) {
			String res = null;
			if(stringList != null && !stringList.isEmpty()) {
				StringBuffer br = new StringBuffer();
				for (String string : stringList) {
					br.append(string).append(",");
				}
				br.deleteCharAt(br.length()-1);
				res=br.toString();
			}
			return res;
		}
		
		private JSONArray toJSONArray(List<String> list) {
			JSONArray arr = new JSONArray();
			if(list != null) {
				for (String item : list) {
					arr.put(item);
				}
			}
			return arr;
		}
		
		public JSONObject toJSONObject() {
			JSONObject obj = new JSONObject();
			try {
				if(crudTypes != null && !crudTypes.isEmpty()) {
					obj.put("crud-types", toJSONArray(crudTypes));
				}
				if(capabilities != null && !capabilities.isEmpty()) {
					obj.put("capabilities", toJSONArray(capabilities));
				}
				if(tasks != null && !tasks.isEmpty()) {
					obj.put("tasks", toJSONArray(tasks));
				}
			} catch(Exception e) {
				System.out.println("Error in RBACInfo::toJSONObject");
				e.printStackTrace();
			}

			return obj;
		}
}
