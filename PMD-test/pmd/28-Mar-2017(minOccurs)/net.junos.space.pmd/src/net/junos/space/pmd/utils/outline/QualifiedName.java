package net.junos.space.pmd.utils.outline;

import java.util.ArrayList;
import java.util.List;

public class QualifiedName {
	
	public QualifiedName(String simpleName, String contextQualifiedName) {
		super();
		this.simpleName = simpleName;
		this.contextQualifiedName = contextQualifiedName;
		this.values = new ArrayList<String>() ;
	}

	public QualifiedName( String shortName, List<String> values) {
		super();
		this.values = values != null ? new ArrayList<String>(values) : new ArrayList<String>() ;
		this.simpleName = shortName;
		contextQualifiedName = null ;
	}
	
	public QualifiedName(String qualifiedNameString) {
		super();
		
		int ind = qualifiedNameString.lastIndexOf(".") ;
		if(ind < 0)
			ind = -1 ;
		
		this.simpleName = qualifiedNameString.substring(ind+1) ;
		
		this.values = new ArrayList<String>();
		values.add(qualifiedNameString) ;		
		
		contextQualifiedName = null ;
	}

	final ArrayList<String> values ;
	
	final String simpleName ;
	
	private final String contextQualifiedName ;
	
	public List<String> getValues(){
		return values ;
	}

	public String getSimpleName() {
		return simpleName;
	}

	public String getContextQualifiedName() {
		return contextQualifiedName;
	}
	
	public boolean hasValue(String str){
		
		for( String s : values ){
			if(s.equals(str))
				return true ;
		}
		return false ;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((contextQualifiedName == null) ? 0 : contextQualifiedName
						.hashCode());
		result = prime * result
				+ ((simpleName == null) ? 0 : simpleName.hashCode());
		result = prime * result + ((values == null) ? 0 : values.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		QualifiedName other = (QualifiedName) obj;
		if (contextQualifiedName == null) {
			if (other.contextQualifiedName != null)
				return false;
		} else if (!contextQualifiedName.equals(other.contextQualifiedName))
			return false;
		if (simpleName == null) {
			if (other.simpleName != null)
				return false;
		} else if (!simpleName.equals(other.simpleName))
			return false;
		if (values == null) {
			if (other.values != null)
				return false;
		} else if (!values.equals(other.values))
			return false;
		return true;
	}
	
	@Override
	public String toString(){
		if(values == null || values.isEmpty() ){
			return getSimpleName() + "(" + getContextQualifiedName() + ")" ;
		}
		
		StringBuilder bld = new StringBuilder() ;
		int size = values.size();
		for(int i = 0 ; i < size ; i++){
			bld.append(values.get(i)) ;
			if(i<size-1)
				bld.append(", ") ;
		}
		String result = bld.toString();
		return result ;
	}
}
