package net.junos.space.pmd.utils.outline;


public interface IJavaFieldDeclaration extends IDeclaration, IHasJavaType {
	
	boolean isTransient() ;
	
	void setTransient(boolean isTransient ) ;

}
