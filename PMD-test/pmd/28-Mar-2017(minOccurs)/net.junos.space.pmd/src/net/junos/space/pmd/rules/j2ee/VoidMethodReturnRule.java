package net.junos.space.pmd.rules.j2ee;

import java.util.List;

import net.junos.space.pmd.rules.AbstractJavaRuleWithUselessTypeResolution;
import net.sourceforge.pmd.lang.java.ast.ASTBlock;
import net.sourceforge.pmd.lang.java.ast.ASTBlockStatement;
import net.sourceforge.pmd.lang.java.ast.ASTMethodDeclaration;
import net.sourceforge.pmd.lang.java.ast.ASTResultType;
import net.sourceforge.pmd.lang.java.ast.ASTReturnStatement;
import net.sourceforge.pmd.lang.java.ast.ASTStatement;
import net.sourceforge.pmd.lang.java.ast.ASTType;

/**
 * 
 * JAC_066
 *
 */
public class VoidMethodReturnRule extends AbstractJavaRuleWithUselessTypeResolution {
	@Override
	public Object visit(ASTMethodDeclaration node, Object data) {
		try {
			List<ASTResultType> resultTypes = node.findChildrenOfType(ASTResultType.class);
			boolean isVoid = false;
			for (ASTResultType astResultType : resultTypes) {
				List<ASTType> types = astResultType.findChildrenOfType(ASTType.class);
				if (types == null || types.size() == 0)
				{
					isVoid = true;
					break;
				}
			}
			
			if (isVoid)
			{
				List<ASTBlock> blocks = node.findChildrenOfType(ASTBlock.class);
				if (blocks != null)
				{
					for (ASTBlock block : blocks) {
						List<ASTBlockStatement> blockStatements = block.findChildrenOfType(ASTBlockStatement.class);
						if (blockStatements != null)
						{
							for (ASTBlockStatement blockStatement : blockStatements) {
								List<ASTStatement> statements = blockStatement.findChildrenOfType(ASTStatement.class);
								if (statements != null)
								{
									for (ASTStatement statement : statements) {
										List<ASTReturnStatement> returnStatements = statement.findChildrenOfType(ASTReturnStatement.class);
										if (returnStatements != null)
										{
											for (ASTReturnStatement returnStatement : returnStatements) {
												if (returnStatement.getImage() == null || returnStatement.getImage().length() == 0)
												{
													addViolationWithMessage(data, returnStatement, "Do not return in a method with a return type of void");
												}
											}
										}
									}
									
								}
							}
						}
					}
				}
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return super.visit(node, data);
	}
}
