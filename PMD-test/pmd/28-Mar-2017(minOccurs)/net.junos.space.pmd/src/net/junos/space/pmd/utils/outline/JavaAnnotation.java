package net.junos.space.pmd.utils.outline;

import java.util.Collection;
import java.util.HashMap;

public class JavaAnnotation implements IJavaAnnotation {

	public JavaAnnotation(String name) {
		super();
		this.name = name;
	}

	private final String name ;
	
	private final HashMap<String,IAnnotationMemberValue> map = new HashMap<String, IAnnotationMemberValue>() ;	
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public Collection<String> getKeys() {
		return map.keySet() ;
	}

	@Override
	public IAnnotationMemberValue getMemberValue(String key) {
		return map.get(key);
	}

	@Override
	public void setValue(String key, IAnnotationMemberValue value) {
		map.put(key, value) ;
	}
	
}
