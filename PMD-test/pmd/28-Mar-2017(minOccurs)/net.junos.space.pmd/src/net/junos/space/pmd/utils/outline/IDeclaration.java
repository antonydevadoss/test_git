package net.junos.space.pmd.utils.outline;

import java.util.List;


public interface IDeclaration {
	
	boolean isPublic() ;
	
	boolean isProtected() ;
	
	boolean isPrivate() ;
	
	boolean isDefault() ;
	
	void setPublic() ;
	
	void setProtected() ;
	
	void setPrivate() ;
	
	void setDefault() ;	
	
	boolean isStatic() ;
	
	void setStatic( boolean isStatic ) ;
	
	boolean isFinal() ;
	
	void setFinal( boolean isFinal ) ;
	
	String getSimpleName() ;
	
	void setSimpleName( String simpleName ) ;
	
	QualifiedName getQualifiedName() ;
	
	void setQualifiedName( QualifiedName qualifiedName ) ;
	
	String getPackage() ;
	
	void setPackage( String packageName ) ;
	
	IJavaTypeDeclaration getOwner() ;
	
	void setOwner( IJavaTypeDeclaration owner ) ;
	
	int[] getLines() ;
	
	void setLines( int... lines );
	
	List<IJavaAnnotation> getAnnotations() ;
	
	void setAnnotations( List<IJavaAnnotation> annotations) ;
	
	boolean hasAnnotation(String annotationName) ;
}
