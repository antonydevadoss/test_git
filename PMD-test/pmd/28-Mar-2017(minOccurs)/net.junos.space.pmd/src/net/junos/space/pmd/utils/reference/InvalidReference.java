package net.junos.space.pmd.utils.reference;

public class InvalidReference extends AbstractReference{
	
	public InvalidReference(String message, int beginLine, int startLine) {
		super(beginLine, startLine);
		this.message = message;
	}

	final String message ;

	public String getMessage() {
		return message;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((message == null) ? 0 : message.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		InvalidReference other = (InvalidReference) obj;
		if (message == null) {
			if (other.message != null)
				return false;
		} else if (!message.equals(other.message))
			return false;
		return true;
	}

}
