package net.junos.space.pmd.rules.j2ee;

import java.util.ArrayList;
import java.util.Arrays;
import net.junos.space.pmd.utils.reference.SimpleReferenceRule;

public class DontUseJavaIOFileApiRule extends SimpleReferenceRule {
	
	private static ArrayList<String> classNames
			= new ArrayList<String>( Arrays.asList( new String[]{
		"java.io.File",
		"java.io.FileDescriptor",
		"java.io.FileInputStream",
		"java.io.FileOutputStream",
		"java.io.FilePermission",
		"java.io.FileReader",
		"java.io.FileWriter",
		"java.io.RandomAccessFile"
	}) );
	
	protected boolean mustBeChecked(String qualifiedName) {
		return isEnterpriseBeanSuperclassEJB2(qualifiedName);
	}

	protected boolean isInvalid(String refQName)
	{
		boolean result = classNames.contains(refQName);
		return result ;
	}
	
	public String getMessage(){
		return "Do not use the java.io Package to access the file system" ;
	}
}
