package net.junos.space.pmd;

import net.sourceforge.pmd.RuleContext;
import net.sourceforge.pmd.lang.java.ast.ASTClassOrInterfaceDeclaration;
import net.sourceforge.pmd.lang.java.ast.ASTCompilationUnit;

public class PmdData {
	private RuleContext ruleCtx;
	private ASTClassOrInterfaceDeclaration astClassOrInterfaceDeclaration;
	private ClassContext classCtx;
	private String sourceCodeFilename;
	
	public RuleContext getRuleCtx() {
		return ruleCtx;
	}
	public void setRuleCtx(RuleContext ruleCtx) {
		this.ruleCtx = ruleCtx;
	}
	public ClassContext getClassCtx() {
		return classCtx;
	}
	public void setClassCtx(ClassContext classCtx) {
		this.classCtx = classCtx;
	}
	public String getSourceCodeFilename() {
		return sourceCodeFilename;
	}
	public void setSourceCodeFilename(String sourceCodeFilename) {
		this.sourceCodeFilename = sourceCodeFilename;
	}
	public ASTClassOrInterfaceDeclaration getAstClassOrInterfaceDeclaration() {
		return astClassOrInterfaceDeclaration;
	}
	public void setAstClassOrInterfaceDeclaration(
			ASTClassOrInterfaceDeclaration astClassOrInterfaceDeclaration) {
		this.astClassOrInterfaceDeclaration = astClassOrInterfaceDeclaration;
	}
}
