package net.junos.space.pmd.rules.xml;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import net.sourceforge.pmd.RuleContext;
import net.sourceforge.pmd.lang.java.rule.JavaRuleViolation;

public abstract class SimpleXpathRule extends AbstractXMLRule {

	public SimpleXpathRule() {
		super();
	}

	protected abstract boolean isValid(Element element);

	protected abstract String getXpathExpression();

	@Override
	protected void processSingleDocument(String path, Document document,
			RuleContext ctx) {
			
				if (document == null)
					return;	
						
				try {
					XPath xpath = XPathFactory.newInstance().newXPath();	
					XPathExpression expr = xpath.compile(getXpathExpression());
					NodeList nodes = (NodeList) expr.evaluate( document, XPathConstants.NODESET );
				
					for (int i = 0; i < nodes.getLength(); i++) {
			
						Node node = nodes.item(i);
						if(!(node instanceof Element))
							continue ;
								
						Element element = (Element) node;
						if(isValid(element))
							continue ;
						
						int lineNumber = getLineNumber(element);
						if (lineNumber < 0)
							break;
			
						ctx.setSourceCodeFilename(path);
						JavaRuleViolation violation = new JavaRuleViolation(
								this, ctx, null, getMessage(), lineNumber, lineNumber);
						
						ctx.getReport().addRuleViolation(violation);
					}
				} catch (XPathExpressionException e) {
					e.printStackTrace();
				}
			}

}