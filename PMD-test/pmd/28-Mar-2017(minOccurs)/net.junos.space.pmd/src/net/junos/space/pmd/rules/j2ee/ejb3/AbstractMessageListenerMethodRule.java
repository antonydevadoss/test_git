package net.junos.space.pmd.rules.j2ee.ejb3;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import net.junos.space.pmd.utils.outline.AbstractOutlineAccessor;
import net.junos.space.pmd.utils.outline.AnnotationClassMemberValue;
import net.junos.space.pmd.utils.outline.IAnnotationMemberValue;
import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IJavaAnnotation;
import net.junos.space.pmd.utils.outline.IJavaMethodDeclaration;
import net.junos.space.pmd.utils.outline.IJavaType;
import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;
import net.junos.space.pmd.utils.outline.QualifiedName;
import net.sourceforge.pmd.RuleContext;

public class AbstractMessageListenerMethodRule extends AbstractOutlineAccessor{

	protected static final String JAVAX_JMS_MESSAGE_LISTENER = "javax.jms.MessageListener";


	@Override
	public void doEnd(RuleContext ctx) {
		for( IClassOutline outline : getOutlineContainer().getAllOutlines() ){
			
			IJavaTypeDeclaration typeDeclaration = outline.getTypeDeclaration();
			String qualifiedname = outline.getQualifiedNameString() ;
			if(!isMessageDrivenBeanEJB3(qualifiedname) )
				continue ;
			
			ArrayList<IJavaTypeDeclaration> messageListenerInterfaces
					= getMessageListenerInterfaces(typeDeclaration) ;
			
			for( IJavaTypeDeclaration iDecl : messageListenerInterfaces ){
				
				ArrayList<IJavaMethodDeclaration> methods = getMessageListenerMethods( iDecl, outline ) ;				
				for( IJavaMethodDeclaration md : methods ){
					checkMethod(outline, md, ctx ) ;
				}
			}
		}
		super.doEnd(ctx);
	}

	protected void checkMethod(IClassOutline outline,IJavaMethodDeclaration md, RuleContext ctx) {}

	protected ArrayList<IJavaMethodDeclaration> getMessageListenerMethods(
			IJavaTypeDeclaration iDecl, IClassOutline outline) {
		
		ArrayList<IJavaMethodDeclaration> result = new ArrayList<IJavaMethodDeclaration>() ;
		QualifiedName qName = iDecl.getQualifiedName();
		if(qName.hasValue(JAVAX_JMS_MESSAGE_LISTENER)){
			IJavaMethodDeclaration md = findeOnMessageMethod(outline) ;
			if(md != null)
				result.add(md) ;				
		}
		return result ;
	}

	private IJavaMethodDeclaration findeOnMessageMethod(IClassOutline outline) {
		
		List<IJavaMethodDeclaration> methodsBySimpleName = outline.getMethodsBySimpleName("onMessage");
		for( IJavaMethodDeclaration md : methodsBySimpleName ){
			
			if( md.getReturnType() != null)
				continue ;
			
			List<IJavaType> params = md.getParameters();
			if(params == null || params.size() != 1)
				continue ;
			
			IJavaType paramType = params.get(0);
			QualifiedName qName = paramType.getQualifiedName();
			if(qName == null)
				continue ;
			
			if(!qName.hasValue("javax.jms.Message"))
				continue ;
			
			return md ;
		}
		return null;
	}

	protected ArrayList<IJavaTypeDeclaration> getMessageListenerInterfaces( IJavaTypeDeclaration typeDeclaration)
	{
		ArrayList<IJavaTypeDeclaration> result = new ArrayList<IJavaTypeDeclaration>() ;
		List<IJavaAnnotation> annotations = typeDeclaration.getAnnotations();
		for(IJavaAnnotation ann : annotations){
			if(!ann.getName().equals("MessageDriven"))
				continue ;
			
			while(true){
				IAnnotationMemberValue memberValue
					= ann.getMemberValue("messageListenerInterface");
				
				if(memberValue == null)
					break ;
				
				if(!(memberValue instanceof AnnotationClassMemberValue))
					break ;
				
				AnnotationClassMemberValue acmv = (AnnotationClassMemberValue) memberValue ;
				IJavaType type = acmv.getType();
				IJavaTypeDeclaration decl = type.getDeclaration();
				if(decl == null)
					break ;
				
				result.add(decl) ;
				break ;
			}
			break ;			
		}
		if(!result.isEmpty())
			return result ;		
		
		HashSet<String> occuredInterfaces = new HashSet<String>() ;
		
		for( IJavaTypeDeclaration iDecl : typeDeclaration.getAllInterfaces() ){
			QualifiedName qName = iDecl.getQualifiedName();
			if(qName.hasValue(JAVAX_JMS_MESSAGE_LISTENER)){
				if(!occuredInterfaces.contains(JAVAX_JMS_MESSAGE_LISTENER)){
					result.add(iDecl) ;
					occuredInterfaces.add(JAVAX_JMS_MESSAGE_LISTENER) ;
				}
			}
		}		
		return result ;
	}
}
