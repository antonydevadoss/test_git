package net.junos.space.pmd.rules.j2ee;

import net.junos.space.pmd.rules.AbstractJavaRuleWithUselessTypeResolution;
import net.sourceforge.pmd.RuleContext;
import net.sourceforge.pmd.lang.java.ast.ASTMethodDeclaration;
import net.sourceforge.pmd.lang.java.ast.ASTMethodDeclarator;


//JAC_059
public class EqualsAndHashCodeRule extends AbstractJavaRuleWithUselessTypeResolution {

	private static final String EQUALS_KEY = "equals";
	private static final String HASH_KEY = "hashCode";
	
	@Override
	public Object visit(ASTMethodDeclaration node, Object data) {
		RuleContext ctx = (RuleContext) data;
		ASTMethodDeclarator methodDeclarator = node.getFirstChildOfType(ASTMethodDeclarator.class);
		if (methodDeclarator != null)
		{
			if (methodDeclarator.getImage() != null)
			{
				if (methodDeclarator.getImage().equals("equals"))
				{
					ctx.setAttribute(EQUALS_KEY, node);
				}
				
				if (methodDeclarator.getImage().equals("hashCode"))
				{
					ctx.setAttribute(HASH_KEY, node);
				}
			}
		}
		return super.visit(node, data);
	}
	
	
	@Override
	public void end(RuleContext ctx) {
		if (ctx.getAttribute(EQUALS_KEY) != null
				&& ctx.getAttribute(HASH_KEY) == null)
		{
			ASTMethodDeclaration node = (ASTMethodDeclaration) ctx.getAttribute(EQUALS_KEY);
			addViolationWithMessage(ctx, node, "Override the equals and hashCode methods together");
		}
		if (ctx.getAttribute(EQUALS_KEY) == null
				&& ctx.getAttribute(HASH_KEY) != null)
		{
			ASTMethodDeclaration node = (ASTMethodDeclaration) ctx.getAttribute(HASH_KEY);
			addViolationWithMessage(ctx, node, "Override the equals and hashCode methods together");
		}
		super.end(ctx);
	}
}
