package net.junos.space.pmd.rules.j2ee.ejb3;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.junos.space.pmd.utils.outline.AbstractEntityRule;
import net.junos.space.pmd.utils.outline.AnnotationClassMemberValue;
import net.junos.space.pmd.utils.outline.EjbEntity;
import net.junos.space.pmd.utils.outline.IAnnotationMemberValue;
import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IDeclaration;
import net.junos.space.pmd.utils.outline.IJavaAnnotation;
import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;
import net.sourceforge.pmd.RuleContext;

public class EntityCompositePrimaryKeyFieldsRule extends AbstractEntityRule {
	
	
	
	private static final String message = "Fields or properties of the composite primary key class must correspond those of the entity class.";

	@Override
	protected void processEntities(RuleContext ctx) {
		if (AbstractEntityRule.entityRegistry == null) {
			return;
		}
		
		for (EjbEntity ejbEntity : AbstractEntityRule.entityRegistry.values()) {
			IClassOutline classOutline = ejbEntity.getOutline() ;
			if(!classOutline.hasAllSuperclassesDefined(getOutlineContainer()))
				continue ;
			
			IJavaTypeDeclaration typeDeclaration = classOutline.getTypeDeclaration();
			IJavaTypeDeclaration keyTypeDeclaration = null;
			List<IJavaAnnotation> annotations = typeDeclaration.getAnnotations();
			if (annotations != null) {
				for (IJavaAnnotation annotation : annotations) {
					if (annotation.getName().equals("IdClass")) {
						IAnnotationMemberValue memberValue = annotation.getMemberValue("value");
						if (memberValue instanceof AnnotationClassMemberValue) {
							AnnotationClassMemberValue classMemberValue = (AnnotationClassMemberValue)memberValue;
							if (classMemberValue.getType() != null && classMemberValue.getType().getDeclaration() != null ) {
								keyTypeDeclaration = classMemberValue.getType().getDeclaration();
								break;
							}
						}
					}
				}
			}
			
			if (keyTypeDeclaration != null) {
				for (String recVal : keyTypeDeclaration.getQualifiedName().getValues()) {
					if(recVal != null){
						IClassOutline keyClassOutline = getOutlineContainer().getClassOutline(recVal);
						if (keyClassOutline != null) {
							
							EjbEntity keyEntity = AbstractEntityRule.entityRegistry.get(recVal);
							boolean keyFieldAccess = keyEntity == null? true : keyEntity.hasFieldAccess();
							HashMap<String, Property> keyOutlineProperties = getOutlineProperties(keyClassOutline, keyFieldAccess);
							HashMap<String, Property> entityOutlineProperties = getOutlineProperties(classOutline, ejbEntity.hasFieldAccess());
							
							boolean hasAllProperties = true;
							for (String key : keyOutlineProperties.keySet()) {
								
								Property property = entityOutlineProperties.get(key);
								if(property==null){
									hasAllProperties = false;
									break ;
								}
								
								ArrayList<IDeclaration> dList = new ArrayList<IDeclaration>() ;
								if(ejbEntity.hasFieldAccess()){
									dList.add(property.getField()) ;
								}
								else{
									dList.add(property.getGetter());
									dList.add(property.getSetter());
								}
								
								for(IDeclaration d : dList){
									
									if(d!=null){
										
										if( d.hasAnnotation("Id") )
											continue ;
										
										if(d.hasAnnotation("java.persistence.Id"))
											continue ;
									}
									hasAllProperties=false ;
									break;
								}
								
							}
							if (!hasAllProperties) {
								reportError(ctx,typeDeclaration,message) ;
							}
						}
					}
				}
			}
		}
	}
}
