/*******************************************************************************

 * FILE NAME: MissingRootLevelSchemaRule.java

 * PURPOSE:   This is Rule-26 in document

 *

 *

 * Revision History: 

 * DATE:           				  AUTHOR:              	CHANGE:  

 * 4th April, 2011                 Atul Bajpai	       	Initial Version  

 * 

 * Copyright (c) 2011 Juniper Networks. All rights reserved.

 ******************************************************************************/
package net.junos.space.pmd.rules.rest;

import java.io.File;import java.lang.reflect.Method;import java.util.List;import net.junos.space.pmd.utils.PMDConstants;import net.junos.space.pmd.utils.PMDUtils;import net.junos.space.pmd.utils.Project;import net.sourceforge.pmd.RuleContext;import net.sourceforge.pmd.lang.java.ast.ASTClassOrInterfaceDeclaration;import net.sourceforge.pmd.lang.java.rule.AbstractJavaRule;import org.apache.log4j.Logger;

/**
 * Rule ensures that there must be a valid root level schema at appropriate location for REST implementation.
 */
public class MissingRootLevelSchemaRule extends AbstractJavaRule {

	/**
	 * Logger
	 */
	private Logger log = Logger.getLogger(MissingRootLevelSchemaRule.class);
	
	/**
	 * Version
	 */
	private static final String VERSION = "version=";
		
	/**
	 * Root path
	 */
	private static final String ROOT_PATH = "/";
	
	/**
	 * Plus sign in @Produces value
	 */
	private static final String PLUS = "+";
	
	/**
	 * Extension of xsd file
	 */
	private static final String XSD_EXTN = ".xsd";
	
	

	/**
	 * Rule executes by: 
	 * <ol>
	 * <li>find all the classes having @XMLRootElement annotation at class level.</li> 
	 * <li>Gets all interfaces of this class. </li>
	 * <li>Checks if any implementation is having @Path("/") annotation</li> 
	 * <li>Find all the methods of this implemetation </li>
	 * <li>Checks if any method is having @Path("/") annotation</li> 
	 * <li>find the @Produces anotation used in this method. </li>
	 * <li>Split the String used in @Produces annotation to make the absolute name of schema</li> 
	 * <li>checks whether or not this scheme exists at appropriate location.</li>
	 * </ol>
	 */	
	public Object visit(ASTClassOrInterfaceDeclaration classNode, Object data) {
		boolean isRestInterface = false;
		boolean hasXMLRootElementAnnotation = false;
		boolean hasRootLevelXSD = false;
		boolean hasRootMethod = false;
		
		RuleContext ctx = (RuleContext) data;
		String className = classNode.getImage();
		String restInterfaceName = null;
		String xsdName = null;

		try {
			// class having @XMLRootElement
			hasXMLRootElementAnnotation = PMDUtils.isClassAnnotationExists(classNode, PMDConstants.XML_ROOT_ELEMENT);
			
			//Create project and load @Path and @Produces class
			Project project = new Project(PMDConstants.WEB_SVC_PROJECT);
			if (hasXMLRootElementAnnotation) {
				// find all the implementation
				List<Class<?>> impletmentsInterfaceList = PMDUtils.getInterfaceTypes(classNode, project);
				for (Class<?> impletmentsInterface : impletmentsInterfaceList) {
					
					// Re-initialize all variables for each interface
					isRestInterface = false;
					hasRootMethod = false;
					hasRootLevelXSD = false;
					
					// checks if there is @Path("/") annotation at class level
					isRestInterface = PMDUtils.hasAnnotation(impletmentsInterface, PMDConstants.PATH_ANNOTATION);
					if (isRestInterface) {
						Method[] methods = impletmentsInterface.getMethods();
						for (int j = 0; j < methods.length; j++) {
							
							Method method = methods[j];
							String retunClass = method.getReturnType().getSimpleName();
							restInterfaceName = impletmentsInterface.getSimpleName();
							
							if (retunClass.equals(className) || retunClass.equals(restInterfaceName)) {
								// checks if there is @Get annotation at method level
								boolean isGetExists = PMDUtils.hasAnnotation(method, PMDConstants.HttpMethod.GET.toString());
								// checks if there is @Path annotation at method level
								boolean isPathExists = PMDUtils.hasAnnotation(method, PMDConstants.PATH_ANNOTATION);
								if(isGetExists && !isPathExists)
									hasRootMethod = true;
								if (isGetExists && isPathExists) {
									hasRootMethod = false;
									String[] methodPathValues = PMDUtils.getDefaultAnnotationValue(method, PMDConstants.PATH_ANNOTATION);																		
									if (methodPathValues != null && methodPathValues[0] != null && methodPathValues[0].equals(ROOT_PATH)) {
										hasRootMethod = true;
									}
								}
								if(hasRootMethod) {
									String[] producesValues = null;
									boolean isMethodProduceExists = PMDUtils.hasAnnotation(methods[j],PMDConstants.PRODUCES_ANNOTATION);
									if(isMethodProduceExists) {
										producesValues = PMDUtils.getDefaultAnnotationValue(method, PMDConstants.PRODUCES_ANNOTATION);
									}else {
										producesValues = PMDUtils.getDefaultAnnotationValue(impletmentsInterface, PMDConstants.PRODUCES_ANNOTATION);
									}	
									if(producesValues != null && producesValues.length > 0) {
										xsdName = producesValues[0];
										String projectPath = project.getProjectPath();
										if(xsdName.indexOf(ROOT_PATH) != -1 && xsdName.indexOf(PLUS) != -1 && xsdName.indexOf(VERSION) != -1){
											xsdName = xsdName.substring(xsdName.indexOf(ROOT_PATH) + 1, xsdName.indexOf(PLUS));
											String version = producesValues[0].substring(producesValues[0].indexOf(VERSION) + VERSION.length(), producesValues[0].length());
											xsdName = xsdName + "_v" + version + XSD_EXTN;										
											projectPath = projectPath + PMDConstants.WEB_XML_DIR + xsdName;
											File file = new File(projectPath);
											if (file.exists())
												hasRootLevelXSD = true;
										} else
											hasRootLevelXSD = false;
									}// End if for produceValues null check
									break;
								}// End if for isRoot
							}// End if for returnClass check
							
						}// End loop for methods
					}// End if for rest interface
					
					// For each interface check value and log message 
					if (log.isDebugEnabled()) {
						StringBuilder builder = new StringBuilder();
						builder.append("source file=").append(ctx.getSourceCodeFilename()).append(",class=").append(className);
						builder.append(",isRestInterface=").append(isRestInterface).append(",hasXMLRootElementAnnotation=").append(
								hasXMLRootElementAnnotation).append(",hasRootMethod=").append(hasRootMethod).append(",hasRootLevelXSD=").append(hasRootLevelXSD);
						log.debug(builder.toString());
					}
					if (isRestInterface && hasRootMethod && !hasRootLevelXSD)
						addViolation(data, classNode);
					
				}// End loop for implements interface 
			} // End if for XMLRoot elemenet check

		} catch (Exception e) {
			log.error("Exception in executing class " + className + " of " + ctx.getSourceCodeFilename(), e);
		}
		return super.visit(classNode, data);
	}

}
