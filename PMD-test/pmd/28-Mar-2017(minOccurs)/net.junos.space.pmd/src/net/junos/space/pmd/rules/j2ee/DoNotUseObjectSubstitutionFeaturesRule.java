package net.junos.space.pmd.rules.j2ee;

import java.util.Collection;
import java.util.List;

import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IJavaMethodDeclaration;
import net.junos.space.pmd.utils.outline.IJavaType;
import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;
import net.junos.space.pmd.utils.outline.QualifiedName;
import net.junos.space.pmd.utils.reference.AbstractReferenceAccessor;
import net.junos.space.pmd.utils.reference.ClassMethodsInvocationRegistry;
import net.junos.space.pmd.utils.reference.IReference;
import net.junos.space.pmd.utils.reference.MethodsInvocationContainer;
import net.junos.space.pmd.utils.reference.ResolvedReference;
import net.junos.space.pmd.utils.reference.ClassMethodsInvocationRegistry.MethodInvocationData;
import net.sourceforge.pmd.RuleContext;

public class DoNotUseObjectSubstitutionFeaturesRule extends AbstractReferenceAccessor {
	
	private static final String[] targetClasses = new String[]{
		"java.io.ObjectOutputStream",
	} ;
	
	private static final String[] targetMethods = new String[]{
		"writeObject",
	} ;
	
	@Override
	public String getMessage() {
		return "Do not attempt to use the subclass or Object Substitution Features of the Java Serialization Protocol";
	}	 
	
	@Override
	public void doStart(RuleContext ctx) {
		for(String qName : targetClasses ){
			
			for( String methodName : targetMethods ){
				MethodsInvocationContainer.getInstance().registerTargetMethod(
						qName, methodName, null ) ;
			}
		}
		super.doStart(ctx);
	}
	
	@Override
	public void doEnd(RuleContext ctx) {
		for( String qName : targetClasses ){		
			
			ClassMethodsInvocationRegistry reg =
					MethodsInvocationContainer.getInstance().getInvocationRegistry( qName );		
			
			for( String methodName : targetMethods ){
				
				MethodInvocationData data = reg.getInvocationData( methodName , null );
				Collection<String> classes = data.getClasses();
				
				for(String className : classes){
					
					if(!isProperBean(className))
						continue ;
					
					IClassOutline outline = getOutlineContainer().getClassOutline(className);
					
					String path = outline.getFilePath();			
					ctx.setSourceCodeFilename( path ) ;
					
					List<IReference> references = data.getReferences(className);
							
					for( IReference ref : references ){
						if(!(ref instanceof ResolvedReference))
							continue ;
						
						ResolvedReference rRef = (ResolvedReference) ref ;
						IJavaType type = rRef.getType();
						IJavaTypeDeclaration decl = type.getDeclaration();
						if(decl==null)
							continue ;
						
						QualifiedName qualifiedName = decl.getQualifiedName();
						if(qualifiedName==null)
							continue ;
						
						List<String> values = qualifiedName.getValues();
						if(values.size()!=1)
							continue ;
						
						String recVal = values.iterator().next() ;
						
						IClassOutline ol = getOutlineContainer().getClassOutline(recVal);
						if(ol==null)
							continue ;
						
						List<IJavaMethodDeclaration> methods = ol.getMethodsBySimpleName("replaceObject");
						if( methods == null || methods.isEmpty() )
							continue ;
						
						reportError(ctx, outline, ref) ;
					}
				}
			}
		}
		super.doEnd(ctx);
	}
	
	public boolean isProperBean(String className) {
		return isEnterpriseBeanSuperclassEJB2(className);
	}
}
