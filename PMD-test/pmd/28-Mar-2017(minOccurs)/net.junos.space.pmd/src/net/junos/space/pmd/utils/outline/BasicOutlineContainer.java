package net.junos.space.pmd.utils.outline;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;



public class BasicOutlineContainer implements IOutlineContainer {
	
	public static final String BASIC_OUTLINE_CONTAINER_CONTEXT_KEY = "basic-outine" ;
	
	private static BasicOutlineContainer instance = new BasicOutlineContainer() ;	
	
	public static BasicOutlineContainer getInstance() {
		return instance;
	}

	HashMap<String, IClassOutline> pathToOutlineMap = new HashMap<String, IClassOutline>() ;
	
	HashMap<String, IClassOutline> outlineMap = new HashMap<String, IClassOutline>() ;
	
	HashMap<String, ArrayList<IClassOutline>> outlineArrayMap
				= new HashMap<String, ArrayList<IClassOutline>>() ;
	
	HashMap<String, ArrayList<IClassOutline>> packageMap
				= new HashMap<String, ArrayList<IClassOutline>>() ;
	
	HashMap<String,IJavaTypeDeclaration> declarationsMap
				= new HashMap<String, IJavaTypeDeclaration>() ;
	
	HashMap<String,ArrayList<IJavaTypeDeclaration>> declarationsArrayMap
				= new HashMap<String, ArrayList<IJavaTypeDeclaration>>() ;
	
	HashMap<String,ArrayList<IJavaTypeDeclaration>> unresolvedDeclarationsMap
			= new HashMap<String, ArrayList<IJavaTypeDeclaration>>() ;

	@Override
	public IClassOutline getClassOutline(String qualifiedName) {		
		return outlineMap.get(qualifiedName) ;
	}

	@Override
	public List<IClassOutline> getMultipleOutlines(String simpleName) {		
		return outlineArrayMap.get(simpleName) ;
	}

	@Override
	public void putClassOutline(IClassOutline outline) {
		
		IJavaTypeDeclaration type = outline.getTypeDeclaration();
		String qName = outline.getQualifiedNameString() ;
		String sName = type.getSimpleName();
		
		outlineMap.put(qName, outline) ;
		
		ArrayList<IClassOutline> lst = outlineArrayMap.get(sName);
		if( lst == null ){
			lst = new ArrayList<IClassOutline>() ;
			outlineArrayMap.put(sName, lst) ;
		}
		
		int ind = -1 ;
		for( int i = 0 ; i < lst.size() ; i++ ){
			IClassOutline o = lst.get(i);
			String qName0 = o.getQualifiedNameString() ;
			if( !qName.equals(qName0) )
				continue ;
			
			ind = i ;
			break ;
		}
		if( ind < 0 ){
			lst.add(outline) ;
		}
		else{
			lst.set(ind, outline) ;
		}
		
		String packageName = outline.getTypeDeclaration().getPackage() ;
		ArrayList<IClassOutline> packageContent = packageMap.get(packageName) ;
		if(packageContent == null){
			packageContent = new ArrayList<IClassOutline>() ;
			packageMap.put(packageName, packageContent) ;
		}
		packageContent.add(outline) ;
	}

	@Override
	public IJavaTypeDeclaration getTypeDeclaration(String qualiFiedName) {
		return declarationsMap.get(qualiFiedName) ;
	}

	@Override
	public void putTypeDeclaration(IJavaTypeDeclaration type) {
		
		List<String> values = type.getQualifiedName().getValues();
		if(values.size()!=1){
			putUnresolvedDeclaration(type) ;
			return ;
		}
		
		String qName = values.iterator().next() ;		
		String sName = type.getSimpleName();
		
		declarationsMap.put( qName, type ) ;
		
		ArrayList<IJavaTypeDeclaration> lst = declarationsArrayMap.get(sName);
		if( lst == null ){
			lst = new ArrayList<IJavaTypeDeclaration>() ;
			declarationsArrayMap.put(sName, lst) ;
		}
		
		int ind = -1 ;
		for( int i = 0 ; i < lst.size() ; i++ ){
			IJavaTypeDeclaration o = lst.get(i);
			List<String> vals = o.getQualifiedName().getValues();
			if(vals.size() != 1)
				continue ;
			
			String qName0 = vals.iterator().next() ;
			if( !qName.equals(qName0) )
				continue ;
			
			ind = i ;
			break ;
		}
		if( ind < 0 ){
			lst.add(type) ;
		}
		else{
			lst.set(ind, type) ;
		}
	}
	
	private void putUnresolvedDeclaration(IJavaTypeDeclaration type) {
		
		String contextQualifiedName = type.getQualifiedName().getContextQualifiedName();
		ArrayList<IJavaTypeDeclaration> lst
			= unresolvedDeclarationsMap.get(contextQualifiedName );
		
		if( lst == null ){
			lst = new ArrayList<IJavaTypeDeclaration>() ;
			unresolvedDeclarationsMap.put(contextQualifiedName, lst) ;
			lst.add(type) ;
			return ;
		}
		
		for( IJavaTypeDeclaration jtd : lst )
			if(jtd.getSimpleName().equals(type.getSimpleName()))
				return ;
		
		lst.add(type) ;		
	}

	@Override
	public List<IJavaTypeDeclaration> getMultipleDeclarations(String simpleName) {
		return declarationsArrayMap.get(simpleName);
	}

	@Override
	public List<IClassOutline> getAllOutlines() {
		return new ArrayList<IClassOutline>(this.outlineMap.values());
	}

	@Override
	public List<IJavaTypeDeclaration> getAllTypeDeclarations() {
		return new ArrayList<IJavaTypeDeclaration>(this.declarationsMap.values());
	}

	private boolean isComplete = false ;
	@Override
	public void clear() {
		isComplete = false ;
		outlineMap.clear() ;
		outlineArrayMap.clear() ;
		declarationsMap.clear() ;
		declarationsArrayMap.clear() ;
		unresolvedDeclarationsMap.clear() ;
	}

	@Override
	public void setPath(IClassOutline outline, String path) {
		pathToOutlineMap.put(path, outline) ;		
	}

	@Override
	public IClassOutline getOutlineByPath(String path) {
		return pathToOutlineMap.get(path) ;
	}

	@Override
	public List<IClassOutline> getPackageContent(String packageName) {
		return packageMap.get(packageName) ;
	}

	public void setComplete(boolean isComplete) {
		this.isComplete = isComplete;
	}

	@Override
	public boolean isComplete() {
		return isComplete ;
	}

	@Override
	public IJavaTypeDeclaration getTypeDeclaration(String simpleName,String contextClassQualifiedName) {
		ArrayList<IJavaTypeDeclaration> lst = unresolvedDeclarationsMap.get(contextClassQualifiedName);
		if(lst == null)
			return null;
		
		for( IJavaTypeDeclaration dec : lst )
			if(dec.getSimpleName().equals(simpleName))
				return dec ;
		
		return null;
	}
	
	public List<IJavaTypeDeclaration> getUnresolvedDeclarations(){
		
		ArrayList<IJavaTypeDeclaration> result = new ArrayList<IJavaTypeDeclaration>(  );
		for( ArrayList<IJavaTypeDeclaration> lst : unresolvedDeclarationsMap.values() ) {
			result.addAll(lst) ;			
		}
		return result ;
	}
	
}
