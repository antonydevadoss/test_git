package net.junos.space.pmd.utils.reference;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Properties;

import net.junos.space.pmd.rules.AbstractJavaRuleWithUselessTypeResolution;
import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IDeclaration;
import net.junos.space.pmd.utils.outline.IJavaFieldDeclaration;
import net.junos.space.pmd.utils.outline.IJavaMethodDeclaration;
import net.junos.space.pmd.utils.outline.IJavaType;
import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;
import net.junos.space.pmd.utils.outline.IOutlineContainer;
import net.junos.space.pmd.utils.outline.QualifiedName;
import net.sourceforge.pmd.RuleContext;
import net.sourceforge.pmd.lang.java.ast.ASTClassOrInterfaceDeclaration;
import net.sourceforge.pmd.lang.java.ast.ASTCompilationUnit;
import net.sourceforge.pmd.lang.java.ast.ASTEnumDeclaration;
import net.sourceforge.pmd.lang.java.ast.ASTPackageDeclaration;
import net.sourceforge.pmd.lang.java.ast.AbstractJavaAccessTypeNode;

public class AbstractReferenceAccessor extends AbstractJavaRuleWithUselessTypeResolution {
	
	private ReferenceCollector collector = new ReferenceCollector() ;
	
	private IReferenceContainer referenceContainer ;
	
	private MethodsInvocationContainer methodInvocationContainer ;	
	
	static private boolean finishedEndPahase = false ;
	static private boolean finishedStartPahase = false ;
	
	
	final public Object visit(ASTCompilationUnit node, Object data) {		
		
		setReferenceContainer(data) ;
		
		String packageName =  "" ;
		ASTPackageDeclaration packageDeclaration = node.getPackageDeclaration();
		if(packageDeclaration!=null)
			packageName = packageDeclaration.getPackageNameImage() ;
		
		AbstractJavaAccessTypeNode classNode
				= node.getFirstDescendantOfType( ASTClassOrInterfaceDeclaration.class );
		if(classNode == null)
			classNode = node.getFirstDescendantOfType( ASTEnumDeclaration.class );
		
		if(classNode != null){
			String simpleName = classNode.getImage();
			String qualifiedName = packageName + "." + simpleName ;
			
			IClassReferenceStorage<IReference> classRefs = getReferenceContainer()
					.getTemporaryReferenceListForClass(qualifiedName);
			
			if( classRefs == null ){		
				collector.visit(node, data) ;
			}
		}
		return doVisitCompilationUnit( node, data );
	}
	
	public Object doVisitCompilationUnit(ASTCompilationUnit node, Object data){
		return super.visit(node, data) ;
	}
	
	@Override
	public boolean usesTypeResolution() {
		return false ;
	}

	public void doStart(RuleContext ctx){} ;

	public void doEnd(RuleContext ctx){} ;

	@Override
	final public void start(RuleContext ctx) {

		setReferenceContainer(ctx) ;		
		setMethodInvocationContainer(ctx) ;

		if(!finishedStartPahase){
			MethodsInvocationContainer.getInstance().clear() ;
			getReferenceContainer().clear() ;
		}
		
		collector.start(ctx) ;
		
		doStart(ctx);
		
		finishedStartPahase = true ;
		finishedEndPahase = false ;
	}

	@Override
	final public void end(RuleContext ctx) {
		setReferenceContainer(ctx) ;
		setMethodInvocationContainer(ctx) ;
		collector.end(ctx) ;
		
		if(!finishedEndPahase){
			resolveAll() ;
			getReferenceContainer().clearTemporaryRefs() ;
			writeDump(ctx,this.referenceContainer) ;
		}
		
		finishedEndPahase = true ;
		finishedStartPahase = false ;
		doEnd(ctx) ;
	}
	

	private void setMethodInvocationContainer(RuleContext ctx) {
		this.methodInvocationContainer = MethodsInvocationContainer.getInstance() ;		
	}

	private IClassReferenceStorage<IReference> currentClassReferenceStorage;
	
	private void resolveAll() {
		
		for( IClassReferenceStorage<IReference> cStorage : getReferenceContainer().getAllTemporaryClassStorages() ){
			
			currentClassReferenceStorage = cStorage ;
			
			for( IDeclaration method : cStorage.getMethods() ){
				List<IReference> methodReferences = cStorage.getMethodReferences(method);
				Collection<SimpleClassReference> resolved = resolve(methodReferences) ;
				if(!resolved.isEmpty()){
					getReferenceContainer().registerResolvedRefs(method, resolved) ;
				}
			}
		}		
	}

	private Collection<SimpleClassReference> resolve(List<IReference> methodReferences) {
		
		LinkedHashSet<SimpleClassReference> result = new LinkedHashSet<SimpleClassReference>() ;
		for( IReference ref : methodReferences ){			
			resolve(ref, result) ;
		}		
		return result;
	}

	private IJavaType resolve(IReference ref, LinkedHashSet<SimpleClassReference> set) {
		
		IJavaType resultType = null ;
		
		try{
			
			
		
		if( ref instanceof MethodInvocationReference ){
			
			while(true){
				MethodInvocationReference cr = (MethodInvocationReference) ref ;	
				
				IJavaType headType = resolve(cr.getHeadReference(), set) ;
				if(headType == null)
					break ;				
				
				String methodName = cr.getMethodName();
				
				ArrayList<IJavaType> paramTypes = new ArrayList<IJavaType>() ;
				List<IReference> args = cr.getArguments();
				if(args != null){
					for( IReference argRef : args ){
						paramTypes.add( resolve(argRef,set) ) ;
					}
				}
				
				registerMethodInvocation(headType, methodName, paramTypes, ref ) ;
				
				if( methodName == null )
					break ;
				
				QualifiedName qualifiedName = headType.getQualifiedName();
				if(qualifiedName == null)
					break ;
				
				List<String> values = qualifiedName.getValues();
				if(values.size()!=1)
					break ;
				
				String headName = values.iterator().next() ;
				
				IClassOutline outline
						= collector.getOutlieAccessor().getOutlineContainer().getClassOutline(headName);
				
				if(outline == null)
					break ;				
					
				IJavaMethodDeclaration targetMethodDeclaration = null ;					
				List<IJavaMethodDeclaration> methods = outline.getMethodsBySimpleName(methodName);
				if(methods.size() == 1){
					targetMethodDeclaration = methods.get(0);						
				}
				else{					
					for( IJavaMethodDeclaration md : methods ){
							
						if( matches(md,paramTypes) ){
							targetMethodDeclaration = md ;
							break ;
						}							
					}
				}
					
				if(targetMethodDeclaration != null){
					resultType = getMethodValue( outline, headType, targetMethodDeclaration ) ;					
				}
				break ;
			}
		}else if (ref instanceof AllocationReference ){
			
			while(true){
				AllocationReference allocRef = (AllocationReference) ref ;	
				
				IJavaType headType = allocRef.getType() ;
				if(headType == null)
					break ;
				
				String methodName = headType.getSimpleName() ;//it's actually a constructor
				
				ArrayList<IJavaType> paramTypes = new ArrayList<IJavaType>() ;
				List<IReference> args = allocRef.getArguments();
				if(args != null){
					for( IReference argRef : args ){
						paramTypes.add( resolve(argRef,set) ) ;
					}
				}
				
				registerMethodInvocation(headType, methodName, paramTypes, ref ) ;
				
				resultType = headType ;					
				break ;
			}
			
		}else if( ref instanceof FieldAccessReference ){
			
			while(true){
				FieldAccessReference fRef = (FieldAccessReference) ref ;
				IJavaType headType = resolve(fRef.getHeadReference(), set) ;
				if(headType == null)
					break ;
			
				List<String> values = headType.getQualifiedName().getValues();
				if(values.size()!=1)
					break ;
				
				String headName = values.iterator().next() ;			
				IClassOutline outline
						= collector.getOutlieAccessor().getOutlineContainer().getClassOutline(headName);
			
				if(outline == null)
					break ;
				
				IJavaFieldDeclaration fDecl = outline.getFieldBySimpleName(fRef.getFieldName());
				if(fDecl == null)
					break ;
				
				resultType = fDecl.getType() ;
				break ;
			}
			
		}else if (ref instanceof ResolvedReference ){
			ResolvedReference rRef = (ResolvedReference) ref ;			
			resultType = rRef.getType() ;
		} else if(ref instanceof LiteralReference){
			LiteralReference lRef = (LiteralReference) ref ;
			return new JavaTypeReducedImpl( lRef.getType() ) ;
		} else if(ref instanceof NullLiteralReference){
			return new NullTypeImpl() ;
		}
		
		
		} catch(Exception e){
			resultType = new UnresolvedReferenceType() ;
		}
		
		if(resultType != null){			
			contributeType(resultType, set, ref.getBeginLine(), ref.getEndLine() );
		}
		
		return resultType ;
	}

	private void registerMethodInvocation(
			IJavaType type,String methodName, ArrayList<IJavaType> paramTypes, IReference ref)
	{
		ArrayList<String> params = new ArrayList<String>() ;
		
		for(IJavaType paramType : paramTypes){
			if( paramType == null )
				return ;
			
			String simpleName ;
			if( paramType instanceof NullTypeImpl ){
				simpleName = MethodsInvocationContainer.NULL_TYPE ;
			}
			else
				simpleName = paramType.getSimpleName();
			
			params.add(simpleName) ;			
		}
		
		IJavaTypeDeclaration declaration = type.getDeclaration();
		if(declaration == null)
			return ;
		
		getMethodInvocationContainer().registarTargetMethodInvocation(
				type, methodName, params, currentClassReferenceStorage.getQualifiedName(), ref ) ;
		
//		IJavaType superClass = declaration.getSuperClass();
//		if(superClass != null)
//			registerMethodInvocation(superClass, methodName, paramTypes, ref) ;
//		
//		List<IJavaType> interfaces = type.getDeclaration().getInterfaces();
//		if(interfaces != null){
//			for(IJavaType iType : interfaces)
//				registerMethodInvocation(iType, methodName, paramTypes, ref) ;
//		}
	}

	private boolean matches( IJavaMethodDeclaration md, ArrayList<IJavaType> paramTypes) {
		
		List<IJavaType> parameters = md.getParameters();
		int size = parameters.size();
		if(size != paramTypes.size())
			return false ;
		
l0:		for( int i = 0 ; i < size ; i++ ){

			QualifiedName qName1 = parameters.get(i).getQualifiedName();
			
			IJavaType param2 = paramTypes.get(i);
			if(param2 == null)
				return false ;
			
			if( param2 instanceof NullTypeImpl )
				continue ;
			
			QualifiedName qName2 = param2.getQualifiedName();
			for( String s : qName1.getValues() ){
				if(qName2.getValues().contains(s))
					continue l0;
			}
			return false ;
		}		
		return true ;
	}

	private IJavaType getMethodValue(IClassOutline targetOutline, IJavaType target, IJavaMethodDeclaration md)
	{
		return md.getReturnType();
	}

	private void contributeType(IJavaType type, LinkedHashSet<SimpleClassReference> set, int beginLine, int endLine) {
		
		QualifiedName qName = type.getQualifiedName();
		if(qName==null)
			return ;
		
		for( String qualifiedName : qName.getValues() ){		
			SimpleClassReference rRef = new SimpleClassReference(qualifiedName, beginLine, endLine ) ;
			set.add(rRef) ;
			
			List<IJavaType> genericParameters = type.getGenericParameters();
			if( genericParameters == null )
				continue ;
			
			for( IJavaType paramType : genericParameters){
				contributeType(paramType, set, beginLine, endLine) ;
			}
		}
	}

	public IReferenceContainer getReferenceContainer() {
		return referenceContainer;
	}
	
	
	
	private void writeDump(RuleContext ctx, IReferenceContainer rc) {		
		
		Object pathObj = ctx.getAttribute("output.properties.path");
		if(pathObj == null)
			return ;
		
		String path = pathObj.toString();		
			
		Properties prop = new Properties() ;
		try {
			FileInputStream fis = new FileInputStream( new File(path));			
			prop.load(fis) ;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}		
			
		String dumpPath = prop.getProperty("dump.path.reference") ;
		if(dumpPath == null)
			return ;
		
		DefaultReferencePrinter printer = new DefaultReferencePrinter() ;
		printer.writeDump(getReferenceContainer(), dumpPath) ;
	}
	
	private void setReferenceContainer(Object data) {
		
		this.referenceContainer = BasicReferenceContainer.getInstance() ;//null ;
		collector.setReferenceContainer( getReferenceContainer() ) ;
	}
	
	protected IOutlineContainer getOutlineContainer(){
		return collector.getOutlieAccessor().getOutlineContainer() ;
	}

	public MethodsInvocationContainer getMethodInvocationContainer() {
		return methodInvocationContainer;
	}
	
	protected boolean isEnterpriseBeanEJB3(String className) {		
		return collector.getOutlieAccessor().isEnterpriseBeanEJB3(className) ;
	}
	
	protected boolean isEnterpriseBeanEJB2(String className) {
		return collector.getOutlieAccessor().isEnterpriseBeanEJB2(className) ;
	}
	
	protected boolean isEnterpriseBeanSuperclassEJB3(String className) {		
		return collector.getOutlieAccessor().isEnterpriseBeanSuperclassEJB3(className) ;
	}
	
	protected boolean isEnterpriseBeanSuperclassEJB2(String className) {
		return collector.getOutlieAccessor().isEnterpriseBeanSuperclassEJB2(className) ;
	}
	
	protected void reportError(RuleContext ctx, IClassOutline outline, IReference ref) {
		reportError(ctx, outline, ref.getBeginLine(), ref.getEndLine() ) ;		
	}
}
