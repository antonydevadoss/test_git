/*******************************************************************************
 * FILE NAME: MissingProducesRule.java
 * PURPOSE: This is Rule-3 in document
 *
 *
 * Revision History: 
 * DATE:           				  AUTHOR:              	CHANGE:  
 * Tue Feb 09, 2011               Atul Bajpai       	Initial Version  
 * 
 * 
 ******************************************************************************/
package net.junos.space.pmd.rules.rest;

import net.junos.space.pmd.utils.PMDConstants;import net.junos.space.pmd.utils.PMDUtils;import net.sourceforge.pmd.RuleContext;import net.sourceforge.pmd.lang.java.ast.ASTMethodDeclarator;import net.sourceforge.pmd.lang.java.rule.AbstractJavaRule;import org.apache.log4j.Logger;

/**
 * This class validates, any REST method returning some DTO must have 
 * @Produces Annotation, either in method level or Class level
 */
public class MissingProducesRule extends AbstractJavaRule {

	/**
	 * Logger
	 */
	private static Logger log = Logger.getLogger(MissingProducesRule.class);

	/**
	 * This method visits all methods in Class and checks if any REST method returning 
	 * some DTO do not have @Produces Annotation. In this case It checks whether the 
	 * Class is having @Producess Annotation or not.
	 */
	public Object visit(ASTMethodDeclarator methodNode, Object data) {
		RuleContext ctx = (RuleContext) data;
		String methodName = methodNode.getImage();
		try {
			boolean isProduceAnnotationExists = false;
			boolean isRest = false;
			boolean isDTO = false;

			// checks whether the method is REST or not.
			isRest = PMDUtils.isRestMethod(methodNode);
			if (isRest) {
				isDTO = PMDUtils.isReturningDTO(methodNode); // Checks the Result Type of method (DTO type)
				if (isDTO) {
					isProduceAnnotationExists = PMDUtils.isMethodAnnotationExists(methodNode, PMDConstants.PRODUCES_ANNOTATION);
					if (!isProduceAnnotationExists)
						isProduceAnnotationExists = PMDUtils
								.isClassAnnotationExists(methodNode, PMDConstants.PRODUCES_ANNOTATION);
				}
			}
			
			if(log.isDebugEnabled()){
				StringBuilder builder = new StringBuilder();
				builder.append("source file=").append(ctx.getSourceCodeFilename()).append(",method=").append(methodName);
				builder.append(",isRest=").append(isRest).append(",isProduceAnnotationExists=").append(isProduceAnnotationExists);
				log.debug(builder.toString());
			}
			
			if (isRest && isDTO && !isProduceAnnotationExists)
				addViolation(data, methodNode);
		} catch (Exception e) {
			log.error("Exception in executing method " + methodName + " of " + ctx.getSourceCodeFilename(), e);
		}
		return super.visit(methodNode, data);
	}

}
