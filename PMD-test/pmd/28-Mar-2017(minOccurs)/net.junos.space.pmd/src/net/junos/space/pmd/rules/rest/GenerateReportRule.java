package net.junos.space.pmd.rules.rest;



import java.io.File;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import net.juniper.common.client.nativejson.JSONArray;
import net.juniper.common.client.nativejson.JSONObject;
import net.juniper.jmp.annotation.rbac.CRUDEnum;
import net.juniper.jmp.annotation.rbac.RBAC;
import net.juniper.patch.core.PATCH;
import net.junos.space.pmd.ClassContext;
import net.junos.space.pmd.PMDContext;
import net.junos.space.pmd.rules.rest.RBACInfo;
import net.junos.space.pmd.utils.CustomUtils;
import net.junos.space.pmd.utils.PMDUtils;
import net.sourceforge.pmd.RuleContext;
import net.sourceforge.pmd.lang.ast.Node;
import net.sourceforge.pmd.lang.java.ast.ASTClassOrInterfaceDeclaration;
import net.sourceforge.pmd.lang.java.ast.ASTCompilationUnit;
import net.sourceforge.pmd.lang.java.ast.ASTMethodDeclarator;
import net.sourceforge.pmd.lang.java.rule.AbstractJavaRule;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;



public class GenerateReportRule extends AbstractJavaRule {

	public static final String CLASS_NAME = "CLASS_NAME";
	public static final String PACKAGE_NAME = "PACKAGE_NAME";
	public static final String PATH_VALUE = "PATH_VALUE";
	public static final String PMD_CTX = "PMD_CTX";
	public static final String CONTEXT_ROOT = "context-root";
	public static final String PATH = "Path";
	public static final String CONSUMES = "Consumes";
	public static final String PRODUCES = "Produces";
	public static final String RBAC = "RBAC";
	
	public static ThreadLocal<PMDContext> pmdCtxTL = new ThreadLocal<PMDContext>();
	public static ThreadLocal<ClassContext> classCtxTL = new ThreadLocal<ClassContext>();
	
	
	/** 
	 * Rule start
	 * Initailize global params
	 */
	public void start(RuleContext paramRuleContext) {
		try {
			paramRuleContext.removeAttribute(PMD_CTX);
			paramRuleContext.setAttribute(PMD_CTX, PMDContext.getInstance());
		} catch(Exception e) {
			System.out.println("Error while initializing Report");
			e.printStackTrace();
		}
	}
	/**
	 * File Start
	 * Initialize params for a source file
	 */
    public void apply(List<? extends Node> paramList, RuleContext paramRuleContext) {
		try {
	    	pmdCtxTL.set((PMDContext) paramRuleContext.getAttribute(PMD_CTX));
	    	classCtxTL.remove();
	    	classCtxTL.set(new ClassContext());
	    	
	    	Map<String, String> contextRootMap = pmdCtxTL.get().getContextRootMap();
	    	
	    	String projectRoot = getProjectRootPath(paramRuleContext);
	    	classCtxTL.get().setProjectRoot(projectRoot);
	    	if(!contextRootMap.containsKey(projectRoot)) {
	    		contextRootMap.put(projectRoot, getContextRoot(projectRoot));
	    	}
	    	classCtxTL.get().setContextRoot(contextRootMap.get(projectRoot));
		} catch(Exception e) {
			System.out.println("Error while initializing File context");
			e.printStackTrace();
		}
		super.apply(paramList, paramRuleContext);
    }
    /**
     * Rule End
     * Finalize and print report
     */
	public void end(RuleContext paramRuleContext) {
		try {
			boolean reportHeader = false;
			List<RestInfo> infoList = (List<RestInfo>) ((PMDContext)paramRuleContext.getAttribute(PMD_CTX)).getReportList();
			handleCSV(infoList);
			handleJSON(infoList);
		} catch(Exception e) {
			System.out.println("Error while finalizing Report");
			e.printStackTrace();
		}
		
	}
	
	private void handleCSV(List<RestInfo> infoList) {
		if(infoList != null) {
			StringBuffer br =new StringBuffer();
			br.append("HTTP-Method").append(",")
			.append("API").append(",")
			.append("Consumes").append(",")
			.append("Produces").append(",")
			.append("Version").append(",")
			.append("RBAC").append(",")
			.append("Java-Class").append(",")
			.append("Java-Method").append(",")
			.append("Source-Path");
			br.append("\n");
			for (RestInfo restInfo : infoList) {
				if(restInfo != null) {
					br.append(csvEscape(restInfo.getHTTPMethodName())).append(",");
					br.append(csvEscape(restInfo.getApiURL())).append(",");
					br.append(csvEscape(join(restInfo.getConsumeTypes()))).append(",");
					br.append(csvEscape(join(restInfo.getProduceTypes()))).append(",");
					br.append(csvEscape(restInfo.getVersion() != null ? restInfo.getVersion().toString() : null)).append(",");
					br.append(csvEscape(restInfo.getRbacInfo() != null ? restInfo.getRbacInfo().toString() : null)).append(",");
					br.append(csvEscape(restInfo.getJavaClassName())).append(",");
					br.append(csvEscape(restInfo.getJavaMethodName())).append(",");
					br.append(csvEscape(restInfo.getSourcePath()));
					br.append("\n");
				}
			}
			br.deleteCharAt(br.length()-1);
			
			System.out.println(br.toString());
		}
	}
	
	
	private void handleJSON(List<RestInfo> infoList) {
		try {
			if(infoList != null) {
				JSONArray arr = new JSONArray();
				for (RestInfo restInfo : infoList) {
					if(restInfo != null) {
						JSONObject obj = new JSONObject();
						obj.put("http-method", restInfo.getHTTPMethodName());
						obj.put("api", restInfo.getApiURL());
						obj.put("consumes", toJSONArray(restInfo.getConsumeTypes()));
						obj.put("produces", toJSONArray(restInfo.getProduceTypes()));
						obj.put("version", restInfo.getVersion());
						obj.put("rbac", restInfo.getRbacInfo() != null ? restInfo.getRbacInfo().toJSONObject() : null);
						
						obj.put("java-class", restInfo.getJavaClassName());
						obj.put("java-method", restInfo.getJavaMethodName());
						obj.put("source-path", restInfo.getSourcePath());
						
						arr.put(obj);
					}
				}
				
				System.out.println(arr.toString());
			}
		} catch(Exception e) {
			System.out.println("Error while handleJSON");
			e.printStackTrace();
		}

	}
	
	private JSONArray toJSONArray(List<String> list) {
		JSONArray arr = new JSONArray();
		if(list != null) {
			for (String item : list) {
				arr.put(item);
			}
		}
		return arr;
	}
	
	private String join(List<String> stringList) {
		String res = null;
		if(stringList != null && !stringList.isEmpty()) {
			StringBuffer br = new StringBuffer();
			for (String string : stringList) {
				br.append(string).append(",");
			}
			br.deleteCharAt(br.length()-1);
			res=br.toString();
		}
		return res;
	}
	private String csvEscape(String input) {
		String res = "\"\"";
		if(input != null && !input.isEmpty()) {
			StringBuffer br  = new StringBuffer();
			br.append("\"").append(input.replaceAll("\"", "\"\"")).append("\"");
			res = br.toString();
		}
		return res;
	}

	private static Logger log = Logger.getLogger(GenerateReportRule.class);

	public Object visit(ASTCompilationUnit compilationUnit, Object data) {
		return super.visit(compilationUnit, data);
	}
	
	public Object visit(ASTClassOrInterfaceDeclaration classNode, Object data) {
		RuleContext ctx = (RuleContext) data;
		try {
			String className = classNode.getImage();
			String packageName = CustomUtils.getPackageName(classNode.getFirstParentOfType(ASTCompilationUnit.class));
			
			classCtxTL.get().setClassName(className);
			classCtxTL.get().setPackageName(packageName);
			
			crawlForRestInfo(Class.forName(packageName+"."+className), data);
		} catch(Throwable e) {
			System.out.println("Error while initializing class context - "+ctx.getSourceCodeFilename());
			e.printStackTrace();
		}
		return data;
		
	}
	
	private void crawlForRestInfo(Class<?> classObj, Object data) {
		try {
			RuleContext ctx = (RuleContext) data;
			classCtxTL.get().setPathAnnotationValue(getPathValue(classObj));
			
			Method[] methods = classObj.getMethods();
			if(methods != null) {
				for (Method method : methods) {
					String restHTTPMethod = getRestHTTPMethod(method);
					//Rest Method
					if(restHTTPMethod != null) {
						RestInfo info = new RestInfo();
						String url = getRestUrl(method);
						info.setApiURL(url.replaceAll("/+", "/"));
						info.setHTTPMethodName(restHTTPMethod);
						info.setConsumeTypes(getConsumesValueList(method));
						info.setProduceTypes(getProducesValueList(method));
						info.setRbacInfo(getRbacInfo(method));
						info.setVersion(getVersionFromMimeTypes(info));
						
						info.setJavaClassName(classObj.getName());
						info.setJavaMethodName(method.toGenericString());
						info.setSourcePath(ctx.getSourceCodeFilename());
						
						pmdCtxTL.get().addToReportList(info);
					}
				}
			}

			
		} catch(Exception e) {
			System.out.println("Error while processing class object");
			e.printStackTrace();
		}
		
	}
	
	private RBACInfo getRbacInfo(Method method) {
		RBACInfo info = null;
		try {
			if(method != null) {
				RBAC rbacAnnotation = method.getAnnotation(RBAC.class);
				if(rbacAnnotation != null) {
					info = new RBACInfo();
					if(rbacAnnotation.capability() != null) {
						info.setCapabilities(Arrays.asList(rbacAnnotation.capability()));
					}
					if(rbacAnnotation.task() != null) {
						info.setTasks(Arrays.asList(rbacAnnotation.task()));
					}
					CRUDEnum[] crudEnums = rbacAnnotation.type();
					if(crudEnums != null && crudEnums.length > 0) {
						List<String> crudTypes = new ArrayList<String>();
						for (CRUDEnum crudEnum : crudEnums) {
							switch (crudEnum) {
							case CREATE:
								crudTypes.add("CREATE");
								break;
							case ALL:
								crudTypes.add("ALL");
								break;
							case DELETE:
								crudTypes.add("DELETE");
								break;
							case READ:
								crudTypes.add("READ");
								break;
							case UPDATE:
								crudTypes.add("UPDATE");
								break;
							}
						}
						info.setCrudTypes(crudTypes);
					}
					
				}
			}
		} catch(Exception e) {
			System.out.println("Error while processing method annotation");
			e.printStackTrace();
		}
		return info;
	}
	private List<String> getConsumesValueList(Method method) {
		List<String> result = null;
		try {
			if(method != null) {
				Consumes consumes = method.getAnnotation(Consumes.class);
				if(consumes != null) {
					result = Arrays.asList(consumes.value());
				}
			}
		} catch(Exception e) {
			System.out.println("Error while processing consumes annotation");
			e.printStackTrace();
		}
		return result;
	}
	
	private List<String> getProducesValueList(Method method) {
		List<String> result = null;
		try {
			if(method != null) {
				Produces consumes = method.getAnnotation(Produces.class);
				if(consumes != null) {
					result = Arrays.asList(consumes.value());
				}
			}
		} catch(Exception e) {
			System.out.println("Error while processing produces annotation");
			e.printStackTrace();
		}
		return result;
	}
	private String getRestHTTPMethod(Method method) {
		String restHTTPMethod = null;
		try {
			if(method != null) {
				if(method.getAnnotation(GET.class) != null) {
					restHTTPMethod = "GET";
				} else if(method.getAnnotation(POST.class) != null) {
					restHTTPMethod = "POST";
				} else if(method.getAnnotation(PUT.class) != null) {
					restHTTPMethod = "PUT";
				} else if(method.getAnnotation(DELETE.class) != null) {
					restHTTPMethod = "DELETE";
				} else if(method.getAnnotation(PATCH.class) != null) {
					restHTTPMethod = "PATCH";
				}
			}
		} catch(Exception e) {
			System.out.println("Error while extracting http method type");
			e.printStackTrace();
		}
		return restHTTPMethod;
	}
	private String getPathValue(Class<?> classObj) {
		String pathVal = null;
		if(classObj != null) {
			Path p = classObj.getAnnotation(Path.class);
			if(p != null)
			pathVal = p.value();
		}
		return pathVal;
	}
	private Integer getVersionFromMimeTypes(RestInfo info) {
		Integer version = null;
		try {
			if(info != null) {
				String consumeVersion = null;
				String produceVersion = null;
				List<String> consumeTypes = info.getConsumeTypes();
				List<String> produceTypes = info.getProduceTypes();
				if(consumeTypes != null && !consumeTypes.isEmpty()) {
					consumeVersion = extractVersion(consumeTypes.get(0));
				}
				if(produceTypes != null && !produceTypes.isEmpty()) {
					produceVersion = extractVersion(produceTypes.get(0));
				}
				
				if(consumeVersion != null && produceVersion != null) {
					if(produceTypes.get(0).contains("vnd.net.juniper.space.job-management.task+")) {
						version = Integer.parseInt(consumeVersion);
					} else {
						if(consumeVersion.equals(produceVersion)) {
							version = Integer.parseInt(consumeVersion);
						}
					}
				} else if(consumeVersion != null){
					version = Integer.parseInt(consumeVersion);
				} else if(produceVersion != null) {
					if(!produceTypes.get(0).contains("vnd.net.juniper.space.job-management.task+")){
						version = Integer.parseInt(produceVersion);
					}
				}
			}
		} catch(Exception e) {
			System.out.println("Error while getVersionFromMimeTypes");
			e.printStackTrace();
		}
		return version;
	}
	
	private String extractVersion(String mimeType) {
		String version=null;
		if(mimeType != null && !mimeType.isEmpty()) {
			try {
				String rootPath = "";
				String versionPattern = "version=(?<version>\\d+);";
				Pattern pattern = Pattern.compile(versionPattern);
		        Matcher matcher = pattern.matcher(mimeType);
		        if(matcher.find()) {
		        	version = matcher.group("version");
		        }
			} catch(Exception e) {
				System.out.println("Error while extracting version from mime type");
				e.printStackTrace();
			}
		}
		return version;
	}
	private String getProjectRootPath(RuleContext ctx) {
		String projectRootPath = null;
		try {
			String sourceFilePath = ctx.getSourceCodeFilename();
			String rootPath = "";
			String fileRootPattern = "(?<path>/.*/(CMP|CEMS)/[^/]+/)(.*)";
			Pattern pattern = Pattern.compile(fileRootPattern);
	        Matcher matcher = pattern.matcher(sourceFilePath);
	        if(matcher.find()) {
	        	rootPath = matcher.group("path");
	        	if(rootPath != null) {
	        		projectRootPath = rootPath;
	        	}
	        }
		} catch(Exception e) {
			System.out.println("Error while getting project root path");
			e.printStackTrace();
		}
		return projectRootPath;
	}
	
	private String getContextRoot(String projectRootPath) {
		String contextRoot = null;
		try {
        	if(projectRootPath != null) {
        		String jbossWebPath = projectRootPath + "web/WEB-INF/jboss-web.xml";
	            File jbossWebFile = new File(jbossWebPath);
	            
	            if(jbossWebFile.exists()) {
	                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	                DocumentBuilder builder = factory.newDocumentBuilder();
	                Document dd = builder.parse(jbossWebFile);
	                NodeList nodeList = dd.getElementsByTagName(CONTEXT_ROOT);
	                if(nodeList != null && nodeList.item(0) != null) {
	                	contextRoot = nodeList.item(0).getTextContent();
	                	if(!(contextRoot.charAt(0) == '/')) {
	                		contextRoot = "/" + contextRoot;
	                	}
	                	if(contextRoot.charAt(contextRoot.length()-1) == '/') {
	                		contextRoot = contextRoot.substring(0, contextRoot.length()-1);
	                	}
	                }
	            }
	        }
			
		} catch(Exception e) {
			System.out.println("Error while getting context root");
			e.printStackTrace();
		}
		return contextRoot;
	}
	
	private String getRestUrl(Method method) throws Exception {
		String url = null;
		try {
			String urlFromMethod = null;
			Path methodPath = method.getAnnotation(Path.class);
			if(methodPath != null) {
				urlFromMethod = methodPath.value();
			}
			
			StringBuffer br = new StringBuffer();
			if(classCtxTL.get().getContextRoot() != null) {
				br.append(classCtxTL.get().getContextRoot()).append("/");
			}
			if(classCtxTL.get().getPathAnnotationValue() != null) {
				br.append(classCtxTL.get().getPathAnnotationValue()).append("/");
			}
			if(urlFromMethod != null) {
				br.append(urlFromMethod);
			}
			url = br.toString();
		} catch(Exception e) {
			System.out.println("Error while constructing rest URL");
			e.printStackTrace();
		}
		return url;
	}
}

