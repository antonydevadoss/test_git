/*******************************************************************************
 * FILE NAME: AvoidConsumesForOnlyAnnotationParameterRule.java
 * PURPOSE:   This is Rule-7 of document.
 *
 *
 * Revision History: 
 * DATE:           				  AUTHOR:              	CHANGE:  
 * Tue Feb 09, 2011               Atul Bajpai       	Initial Version  
 * 
 * 
 ******************************************************************************/
package net.junos.space.pmd.rules.rest;

import net.junos.space.pmd.utils.PMDConstants;import net.junos.space.pmd.utils.PMDUtils;import net.sourceforge.pmd.RuleContext;import net.sourceforge.pmd.lang.java.ast.ASTMethodDeclarator;import net.sourceforge.pmd.lang.java.rule.AbstractJavaRule;import org.apache.log4j.Logger;

/**
 * This class provides the facility to verify that the methods should not have @Consumes
 * Annotation, if the Input parameters are other annotations like @PathParam, @QueryParam
 * etc.
 */
public class AvoidConsumesForAnnotationParameterOnlyRule extends AbstractJavaRule {

	/**
	 * Logger
	 */
	private static Logger log = Logger.getLogger(AvoidConsumesForAnnotationParameterOnlyRule.class);
	
	/*
	 * This method checks for @Consumes annotation while Input parameters are
	 * other annotation i.e. @PathParam, @QueryParam etc.
	 */
	public Object visit(ASTMethodDeclarator methodNode, Object data) {
		RuleContext ctx = (RuleContext) data;
		String methodName = methodNode.getImage();
		try {
			boolean isConsumesAnnotationExists = false;
			boolean isAnnotationParameterExistsOnly = false;
			boolean isRest = false;
					
			// checks whether the method is REST or not.
			isRest = PMDUtils.isRestMethod(methodNode);
			if (isRest) {
				//checks whether method parameters contain Annotation parameters only
				isAnnotationParameterExistsOnly = PMDUtils.hasOnlyAnnotationParameters(methodNode);
				if(isAnnotationParameterExistsOnly) 
					isConsumesAnnotationExists = PMDUtils.isMethodAnnotationExists(methodNode, PMDConstants.CONSUMES_ANNOTATION);
			}
			
			if(log.isDebugEnabled()){
				StringBuilder builder = new StringBuilder();
				builder.append("source file=").append(ctx.getSourceCodeFilename()).append(",method=").append(methodName);
				builder.append(",isRest=").append(isRest).append(",isConsumesAnnotationExists=").append(isConsumesAnnotationExists)
						.append(",isAnnotationParameterExistsOnly=").append(isAnnotationParameterExistsOnly);
				log.debug(builder.toString());
			}
			
			if (isRest && isConsumesAnnotationExists && isAnnotationParameterExistsOnly)
				addViolation(data, methodNode);
		} catch(Exception e) {
			log.error("Exception in executing method " + methodName + " of " + ctx.getSourceCodeFilename(), e);
		}
		return super.visit(methodNode, data);
	}
}
