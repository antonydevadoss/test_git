package net.junos.space.pmd.utils.reference;

import java.util.ArrayList;
import java.util.HashMap;



public class VariablesStack {
	
	private ArrayList<HashMap<String,IReference>> stack
			= new ArrayList<HashMap<String,IReference>>() ;
	
	private ArrayList<HashMap<String,VariableInfo>> infoStack
			= new ArrayList<HashMap<String,VariableInfo>>() ;
	
	public IReference getVariable(String name){
		
		for( int i = stack.size() ; i > 0 ; i-- ){
			HashMap<String, IReference> block = stack.get(i-1);
			IReference ref = block.get(name);
			if(ref != null)
				return ref ;
		}		
		return null ;
	}
	
	public VariableInfo getVariableInfo(String name){
		
		for( int i = infoStack.size() ; i > 0 ; i-- ){
			HashMap<String, VariableInfo> block = infoStack.get(i-1);
			VariableInfo info = block.get(name);
			if(info != null)
				return info ;
		}		
		return null ;
	}
	
	public void push(){
		HashMap<String, IReference> block = new HashMap<String, IReference>() ;
		stack.add( block ) ;
		
		HashMap<String, VariableInfo> block1 = new HashMap<String, VariablesStack.VariableInfo>() ;
		infoStack.add( block1 ) ;
		
	}
	
	public void pop() {
		if(!stack.isEmpty())
			stack.remove( stack.size()-1 ) ;
		
		if(!infoStack.isEmpty())
			infoStack.remove( infoStack.size()-1 ) ;
	}
	
	public void addVariable( String name, IReference var, VariableInfo info ){
		if(var == null)
			return ;
		
		stack.get(stack.size()-1).put(name, var) ;
		
		if( info == null )
			info = new VariableInfo(false) ;
		
		infoStack.get(infoStack.size()-1).put(name, info) ;
	}
	
	public static class VariableInfo{
		
		final boolean isArray ;

		public VariableInfo(boolean isArray) {
			super();
			this.isArray = isArray;
		}

		public boolean isArray() {
			return isArray;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + (isArray ? 1231 : 1237);
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			VariableInfo other = (VariableInfo) obj;
			if (isArray != other.isArray)
				return false;
			return true;
		}
	}

}
