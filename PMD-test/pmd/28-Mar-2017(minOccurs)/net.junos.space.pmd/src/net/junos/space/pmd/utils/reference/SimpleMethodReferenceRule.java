package net.junos.space.pmd.utils.reference;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.reference.ClassMethodsInvocationRegistry.MethodInvocationData;
import net.sourceforge.pmd.RuleContext;

public class SimpleMethodReferenceRule extends AbstractReferenceAccessor {
	@Override
	public final void doStart(RuleContext ctx) {
		for (String qName : getTargetClasses() ) {

			for (String methodName : getTargetMethods()) {
				
				List<String> params = getParams(qName,methodName) ;				
				MethodsInvocationContainer.getInstance().registerTargetMethod(
						qName, methodName, params );
			}
		}
		super.doStart(ctx);
	}
	
	@Override
	public final void doEnd(RuleContext ctx) {
		for (String qName : getTargetClasses() ) {

			ClassMethodsInvocationRegistry reg = MethodsInvocationContainer
					.getInstance().getInvocationRegistry(qName);

			for (String methodName : getTargetMethods() ) {
				
				List<String> params = getParams(qName, methodName) ;

				MethodInvocationData data = reg.getInvocationData(methodName, params);
				Collection<String> classes = data.getClasses();

				for (String className : classes) {
					
					if(!mustBeChecked(className))
						continue ;

					IClassOutline outline = getOutlineContainer().getClassOutline(className);
					String path = outline.getFilePath();
					ctx.setSourceCodeFilename(path);

					List<IReference> references = data.getReferences(className);

					for (IReference ref : references) {
						reportError(ctx,outline,ref) ;
					}
				}
			}
		}
		super.doEnd(ctx);
	}

	protected List<String> getParams(String qName, String methodName) {
		return null;
	}

	protected List<String> getTargetMethods() {
		return new ArrayList<String>() ;
	}
	
	public String getMessage(){
		return "" ;
	}

	protected List<String> getTargetClasses() {
		return new ArrayList<String>() ;
	}
	

	protected boolean mustBeChecked(String className){
		return true;
	}
}
