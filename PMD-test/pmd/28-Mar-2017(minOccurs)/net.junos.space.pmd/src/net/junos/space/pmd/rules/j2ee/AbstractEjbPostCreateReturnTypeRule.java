package net.junos.space.pmd.rules.j2ee;

import java.util.ArrayList;
import java.util.List;

import net.junos.space.pmd.utils.outline.AbstractOutlineAccessor;
import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IJavaMethodDeclaration;
import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;
import net.sourceforge.pmd.RuleContext;

public abstract class AbstractEjbPostCreateReturnTypeRule extends
		AbstractOutlineAccessor {

	public AbstractEjbPostCreateReturnTypeRule() {
		super();
	}

	public abstract boolean isProperBean(IJavaTypeDeclaration decl);

	@Override
	public void doEnd(RuleContext ctx) {
		
l0:		for (IClassOutline outline : getOutlineContainer().getAllOutlines()) {
	
			if(!outline.hasAllSuperclassesDefined(getOutlineContainer()))
				continue ;
				
			IJavaTypeDeclaration decl = outline.getTypeDeclaration();
				
			if ( !isProperBean(decl))
				continue ;
			
			List<IClassOutline> definedSuperclasses = outline.getDefinedSuperclasses(getOutlineContainer());
			ArrayList<IClassOutline> hierarchy = new ArrayList<IClassOutline>() ;
			hierarchy.add(outline) ;
			hierarchy.addAll(definedSuperclasses) ;
			for(IClassOutline o : hierarchy)
			{
				for (IJavaMethodDeclaration method : o.getMethodsBySimpleName(getMethodName())) {
					if (method.getReturnType() == null) {
						continue l0;
					}
				}
			}
			reportError(ctx, outline, outline.getTypeDeclaration());
		}
		super.doEnd(ctx);
	}

	public String getMethodName() {
		return "ejbPostCreate";
	}

}