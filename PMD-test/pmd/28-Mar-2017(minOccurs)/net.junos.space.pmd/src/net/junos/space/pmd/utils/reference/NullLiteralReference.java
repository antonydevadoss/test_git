package net.junos.space.pmd.utils.reference;

public class NullLiteralReference  extends AbstractReference{

	public NullLiteralReference( int beginLine, int startLine){
		super(beginLine, startLine);
	}	
	
	public String toString(){
		return "Null Reference" ;
	}
}
