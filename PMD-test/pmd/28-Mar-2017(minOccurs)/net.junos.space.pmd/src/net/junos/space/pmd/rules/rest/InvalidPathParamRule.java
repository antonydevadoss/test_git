/*******************************************************************************
 * FILE NAME: InvalidPathParamRule.java
 * PURPOSE: This is Rule-9 of document 
 *
 *
 * Revision History: 
 * DATE:           				  AUTHOR:              	CHANGE:  
 * Tue Feb 22, 2011               Atul Bajpai       	Initial Version  
 * 
 * 
 ******************************************************************************/
package net.junos.space.pmd.rules.rest;

import java.util.List;import net.junos.space.pmd.utils.PMDConstants;import net.junos.space.pmd.utils.PMDUtils;import net.sourceforge.pmd.RuleContext;import net.sourceforge.pmd.lang.java.ast.ASTMethodDeclarator;import net.sourceforge.pmd.lang.java.rule.AbstractJavaRule;import org.apache.log4j.Logger;

/**
 * This class checks whether or not REST method is expecting @PathParam as input 
 * parameter, but the attribute in @PathParam is not present in attribute in 
 * @PathParam annotation (either attribute is not present or not enclosed in curly 
 * braces).
 */
public class InvalidPathParamRule extends AbstractJavaRule {

	/**
	 * Logger
	 */
	private static Logger log = Logger.getLogger(InvalidPathParamRule.class);

	/**
	 * Visits all the Rest Methods of this class and checks the validity of 
	 * @PathParam annotation used as a parameter in A Rest method.
	 */
	public Object visit(ASTMethodDeclarator methodNode, Object data) {
		RuleContext ctx = (RuleContext) data;
		String methodName = methodNode.getImage();
		try {
			boolean isRestMethod = false;
			boolean isPathParamExist = false;
			boolean isMethodPathExists = false;
			boolean isClassPathExists = false;
			boolean hasValidPathParam = true;

			// checks whether the method is REST or not.
			isRestMethod = PMDUtils.isRestMethod(methodNode);
			if (isRestMethod) {

				// checks if @PathParam exists in method parameter
				isPathParamExist = PMDUtils.hasAnnotationParameter(methodNode, PMDConstants.PATHPARAM_ANNOTATION);
				if (isPathParamExist) {

					String methodPathAnnotationValue = "";
					String classPathAnnotationValue = "";
					// List of all annotation value used in a method parameter
					List<String> annotationValues = PMDUtils.getModifiedParameterAnnotationValue(methodNode,
							PMDConstants.PATHPARAM_ANNOTATION, PMDConstants.CURLYBRACE_OPENING, PMDConstants.CURLYBRACE_CLOSING);

					// checks if @Path exists as a method annotation
					isMethodPathExists = PMDUtils.isMethodAnnotationExists(methodNode, PMDConstants.PATH_ANNOTATION);
					// checks if @Path exists as a class annotation
					isClassPathExists = PMDUtils.isClassAnnotationExists(methodNode, PMDConstants.PATH_ANNOTATION);
					if (isMethodPathExists || isClassPathExists) {

						if (isMethodPathExists)
							methodPathAnnotationValue = PMDUtils.getAnnotationValue(methodNode,PMDUtils.getMethodAnnotation(methodNode,
									PMDConstants.PATH_ANNOTATION));

						if (isClassPathExists)
							classPathAnnotationValue = PMDUtils.getAnnotationValue(methodNode,PMDUtils.getClassAnnotation(methodNode,
									PMDConstants.PATH_ANNOTATION));
						for (int k = 0; k < annotationValues.size(); k++) {
							String annotationValue = annotationValues.get(k);
							if (!(methodPathAnnotationValue.contains(annotationValue) || classPathAnnotationValue
									.contains(annotationValue)))
								hasValidPathParam = false;
						}
					}
				}

			}
			
			if(log.isDebugEnabled()){
				StringBuilder builder = new StringBuilder();
				builder.append("source file=").append(ctx.getSourceCodeFilename()).append(",method=").append(methodName);
				builder.append(",isRestMethod=").append(isRestMethod).append(",isPathParamExist=").append(isPathParamExist).append(
						",hasValidPathParam=").append(hasValidPathParam);
				log.debug(builder.toString());
			}
			
			if (isRestMethod && isPathParamExist && !hasValidPathParam)
				addViolation(data, methodNode);
		} catch (Exception e) {
			log.error("Exception in executing method " + methodName + " of " + ctx.getSourceCodeFilename(), e);
		}
		return super.visit(methodNode, data);
	}
}
