package net.junos.space.pmd;

import java.util.Map;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import net.juniper.jmp.interceptors.hateoas.HATEOAS;

public class MethodInfo {
	private String name;
	private XmlElement xmlElement;
	private XmlAttribute xmlAttribute;
	private HATEOAS hateoas;
	private String fieldType = "xs:element";
	private boolean isCollectionType = false;
	private boolean isPrimitive = false;
	private boolean isRequired = false;
	private boolean isFilterable = false;
	private boolean isSortable = false;
	private boolean hasCDATA = false;
	private Map<String, Object> generatedAnnotDetails;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public XmlElement getXmlElement() {
		return xmlElement;
	}
	public void setXmlElement(XmlElement xmlElement) {
		this.xmlElement = xmlElement;
	}
	public boolean isCollectionType() {
		return isCollectionType;
	}
	public void setCollectionType(boolean isCollectionType) {
		this.isCollectionType = isCollectionType;
	}
	public boolean isFilterable() {
		return isFilterable;
	}
	public void setFilterable(boolean isFilterable) {
		this.isFilterable = isFilterable;
	}
	public boolean isSortable() {
		return isSortable;
	}
	public void setSortable(boolean isSortable) {
		this.isSortable = isSortable;
	}
	public boolean isHasCDATA() {
		return hasCDATA;
	}
	public void setHasCDATA(boolean hasCDATA) {
		this.hasCDATA = hasCDATA;
	}
	public XmlAttribute getXmlAttribute() {
		return xmlAttribute;
	}
	public void setXmlAttribute(XmlAttribute xmlAttribute) {
		this.xmlAttribute = xmlAttribute;
	}
	public String getFieldType() {
		return fieldType;
	}
	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}
	public Map<String, Object> getGeneratedAnnotDetails() {
		return generatedAnnotDetails;
	}
	public void setGeneratedAnnotDetails(Map<String, Object> generatedAnnotDetails) {
		this.generatedAnnotDetails = generatedAnnotDetails;
	}
	public HATEOAS getHateoas() {
		return hateoas;
	}
	public void setHateoas(HATEOAS hateoas) {
		this.hateoas = hateoas;
	}
	public boolean isPrimitive() {
		return isPrimitive;
	}
	public void setPrimitive(boolean isPrimitive) {
		this.isPrimitive = isPrimitive;
	}
	public boolean isRequired() {
		return isRequired;
	}
	public void setRequired(boolean isRequired) {
		this.isRequired = isRequired;
	}
}
