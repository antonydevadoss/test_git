package net.junos.space.pmd.rules.j2ee;

import net.junos.space.pmd.utils.reference.SimpleReferenceRule;

public class DontUseUIAPIRule extends SimpleReferenceRule {
	
	protected boolean mustBeChecked(String qualifiedName) {
		return isEnterpriseBeanSuperclassEJB2(qualifiedName);
	}

	protected boolean isInvalid(String refQName)
	{
		boolean result = refQName.startsWith("java.awt.")
				 		|| refQName.startsWith("org.eclipse.swt.")
				 		|| refQName.startsWith("javax.swing.");
		
		return  result;
	}
	
	public String getMessage(){
		return "Do Not Use A AWT, Swing Or Other UI API In An EJB" ;
	}
}