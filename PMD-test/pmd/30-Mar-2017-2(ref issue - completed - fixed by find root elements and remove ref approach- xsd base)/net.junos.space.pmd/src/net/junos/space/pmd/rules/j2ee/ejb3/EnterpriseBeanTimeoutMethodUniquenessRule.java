package net.junos.space.pmd.rules.j2ee.ejb3;

import java.util.ArrayList;
import net.junos.space.pmd.utils.outline.AbstractOutlineAccessor;
import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IJavaMethodDeclaration;
import net.sourceforge.pmd.RuleContext;

public class EnterpriseBeanTimeoutMethodUniquenessRule extends AbstractOutlineAccessor {

	
	@Override
	public String getMessage() {		
		return "The enterprise bean is not allowed to have multiple timeout methods.";
	}

	@Override
	public void doEnd(RuleContext ctx) {
		
		for( IClassOutline outline : getOutlineContainer().getAllOutlines() ){
			
			String qualifiedName = outline.getQualifiedNameString();
			if(!isEnterpriseBeanEJB3(qualifiedName))
				continue ;
				
			ArrayList<IJavaMethodDeclaration> timeoutMethods = 
					EnterpriseBeanTimeoutMethodStaticFinalRule.extractAllTimeoutMethods(outline,getOutlineContainer()) ;
			
			if(timeoutMethods.size()<=1)
				continue ;
			
			for(IJavaMethodDeclaration md: timeoutMethods)
				reportError(ctx, outline, md) ;
		}		
	}
}
