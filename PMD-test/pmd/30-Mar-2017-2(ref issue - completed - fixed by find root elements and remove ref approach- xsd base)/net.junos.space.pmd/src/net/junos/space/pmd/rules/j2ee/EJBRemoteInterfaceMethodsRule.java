package net.junos.space.pmd.rules.j2ee;

import java.util.List;

import net.junos.space.pmd.rules.AbstractJavaRuleWithUselessTypeResolution;
import net.sourceforge.pmd.lang.java.ast.ASTClassOrInterfaceDeclaration;
import net.sourceforge.pmd.lang.java.ast.ASTCompilationUnit;
import net.sourceforge.pmd.lang.java.ast.ASTImportDeclaration;
import net.sourceforge.pmd.lang.java.ast.ASTMethodDeclaration;
import net.sourceforge.pmd.lang.java.ast.ASTName;
import net.sourceforge.pmd.lang.java.ast.ASTNameList;

public class EJBRemoteInterfaceMethodsRule extends AbstractJavaRuleWithUselessTypeResolution {
	@Override
	public Object visit(ASTClassOrInterfaceDeclaration node, Object data) {
		try {
			if (node.isInterface() && Utils.isClassOrInterfaceExtends(node, "EJBObject", "javax.ejb"))
			{
				String prefix = "java.rmi.";
				ASTCompilationUnit cu = node.getFirstParentOfType(ASTCompilationUnit.class);
				if (cu != null)
				{
					List<ASTImportDeclaration> ids = cu.findChildrenOfType(ASTImportDeclaration.class);
					if (ids != null)
					{
						for (ASTImportDeclaration importDeclaration : ids) {
							ASTName name = importDeclaration.getFirstChildOfType(ASTName.class);
							if (name != null 
									&& name.getImage() != null 
									&& (name.getImage().equals("java.rmi.*") || 
											name.getImage().equals("java.rmi.RemoteException")))
							{
								prefix = "";
							}
						}
					}
				}
				
				List<ASTMethodDeclaration> methods = Utils.getClassOrInterfaceMethods(node);
				
				for (ASTMethodDeclaration methodDeclaration : methods) {
					ASTNameList nameList = methodDeclaration.getFirstChildOfType(ASTNameList.class);
					boolean isThrowsRemoteException = false;
					if (nameList != null)
					{
						for (ASTName name : nameList.findChildrenOfType(ASTName.class))
						{
							if (name.getImage() != null && 
									(name.getImage().equals(prefix + "RemoteException") || 
											name.getImage().equals("java.rmi.RemoteException")))
							{
								isThrowsRemoteException = true;
							}
						}
					}
					
					if (!isThrowsRemoteException)
					{
						addViolationWithMessage(data, methodDeclaration, "Make a method of an EJB Remote Interface throw java.rmi.RemoteException");
					}
				}
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return super.visit(node, data);
	}
}
