package net.junos.space.pmd.rules.xml;

import java.io.File;
import java.io.FileFilter;
import java.util.HashSet;
import java.util.List;

import net.junos.space.pmd.rules.AbstractJavaRuleWithUselessTypeResolution;
import net.sourceforge.pmd.RuleContext;
import net.sourceforge.pmd.lang.ast.Node;
import net.sourceforge.pmd.lang.java.rule.AbstractJavaRule;


public class JavaProjectCollector extends AbstractJavaRuleWithUselessTypeResolution {
	
	private static HashSet<String> pathSet ;
	
	private static HashSet<File> projects ;
	private static HashSet<File> workspaces;
	private static HashSet<String> processed;
	
	private static boolean isComplete = false ;
	
	private static DotProjectFilter dotProjectFilter = new DotProjectFilter() ;
	
	@Override
	public final void apply(List<? extends Node> astCompilationUnits, RuleContext ctx) {
		
		File file = ctx.getSourceCodeFile();
		if(file == null)
			return ;
		
		String path = file.getAbsolutePath();
		pathSet.add(path) ;
		doApply(astCompilationUnits, ctx) ;
	}

	public final void start(RuleContext ctx){
		if(pathSet == null)
			pathSet = new HashSet<String>() ;
		
		pathSet.clear() ;
		isComplete = false ;
		doStart(ctx) ;
	} ;


	
	public final void end(RuleContext ctx){
		if(!isComplete){		
			isComplete = true ;
			
			processData();
		}
		doEnd(ctx) ;
	}
	
	protected void doApply(List<? extends Node> astCompilationUnits, RuleContext ctx){}
	
	protected void doStart(RuleContext ctx) {}

	protected void doEnd(RuleContext ctx) {}

	private void processData() {
		projects = new HashSet<File>() ;
		workspaces = new HashSet<File>() ;
		processed = new HashSet<String>() ;
		
		for( String path : pathSet ){
			File file = new File(path) ;
			processFile(file) ;
		}
		
		for( File wsFile : workspaces ){
			File[] listFiles = wsFile.listFiles();
			if( listFiles == null )
				continue ;
			
			for( File f : listFiles ){
				if( isProject(f) )
					projects.add(f) ;
			}
		}
		
		workspaces = null ;
		processed = null ;
		pathSet = null ;
	}

	private void processFile(File file) {
		
		if(file == null)
			return ;
		
		File parent = file.getParentFile();
		
		String path = file.getAbsolutePath();
		if(processed.contains(path))
			return ;
		
		processed.add(path) ;
		
		if(file.isDirectory()) {			
			
			if( isProject(file) ){				
				projects.add(file) ;
				if(parent!= null)
					workspaces.add(parent) ;
			}			
		}
		processFile(parent) ;
	}

	private boolean isProject(File file)
	{
		if( !file.isDirectory() )
			return false ;
		
		File[] listFiles = file.listFiles( dotProjectFilter );
		if(listFiles == null)
			return false ;
		
		return listFiles.length != 0;
	} ;
	
	private static class DotProjectFilter implements FileFilter{

		@Override
		public boolean accept(File pathname) {
			return pathname.getName().equals(".project");
		}
	}

	public static HashSet<File> getProjects() {
		return projects;
	}

}
