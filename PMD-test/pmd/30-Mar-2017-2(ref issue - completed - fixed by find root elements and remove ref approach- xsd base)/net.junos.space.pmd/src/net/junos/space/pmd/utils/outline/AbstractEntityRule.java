package net.junos.space.pmd.utils.outline;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import net.sourceforge.pmd.RuleContext;
import net.sourceforge.pmd.lang.java.rule.JavaRuleViolation;



public class AbstractEntityRule extends AbstractOutlineAccessor {

	public static HashMap<String,EjbEntity> entityRegistry ;
	
	private static boolean isComplete ;
	
	public static final LinkedHashSet<String> fieldAndMethodMappingAnnotations =
			new LinkedHashSet<String>( Arrays.asList( new String[]{
		
		"Column",
		"JoinColumn",
		"JoinColumns",
		"Id",
		"GeneratedValue",
		"AttributeOverride",
		"AttributeOverrides",
		"AssociationOverride",
		"AssociationOverrides",
		"EmbeddedId",
		"Transient",
		"Version",
		"Basic",
		"Lob",
		"Temporal",
		"Enumerated",
		"OneToOne",
		"OneToMany",
		"ManyToOne",
		"ManyToMany",
		"JoinTable",
		"MapKey",
		"OrderBy",
		"PrimaryKeyJoinColumn",
		"PrimaryKeyJoinColumns",
		"Embedded",
		"SequenceGenerator",
		"TableGenerator"
	} ) ) ;
	
	protected void processEntities(RuleContext ctx) {}
	
	private static HashMap<String,ArrayList<Integer>> violationsMap = new HashMap<String, ArrayList<Integer>>() ; 
	
	public final void doStart(RuleContext ctx){
		entityRegistry = null ;
		isComplete = false ;
	}
	
	
	public final void doEnd(RuleContext ctx){
		
		if(!isComplete)
		{		
			isComplete = true ;
			entityRegistry = new HashMap<String, EjbEntity>() ;
			violationsMap.clear() ;
			
			for(IClassOutline outline : getOutlineContainer().getAllOutlines() ){
				IJavaTypeDeclaration decl = outline.getTypeDeclaration();
				String qName = outline.getQualifiedNameString() ;
				
				Set<String> targetAnnotations = getTargetAnnotations();
				List<IJavaAnnotation> annotations = decl.getAnnotations();
				if(annotations == null)
					continue ;
				
				for(IJavaAnnotation ann : annotations){
					String name = ann.getName();
					if( targetAnnotations.contains(name) ){
	
						EjbEntity en = new EjbEntity(outline) ;
						entityRegistry.put(qName, en) ;
						break ;
					}
				}
			}
		}
		processEntities( ctx ) ;
	}
	
	protected Set<String> getTargetAnnotations() {
		HashSet<String> set = new HashSet<String>();
		set.add("Entity") ;
		return set ;
	}
	
	protected boolean hasMappingAnnotation(IDeclaration fDecl) {
		
		List<IJavaAnnotation> annotations = fDecl.getAnnotations();
		if(annotations ==null)
			return false ;
		
		for(IJavaAnnotation ann:annotations){
			String name = ann.getName();
			if( AbstractEntityRule.fieldAndMethodMappingAnnotations.contains(name) )
				return true ;			
		}
		return false ;		
	}
	
	protected void reportError( RuleContext ctx, IDeclaration decl, String msg )
	{
		
		IDeclaration owner=null; 
		if(decl instanceof IJavaTypeDeclaration){
			owner=decl ;
		}
		else{
			owner=decl.getOwner();
		}		
		List<String> values = owner.getQualifiedName().getValues();
		if(values==null||values.isEmpty())
			return ;
		String qName = values.iterator().next() ;
		IClassOutline classOutline = getOutlineContainer().getClassOutline(qName);
		if(classOutline==null)
			return ;
		
		String path = classOutline.getFilePath();
		int beginLine = decl.getLines()[0];
		int endLine = decl.getLines()[1];
		
		String key=path+msg ;
		ArrayList<Integer> lineArray = violationsMap.get(key);
		if(lineArray==null){
			lineArray = new ArrayList<Integer>() ;
			violationsMap.put(key, lineArray) ;
		}
		if(lineArray.contains(beginLine))
			return ;
		
		lineArray.add(beginLine) ;
		
		ctx.setSourceCodeFilename( path ) ;		
		
		JavaRuleViolation violation
				= new JavaRuleViolation(this, ctx, null, msg, beginLine,endLine ) ;

		ctx.getReport().addRuleViolation(violation) ;
		ctx.setSourceCodeFilename( null ) ;
	}
	
}
