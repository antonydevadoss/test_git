package net.junos.space.pmd.rules.j2ee;

import java.util.List;

import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;
import net.junos.space.pmd.utils.outline.SpecialInspector;
import net.sourceforge.pmd.lang.java.ast.ASTArgumentList;
import net.sourceforge.pmd.lang.java.ast.ASTExpression;
import net.sourceforge.pmd.lang.java.ast.ASTPrimaryExpression;
import net.sourceforge.pmd.lang.java.ast.ASTPrimaryPrefix;
import net.sourceforge.pmd.lang.java.ast.ASTPrimarySuffix;
import net.sourceforge.pmd.lang.java.ast.ASTReturnStatement;

public class EnterpriseBeanPassThisReferenceRule extends SpecialInspector {

	
	
	@Override
	public String getMessage() {
		return "Do not pass an EJB's this reference as an argument or method result";
	}
	
	@Override
	public Object visit(ASTReturnStatement node, Object data) {

		ASTExpression expression = node.getFirstChildOfType(ASTExpression.class);
		if (expression != null) {
			ASTPrimaryExpression primaryExpression = expression.getFirstChildOfType(ASTPrimaryExpression.class);			
			if (primaryExpression != null) {
				ASTPrimaryPrefix prefix = primaryExpression.getFirstChildOfType(ASTPrimaryPrefix.class);
				List<ASTPrimarySuffix> suffixes = primaryExpression.findChildrenOfType(ASTPrimarySuffix.class);
				if (prefix != null && prefix.usesThisModifier() && (suffixes == null || suffixes.isEmpty())) {
					registerPossibleViolation(node);	
				}
			}
		}
		return super.visit(node, data);
	}
	


	@Override
	public Object visit(ASTArgumentList node, Object data) {

		List<ASTExpression> expressions = node.findChildrenOfType(ASTExpression.class);
		if (expressions != null && !expressions.isEmpty()) {
			for (ASTExpression expression : expressions) {
				ASTPrimaryExpression primaryExpression = expression.getFirstChildOfType(ASTPrimaryExpression.class);			
				if (primaryExpression != null) {
					ASTPrimaryPrefix prefix = primaryExpression.getFirstChildOfType(ASTPrimaryPrefix.class);
					List<ASTPrimarySuffix> suffixes = primaryExpression.findChildrenOfType(ASTPrimarySuffix.class);
					if (prefix != null && prefix.usesThisModifier() && (suffixes == null || suffixes.isEmpty())) {
						registerPossibleViolation(node);	
					}
				}
			}
		}
		return super.visit(node, data);
	}
	
	protected boolean mustBeChecked(IJavaTypeDeclaration decl) {
		return isEnterpriseBeanSuperclassEJB2(decl);
	}
	
	@Override
	protected String getOwnKey() {		
		return this.getClass().getCanonicalName() ;
	}
}
