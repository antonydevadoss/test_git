package net.junos.space.pmd.utils.reference;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IDeclaration;
import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;



public class ClassReferenceStorage<T> implements IClassReferenceStorage<T> {
	
	public ClassReferenceStorage( IClassOutline outline) {
		super();
		this.outline = outline ;
	}

	private IClassOutline outline ;
	
	private ArrayList<T> refList;
	
	private ArrayList<IDeclaration> methods ;
	
	private ArrayList<Integer> offsets ;
	
	
	/* (non-Javadoc)
	 * @see com.onpositive.pmd.reference.collector.IClassReferenceStorage#getQualifiedName()
	 */
	@Override
	public String getQualifiedName() {
		return outline.getQualifiedNameString() ;
	}

	/* (non-Javadoc)
	 * @see com.onpositive.pmd.reference.collector.IClassReferenceStorage#getClassReferences()
	 */
	@Override
	public List<T> getClassReferences(){
		return refList ;
	}
	
	/* (non-Javadoc)
	 * @see com.onpositive.pmd.reference.collector.IClassReferenceStorage#getMethodReferences(java.lang.String)
	 */
	@Override
	public List<T>getMethodReferences( IDeclaration method ){
		
		int size = methods.size();
		for( int i = 0 ; i < size ; i++ )
		{			
			if(!methods.get(i).equals(method))
				continue ;
			
			int start = offsets.get(i) ;
			int end = offsets.get(i+1) ;
			return refList.subList(start, end) ;
		}
		return null ;
	}
	
	/* (non-Javadoc)
	 * @see com.onpositive.pmd.reference.collector.IClassReferenceStorage#addMethodReferences(java.lang.String, java.util.Collection)
	 */
	@Override
	public void addMethodReferences(IDeclaration method, Collection<T> refs){
		
		if( refList == null ){
			refList = new ArrayList<T>() ;
			offsets = new ArrayList<Integer>() ;
			methods = new ArrayList<IDeclaration>() ;
			offsets.add(0) ;
		}
		
		refList.addAll(refs) ;
		offsets.add( refList.size() ) ;
		methods.add(method) ;
	}

	@Override
	public List<IDeclaration> getMethods() {
		return methods ;
	}

	@Override
	public IJavaTypeDeclaration getDeclaration() {
		return outline.getTypeDeclaration() ;
	}
	
}
