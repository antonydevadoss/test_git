/*******************************************************************************
 * FILE NAME: InvalidMethodRule.java
 * PURPOSE:  This is Rule-14 in document
 *
 *
 * Revision History: 
 * DATE:           				  AUTHOR:              	CHANGE:  
 * Tue Feb 22nd, 2011               Atul Bajpai       	Initial Version  
 * 
 * 
 ******************************************************************************/
package net.junos.space.pmd.rules.rest;

import java.util.List;import net.junos.space.pmd.utils.PMDConstants;import net.junos.space.pmd.utils.PMDUtils;import net.sourceforge.pmd.RuleContext;import net.sourceforge.pmd.lang.java.ast.ASTMethodDeclarator;import net.sourceforge.pmd.lang.java.rule.AbstractJavaRule;import org.apache.log4j.Logger;

/**
 * This class check whether or not REST method starts with 'add...' and 
 * Http method is not POST. Same could be defined for 'get...', 'delete...',
 * 'update...'
 */
public class InvalidMethodRule extends AbstractJavaRule {

	/**
	 * Logger
	 */
	private static Logger log = Logger.getLogger(InvalidMethodRule.class);

	/**
	 * Visits all the REST methods of class and checks the consistency of 
	 * method name and Http Media Type.
	 */
	public Object visit(ASTMethodDeclarator methodNode, Object data) {
		RuleContext ctx = (RuleContext) data;
		String methodName = methodNode.getImage();
		try {
			boolean isValidHttpMethod = false;
			String httpMethod = null;
			boolean isRestMethod = false;
			PMDConstants.Method method = null;

			// checks whether the method is REST or not.
			isRestMethod = PMDUtils.isRestMethod(methodNode);
			if (isRestMethod) {
				// Returns httpMethod Annotation name
				List<String> lstHTTPAnnotation = PMDUtils.getHttpMethods(methodNode);
				method = PMDConstants.Method.getMethod(methodName);
				if (method != null) {
					httpMethod = lstHTTPAnnotation.get(0);
					isValidHttpMethod = method.isValidHttpMethod(httpMethod);
				}
			}
			
			if(log.isDebugEnabled()){
				StringBuilder builder = new StringBuilder();
				builder.append("source file=").append(ctx.getSourceCodeFilename()).append(",method=").append(methodName);
				builder.append(",isRestMethod=").append(isRestMethod).append(",isValidHttpMethod=").append(isValidHttpMethod).append(
						",method=").append(method);
				log.debug(builder.toString());
			}
			
			if (isRestMethod && !isValidHttpMethod && method != null)
				addViolation(data, methodNode);
		} catch (Exception e) {
			log.error("Exception in executing method " + methodName + " of " + ctx.getSourceCodeFilename(), e);
		}
		return super.visit(methodNode, data);
	}

}
