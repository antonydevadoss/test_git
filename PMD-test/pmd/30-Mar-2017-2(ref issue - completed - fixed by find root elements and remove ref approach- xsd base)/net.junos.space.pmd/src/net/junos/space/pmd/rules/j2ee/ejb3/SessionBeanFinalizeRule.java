package net.junos.space.pmd.rules.j2ee.ejb3;

import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;

public class SessionBeanFinalizeRule extends net.junos.space.pmd.rules.j2ee.SessionBeanFinalizeRule {

	@Override
	protected boolean mustBeChecked(IJavaTypeDeclaration typeDeclaration) {
		return isEnterpriseBeanEJB3(typeDeclaration);
	}
}
