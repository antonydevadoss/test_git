package net.junos.space.pmd.rules.j2ee.ejb3;

public class DontUseJavaIOFileApiRule extends net.junos.space.pmd.rules.j2ee.DontUseJavaIOFileApiRule {
	
	protected boolean mustBeChecked(String qualifiedName) {
		return isEnterpriseBeanSuperclassEJB3(qualifiedName);
	}
}
