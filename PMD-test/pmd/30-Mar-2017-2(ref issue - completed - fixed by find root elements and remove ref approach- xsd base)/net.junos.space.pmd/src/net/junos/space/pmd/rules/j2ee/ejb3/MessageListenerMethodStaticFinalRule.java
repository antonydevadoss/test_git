package net.junos.space.pmd.rules.j2ee.ejb3;

import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IJavaMethodDeclaration;
import net.sourceforge.pmd.RuleContext;

public class MessageListenerMethodStaticFinalRule extends AbstractMessageListenerMethodRule {
	
	public String getMessage(){
		return "The message listener methods of the message-driven bean must not be declared as final or static." ;
	}
	
	protected void checkMethod(IClassOutline outline, IJavaMethodDeclaration md, RuleContext ctx) {
		
		if( md.isStatic() )
			reportError(ctx, outline, md) ;
		
		if( md.isFinal() )
			reportError(ctx, outline, md) ;		
	}

}
