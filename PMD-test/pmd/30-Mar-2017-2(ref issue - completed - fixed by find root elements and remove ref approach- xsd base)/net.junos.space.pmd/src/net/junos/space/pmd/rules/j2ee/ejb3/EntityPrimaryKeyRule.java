package net.junos.space.pmd.rules.j2ee.ejb3;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import net.junos.space.pmd.utils.outline.AbstractEntityRule;
import net.junos.space.pmd.utils.outline.EjbEntity;
import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IJavaAnnotation;
import net.junos.space.pmd.utils.outline.IJavaFieldDeclaration;
import net.junos.space.pmd.utils.outline.IJavaMethodDeclaration;
import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;
import net.sourceforge.pmd.RuleContext;

public class EntityPrimaryKeyRule extends AbstractEntityRule {
	private static final String message = "Every entity must have a primary key.";
	@Override
	protected void processEntities(RuleContext ctx) {
		if (AbstractEntityRule.entityRegistry == null) {
			return;
		}
		
		for (EjbEntity ejbEntity : AbstractEntityRule.entityRegistry.values()) {
			IClassOutline classOutline = ejbEntity.getOutline() ;
			if(!classOutline.hasAllSuperclassesDefined(getOutlineContainer()))
				continue ;
			
			IJavaTypeDeclaration typeDeclaration = classOutline.getTypeDeclaration();
			
			boolean isEmbeddable = false;
			List<IJavaAnnotation> entityAnnotations = typeDeclaration.getAnnotations();
			for (IJavaAnnotation annotation : entityAnnotations) {
				if (annotation.getName().equals("Embeddable")) {
					isEmbeddable = true;
					break;
				}
			}
			if (isEmbeddable) {
				continue;
			}
			
			List<IJavaFieldDeclaration> fields = ejbEntity.getPersistentFields();
			List<IJavaMethodDeclaration> methods = classOutline.getMethods();
			boolean hasPK = false;
			if (ejbEntity.hasFieldAccess()) {
				for (IJavaFieldDeclaration field : fields) {
					
					List<IJavaAnnotation> annotations = field.getAnnotations();
					if (annotations != null) {
						for (IJavaAnnotation annotation : annotations) {
							if (annotation.getName().equals("Id")
									|| annotation.getName().equals("EmbeddedId")) {
								hasPK = true;
								break;
							}
						}
						if (hasPK) {
							break;
						}
					}
				}
			} else {
				for (IJavaMethodDeclaration method : methods) {
					
					List<IJavaAnnotation> annotations = method.getAnnotations();
					if (annotations != null) {
						for (IJavaAnnotation annotation : annotations) {
							if (annotation.getName().equals("Id")
									|| annotation.getName().equals("EmbeddedId")) {
								hasPK = true;
								break;
							}
						}
						if (hasPK) {
							break;
						}
					}
				}
			}
			
			if (!hasPK) {
				reportError(ctx,typeDeclaration,message) ;
			}
		}
		super.processEntities(ctx);
	}
	
	protected Set<String> getTargetAnnotations() {
		HashSet<String> set = new HashSet<String>();
		set.add("Entity") ;
		
		return set ;
	}
}
