package net.junos.space.pmd.utils.reference;

public interface IReference {
	
	int getBeginLine() ;
	
	int getEndLine() ;

}
