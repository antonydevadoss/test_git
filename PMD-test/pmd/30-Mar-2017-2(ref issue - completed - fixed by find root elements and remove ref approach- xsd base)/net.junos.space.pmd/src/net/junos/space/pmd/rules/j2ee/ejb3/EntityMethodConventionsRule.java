package net.junos.space.pmd.rules.j2ee.ejb3;

import java.util.ArrayList;
import java.util.List;

import net.junos.space.pmd.utils.outline.AbstractEntityRule;
import net.junos.space.pmd.utils.outline.EjbEntity;
import net.junos.space.pmd.utils.outline.IJavaFieldDeclaration;
import net.junos.space.pmd.utils.outline.IJavaMethodDeclaration;
import net.junos.space.pmd.utils.outline.IJavaType;
import net.junos.space.pmd.utils.outline.QualifiedName;
import net.sourceforge.pmd.RuleContext;

public class EntityMethodConventionsRule extends AbstractEntityRule{
	
	
	private static final String message = 
			"It is required that the entity or embeddedable class follow the method conventions for a JavaBean when persistent properties are used."
			+ "In this case, for every persistent property \"property\" of type T of the entity, there are getter and setter methods: "
			+ "T getProperty() and setProperty(T t)" ;
	
	protected void processEntities(RuleContext ctx) {
		
		if (AbstractEntityRule.entityRegistry == null) {
			return;
		}
		for (EjbEntity ejbEntity : AbstractEntityRule.entityRegistry.values()) {
			
			if(!ejbEntity.hasPropertyAccess())
				continue ;

			ArrayList<IJavaFieldDeclaration> persistentFields = ejbEntity.getNonTransientFields() ;
			if(persistentFields==null)
				continue ;
			
			for( IJavaFieldDeclaration fDecl : persistentFields ){

				if(hasMappingAnnotation(fDecl))
					continue ;

				if( !gotGetterAndSetter( fDecl, ejbEntity ) ){
					reportError(ctx,fDecl, message);
				}
			}
		}	
	}
	
	private boolean gotGetterAndSetter(IJavaFieldDeclaration fDecl, EjbEntity ejbEntity)
	{
		String name = fDecl.getSimpleName() ;
		String _name = Character.toUpperCase(name.charAt(0)) + name.substring(1) ;
		
		boolean gotSetter = false ;
		boolean gotGetter = false ;
		
		String getterName = "get" + _name ;
		String setterName = "set" + _name ;
		String boolGetterName = "is" + _name ;
		
		ArrayList<IJavaMethodDeclaration> gettersList
			= ejbEntity.getNonTransientPropertiesMap().get(getterName);
		
		if(gettersList != null){
			for( IJavaMethodDeclaration md : gettersList ){
				if(isGetter(fDecl,md)){
					gotGetter = true ;
					break ;
				}
			}
		}
		if(!gotGetter && isBoolean(fDecl)){
			
			ArrayList<IJavaMethodDeclaration> bGettersList
				= ejbEntity.getNonTransientPropertiesMap().get(boolGetterName);
		
			if(bGettersList != null){
				for( IJavaMethodDeclaration md : bGettersList ){
					if(isBooleanGetter(md)){
						gotGetter = true ;
						break ;
					}
				}
			}
		}
		if(!gotGetter)
			return false ;
			
		ArrayList<IJavaMethodDeclaration> settersList
			= ejbEntity.getNonTransientPropertiesMap().get(setterName);
	
		if(settersList != null){
			for( IJavaMethodDeclaration md : settersList ){
				if(isSetter(fDecl,md)){
					gotSetter = true ;
					break ;
				}
			}
		}	
		
		return gotGetter && gotSetter ;
	}
	
	
	private boolean isSetter(IJavaFieldDeclaration fDecl,IJavaMethodDeclaration md)
	{
		md.getReturnType() ;
		if(md.getReturnType() != null)
			return false ;
		
		List<IJavaType> params = md.getParameters();
		if(params == null || params.size() != 1)
			return false ;
		
		IJavaType fieldType = fDecl.getType();
		IJavaType argType = params.get(0);
		
		return argType.equals(fieldType);
	}

	private boolean isBooleanGetter(IJavaMethodDeclaration md) {
		
		List<IJavaType> params = md.getParameters();
		if(params !=null && !params.isEmpty())
			return false ;
		
		IJavaType returnType = md.getReturnType();
		return isBooleanType(returnType) ;
	}

	private boolean isBoolean(IJavaFieldDeclaration fDecl) {
		IJavaType type = fDecl.getType();
		if(type == null)
			return false ;

		return isBooleanType(type) ;
	}

	protected boolean isBooleanType(IJavaType type) {
		if(type == null)
			return false ;
		
		QualifiedName typeName = type.getQualifiedName();
		boolean isBooleanType = typeName.equals("boolean") || typeName.equals("java.lang.Boolean");
		return isBooleanType;
	}

	private boolean isGetter(IJavaFieldDeclaration fDecl, IJavaMethodDeclaration md)
	{
		List<IJavaType> params = md.getParameters();
		if(params !=null && !params.isEmpty())
			return false ;
		
		IJavaType returnType = md.getReturnType();
		if(returnType == null)
			return false ;
		
		IJavaType fieldType = fDecl.getType();
		return returnType.equals(fieldType);
	}

}
