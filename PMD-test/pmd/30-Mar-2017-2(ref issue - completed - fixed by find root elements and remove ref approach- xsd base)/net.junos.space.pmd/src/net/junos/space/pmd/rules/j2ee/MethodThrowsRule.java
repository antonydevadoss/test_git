package net.junos.space.pmd.rules.j2ee;

import java.util.List;

import net.junos.space.pmd.utils.outline.AbstractOutlineAccessor;
import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IJavaMethodDeclaration;
import net.junos.space.pmd.utils.outline.IJavaType;
import net.junos.space.pmd.utils.outline.QualifiedName;
import net.sourceforge.pmd.RuleContext;

public class MethodThrowsRule extends AbstractOutlineAccessor {
	/**
	 * JAC_069
	 */
	@Override
	public void doEnd(RuleContext ctx) {
		for (IClassOutline outline : getOutlineContainer().getAllOutlines()) {
			for (IJavaMethodDeclaration method : outline.getMethods()) {
				List<IJavaType> exceptions = method.getExceptions();
				for( IJavaType ex : exceptions ){
					
					QualifiedName qName = ex.getQualifiedName();
					if(qName==null){
						String sName = ex.getSimpleName();
						if("Exception".equals(sName) || "Throwable".equals(sName))
						{
							reportError(ctx, outline, method);
							break;
						}
					}
					else {
						List<String> values = qName.getValues();
						if (!values.contains("java.lang.Exception") && !values.contains("java.lang.Throwable")) {
							reportError(ctx, outline, method);
							break ;
						}						
					}
				}
			}
		}
		super.doEnd(ctx);
	}
	
	@Override
	public String getMessage() {
		return "Do not declare a method that throws " +
				"java.lang.Exception " +
				"or " +
				"java.lang.Throwable";
	}
}
