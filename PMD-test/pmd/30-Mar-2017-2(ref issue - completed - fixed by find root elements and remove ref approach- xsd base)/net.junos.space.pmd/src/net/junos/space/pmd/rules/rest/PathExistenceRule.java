/*******************************************************************************
 * FILE NAME: PathExistenceRule.java
 * PURPOSE:  This is Rule-2 in document
 *
 *
 * Revision History: 
 * DATE:           				  AUTHOR:              	CHANGE:  
 * Tue Feb 09, 2011               Atul Bajpai       	Initial Version  
 * 
 * 
 ******************************************************************************/
package net.junos.space.pmd.rules.rest;

import net.junos.space.pmd.utils.PMDConstants;import net.junos.space.pmd.utils.PMDUtils;import net.sourceforge.pmd.RuleContext;import net.sourceforge.pmd.lang.java.ast.ASTMethodDeclarator;import net.sourceforge.pmd.lang.java.rule.AbstractJavaRule;import org.apache.log4j.Logger;

/**
 * This class checks whether the @Path Annotation is present in 
 * Class or Interface level while the class consists REST method.
 */
public class PathExistenceRule extends AbstractJavaRule {

	/**
	 * Logger
	 */
	private static Logger log = Logger.getLogger(PathExistenceRule.class);

	/**
	 * This method visits every method in Class and checks whether it 
	 * is REST method or not and if it is REST find out whether @Path annotation
	 * exists on class/interface or not.
	 */
	public Object visit(ASTMethodDeclarator methodNode, Object data) {
		RuleContext ctx = (RuleContext) data;
		String methodName = methodNode.getImage();
		try {
			boolean isPathPresent = false;
			boolean isRest = false;

			// checks whether the method is REST or not.
			isRest = PMDUtils.isRestMethod(methodNode);
			if (isRest) {
				isPathPresent = PMDUtils.isClassAnnotationExists(methodNode, PMDConstants.PATH_ANNOTATION);
			}
			
			if(log.isDebugEnabled()){
				StringBuilder builder = new StringBuilder();
				builder.append("source file=").append(ctx.getSourceCodeFilename()).append(",method=").append(methodName);
				builder.append(",isRest=").append(isRest).append(",isPathPresent=").append(isPathPresent);
				log.debug(builder.toString());
			}
			
			if (isRest && !isPathPresent)
				addViolation(data, methodNode);
		} catch (Exception e) {
			log.error("Exception in executing method " + methodName + " of " + ctx.getSourceCodeFilename(), e);
		}
		return super.visit(methodNode, data);

	}
}
