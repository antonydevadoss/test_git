package net.junos.space.pmd.utils.outline;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;



public interface IOutlinePrinter {
	
	void writeDump(IOutlineContainer oc, String path) ;
	
	void print( IOutlineContainer container, OutputStream os, String indent, String indentIncrement)
			throws UnsupportedEncodingException, IOException;
	
	void print( IClassOutline outline, OutputStream os, String indent, String indentIncrement)
			throws UnsupportedEncodingException, IOException;
	
	void print( IJavaType type, OutputStream os, String indent, String indentIncrement)
			throws UnsupportedEncodingException, IOException;
	
	void print( IJavaFieldDeclaration field, OutputStream os, String indent, String indentIncrement)
			throws UnsupportedEncodingException, IOException;
	
	void print( IJavaMethodDeclaration method, OutputStream os, String indent, String indentIncrement)
			throws UnsupportedEncodingException, IOException;
	
	void print( IJavaTypeDeclaration typeDeclaration, OutputStream os, String indent, String indentIncrement)
			throws UnsupportedEncodingException, IOException;

}
