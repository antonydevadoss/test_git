package net.junos.space.pmd.rules.j2ee.ejb3;

public class DoNotAttemptToListenSocketRule extends
			net.junos.space.pmd.rules.j2ee.DoNotAttemptToListenSocketRule {

	protected boolean mustBeChecked(String className) {		
		return isEnterpriseBeanSuperclassEJB3(className);
	}
}
