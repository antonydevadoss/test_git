package net.junos.space.pmd;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.junos.space.pmd.rules.rest.RestInfo;

public class PMDContext {
	
	private static PMDContext context = new PMDContext();
	
	public static PMDContext getInstance() {
		return context;
	}
	
	private Map<String, String> contextRootMap = new HashMap<String, String>();
	private List<RestInfo> reportList = new ArrayList<RestInfo>();
	private Map<String, PmdData> classMap = new HashMap<String, PmdData>(1500);	
	private PMDContext(){
		
	}

	public Map<String, String> getContextRootMap() {
		return contextRootMap;
	}

	public void setContextRootMap(Map<String, String> contextRootMap) {
		this.contextRootMap = contextRootMap;
	}

	public List<RestInfo> getReportList() {
		return reportList;
	}

	public void setReportList(List<RestInfo> reportList) {
		this.reportList = reportList;
	}
	
	public synchronized void addToReportList(RestInfo info) {
		this.reportList.add(info);
	}

	public Map<String, PmdData> getClassMap() {
		return classMap;
	}

	public void setClassMap(Map<String, PmdData> classMap) {
		this.classMap = classMap;
	}
	
	public synchronized PmdData putInClassMap(String className, PmdData data) {
		return this.classMap.put(className, data);
	}
	
}
