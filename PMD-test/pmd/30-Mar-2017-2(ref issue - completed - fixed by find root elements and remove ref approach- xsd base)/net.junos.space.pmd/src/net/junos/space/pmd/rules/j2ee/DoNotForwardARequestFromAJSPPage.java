package net.junos.space.pmd.rules.j2ee;

import net.junos.space.pmd.rules.xml.LineByLineJSPValidator;
import net.sourceforge.pmd.RuleContext;
import net.sourceforge.pmd.lang.java.rule.JavaRuleViolation;

public class DoNotForwardARequestFromAJSPPage extends LineByLineJSPValidator {

	private static final String message = "Do Not Forward A Request From A JSP Page";
	
	protected void validateLine(String line, int lineNumber, String path, RuleContext ctx) {
		
		if(line.contains("<jsp:forward page")){
			
			String backup = ctx.getSourceCodeFilename();
			ctx.setSourceCodeFilename(path) ;
			JavaRuleViolation violation
				= new JavaRuleViolation(this, ctx, null, message, lineNumber, lineNumber );			
			ctx.getReport().addRuleViolation(violation) ;
			ctx.setSourceCodeFilename( backup ) ;
		}
	}
}
