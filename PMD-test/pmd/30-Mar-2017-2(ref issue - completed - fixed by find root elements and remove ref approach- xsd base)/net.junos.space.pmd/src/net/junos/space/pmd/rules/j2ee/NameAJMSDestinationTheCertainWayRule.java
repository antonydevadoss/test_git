package net.junos.space.pmd.rules.j2ee;


import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import net.junos.space.pmd.rules.xml.AbstractXMLRule;
import net.junos.space.pmd.rules.xml.SimpleXpathRule;

public class NameAJMSDestinationTheCertainWayRule extends SimpleXpathRule {
	
	

	@Override
	protected String getXpathExpression() {
		return "//resource-env-ref/resource-env-ref-name";
	}

	public String getMessage() {
		return "Name a JMS destination like [Name]Queue or [Name]Topic. JMS destinations must either have a suffix Queue or Topic depending on their jms type.";
	}

	protected String getTargetXMLName() {
		return AbstractXMLRule.XML_NAME_EJB_JAR;
	}

	@Override
	protected boolean isValid(Element element) {
		
		Node parentNode = element.getParentNode();
		if(!(parentNode instanceof Element))
			return true ;
		
		Element parent = (Element) parentNode ;
		
		NodeList childNodes = parent.getChildNodes();
		if(childNodes == null)
			return true ;
		
		Element nameElement = element ;
		Element typeElement = null ;
		
		int length = childNodes.getLength();
		for( int i = 0 ; i < length ; i++ ){
			Node node = childNodes.item(i);
			if(!(node instanceof Element))
				continue ;
			
			Element el = (Element) node ;
			String tagName = el.getTagName() ;
			if( tagName.equals("resource-env-ref-type") ){
				typeElement = el ;
			}
		}
		
		String nameString = nameElement.getTextContent() ;		
		String shortType = null ;
		while(true){

			if( typeElement == null )
				break ;

			String typeString = typeElement.getTextContent() ;
			if(typeString == null)
				break ;

			String typeString_lc = typeString.toLowerCase() ;
			if(typeString_lc.endsWith("queue")){
				shortType = "Queue" ;
			}
			else if(typeString_lc.endsWith("topic")){
				shortType = "Topic" ;
			}
			break ;
		}
		
		if( shortType == null ){
			return ( nameString.endsWith("Queue") || nameString.endsWith("Topic") ) ; 
		}
		else{
			return nameString.endsWith( shortType ) ;
		}
	}
}
