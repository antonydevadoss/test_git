package net.junos.space.pmd.rules.j2ee;

import java.util.List;

import net.junos.space.pmd.utils.outline.AbstractOutlineAccessor;
import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;
import net.junos.space.pmd.utils.outline.QualifiedName;
import net.sourceforge.pmd.RuleContext;

public class MessageBeanImplementationImplementRule extends AbstractOutlineAccessor {
	
	@Override
	public String getMessage() {
		return "Make a Message Bean implementation implement javax.jms.MessageListener";
	}
	
	@Override
	public void doEnd(RuleContext ctx) {
		for( IClassOutline outline : getOutlineContainer().getAllOutlines() ){		
			
			if(!outline.hasAllSuperclassesDefined(getOutlineContainer()))
				continue ;
			
			if(!isMessageDrivenBeanEJB2(outline.getTypeDeclaration()))
				continue ;
			
			IJavaTypeDeclaration typeDeclaration = outline.getTypeDeclaration();
			
			List<IJavaTypeDeclaration> undefinedInterfaces
					= typeDeclaration.getUndefinedInterfaces( getOutlineContainer() );
			
			boolean gotExtraInterfaces = false ;
l0:			for(IJavaTypeDeclaration iDecl : undefinedInterfaces){
				for(String str : iDecl.getQualifiedName().getValues()){
					if(str.equals("javax.ejb.MessageDrivenBean"))
						continue l0;
				}
				gotExtraInterfaces=true ;
				break ;
			}			
			if(gotExtraInterfaces)
				continue ;
			
			List<IJavaTypeDeclaration> undefinedSuperClasses
					= typeDeclaration.getUndefinedSuperClasses( getOutlineContainer() );
	
			if(!undefinedSuperClasses.isEmpty())
				continue ;
			
			
			boolean isImplementsMessageListener = false;
			for (IJavaTypeDeclaration interfaceDeclaration : typeDeclaration.getAllInterfaces()) {
				QualifiedName qName = interfaceDeclaration.getQualifiedName();
				for (String qNameStr : qName.getValues()) {
						
					if (qNameStr.equals("javax.jms.MessageListener")){
						isImplementsMessageListener = true;
						break ;
					}
				}
			}
				
			if (!isImplementsMessageListener){
				reportError(ctx, outline, typeDeclaration) ;
			}
		}
		super.doEnd(ctx);
	}
}
