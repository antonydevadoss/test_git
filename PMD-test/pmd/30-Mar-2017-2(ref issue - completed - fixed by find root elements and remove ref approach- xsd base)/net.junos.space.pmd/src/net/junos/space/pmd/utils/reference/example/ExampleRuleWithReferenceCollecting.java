package net.junos.space.pmd.utils.reference.example;

import java.util.List;

import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IDeclaration;
import net.junos.space.pmd.utils.outline.IJavaMethodDeclaration;
import net.junos.space.pmd.utils.reference.AbstractReferenceAccessor;
import net.junos.space.pmd.utils.reference.IClassReferenceStorage;
import net.junos.space.pmd.utils.reference.SimpleClassReference;
import net.sourceforge.pmd.RuleContext;

public class ExampleRuleWithReferenceCollecting extends AbstractReferenceAccessor {

	
	public void doEnd(RuleContext ctx){
		
		for( IClassReferenceStorage<SimpleClassReference> cs : getReferenceContainer().getAllResolvedClassStorages() ){
			String qualifiedName = cs.getQualifiedName();
			cs.getClassReferences() ;
			
			for(IDeclaration method : cs.getMethods()){
				List<SimpleClassReference> methodReferences = cs.getMethodReferences(method);
			}
			
			IClassOutline classOutline = getOutlineContainer().getClassOutline(qualifiedName);
			String filePath = classOutline.getFilePath();
		}		
	}
}
