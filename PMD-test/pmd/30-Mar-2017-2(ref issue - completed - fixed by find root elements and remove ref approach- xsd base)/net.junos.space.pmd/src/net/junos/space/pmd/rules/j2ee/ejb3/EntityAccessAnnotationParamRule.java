package net.junos.space.pmd.rules.j2ee.ejb3;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import net.junos.space.pmd.utils.outline.AbstractEntityRule;
import net.junos.space.pmd.utils.outline.EjbEntity;
import net.junos.space.pmd.utils.outline.IDeclaration;
import net.junos.space.pmd.utils.outline.IJavaFieldDeclaration;
import net.junos.space.pmd.utils.outline.IJavaMethodDeclaration;
import net.sourceforge.pmd.RuleContext;

public class EntityAccessAnnotationParamRule extends AbstractEntityRule {
	private static final String message = "Object/relational annotations placement should correspond the selected access type.";
	@Override
	protected void processEntities(RuleContext ctx) {
		if (AbstractEntityRule.entityRegistry == null) {
			return;
		}
		for (EjbEntity ejbEntity : AbstractEntityRule.entityRegistry.values()) {
			
			List<? extends IDeclaration> invalidMembers = null ;
			
			if( ejbEntity.hasFieldAccess() ){
				List<IJavaMethodDeclaration> mappingMethods = ejbEntity.getMappingMethods();
				if( !mappingMethods.isEmpty() )
					invalidMembers = mappingMethods ;				
			}
			else if( ejbEntity.hasPropertyAccess() ){
				List<IJavaFieldDeclaration> mappingFields = ejbEntity.getMappingFields();
				if( !mappingFields.isEmpty() )
					invalidMembers = mappingFields ;				
			}
			
			if(invalidMembers==null)
				continue ;
			
			for(IDeclaration decl : invalidMembers){			
				reportError(ctx,decl,message) ;
			}
		}
		super.processEntities(ctx);
	}
	
	protected Set<String> getTargetAnnotations() {
		HashSet<String> set = new HashSet<String>();
		set.add("Entity") ;
		set.add("Embeddable") ;
		return set ;
	}
}
