package net.junos.space.pmd.utils.reference;

public class NullTypeImpl extends JavaTypeReducedImpl {

	public NullTypeImpl() {
		super("NULL");
	}

}
