package net.junos.space.pmd.rules;

import java.util.Iterator;

import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IDeclaration;
import net.sourceforge.pmd.RuleContext;
import net.sourceforge.pmd.RuleViolation;
import net.sourceforge.pmd.lang.java.rule.AbstractJavaRule;
import net.sourceforge.pmd.lang.java.rule.JavaRuleViolation;

public class AbstractJavaRuleWithUselessTypeResolution extends AbstractJavaRule {

	
	@Override
	public boolean usesTypeResolution() {
		return false;
	}
	
	protected void reportError( RuleContext ctx, IClassOutline outline, IDeclaration decl )
	{
		int beginLine = decl.getLines()[0];
		int endLine = decl.getLines()[1];
		
		reportError(ctx, outline, beginLine, endLine) ;
	}
	
	protected void reportError( RuleContext ctx, IClassOutline outline, int beginLine, int endLine )
	{
		String path = outline.getFilePath();
		for( Iterator<RuleViolation> iter = ctx.getReport().iterator(); iter.hasNext() ; ){
			
			RuleViolation rv = iter.next() ;
			String violationPath = rv.getFilename() ;
			if(!path.equals(violationPath))
				continue ;
			
			int line = rv.getBeginLine();
			if(line == beginLine)
				return ;
		}
		
					
		ctx.setSourceCodeFilename( path ) ;		
		
		JavaRuleViolation violation
				= new JavaRuleViolation(this, ctx, null, getMessage(), beginLine,endLine ) ;

		ctx.getReport().addRuleViolation(violation) ;
		ctx.setSourceCodeFilename( null ) ;
	}
}
