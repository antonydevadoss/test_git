package net.junos.space.pmd.utils.outline;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;



public class DefaultOutlinePrinter implements IOutlinePrinter {

	String lineSeparator = System.getProperty("line.separator");
	
	@Override
	public void print(IOutlineContainer container, OutputStream os,
			String indent, String indentIncrement)
			throws UnsupportedEncodingException, IOException {

		List<IClassOutline> allOutlines = container.getAllOutlines();
		int count = allOutlines.size() ;
		
		os.write( "=========== Outlines =======".getBytes("UTF-8") ) ;
		os.write( (lineSeparator+lineSeparator).getBytes("UTF-8") ) ;
		
		for( IClassOutline ol : allOutlines ){			
			this.print(ol,os, indent, indentIncrement ) ;
				
			if( count-- > 0 ){
				os.write( indent.getBytes("UTF-8")) ;
				os.write( "==================".getBytes("UTF-8") ) ;
				os.write( (lineSeparator).getBytes("UTF-8") ) ;
				
			}
		}
		
		os.write( "=========== Type Declarations =======".getBytes("UTF-8") ) ;
		os.write( (lineSeparator+lineSeparator).getBytes("UTF-8") ) ;
		
		for( IJavaTypeDeclaration td : container.getAllTypeDeclarations() ){
			print(td, os, indent, indentIncrement) ;
			os.write( (lineSeparator ).getBytes("UTF-8") ) ;
		}
	}

	@Override
	public void print(IClassOutline outline, OutputStream os, String indent,
			String indentIncrement) throws UnsupportedEncodingException, IOException
	{
		IJavaTypeDeclaration type = outline.getTypeDeclaration() ;
		
		
		List<IJavaAnnotation> annotations = outline.getTypeDeclaration().getAnnotations();
		if(annotations != null){
			for( IJavaAnnotation annotation : annotations ){
				print(annotation,os,indent,indentIncrement) ;
			}
		}
		
		StringWriter sw = new StringWriter() ;
		StringBuilder modifiers = new StringBuilder() ;
		if( type.isFinal() )
			modifiers.append("final ") ;
		
		if( type.isStatic() )			
			modifiers.append("static ") ;
		
		if( type.isAbstract() )
			modifiers.append("abstract ") ;
		
		if( type.isPublic() )
			modifiers.append("public ") ;
		else if( type.isPrivate() )
			modifiers.append("private ") ;
		else if( type.isProtected() )
			modifiers.append("protected ") ;
		
		
		sw.append( indent ) ;
		QualifiedName qualifiedName = type.getQualifiedName();
		for(String name : qualifiedName.getValues())
			sw.append(name).append(" ") ;
		
		sw.append(lineSeparator) ;
		
		sw.append( indent ) ;
		sw.append( modifiers.toString() ) ;
		
		if(type.isInterface())
			sw.append("interface ") ;
		else if(type.isEnum())
			sw.append("enum ") ;
		else
			sw.append("calss ") ;
		
		sw.append( type.getSimpleName() ) ;
		
		int genericParamsCount = type.getGenericParamsCount();
		if( genericParamsCount > 0 ){
			sw.append("<") ;
			List<String> genericParamNames = type.getGenericParamNames();
			for(int i = 0 ; i < genericParamsCount ; i++){
				sw.append( genericParamNames.get(i) ) ;
				if(i != genericParamsCount-1)
					sw.append(", ") ;
			}
			sw.append(">") ;
		}
		
		sw.append( lineSeparator ).append(lineSeparator) ;
		
		String str = sw.toString() ;
		os.write( str.getBytes("UTF-8")) ;
		
		String indent1 = indent + indentIncrement ;
		
		for( IJavaMethodDeclaration md : outline.getConstructors() )
			this.print(md,os, indent1, indentIncrement) ;
		
		os.write( lineSeparator.getBytes("UTF-8") ) ;		
		
		for( IJavaFieldDeclaration fd : outline.getFields() )
			this.print(fd,os, indent1, indentIncrement) ;
		
		os.write( lineSeparator.getBytes("UTF-8") ) ;
		
		for( IJavaMethodDeclaration md : outline.getMethods() )
			this.print(md,os, indent1, indentIncrement) ;
		
		os.write( lineSeparator.getBytes("UTF-8") ) ;		
	}

	private void print(IJavaAnnotation annotation, OutputStream os, String indent, String indentIncrement)
			throws UnsupportedEncodingException, IOException
	
	{
		StringWriter sw = new StringWriter() ;
		sw.append(indent) ;
		sw.append("@") ;
		sw.append(annotation.getName()) ;
		
		ArrayList<String> keys = new ArrayList<String>(annotation.getKeys());
		int size = keys.size();
		if( size != 0 ){
			sw.append("(") ;
			if( size == 1 && keys.contains("value")){				
				sw.append( annotation.getMemberValue("value").getStringValue() ) ;
				
			}
			else{
				for( int i = 0 ; i < size ; i++ ){
					String key = keys.get(i) ;
					sw.append( key ) ;
					sw.append( "=" ) ;					
					sw.append( annotation.getMemberValue(key).getStringValue() ) ;					
					if( i < size-1 )
						sw.append(", ") ;
				}
			}
			sw.append(")") ;
		}
		sw.append(lineSeparator) ;
		
		String result = sw.toString();
		os.write(result.getBytes("UTF-8")) ;
	}

	@Override
	public void print(IJavaType type, OutputStream os, String indent,
			String indentIncrement) throws UnsupportedEncodingException,
			IOException {
		
		ByteArrayOutputStream bos = new ByteArrayOutputStream() ;
		bos.write( indent.getBytes("UTF-8") ) ;
		
		QualifiedName qualifiedName = type.getQualifiedName();
		if( qualifiedName != null ){
			List<String> qNameValues = qualifiedName.getValues();
			if( qNameValues == null || qNameValues.isEmpty() ){
				bos.write( (qualifiedName.getSimpleName() + " (no values)").getBytes("UTF-8") ) ;
			}
			else{
				int size = qNameValues.size();
				for( int i = 0 ; i < size ; i++ ){
					String name = qNameValues.get(i) ;
					bos.write( name.getBytes("UTF-8") ) ;
					if(i<size-1)
						bos.write( ", ".getBytes("UTF-8") ) ;
				}
			}
		}
		else{
			bos.write( (type.getSimpleName() + " (no qualified name)").getBytes("UTF-8") ) ;
		}
		
		List<IJavaType> gParams = type.getGenericParameters();
		if(gParams!=null && !gParams.isEmpty() ){
			bos.write("<".getBytes("UTF-8")) ;
			
			int size = gParams.size();
			for( int i = 0 ; i < size ; i++ ){
				IJavaType param = gParams.get(i) ;
				this.print(param, bos, "", "" ) ;
				if(i!= size-1)
					bos.write(", ".getBytes("UTF-8")) ;					
			}
			
			bos.write(">".getBytes("UTF-8")) ;
		}
		if( type.isArray() ){
			int count = type.getArrayDepth() ;
			while( count-- > 0 ){				
				bos.write("[]".getBytes("UTF-8")) ;
			}
		}
		os.write( bos.toByteArray() ) ;
	}

	@Override
	public void print(IJavaFieldDeclaration field, OutputStream os,
			String indent, String indentIncrement)
			throws UnsupportedEncodingException, IOException {
		
		List<IJavaAnnotation> annotations = field.getAnnotations();
		if(annotations != null){
			for( IJavaAnnotation annotation : annotations ){
				print(annotation,os,indent,indentIncrement) ;
			}
		}
		
		StringWriter sw = new StringWriter() ;		
		
		StringBuilder modifiers = new StringBuilder() ;
		if( field.isFinal() )
			modifiers.append("final ") ;
		
		if( field.isStatic() )			
			modifiers.append("static ") ;
		
		if( field.isPublic() )
			modifiers.append("public ") ;
		else if( field.isPrivate() )
			modifiers.append("private ") ;
		else if( field.isProtected() )
			modifiers.append("protected ") ;
		
		sw.append( indent ) ;
		sw.append( modifiers.toString() ) ;
		
		String str = sw.toString() ;
		os.write( str.getBytes("UTF-8") ) ;
		
		print(field.getType(), os, "", "" ) ;
		
		sw = new StringWriter() ;
		sw.append(" ").append( field.getSimpleName() ) ;
		sw.append( lineSeparator ) ;
		
		str = sw.toString() ;
		os.write( str.getBytes("UTF-8") ) ;	

	}

	@Override
	public void print(IJavaMethodDeclaration method, OutputStream os,
			String indent, String indentIncrement)
			throws UnsupportedEncodingException, IOException {
		
		List<IJavaAnnotation> annotations = method.getAnnotations();
		if(annotations != null){
			for( IJavaAnnotation annotation : annotations ){
				print(annotation,os,indent,indentIncrement) ;
			}
		}
		
		os.write( (indent+method.getQualifiedName().getValues().iterator().next() + "()").getBytes("UTF-8") ) ;
		List<IJavaType> exceptions = method.getExceptions();
		if(exceptions!= null && ! exceptions.isEmpty()){
			
			os.write(lineSeparator.getBytes("UTF-8")) ;
			os.write((indent+indentIncrement+ "throws ").getBytes("UTF-8")) ;
			int size = exceptions.size();
			
			for( int i = 0 ; i < size ; i++ ){
				IJavaType exception = exceptions.get(i) ;
				print(exception,os,"", "") ;
				if( i < size-1 )
					os.write(", ".getBytes("UTF-8")) ;
			}
		}
		os.write( lineSeparator.getBytes("UTF-8") ) ;
		
		StringWriter sw = new StringWriter() ;
		IJavaType returnType = method.getReturnType();
		if(returnType != null){
			print( returnType, os, indent, indentIncrement) ;
			sw.write(" ") ;
		}
		else{
			sw.write( indent+"void ") ;
		}
		sw.append( lineSeparator ) ;
		
		int genericParamsCount = method.getGenericParamsCount();
		if( genericParamsCount > 0 ){
			sw.append("<") ;
			List<String> genericParamNames = method.getGenericParamNames();
			for(int i = 0 ; i < genericParamsCount ; i++){
				sw.append( genericParamNames.get(i) ) ;
				if(i != genericParamsCount-1)
					sw.append(", ") ;
			}
			sw.append(">") ;
		}
		sw.append( method.getSimpleName()+"(" ) ;
		
		String str = sw.toString() ;
		os.write( str.getBytes("UTF-8") ) ;
		
		List<IJavaType> params = method.getParameters();
		int size = params.size();
		for( int i = 0 ; i < size ; i++ ){
			IJavaType param = params.get(i) ;			
			print(param,os,"","") ;
			if( i < size-1 )
				os.write(", ".getBytes("UTF-8")) ;
		}
		os.write( (")"+lineSeparator+lineSeparator).getBytes("UTF-8") ) ;
	}

	@Override
	public void print(IJavaTypeDeclaration typeDeclaration, OutputStream os,
			String indent, String indentIncrement)
			throws UnsupportedEncodingException, IOException {
		
		StringWriter sw = new StringWriter() ;
		QualifiedName qualifiedName = typeDeclaration.getQualifiedName();
		
		List<String> values = qualifiedName.getValues();
		int size = values.size();
		for( int i = 0 ; i < size ; i++){
			String name = values.get(i) ;
			sw.append(name) ;
			if(i < size-1 )
				sw.append(", ") ;
		}
		
		sw.append(lineSeparator) ;
		
		if(typeDeclaration.isEnum()){
			String indent1 = indent+indentIncrement ;
			sw.append(indent1).append("Enum constants:").append(lineSeparator) ;			
			List<IEnumConstant> ecList = typeDeclaration.getEnumConstants();
			if(ecList != null){
				for(IEnumConstant ec : ecList){
					sw.append(indent1).append(ec.getName()).append(lineSeparator) ;					
				}
			}
		}
		
		String str = sw.toString() ;
		os.write( str.getBytes("UTF-8") ) ;
		
		IJavaType superClass = typeDeclaration.getSuperClass();
		if( superClass != null ){
			os.write( (lineSeparator+indent+"Extends:"+lineSeparator+indent+indentIncrement).getBytes("UTF-8") ) ;
			print(superClass,os,"","") ;
			os.write( lineSeparator.getBytes("UTF-8") ) ;			
		}
		
		List<IJavaType> interfaces = typeDeclaration.getInterfaces();
		if(interfaces != null && !interfaces.isEmpty()){
			os.write( (lineSeparator+indent+"Implements:"+lineSeparator).getBytes("UTF-8") ) ;
			for(IJavaType iFace : interfaces){
				print(iFace,os,indent+indentIncrement,"") ;
				os.write( lineSeparator.getBytes("UTF-8") ) ;
			}			
		}
		
		List<IJavaTypeDeclaration> nested = typeDeclaration.getNestedClasses();
		if(nested != null && !nested.isEmpty()){
			sw = new StringWriter() ;
			sw.append( lineSeparator+indent+"Nested classes:"+lineSeparator ) ;
			for(IJavaTypeDeclaration nc : nested){
				sw.append(indent+indentIncrement) ;
				QualifiedName qualifiedName1 = nc.getQualifiedName();
				for(String name : qualifiedName1.getValues())
					sw.append(name) ;
				
				sw.append(lineSeparator) ;
				sw.append(lineSeparator) ;
			}
			
			str = sw.toString() ;
			os.write( str.getBytes("UTF-8") ) ;
		}
	}

	@Override
	public void writeDump(IOutlineContainer oc, String path) {
		File f = new File( path ) ;
		f.getParentFile().mkdirs() ;
		if(f.exists())
			f.delete() ;
		
		try {
			f.createNewFile() ;
			FileOutputStream fos = new FileOutputStream(f) ;
			print( oc, fos, "\t", "\t\t" ) ;
			fos.close() ;			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
