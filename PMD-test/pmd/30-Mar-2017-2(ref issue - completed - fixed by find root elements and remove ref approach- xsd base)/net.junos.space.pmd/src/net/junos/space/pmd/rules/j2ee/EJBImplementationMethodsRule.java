package net.junos.space.pmd.rules.j2ee;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import net.junos.space.pmd.utils.outline.AbstractOutlineAccessor;
import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IJavaMethodDeclaration;
import net.junos.space.pmd.utils.outline.IJavaType;
import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;
import net.junos.space.pmd.utils.outline.QualifiedName;
import net.sourceforge.pmd.RuleContext;

public class EJBImplementationMethodsRule extends AbstractOutlineAccessor {
	
	
	@Override
	public void doEnd(RuleContext ctx) {
		for (IClassOutline outline : getOutlineContainer().getAllOutlines()) {
			
			if (!isEnterpriseBeanEJB2(outline.getTypeDeclaration()))
				continue ;
			
			ArrayList<IJavaMethodDeclaration> nonHomeAndRemoteMethods = extractNonHomeAndRemoteMethods(outline) ;  			
			
			for (IJavaMethodDeclaration method : nonHomeAndRemoteMethods) {
				
				for (IJavaType exception : method.getExceptions()) {
					if ( !isKnownRemoteExceptionSubclass(exception) )
						continue ;
					
					if(validMethods.contains(method.getSimpleName()))
						continue ;
					
					reportError(ctx, outline, method);
					break;
				}
			}
		}
		super.doEnd(ctx);
	}


	private ArrayList<IJavaMethodDeclaration> extractNonHomeAndRemoteMethods(IClassOutline outline) {

		ArrayList<IClassOutline> remoteAndHomeInterfaces = new ArrayList<IClassOutline>() ;
		
		List<IJavaTypeDeclaration> allInterfaces = outline.getTypeDeclaration().getAllInterfaces();
		for(IJavaTypeDeclaration decl : allInterfaces){
			List<IJavaTypeDeclaration> allInterfaces2 = decl.getAllInterfaces();
			
			boolean isRemoteOrHome = false ;
			for(IJavaTypeDeclaration decl2 : allInterfaces2){
				QualifiedName qualifiedName = decl2.getQualifiedName();
				if(qualifiedName == null)
					continue ;
				
				List<String> values = qualifiedName.getValues();
				if(!values.contains("javax.ejb.EJBObject") && !values.contains("javax.ejb.EJBHom"))
					continue ;
				
				isRemoteOrHome = true ;
				break ;
			}
			if(!isRemoteOrHome)
				continue ;
			
			for(String s : decl.getQualifiedName().getValues()){
				IClassOutline o = getOutlineContainer().getClassOutline(s);
				if(o==null)
					continue ;
				
				remoteAndHomeInterfaces.add(o) ;
			}
				
		}
		
		List<IClassOutline> definedSuperclasses = outline.getDefinedSuperclasses(getOutlineContainer());
		ArrayList<IClassOutline> hierarchy = new ArrayList<IClassOutline>() ;
		hierarchy.add(outline) ;
		hierarchy.addAll(definedSuperclasses) ;
		
		ArrayList<IJavaMethodDeclaration> result = new ArrayList<IJavaMethodDeclaration>() ;
l1:		for(IJavaMethodDeclaration method : outline.getMethods() ){
			
			IJavaType returnType = method.getReturnType();
			List<IJavaType> parameters = method.getParameters();
			
			String name = method.getSimpleName() ;

			for( IClassOutline o : remoteAndHomeInterfaces ){
				
				List<IJavaMethodDeclaration> methods = o.getMethodsBySimpleName(name);
l0:				for(IJavaMethodDeclaration md : methods){
					
					IJavaType rt = md.getReturnType();
					if( (rt==null)!=(returnType==null) )
						continue ;
					
					if(rt!= null && rt.getSimpleName() != returnType.getSimpleName())
						continue ;
					
					List<IJavaType> params = md.getParameters();
					if( (parameters==null || parameters.isEmpty() )!=(params==null || params.isEmpty() ) )
						continue ;
					
					if(params != null){
						
						int size = params.size();
						if( size != parameters.size() )
							continue ;
						
						for(int i = 0 ; i < size ; i++){
							IJavaType param1 = parameters.get(i);
							IJavaType param2 = params.get(i);
							if(param1.getSimpleName() != param2.getSimpleName())
								continue l0;
						}
						continue l1;						
					}
				}				
			}	
			result.add(method) ;
		}
		
		return result ;
	}

	@Override
	public String getMessage() {
		return "Do not throw a RemoteException from an EJB implementation method";
	}
	
	
	private static final HashSet<String> validMethods = new HashSet<String>(
			Arrays.asList( new String[]{
		 "getEJBHome",
		 "getHandle",
		 "getPrimaryKey",
		 "isIdentical",
		 "remove",
		 "getEJBMetaData",
		 "getHomeHandle"		
	})  ) ;
	
}
