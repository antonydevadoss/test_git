package net.junos.space.pmd.utils.outline.example;

import net.junos.space.pmd.utils.outline.AbstractOutlineAccessor;
import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IJavaFieldDeclaration;
import net.sourceforge.pmd.RuleContext;
import net.sourceforge.pmd.lang.java.rule.JavaRuleViolation;

public class ExampleRuleWithOutline extends AbstractOutlineAccessor {

	@Override
	public
	void doStart(RuleContext ctx) {
		// TODO Auto-generated method stub

	}

	@Override
	public
	void doEnd(RuleContext ctx) {

		for( IClassOutline outline : getOutlineContainer().getAllOutlines() ){
			for( IJavaFieldDeclaration fd : outline.getFields() ){
				if(fd.getSimpleName().equals("badField")){
					
					reportError(ctx, outline, fd) ;
				}
			}
		}
	}
	
	public String getMessage(){
		return "Bad Field Name !!!" ;
	}

}
