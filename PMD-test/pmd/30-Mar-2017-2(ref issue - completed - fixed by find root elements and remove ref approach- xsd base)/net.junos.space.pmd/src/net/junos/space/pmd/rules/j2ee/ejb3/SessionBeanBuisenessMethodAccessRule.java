package net.junos.space.pmd.rules.j2ee.ejb3;

import java.util.ArrayList;
import java.util.HashSet;
import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IJavaMethodDeclaration;
import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;
import net.sourceforge.pmd.RuleContext;

public class SessionBeanBuisenessMethodAccessRule extends
		AbstractBusinessInterfacesInspector {
	private static final String message = "The business method of a session bean must be declared as public.";
	
	@Override
	protected void inspectMethod(IJavaMethodDeclaration method,
			RuleContext ctx, HashSet<String> inspectedTypes) {
		if (method != null && !method.isPublic()) {
			for (String ownerQualifiedName : method.getOwner().getQualifiedName().getValues()) {
				IClassOutline ownerOutline = getOutlineContainer().getClassOutline(ownerQualifiedName);
				reportError(ctx, ownerOutline, method) ;
			}
		}
	}
	
	@Override
	public String getMessage() {
		return message;
	}
	
	@Override
	protected ArrayList<IClassOutline> extractTargetInterfaces(
			IJavaTypeDeclaration typeDeclaration) {
		
		return extractAllBusinessInterfaces(typeDeclaration) ;
	}
}
