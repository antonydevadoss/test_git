package net.junos.space.pmd.utils.outline;

public class AnnotationClassMemberValue implements IAnnotationMemberValue {

	public AnnotationClassMemberValue(IJavaType type) {
		super();
		this.type = type;
	}

	IJavaType type ;
	
	@Override
	public String getStringValue() {		
		return type.toString() ;
	}

	@Override
	public int getKind() {		
		return IJavaAnnotation.ANNOTATION_TYPE_CLASS_NAME ;
	}

	public IJavaType getType() {
		return type;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AnnotationClassMemberValue other = (AnnotationClassMemberValue) obj;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}

}
