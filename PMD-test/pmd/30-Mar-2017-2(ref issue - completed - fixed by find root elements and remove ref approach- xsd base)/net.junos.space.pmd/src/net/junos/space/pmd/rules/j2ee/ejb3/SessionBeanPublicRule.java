package net.junos.space.pmd.rules.j2ee.ejb3;

import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;

public class SessionBeanPublicRule extends net.junos.space.pmd.rules.j2ee.SessionBeanPublicRule {

	@Override
	protected boolean mustBeChecked(IJavaTypeDeclaration typeDeclaration) {
		return isEnterpriseBeanEJB3(typeDeclaration);
	}
}
