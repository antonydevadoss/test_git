<?xml version="1.0"?>

<ruleset name="Junos Space Rules"
    xmlns="http://pmd.sf.net/ruleset/1.0.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://pmd.sf.net/ruleset/1.0.0 http://pmd.sf.net/ruleset_xml_schema.xsd"
    xsi:noNamespaceSchemaLocation="http://pmd.sf.net/ruleset_xml_schema.xsd">
  
	<description>JunosSpace Rules are used to describe the rules for Junos Space SDK PMD plugin</description>

	<rule name="MissingHttpMethodRule"
			since="JS-0.1"
		  message="Missing Http Method Annotation - REST Method is not having any Http method annotation(@GET/@PUT/@DELETE/@POST)."
		  class="net.junos.space.pmd.rules.rest.MissingHttpMethodRule">          
	  <description>
		This rule verifies that Http Method annotation(@GET/@POST/@PUT/@DELETE) should be present on every rest method which is annotated with @Path/@Produces/@Consumes annotation.
	  </description>
	  <priority>1</priority>
	  <example>
		<![CDATA[
			@Path("/")
			public interface HelloWorldIf {

				//no @GET/@POST/@PUT/@DELETE exists
				@Path("/countries")
				public List<Country> getCountries(); 
			}
		]]>
	 </example>
	</rule>
	
	
	<rule name="PathExistenceRule"
			since="JS-0.1"
          message="Missing @Path Annotation - @Path annotation should present on enclosing rest Interface/Class."
          class="net.junos.space.pmd.rules.rest.PathExistenceRule">
      <description>
		This rule verifies that @Path annotation should be present on class/interface of every rest method.
      </description>
      <priority>1</priority>
      <example>
		<![CDATA[
			//no @Path exists
			public interface HelloWorldIf {

				@GET
				@Path("/countries")
				public List<Country> getCountries(); 
			}
		]]>
      </example>
    </rule>
	
	
	<rule name="MissingProducesRule"
    		 since="JS-0.1"
          message="Missing @Produces Annotation - @Produces Annotation should be present on method or on Interface/Class."
          class="net.junos.space.pmd.rules.rest.MissingProducesRule">
      <description>
		This rule verifies that if REST method is returning DTO, then @Produces annotation should be present either on REST method or on Interface/Class.
      </description>
      <priority>1</priority>
      <example>
		<![CDATA[			
			@Path("/")
			//no @Produce exists (either in class level or method level)
			public interface HelloWorldIf {

				@GET
				@Path("/countries")
				//no @Produce exists (either in class level or method level)
				public List<Country> getCountries(); 
			}
		]]>
      </example>
    </rule>
	
	
	<rule name="AvoidProducesRule"
    		 since="JS-0.1"
          message="Invalid @Produces Usage - No need to use @Produces annotation on REST method as it is not returning any DTO."
          class="net.junos.space.pmd.rules.rest.AvoidProducesRule">
      <description>
		This rule verifies that if REST method is not returning anything (return type is 'void'), then there is no need to use @Produces annotation on REST method.
      </description>
      <priority>3</priority>
      <example>
		<![CDATA[
			@Path("/")
			public interface HelloWorldIf {

				@GET
				@Path("/countries")
				@Produces("application/json") // no need to use @Produces here
				public void showDetails(); 
			}
		]]>
      </example>
    </rule>
	
	
	<rule name="MissingConsumesRule"
    		 since="JS-0.1"
          message="Missing @Consumes Annotation - @Consumes should be present on method or on Interface/Class."
          class="net.junos.space.pmd.rules.rest.MissingConsumesRule">
      <description>
		This rule verifies that if REST method is expecting one DTO as one of the input parameter, then @Consumes annotation should be present either on method or on Interface/Class.
      </description>
      <priority>1</priority>
      <example>
		<![CDATA[
			@Path("/")
			// no @Consumes annotation (either on class level or method level)
			public interface HelloWorldIf {

				@POST
				@Path("/country")
				// no @Consumes annotation (either on class level or method level)
				public void addCountry(Country param0); 
			}
		]]>
      </example>
    </rule>
	
	
	<rule name="AvoidConsumesForNoParameterRule"
    		 since="JS-0.1"
          message="Invalid @Consumes Usage - No need to use @Consumes annotation on method as method is not expecting any input."
          class="net.junos.space.pmd.rules.rest.AvoidConsumesForNoParameterRule">
      <description>
		This rule verifies that if REST method is not expecting any input, then there is no need to have @Consumes annotation on REST method.
      </description>
      <priority>3</priority>
      <example>
		<![CDATA[
			@Path("/")
			public interface HelloWorldIf {

				@POST
				@Path("/country")
				@Consumes("application/xml") //no need to use @Consumes here
				public void country(); 
			}
		]]>
      </example>
    </rule>
	
	
	<rule name="AvoidConsumesForAnnotationParameterOnlyRule"
    		 since="JS-0.1"
          message="Invalid @Consumes Usage - No need to use @Consumes annotation as method is expecting only parameter annotations as input."
          class="net.junos.space.pmd.rules.rest.AvoidConsumesForAnnotationParameterOnlyRule">
      <description>
		This rule verifies that if REST method is expecting only @PathParam or/and @QueryParam as input parameter(s), then there is no need to have @Consumes annotation on REST method.
      </description>
      <priority>3</priority>
      <example>
		<![CDATA[
			@Path("/")
			public interface HelloWorldIf {
				
				@POST
				@Path("/country/{countryid}")
				@Consumes("application/xml") // no need to use @Consumes here
				public void country(@PathParam("countryid") int param0); 
			}
		]]>
      </example>
    </rule>
	
	
	<rule name="InvalidRESTMethodRule"
    		 since="JS-0.1"
          message="Invalid REST method - Only one DTO is allowed as input parameter of REST method."
          class="net.junos.space.pmd.rules.rest.InvalidRESTMethodRule">
      <description>
		This rule verifies that REST method should not have more than one DTO as input parameter.
      </description>
      <priority>1</priority>
      <example>
		<![CDATA[
			@Path("/")
			public interface HelloWorldIf {

				@POST
				@Path("/country/{countryid}")
				@Consumes("application/xml") 
				public void addCountry(Country param0, State param1); //more than one DTO! 
			}
		]]>
      </example>
    </rule>
	
	<rule name="MissingRBACRule"
    		 since="JS-0.1"
          message="Missing @RBAC annotation - @RBAC annotation may be present on method."
          class="net.junos.space.pmd.rules.rest.MissingRBACRule">
      <description>
		This rule verifies that @RBAC annotation should be present on REST method.
      </description>
      <priority>5</priority>
      <example>
		<![CDATA[
			@Path("/")
			public interface HelloWorldIf {

				@GET 
				@Path("/countries") 
				@Produces("application/json")
				// no @RBAC(type = { CRUDEnum.CREATE }, capability = { "HelloWorldCap" }) exists
				public List<Country> getCountries(); 
			}
		]]>
      </example>
    </rule>
	
	
	<rule name="InvalidMethodRule"
    		 since="JS-0.1"
          message="Inconsistency in method name and HTTP method type."
          class="net.junos.space.pmd.rules.rest.InvalidMethodRule">
      <description>
		This rule verifies that 
			if REST method starts with 'add...' then Http method would be POST 
			if REST method starts with 'get...' then Http method would be GET
			if REST method starts with 'update...' then Http method would be PUT
			if REST method starts with 'delete...' then Http method would be DELETE
      </description>
      <priority>5</priority>
      <example>
		<![CDATA[
			@Path("/")
			public interface HelloWorldIf {

				@POST // should be @GET
				@Path("/countries") 
				@Produces("application/json")
				public List<Country> getCountries(); 
				
				@GET // should be @POST
				@Path("/country")
				@Consumes("application/json")
				public void addCountry(Country country);
				
				@PUT // should be @DELETE
				@Path("/country/{id}")
				public void deleteCountry(@PathParam("id") int param0);
				
				@DELETE // should be @PUT
				@Path("/country/{id}")
				@Consumes("application/json")
				public void updateCountry(Country param0, @PathParam("id") int param1);
			}
		]]>
      </example>
    </rule>
	
	

	
	<rule name="InvalidDeleteUsageRule"
    		 since="JS-0.1"
          message="Invalid @DELETE usage - @DELETE REST method neither requires DTO as input parameter nor requires @Consumes annotation."
          class="net.junos.space.pmd.rules.rest.InvalidDeleteUsageRule">
      <description>
		This rule verifies that if REST method has @DELETE annotation, then REST method should neither expect DTO nor have @Consumes annotation.
      </description>
      <priority>3</priority>
      <example>
		<![CDATA[
			@Path("/")
			public interface HelloWorldIf {

				@Path("country/{id}")				
				@Consumes("application/json) // no need to use @Consumes annotation
				@DELETE
				public void deleteCounntry(@PathParam("id") int param0, Country param1); // no need to use DTO as input 
			}
		]]>
      </example>
    </rule>
   
     <rule name="MoreThanOneHttpMethodRule"
    		 since="JS-0.1"
          message="Invalid REST Resource - More than one Http Method Annotation is present on REST method."
          class="net.junos.space.pmd.rules.rest.MoreThanOneHttpMethodAnnotationRule">
      <description>
		This rule verifies that more than one Http method annotation should not be present on REST method.
      </description>
      <priority>3</priority>
      <example>
		<![CDATA[
			@Path("/")
			public interface HelloWorldIf {

				@POST
				@GET   //more than one annotation exists (@POST and @GET exists)				 
				@Path("/country")
				@Consumes("application/vnd.net.juniper.space.hello-world.state+json;version=1;charset=UTF-8")
				public void addCountry(Country param0);  
			}
		]]>
      </example>
    </rule>
    
   </ruleset>	
