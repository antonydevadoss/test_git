package net.junos.space.pmd.rules.j2ee.ejb3;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IJavaAnnotation;
import net.junos.space.pmd.utils.outline.IJavaMethodDeclaration;
import net.junos.space.pmd.utils.outline.IJavaType;
import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;
import net.sourceforge.pmd.RuleContext;

public class EntitySerializableRule extends AbstractBusinessInterfacesInspector {//AbstractEntityRule {

	
	private static final String message = "If an entity instance is to be passed by value as a detached object (e.g., through a remote interface), the entity class must implement the Serializable interface.";

	
	@Override
	protected void inspectMethod(IJavaMethodDeclaration method,
			RuleContext ctx, HashSet<String> inspectedTypes) {
		
		while(true){
			IJavaType returnType = method.getReturnType();
			if (returnType == null)
				break ;
			
			IClassOutline returnTypeOutline = null;
			for (String returnTypeQName : returnType.getQualifiedName().getValues()) {
				returnTypeOutline = getOutlineContainer().getClassOutline(returnTypeQName);
				if (returnTypeOutline != null) {
					break;
				}
			}
			
			if (returnTypeOutline == null)
				break ;
			
			if(!returnTypeOutline.hasAllSuperclassesDefined(getOutlineContainer()))
				break ;
			
			IJavaTypeDeclaration typeDeclaration = returnTypeOutline.getTypeDeclaration();
			List<IJavaAnnotation> annotations = typeDeclaration.getAnnotations();
			if(annotations==null)
				break ;			
			
			boolean isEntity = false;			
			for (IJavaAnnotation annotation : annotations) {
				if (annotation.getName().equals("Entity")
						|| annotation.getName().equals("javax.persistence.Entity")) {
					isEntity = true;
					break;
				}
			}
				
			if (!isEntity)
				break ;
			
			boolean isSerializable = false;
l0:			for (IJavaTypeDeclaration interfaceDeclaration : typeDeclaration.getAllInterfaces()) {
				for (String interfaceQName : interfaceDeclaration.getQualifiedName().getValues()) {
					if (interfaceQName.equals("java.io.Serializable")) {
						isSerializable = true;
						break l0;
					}
				}
			}
					
			if (isSerializable)
				break ;
			
			reportError(ctx, returnTypeOutline, typeDeclaration);
			break ;			
		}
	}
	
	
	@Override
	public String getMessage() {
		
		
		
		return message;
	}
	
	@Override
	protected ArrayList<IClassOutline> extractTargetInterfaces(
			IJavaTypeDeclaration typeDeclaration) {
		ArrayList<IClassOutline> extractRemoteInterfacesFromAnnotation = extractRemoteInterfacesFromAnnotation(typeDeclaration);
		return extractRemoteInterfacesFromAnnotation;
	}
}
