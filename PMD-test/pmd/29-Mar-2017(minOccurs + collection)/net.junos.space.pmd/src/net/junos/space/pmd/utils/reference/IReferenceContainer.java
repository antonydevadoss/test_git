package net.junos.space.pmd.utils.reference;

import java.util.Collection;
import java.util.List;

import net.junos.space.pmd.utils.outline.IDeclaration;



public interface IReferenceContainer {
	
	void register(IDeclaration method, Collection<IReference> referenceSet) ;
	
	IClassReferenceStorage<IReference> getTemporaryReferenceListForClass(String classQualifiedName) ;
	
	Collection<IClassReferenceStorage<IReference>>getAllTemporaryClassStorages() ;
	
	Collection<IClassReferenceStorage<SimpleClassReference>>getAllResolvedClassStorages() ;
		
	IClassReferenceStorage<SimpleClassReference> getResolvedReferenceListForClass(String classQualifiedName) ;

	void clear();
	
	void clearTemporaryRefs() ;

	void registerResolvedRefs(IDeclaration method, Collection<SimpleClassReference> referenceSet);
	
	
}
