package net.junos.space.pmd.utils.outline;

import net.sourceforge.pmd.RuleContext;

public abstract class SimpleInspector extends AbstractOutlineAccessor {

	public SimpleInspector() {
		super();
	}

	protected abstract boolean mustBeChecked(IJavaTypeDeclaration typeDeclaration);

	public void doEnd(RuleContext ctx) {
		
		for(IClassOutline outline : getOutlineContainer().getAllOutlines() ){
			
			IJavaTypeDeclaration decl = outline.getTypeDeclaration();
			if(!mustBeChecked(decl))
				continue ;
			
			if(!haveError(decl,outline))
				continue ;
		
			reportError(ctx, outline, decl);
		}
	}

	protected abstract boolean haveError(IJavaTypeDeclaration decl, IClassOutline outline);

}