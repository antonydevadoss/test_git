/*******************************************************************************
 * FILE NAME: MissingHttpMethodRule.java
 * PURPOSE:  This is Rule-1 in document 
 *
 *
 * Revision History: 
 * DATE:           				  AUTHOR:              	CHANGE:  
 * Tue Feb 09, 2011               Atul Bajpai       	Initial Version  
 * 
 * 
 ******************************************************************************/
package net.junos.space.pmd.rules.rest;

import java.util.List;import net.junos.space.pmd.utils.PMDConstants;import net.junos.space.pmd.utils.PMDConstants.HttpMethod;import net.junos.space.pmd.utils.PMDUtils;import net.sourceforge.pmd.RuleContext;import net.sourceforge.pmd.lang.java.ast.ASTAnnotation;import net.sourceforge.pmd.lang.java.ast.ASTMethodDeclarator;import net.sourceforge.pmd.lang.java.rule.AbstractJavaRule;import org.apache.log4j.Logger;

/**
 * NoHttpMethodRule is scanning all rest APIs and verifies that rest resource should have HTPP Method annotation (@GET, @POST,
 * 
 * @PUT, @DELETE)
 */
public class MissingHttpMethodRule extends AbstractJavaRule {

	/**
	 * Logger
	 */
	private static Logger log = Logger.getLogger(MissingHttpMethodRule.class);

	/**
	 * This method verifies that rest resource should have HTPP Method annotation (@GET, @POST, @PUT, @DELETE)
	 */
	public Object visit(ASTMethodDeclarator methodNode, Object data) {
		RuleContext ctx = (RuleContext) data;
		String methodName = methodNode.getImage();
		try {
			boolean isHTTPMethodAnnotationExists = false;
			boolean isPathConsumeProduceExists = false;

			List<ASTAnnotation> annotations = PMDUtils.getMethodAnnotations(methodNode);

			for (int i = 0; i < annotations.size(); i++) {
				String annotationName = PMDUtils.getAnnotationName(annotations.get(i));
				if (annotationName.equals(PMDConstants.PATH_ANNOTATION)
						|| annotationName.equals(PMDConstants.PRODUCES_ANNOTATION)
						|| annotationName.equals(PMDConstants.CONSUMES_ANNOTATION))
					isPathConsumeProduceExists = true;
				if (HttpMethod.isValidHttpMethod(annotationName))
					isHTTPMethodAnnotationExists = true;
			}
			
			if(log.isDebugEnabled()){
				StringBuilder builder = new StringBuilder();
				builder.append("source file=").append(ctx.getSourceCodeFilename()).append(",method=").append(methodName);
				builder.append(",isPathConsumeProduceExists=").append(isPathConsumeProduceExists).append(
						",isHTTPMethodAnnotationExists=").append(isHTTPMethodAnnotationExists);
				log.debug(builder.toString());
			}
			
			if (isPathConsumeProduceExists && !isHTTPMethodAnnotationExists)
				addViolation(data, methodNode);
		} catch (Exception e) {
			log.error("Exception in executing method " + methodName + " of " + ctx.getSourceCodeFilename(), e);
		}
		return super.visit(methodNode, data);
	}
}
