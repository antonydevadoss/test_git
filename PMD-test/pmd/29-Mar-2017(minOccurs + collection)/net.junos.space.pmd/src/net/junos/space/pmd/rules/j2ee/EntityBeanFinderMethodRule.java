package net.junos.space.pmd.rules.j2ee;

import net.junos.space.pmd.utils.outline.AbstractOutlineAccessor;
import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IJavaMethodDeclaration;
import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;
import net.sourceforge.pmd.RuleContext;

public class EntityBeanFinderMethodRule extends AbstractOutlineAccessor {
	
	@Override
	public String getMessage() {
		return "Do not declare a finder method for a CMP EJB";
	}
	
	@Override
	public void doEnd(RuleContext ctx) {
		for (IClassOutline outline : getOutlineContainer().getAllOutlines()) {
			
			IJavaTypeDeclaration decl = outline.getTypeDeclaration();
			if(!decl.isAbstract())
				continue ;
			
			if (!isEntityBeanEJB2(decl))
				continue ;
			
			for (IJavaMethodDeclaration method : outline.getMethods()) {
				if (method.getSimpleName().startsWith("ejbFind")) {
					reportError(ctx, outline, method);
				}
			}
		}
		super.doEnd(ctx);
	}
}