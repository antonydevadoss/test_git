package net.junos.space.pmd.rules.j2ee.ejb3;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IJavaMethodDeclaration;
import net.junos.space.pmd.utils.outline.IJavaType;
import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;
import net.junos.space.pmd.utils.outline.QualifiedName;
import net.sourceforge.pmd.RuleContext;

public class SessionBeanMethodThrowsRule extends AbstractBusinessInterfacesInspector {
	private static final String message = "The remote business interface is not required or expected to be a" 
											+ "java.rmi.Remote interface. The throws clause should not include the"
											+ "java.rmi.RemoteException. The methods of the business interface may only throw the"
											+ "java.rmi.RemoteException if the interface extends java.rmi.Remote.";

	@Override
	protected void inspectMethod(IJavaMethodDeclaration method,	RuleContext ctx, HashSet<String> inspectedTypes) {
		List<IJavaType> exceptions = method.getExceptions();
		if(exceptions==null||exceptions.isEmpty())
			return;
		
		for( IJavaType ex : exceptions ){
			
			if(!isKnownRemoteExceptionSubclass(ex))
				continue ;
			
			String ownerQualifiedName = method.getOwner().getQualifiedName().getValues().get(0);
			IClassOutline classOutline = getOutlineContainer().getClassOutline(ownerQualifiedName);
			reportError(ctx, classOutline, method);
			break ;
		}
	}
	
	@Override
	public String getMessage() {
		return message;
	}

	@Override
	protected ArrayList<IClassOutline> extractTargetInterfaces(IJavaTypeDeclaration typeDeclaration) {
		
		ArrayList<IClassOutline> remoteInterfaces = extractRemoteInterfacesFromAnnotation(typeDeclaration);
		ArrayList<IClassOutline> result = new ArrayList<IClassOutline>() ;
l0:		for(IClassOutline outline : remoteInterfaces){
			List<IJavaTypeDeclaration> allInterfaces = outline.getTypeDeclaration().getAllInterfaces();
			for(IJavaTypeDeclaration decl : allInterfaces){
				
				QualifiedName qName = decl.getQualifiedName();
				if(qName==null)
					continue ;
				
				if( qName.getValues().contains("java.rmi.Remote") )
					continue l0;
			}
			result.add(outline);
		}
		return result;
	}
}
