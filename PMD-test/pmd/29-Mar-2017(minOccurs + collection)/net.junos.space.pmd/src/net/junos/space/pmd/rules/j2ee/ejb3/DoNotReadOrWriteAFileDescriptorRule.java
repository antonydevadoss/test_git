package net.junos.space.pmd.rules.j2ee.ejb3;

public class DoNotReadOrWriteAFileDescriptorRule extends
			net.junos.space.pmd.rules.j2ee.DoNotReadOrWriteAFileDescriptorRule {

	protected boolean mustBeChecked(String className) {		
		return isEnterpriseBeanSuperclassEJB3(className);
	}
}
