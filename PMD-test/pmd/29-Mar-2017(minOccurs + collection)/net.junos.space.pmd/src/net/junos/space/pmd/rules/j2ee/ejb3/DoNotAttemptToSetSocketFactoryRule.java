package net.junos.space.pmd.rules.j2ee.ejb3;

public class DoNotAttemptToSetSocketFactoryRule extends
			net.junos.space.pmd.rules.j2ee.DoNotAttemptToSetSocketFactoryRule {

	protected boolean mustBeChecked(String className) {		
		return isEnterpriseBeanSuperclassEJB3(className);
	}
}
