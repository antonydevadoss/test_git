/*******************************************************************************
 * FILE NAME: ModuleXMLReader.java
 * PURPOSE:   This is reading module.xml by using STAX parsing and loading capabilities.
 *
 *
 * Revision History: 
 * DATE:           				  AUTHOR:              	CHANGE:  
 * Mar 03, 2011		              Amit Arora	       	Initial Version  
 * 
 * 
 ******************************************************************************/
package net.junos.space.pmd.utils;

import java.io.File;import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.apache.log4j.Logger;

/**
 * This reader is reading Module.xml using STAX parsing and load all capabilities 
 * in parseXML method. This method returns as soon as it finds out </all-capabilities>
 * as by that time all capabilities have been loaded in memory.
 */
public class ModuleXMLReader extends XMLReader{
	
	/**
	 * List of capabilities defined in module.xml
	 */
	private List<String> capList;
	
	/**
	 * Logger
	 */
	private Logger log = Logger.getLogger(ModuleXMLReader.class);
	
	/**
	 * Default constructor calling parseXML for getting capabilities
	 */
	public ModuleXMLReader(){
		parseXML();
	}	
	
	/**
	 * Returning capabilities
	 * @return
	 */
	public List<String> getCapabilities(){
		return capList;
	}	
	
	@SuppressWarnings("unchecked")
	protected void parseXML()  {
		capList = new ArrayList<String>();
		//Add default capability for comparison; bypassing DefaultRead
		capList.add("DefaultRead");
		FileInputStream fis;
		try {			
			String xmlFilePath = getXMLFilePath(PMDConstants.EAR_PROJECT, PMDConstants.MODULE_XML_DIR, PMDConstants.MODULE_XML_NAME, true);			if(! (new File(xmlFilePath).exists()) )				return ;						fis = new FileInputStream(xmlFilePath);
			XMLEventReader parser = XMLInputFactory.newInstance().createXMLEventReader(fis);			
			while (parser.hasNext()) {
				XMLEvent event = parser.nextEvent();
				if(event.isStartElement()) {
					StartElement start = event.asStartElement();
					if((start.getName().toString()).equals(PMDConstants.ALL_CAPABILITY)) {
						//get next event if there is <all-capability> to find all <capability> tags
						while(parser.hasNext()){
							XMLEvent nextEvent = parser.nextEvent();
							if(nextEvent.isStartElement()) {
								StartElement startEle = nextEvent.asStartElement();
								if((startEle.getName().toString()).equals(PMDConstants.CAPABILITY)) {
									Iterator<Attribute> itr = (Iterator<Attribute>)startEle.getAttributes();
									while(itr.hasNext()) {
										Attribute attr = (Attribute)itr.next();
										if(attr.getName().toString().equals(PMDConstants.NAME))
											capList.add(attr.getValue());
									}//end loop for getting attributes of <capability> (inner loop)
								}//if start element is capbility (inner loop)
							}//if it is start element (inner loop)
							else if(nextEvent.isEndElement()){ //if parser gets </all-capability> return from parsing
								EndElement endEle = nextEvent.asEndElement();
								if((endEle.getName().toString()).equals(PMDConstants.ALL_CAPABILITY))
									return;
							}//if element is end element (inner loop)
						}//End parsing in inner loop						
					}//end check for start element of all-capability
				}// end check for start element
			}//End parsing
		} catch (FileNotFoundException e) {
			log.error(e.getMessage(), e);			
		} catch (XMLStreamException e) {
			log.error(e.getMessage(), e);			
		} catch (FactoryConfigurationError e) {
			log.error(e.getMessage(), e);
		}	
	}
}
	
