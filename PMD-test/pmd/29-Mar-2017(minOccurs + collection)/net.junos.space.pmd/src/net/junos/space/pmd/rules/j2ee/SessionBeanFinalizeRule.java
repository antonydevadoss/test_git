package net.junos.space.pmd.rules.j2ee;

import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IJavaMethodDeclaration;
import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;
import net.junos.space.pmd.utils.outline.SimpleInspector;

public class SessionBeanFinalizeRule extends SimpleInspector {

	public String getMessage() {
		return "Do not override the finalize method for an EJB implementation class";
	}

	@Override
	protected boolean mustBeChecked(IJavaTypeDeclaration typeDeclaration) {
		return isEnterpriseBeanEJB2(typeDeclaration);
	}
	
	public boolean haveError(IJavaTypeDeclaration decl, IClassOutline outline) {
		IJavaMethodDeclaration finalizeMethod = outline.getMethod("finalize", null, null);
		return finalizeMethod!=null ;
	}
}
