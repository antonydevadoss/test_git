package net.junos.space.pmd.utils.reference;

import net.junos.space.pmd.utils.outline.IJavaType;
import net.junos.space.pmd.utils.outline.QualifiedName;

public class ResolvedReference  extends AbstractReference{

	public ResolvedReference(IJavaType type, int beginLine, int endLine) {
		super(beginLine, endLine);
		this.type = type;
	}

	private IJavaType type;

	String getQualifiedTypeName() {
		QualifiedName qualifiedName = type.getQualifiedName();
		if(qualifiedName==null)
			return "Null-Qualified-Name" ;
		
		return qualifiedName.getSimpleName() ;
	}

	@Override
	public String toString() {
		return "Resolved reference: " + getQualifiedTypeName();
	}

	public IJavaType getType() {
		return type;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResolvedReference other = (ResolvedReference) obj;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}
}
