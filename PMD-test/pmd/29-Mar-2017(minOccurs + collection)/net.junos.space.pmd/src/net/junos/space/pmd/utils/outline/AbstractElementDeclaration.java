package net.junos.space.pmd.utils.outline;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class AbstractElementDeclaration implements IDeclaration {
	
	String simpleName ;
	
	QualifiedName qualifiedName ;
	
	String packageName ;

	private boolean isPublic = false ;
	
	private boolean isPrivate = false ;
	
	private boolean isProtected = false ;
	
	private boolean isDefault = true ;
	
	private boolean isStatic ;
	
	private boolean isFinal ;
	
	private int[] lines ;
	
	private List<IJavaAnnotation> annotations = new ArrayList<IJavaAnnotation>() ;
	
	@Override
	public boolean isPublic() {
		return isPublic ;
	}

	@Override
	public boolean isProtected() {
		return isProtected ;
	}

	@Override
	public boolean isPrivate() {
		return isPrivate ;
	}

	@Override
	public boolean isDefault() {
		return isDefault ;
	}

	@Override
	public void setPublic() {
		this.isPublic = true ;
		this.isPrivate = false ;
		this.isProtected = false ;
		this.isDefault = false ;
	}

	@Override
	public void setProtected() {
		this.isPublic = false ;
		this.isPrivate = false ;
		this.isProtected = true ;
		this.isDefault = false ;		
	}

	@Override
	public void setPrivate() {
		this.isPublic = false ;
		this.isPrivate = true ;
		this.isProtected = false ;
		this.isDefault = false ;		
	}

	@Override
	public void setDefault() {
		this.isPublic = false ;
		this.isPrivate = false ;
		this.isProtected = false ;
		this.isDefault = true ;		
	}

	@Override
	public boolean isStatic() {
		return isStatic ;
	}

	@Override
	public void setStatic(boolean isStatic) {
		this.isStatic = isStatic ;		
	}

	@Override
	public boolean isFinal() {
		return isFinal ;
	}

	@Override
	public void setFinal(boolean isFinal) {
		this.isFinal = isFinal ;		
	}

	public String getSimpleName() {
		return simpleName;
	}

	public void setSimpleName(String simpleName) {
		this.simpleName = simpleName;
	}

	public QualifiedName getQualifiedName() {
		return qualifiedName;
	}

	public void setQualifiedName(QualifiedName qualifiedName) {
		this.qualifiedName = qualifiedName;
	}

	public String getPackage() {
		return packageName;
	}

	public void setPackage(String packageName) {
		this.packageName = packageName;
	}
	
	@Override
	public String toString(){
		return qualifiedName.getValues().toString() ;
	}
	
	@Override
	public int[] getLines(){
		return lines ;
	}
	
	public void setLines( int... lines ){
		this.lines = lines ;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (isDefault ? 1231 : 1237);
		result = prime * result + (isFinal ? 1231 : 1237);
		result = prime * result + (isPrivate ? 1231 : 1237);
		result = prime * result + (isProtected ? 1231 : 1237);
		result = prime * result + (isPublic ? 1231 : 1237);
		result = prime * result + (isStatic ? 1231 : 1237);
		result = prime * result + Arrays.hashCode(lines);
		result = prime * result
				+ ((packageName == null) ? 0 : packageName.hashCode());
		result = prime * result
				+ ((qualifiedName == null) ? 0 : qualifiedName.hashCode());
		result = prime * result
				+ ((simpleName == null) ? 0 : simpleName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractElementDeclaration other = (AbstractElementDeclaration) obj;
		if (isDefault != other.isDefault)
			return false;
		if (isFinal != other.isFinal)
			return false;
		if (isPrivate != other.isPrivate)
			return false;
		if (isProtected != other.isProtected)
			return false;
		if (isPublic != other.isPublic)
			return false;
		if (isStatic != other.isStatic)
			return false;
		if (!Arrays.equals(lines, other.lines))
			return false;
		if (packageName == null) {
			if (other.packageName != null)
				return false;
		} else if (!packageName.equals(other.packageName))
			return false;
		if (qualifiedName == null) {
			if (other.qualifiedName != null)
				return false;
		} else if (!qualifiedName.equals(other.qualifiedName))
			return false;
		if (simpleName == null) {
			if (other.simpleName != null)
				return false;
		} else if (!simpleName.equals(other.simpleName))
			return false;
		return true;
	}

	public List<IJavaAnnotation> getAnnotations() {
		return annotations;
	}

	public void setAnnotations(List<IJavaAnnotation> annotations) {
		if(annotations==null)
			return ;
		
		this.annotations = annotations;
	}
	
	public boolean hasAnnotation(String annotationName){
		List<IJavaAnnotation> annotations2 = getAnnotations();
		if(annotations2 == null)
			return false ;
		
		for( IJavaAnnotation ann : annotations2){
			if( ann.getName().equals(annotationName) )
				return true ;
		}		
		return false ;
	}

}
