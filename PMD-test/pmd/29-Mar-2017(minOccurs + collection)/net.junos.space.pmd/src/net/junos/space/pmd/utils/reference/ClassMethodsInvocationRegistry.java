package net.junos.space.pmd.utils.reference;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public class ClassMethodsInvocationRegistry {
	
	public ClassMethodsInvocationRegistry(String qualifiedName) {
		super();
		this.qualifiedName = qualifiedName;
	}

	private final String qualifiedName ;
	
	public String getQualifiedName() {
		return qualifiedName;
	}

	HashMap<String,MethodInvocationData> map1 = new HashMap<String, ClassMethodsInvocationRegistry.MethodInvocationData>() ;
	
	HashMap<String,ArrayList<MethodInvocationData>> map2 = new HashMap<String, ArrayList<MethodInvocationData>>() ;
	
//	public void addInvocation(String methodName, List<String> params, String classQualifiedName, int line)
//	{
//		if( params == null || methodName == null || classQualifiedName == null || line < 0)
//			return ;
//		
//		for( String param : params )
//			if( param == null)
//				return;
//		
//		MethodInvocationData data = map1.get(methodName);
//		if(data == null){
//			ArrayList<MethodInvocationData> dataList = map2.get(methodName);
//			if(dataList == null){
//				data = new MethodInvocationData(methodName,params) ;
//				map1.put(methodName, data) ;
//			}
//			else{
//				for( MethodInvocationData d : dataList ){
//					if(d.getParams().equals(params)){
//						data = d ;
//						break ;
//					}
//				}
//				if(data == null){
//					data = new MethodInvocationData(methodName,params) ;
//					dataList.add(data) ;
//				}
//			}
//		}
//		else if( !data.getParams().equals(params) ){
//			map1.remove(methodName) ;
//			ArrayList<MethodInvocationData> dataList
//					= new ArrayList<ClassMethodsInvocationRegistry.MethodInvocationData>() ;
//			
//			map2.put(methodName, dataList) ;
//			dataList.add(data) ;
//			data = new MethodInvocationData(methodName,params) ;
//			dataList.add(data) ;
//		}
//		data.registerInvocationPosition(classQualifiedName, line) ;
//	}
	
	/**
	 * get all the invocation data on methods with the specified name
	 * @param methodName
	 * @return
	 */
	public List<MethodInvocationData> getInvocationData( String methodName ){
		
		MethodInvocationData data = map1.get(methodName);
		if(data != null){
			ArrayList<MethodInvocationData> lst = new ArrayList<ClassMethodsInvocationRegistry.MethodInvocationData>() ;
			lst.add(data) ;
			return lst ;
		}
		return map2.get(methodName) ;
	}
	
	public MethodInvocationData getInvocationData( String methodName, List<String> params ){//, boolean exact ){
		
		MethodInvocationData data = map1.get(methodName);
		
		if(data != null){
			
			if(data.ignoreParams())
				return data ;

			List<String> params1 = data.getParams();
//			if(exact){			
				if( params1.equals(params) )
					return data ;
//			}
//			else{
//				int matches = countMatches(params, params1);
//				if(matches < 0)
//					return null ;
//				
//				return data ;
//			}
			return null ;
		}
		ArrayList<MethodInvocationData> dataList = map2.get(methodName) ;
//		if(exact){			
			for(MethodInvocationData d : dataList){
				if(d.getParams().equals(params))
					return d ;
			}
			return null ;		
//		}
//		else{
//			
//			int maxMatches = -1 ;
//			MethodInvocationData maxData = null ;
//			
//			for(MethodInvocationData d : dataList){
//				
//				List<String> params1 = d.getParams();				
//				int matches = countMatches(params, params1);
//				if(matches < 0)
//					continue ;
//				
//				if(maxMatches >= matches)
//					continue ;
//				
//				maxMatches = matches ;
//				maxData = d ;
//			}
//			return maxData ;
//		}			
	}

//	private int countMatches(List<String> params, List<String> params1) {
//		int size = params.size();
//		int matches = 0 ;
//						
//		if(params1.size() != size)
//			matches=-1 ;
//		else{			
//		
//			for( int i = 0 ; i < size ; i++ ){						
//				String param = params.get(i);
//				if( param == null )
//					continue ;
//				
//				String param1 = params1.get(i);
//				if(param1.equals(param)){
//					matches++ ;
//					continue ;
//				}
//				matches = -1 ;
//				break ;
//			}
//		}
//		return matches;
//	}
	
	/**
	 * @param params: null means that the method is not supposed to be overloaded and the signature is ignored
	 */
	public void addTargetInvocation(String methodName, List<String> params, String classQualifiedName, IReference ref)
	{
		if( methodName == null || classQualifiedName == null )
			return ;
		
		MethodInvocationData data = map1.get(methodName);
		if(data != null && data.ignoreParams() ){
			data.registerInvocationPosition(classQualifiedName, ref) ;
			return ;
		}
		
		if(params == null)
			return ;
		
		for( String param : params )
			if( param == null)
				return;
		
		if(data == null){
			ArrayList<MethodInvocationData> dataList = map2.get(methodName);
			if(dataList != null){				
				for( MethodInvocationData d : dataList ){
					if(d.getParams().equals(params)){
						d.registerInvocationPosition(classQualifiedName, ref) ;
						break ;
					}
				}
			}
		}
		else if( data.getParams().equals(params) ){
			data.registerInvocationPosition(classQualifiedName, ref) ;
		}

	}
	/**
	 * 
	 * @param methodName
	 * @param params: a list of short names of parameter types. Null if the method is not supposed to be overloaded. 
	 */
	public void registerTarget( String methodName, List<String> params ){
		
		if(params == null){
			map1.put(methodName, new MethodInvocationData(methodName, params)) ;
			map2.remove(methodName) ;
			return ;
		}		
		
		MethodInvocationData data = map1.get(methodName);
		if(data !=null && data.ignoreParams() )
			return ;
		
		if(data==null){
			ArrayList<MethodInvocationData> dataList = map2.get(methodName);
			if(dataList == null){
				map1.put(methodName, new MethodInvocationData(methodName, params)) ;
				return ;
			}
			else{
				for( MethodInvocationData d : dataList ){
					if(d.getParams().equals(params))
						return ;
				}
				dataList.add(new MethodInvocationData(methodName, params)) ;
			}
		}
		
		if(data.getParams().equals(params))
			return ;
		
		ArrayList<MethodInvocationData> dataList = map2.get(methodName);
		if(dataList == null){
			dataList = new ArrayList<ClassMethodsInvocationRegistry.MethodInvocationData>() ;
			map2.put(methodName, dataList) ;
			dataList.add(data) ;
			map1.remove(methodName) ;
		}		
		dataList.add(new MethodInvocationData(methodName, params)) ;
	}
	
	public class MethodInvocationData {
		
		public MethodInvocationData( String methodName, List<String> params) {
			super();
			this.methodName = methodName ;
			this.ignoreParams = params == null ;
			this.params = params!= null ? params : new ArrayList<String>();
			
		}
		
		final boolean ignoreParams ;
		private final String methodName ;

		private final List<String> params ;
		private final HashMap<String,ArrayList<IReference>> invocationsMap
				= new HashMap<String, ArrayList<IReference>>() ; 

		public void registerInvocationPosition(String qualifiedName, IReference ref ){
			
			ArrayList<IReference> lines = invocationsMap.get(qualifiedName) ;
			if( lines == null ){
				lines = new ArrayList<IReference>() ;
				invocationsMap.put(qualifiedName, lines) ;
			}
			
			for( IReference l : lines ){
				if( ref == l )
					return ;
			}
			lines.add(ref) ;		
		}

		public String getMethodName() {
			return methodName;
		}

		public List<String> getParams() {
			return params;
		}

		public Collection<String> getClasses() {
			return invocationsMap.keySet() ;
		}
		
		public List<IReference> getReferences(String qualifiedName){
			return invocationsMap.get(qualifiedName) ;
		}

		public boolean ignoreParams() {
			return ignoreParams;
		}
	}


}
