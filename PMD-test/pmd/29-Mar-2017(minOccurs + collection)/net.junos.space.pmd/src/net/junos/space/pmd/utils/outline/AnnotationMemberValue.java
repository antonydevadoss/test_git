package net.junos.space.pmd.utils.outline;

public class AnnotationMemberValue implements IAnnotationMemberValue {
	
	public AnnotationMemberValue(String value, int kind) {
		super();
		this.value = value;
		this.kind = kind;
	}

	private final String value ;
	
	private final int kind ;
	
	/* (non-Javadoc)
	 * @see net.junos.space.pmd.utils.outline.IAnnotationMemberValue#getStringValue()
	 */
	@Override
	public String getStringValue(){
		return value ;
	}
	
	/* (non-Javadoc)
	 * @see net.junos.space.pmd.utils.outline.IAnnotationMemberValue#getKind()
	 */
	@Override
	public int getKind(){
		return kind ;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + kind;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AnnotationMemberValue other = (AnnotationMemberValue) obj;
		if (kind != other.kind)
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}
}
