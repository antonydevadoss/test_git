package net.junos.space.pmd.rules.j2ee.ejb3;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import net.junos.space.pmd.utils.outline.AbstractOutlineAccessor;
import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IJavaMethodDeclaration;
import net.junos.space.pmd.utils.outline.IJavaType;
import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;
import net.junos.space.pmd.utils.outline.IOutlineContainer;
import net.junos.space.pmd.utils.outline.QualifiedName;
import net.sourceforge.pmd.RuleContext;

public class EnterpriseBeanTimeoutMethodStaticFinalRule extends AbstractOutlineAccessor {

	
	@Override
	public String getMessage() {		
		return "Timeout method of the enterprise bean must not be declared as final or static.";
	}

	@Override
	public void doEnd(RuleContext ctx) {
		
		for( IClassOutline outline : getOutlineContainer().getAllOutlines() ){
			
			String qualifiedName = outline.getQualifiedNameString();
			if(!isEnterpriseBeanEJB3(qualifiedName))
				continue ;
			
			ArrayList<IJavaMethodDeclaration> timeoutMethods = extractAllTimeoutMethods(outline,getOutlineContainer()) ;
						
			for(IJavaMethodDeclaration md: timeoutMethods){
				if(md.isStatic())
					reportError(ctx, outline, md) ;				
				else if(md.isFinal())
					reportError(ctx, outline, md) ;
			}
		}		
	}

	public static ArrayList<IJavaMethodDeclaration> extractAllTimeoutMethods(
			IClassOutline outline, IOutlineContainer outlineContainer)
	{
		List<IJavaTypeDeclaration> allInterfaces = outline.getTypeDeclaration().getAllInterfaces();
		
		HashSet<IJavaMethodDeclaration> timeoutMethods = new HashSet<IJavaMethodDeclaration>() ;
		IJavaMethodDeclaration ejbTimeoutMethod = getEjbTimeoutMethod(outline, allInterfaces,outlineContainer);
		if(ejbTimeoutMethod != null)
			timeoutMethods.add(ejbTimeoutMethod) ;
		
		ArrayList<IJavaMethodDeclaration> annotatedMethods = getAnnotatedTimeoutMethods(outline) ;
		timeoutMethods.addAll(annotatedMethods) ;	
		ArrayList<IJavaMethodDeclaration> result = new ArrayList<IJavaMethodDeclaration>(timeoutMethods);
		return result;
	}

	private static ArrayList<IJavaMethodDeclaration> getAnnotatedTimeoutMethods(
			IClassOutline outline) {
		
		ArrayList<IJavaMethodDeclaration> result = new ArrayList<IJavaMethodDeclaration>() ;
		
		List<IJavaMethodDeclaration> methods = outline.getMethods();
		if(methods == null)
			return result ;
		
		for(  IJavaMethodDeclaration md : methods){
			if(!md.hasAnnotation("Timeout"))
				continue ;
			
			result.add(md) ;			
		}		
		return result;
	}

	protected static IJavaMethodDeclaration getEjbTimeoutMethod(IClassOutline outline,
			List<IJavaTypeDeclaration> allInterfaces, IOutlineContainer outlineContainer) {
		
		if(allInterfaces == null || allInterfaces.isEmpty())
			return null ;
		
		IJavaMethodDeclaration ejbTimeoutMethod = null ;
		List<IClassOutline> definedSuperclasses = outline.getDefinedSuperclasses(outlineContainer);
		ArrayList<IClassOutline> outlines = new ArrayList<IClassOutline>() ;
		outlines.add(outline);
		outlines.addAll(definedSuperclasses) ;
		
		for( IJavaTypeDeclaration iDecl : allInterfaces ){
			
			QualifiedName qName = iDecl.getQualifiedName();
			if(!qName.hasValue("javax.ejb.TimedObject"))
				continue ;
			
			for(IClassOutline co : outlines){			
			
				List<IJavaMethodDeclaration> methodsBySimpleName = co.getMethodsBySimpleName("ejbTimeout");
				if(methodsBySimpleName==null)
					break ;
				
				for( IJavaMethodDeclaration md : methodsBySimpleName ){
					if( md.getReturnType() != null )
						continue ;
					
					List<IJavaType> params = md.getParameters();
					if(params == null || params.size() != 1 )
						continue ;
					
					IJavaType paramType = params.get(0);
					QualifiedName qn = paramType.getQualifiedName();
					if( qn.getSimpleName().equals("Timer") ){
						ejbTimeoutMethod = md ;
						break ;
					}
				}
				break ;
			}
		}
		return ejbTimeoutMethod;
	}
}
