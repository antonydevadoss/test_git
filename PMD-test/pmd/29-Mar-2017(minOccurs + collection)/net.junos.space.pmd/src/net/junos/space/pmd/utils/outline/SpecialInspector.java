package net.junos.space.pmd.utils.outline;

import java.util.ArrayList;
import java.util.HashMap;

import net.sourceforge.pmd.RuleContext;
import net.sourceforge.pmd.lang.java.ast.AbstractJavaNode;

public abstract class SpecialInspector extends AbstractOutlineAccessor {

	private static HashMap<String, HashMap<String, ArrayList<Integer>>> map = null;

	public SpecialInspector() {
		super();
	}

	public void doStart(RuleContext ctx) {
		map=null;		
	}

	public void doEnd(RuleContext ctx) {
		
		if(map==null)
			return ;
		
		HashMap<String, ArrayList<Integer>> ownMap = map.get(getOwnKey());
		if(ownMap==null)
			return ;
		
		for(String qName : ownMap.keySet()){
			
			IClassOutline classOutline = getOutlineContainer().getClassOutline(qName);
			if(classOutline==null)
				continue ;
			
			IJavaTypeDeclaration decl = classOutline.getTypeDeclaration();
			if( !mustBeChecked(decl) )
				continue ;
			
			ArrayList<Integer> lst = ownMap.get(qName);
			if(lst==null)
				continue;
			
			for(int line : lst){
				reportError(ctx, classOutline, line, line);
			}
		}
	}

	abstract protected boolean mustBeChecked(IJavaTypeDeclaration decl);

	protected void registerPossibleViolation(AbstractJavaNode node) {	
		
		IClassOutline currentOutline = getCurrentOutline();
		if(currentOutline==null)
			return ;
		
		int line = node.getBeginLine();
		String qNmae = currentOutline.getQualifiedNameString() ;
		
		if(map==null)
			map = new HashMap<String, HashMap<String,ArrayList<Integer>>>() ;
		
		String ownKey = getOwnKey();
		HashMap<String, ArrayList<Integer>> ownMap = map.get( ownKey );
		if(ownMap==null){
			ownMap=new HashMap<String, ArrayList<Integer>>() ;
			map.put(ownKey, ownMap) ;
		}
		
		ArrayList<Integer> lst = ownMap.get(qNmae);
		if(lst==null){
			lst = new ArrayList<Integer>() ;
			ownMap.put(qNmae, lst);
		}
		
		lst.add(line);
	}

	protected abstract String getOwnKey() ;
}