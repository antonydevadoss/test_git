package net.junos.space.pmd.rules.j2ee;

import java.util.List;

import net.junos.space.pmd.utils.outline.AbstractOutlineAccessor;
import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IJavaMethodDeclaration;
import net.junos.space.pmd.utils.outline.IJavaType;
import net.sourceforge.pmd.RuleContext;

public class MessageBeanEjbCreateMethodParamsRule extends AbstractOutlineAccessor {
	

	@Override
	public void doEnd(RuleContext ctx) {
		
l0:		for (IClassOutline outline : getOutlineContainer().getAllOutlines()) {
			if ( !isMessageDrivenBeanEJB2(outline.getTypeDeclaration()) )
				continue ;
			
			List<IClassOutline> definedSuperclasses = outline.getDefinedSuperclasses(getOutlineContainer());
			definedSuperclasses.add(outline) ;
			
			for( IClassOutline o : definedSuperclasses){			
				for( IJavaMethodDeclaration md : o.getMethodsBySimpleName("ejbCreate") ){
					List<IJavaType> params = md.getParameters();
					if (params != null && !md.getParameters().isEmpty()) {
						reportError(ctx, outline, md);
						continue l0;
					}					
				}
			}
		}
		super.doEnd(ctx);
	}
	
	
	@Override
	public String getMessage() {
		return "Do not declare arguments for an ejbCreate method of a Message Bean";
	}
}
