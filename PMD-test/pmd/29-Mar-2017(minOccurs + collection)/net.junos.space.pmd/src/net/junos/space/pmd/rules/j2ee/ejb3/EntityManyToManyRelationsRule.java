package net.junos.space.pmd.rules.j2ee.ejb3;

import net.junos.space.pmd.utils.outline.AbstractEntityRule;
import net.junos.space.pmd.utils.outline.CollectionAndMapDescendants;
import net.junos.space.pmd.utils.outline.EjbEntity;
import net.junos.space.pmd.utils.outline.IDeclaration;
import net.junos.space.pmd.utils.outline.IJavaType;
import net.sourceforge.pmd.RuleContext;

public class EntityManyToManyRelationsRule extends EntityOneToManyRelationsRule {
	
	@Override
	public String getMessage() {
		return "Relationship modeling annotations @OneToMany and @ManyToMany should be applied to fields or properties of Collection<@Entity> types.";
	}
	
	public boolean isProperMember(IDeclaration field) {
		return field.hasAnnotation("OneToMany") ||
				field.hasAnnotation("javax.persistence.OneToMany") ||
				field.hasAnnotation("ManyToMany") ||
				field.hasAnnotation("javax.persistence.ManyToMany");
	}

	public void validateType(RuleContext ctx, EjbEntity ejbEntity, IJavaType type, IDeclaration decl)
	{
		if (type != null 
				&& type.getGenericParameters() != null 
				&& !type.getGenericParameters().isEmpty()) {
l0:			for (String fieldTypeString : type.getQualifiedName().getValues()) {
				if (CollectionAndMapDescendants.javaUtilCollectionAndMapDescendants.contains(fieldTypeString)) {
				
					IJavaType genericType = type.getGenericParameters().get(0);
					
					for (String genericTypeString : genericType.getQualifiedName().getValues()) {
						
						if(genericTypeString.startsWith("java.lang.")){
							reportError(ctx,ejbEntity.getOutline(), decl) ;
							break l0;
						}
						
						if( getOutlineContainer().getClassOutline(genericTypeString) == null )
							continue ;
												
						if (!AbstractEntityRule.entityRegistry.containsKey(genericTypeString)){
							reportError(ctx,ejbEntity.getOutline(), decl) ;
							break l0;
						}
					}
				}
			}
		}
	}
}
