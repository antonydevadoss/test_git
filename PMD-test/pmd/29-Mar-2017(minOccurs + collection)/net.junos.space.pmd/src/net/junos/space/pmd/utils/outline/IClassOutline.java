package net.junos.space.pmd.utils.outline;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public interface IClassOutline {
	
	String getQualifiedNameString() ;
	
	List<String> getImports() ;
	
	List<String> getPackageImports() ;
	
	void setPackageImports( List<String> packageImports ) ;
	
	void setImports( Collection<String> imports ) ;
	
	IJavaTypeDeclaration getTypeDeclaration() ;
	
	void setTypeDeclaration( IJavaTypeDeclaration type ) ;
	
	List<IJavaMethodDeclaration> getMethods() ;
	
	List<IJavaFieldDeclaration> getFields() ;
	
	void putMethod( IJavaMethodDeclaration method ) ;
	
	void putField(IJavaFieldDeclaration field);
	
	void setMethods( Collection<IJavaMethodDeclaration> methods ) ;
	
	void setFields( Collection<IJavaFieldDeclaration> fields ) ;

	void setFilePath(String filePath);
	
	String getFilePath();

	IJavaFieldDeclaration getFieldBySimpleName(String name);
	
	List<IJavaMethodDeclaration> getMethodsBySimpleName(String name) ;

	void setConstructors(ArrayList<IJavaMethodDeclaration> constructors);
	
	List<IJavaMethodDeclaration> getConstructors() ;
	
	QualifiedName getQualifiedName() ;

	IJavaMethodDeclaration getMethod(String methodName, IJavaType returnType, List<IJavaType> params);
	
	List<IClassOutline> getDefinedSuperclasses(IOutlineContainer outlineContainer) ;
	
	boolean hasAllSuperclassesDefined( IOutlineContainer container ) ;
}
