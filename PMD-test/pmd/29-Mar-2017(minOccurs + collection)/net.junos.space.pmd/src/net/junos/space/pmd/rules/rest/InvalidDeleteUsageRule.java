/*******************************************************************************
 * FILE NAME: InvalidDeleteUsageRule.java
 * PURPOSE: This is Rule-20 of document 
 *
 *
 * Revision History: 
 * DATE:           				  AUTHOR:              	CHANGE:  
 * Tue Feb 23, 2011               Atul Bajpai       	Initial Version  
 * 
 * 
 ******************************************************************************/
package net.junos.space.pmd.rules.rest;

import net.junos.space.pmd.utils.PMDConstants;import net.junos.space.pmd.utils.PMDUtils;import net.sourceforge.pmd.RuleContext;import net.sourceforge.pmd.lang.java.ast.ASTMethodDeclarator;import net.sourceforge.pmd.lang.java.rule.AbstractJavaRule;import org.apache.log4j.Logger;

/**
 * This class defines a rule which verifies that if @DELETE annotation is defined on
 * method, then is should neither expect any DTO nor should have @Consumes annotation
 */
public class InvalidDeleteUsageRule extends AbstractJavaRule {

	/**
	 * Logger
	 */
	private static Logger log = Logger.getLogger(InvalidDeleteUsageRule.class);

	/**
	 * This method checks:
	 * 	a> Method is rest or not
	 *  b> @DELETE annotation exists or not
	 *  c> 	i>  Method has DTO as input or not 
	 *  	ii> @Consumes annotation defined on method or not
	 *  d> if c> is true then log error
	 */
	public Object visit(ASTMethodDeclarator methodNode, Object data) {
		RuleContext ctx = (RuleContext) data;
		String methodName = methodNode.getImage();
		try {
			boolean isRestMethod = false;
			boolean isDeleteExists = false;
			boolean isConsumesExists = false;
			boolean isDTOExists = false;

			// checks whether the method is REST or not.
			isRestMethod = PMDUtils.isRestMethod(methodNode);
			if (isRestMethod) {
				isDeleteExists = PMDUtils.isMethodAnnotationExists(methodNode, PMDConstants.HttpMethod.DELETE.toString());
				if (isDeleteExists) {
					isConsumesExists = PMDUtils.isMethodAnnotationExists(methodNode, PMDConstants.CONSUMES_ANNOTATION);
					isDTOExists = PMDUtils.hasDTOParameters(methodNode);
				}
			}
			
			if(log.isDebugEnabled()){
				StringBuilder builder = new StringBuilder();
				builder.append("source file=").append(ctx.getSourceCodeFilename()).append(",method=").append(methodName);
				builder.append(",isRestMethod=").append(isRestMethod).append(",isDeleteExists=").append(isDeleteExists).append(
						",isConsumesExists=").append(isConsumesExists).append(",isDTOExists=").append(isDTOExists);
				log.debug(builder.toString());
			}
			
			if (isRestMethod && isDeleteExists && (isConsumesExists || isDTOExists))
				addViolation(data, methodNode);
		} catch (Exception e) {
			log.error("Exception in executing method " + methodName + " of " + ctx.getSourceCodeFilename(), e);
		}
		return super.visit(methodNode, data);
	}
}
