package net.junos.space.pmd.rules.j2ee;

import java.util.HashSet;
import java.util.Set;

import net.junos.space.pmd.rules.AbstractJavaRuleWithUselessTypeResolution;
import net.sourceforge.pmd.RuleContext;
import net.sourceforge.pmd.lang.dfa.DataFlowNode;
import net.sourceforge.pmd.lang.java.ast.ASTAssignmentOperator;
import net.sourceforge.pmd.lang.java.ast.ASTClassOrInterfaceDeclaration;
import net.sourceforge.pmd.lang.java.ast.ASTLocalVariableDeclaration;
import net.sourceforge.pmd.lang.java.ast.ASTMethodDeclaration;
import net.sourceforge.pmd.lang.java.ast.ASTName;
import net.sourceforge.pmd.lang.java.ast.ASTPrimaryExpression;
import net.sourceforge.pmd.lang.java.ast.ASTPrimaryPrefix;
import net.sourceforge.pmd.lang.java.ast.ASTPrimarySuffix;
import net.sourceforge.pmd.lang.java.ast.ASTStatement;
import net.sourceforge.pmd.lang.java.ast.ASTStatementExpression;
import net.sourceforge.pmd.lang.java.ast.ASTType;
import net.sourceforge.pmd.lang.java.ast.ASTVariableDeclarator;
import net.sourceforge.pmd.lang.java.ast.ASTVariableInitializer;
import net.sourceforge.pmd.lang.java.ast.TypeNode;

@SuppressWarnings("unused")
public class TestRule extends AbstractJavaRuleWithUselessTypeResolution {

	private RuleContext rc;
	
	private static Set<String> foundClasses = new HashSet<String>();
	
	@Override
    public void start(RuleContext ctx) {
           foundClasses.clear();
           super.start(ctx);
    }
	@Override
    public void end(RuleContext ctx) {
            System.out.println(foundClasses);
            super.end(ctx);
    }

	@Override
	public Object visit(ASTClassOrInterfaceDeclaration node, Object data) {
		foundClasses.add(node.getImage());
		return super.visit(node, data);
	}

	@Override
	public Object visit(ASTPrimaryPrefix node, Object data) {
		DataFlowNode dataflow = node.getDataFlowNode();
		if (TypeNode.class.isAssignableFrom(node.getClass()) && ((TypeNode) node).getType() != null) {
			              Class<?> c = ((TypeNode) node).getType();
			              int k = 0;
			              k++;
		}
		return super.visit(node, data);
	}
	
	 public Object visit(ASTMethodDeclaration methodDeclaration, Object data) {
		 this.rc = (RuleContext) data;
		 
		   
		           final DataFlowNode node = methodDeclaration.getDataFlowNode();
		   
//		  addViolationWithMessage(data, methodDeclaration, "Method...");
		           return super.visit(methodDeclaration, data);
	 }

	@Override
	public Object visit(ASTName node, Object data) {
		if (TypeNode.class.isAssignableFrom(node.getClass()) && ((TypeNode) node).getType() != null) {
            Class<?> c = ((TypeNode) node).getType();
            int k = 0;
            k++;
		}
		return super.visit(node, data);
	}

	@Override
	public Object visit(ASTLocalVariableDeclaration node, Object data) {
		if (TypeNode.class.isAssignableFrom(node.getClass()) && ((TypeNode) node).getType() != null) {
            Class<?> c = ((TypeNode) node).getType();
            int k = 0;
            k++;
		}
		return super.visit(node, data);
	}

	@Override
	public Object visit(ASTType node, Object data) {
		if (TypeNode.class.isAssignableFrom(node.getClass()) && ((TypeNode) node).getType() != null) {
            Class<?> c = ((TypeNode) node).getType();
            int k = 0;
            k++;
		}
		return super.visit(node, data);
	}

	@Override
	public Object visit(ASTVariableDeclarator node, Object data) {
		if (TypeNode.class.isAssignableFrom(node.getClass()) && ((TypeNode) node).getType() != null) {
            Class<?> c = ((TypeNode) node).getType();
            int k = 0;
            k++;
		}
		return super.visit(node, data);
	}

	
	@Override
	public Object visit(ASTVariableInitializer node, Object data) {
		if (TypeNode.class.isAssignableFrom(node.getClass()) && ((TypeNode) node).getType() != null) {
            Class<?> c = ((TypeNode) node).getType();
            int k = 0;
            k++;
		}
		return super.visit(node, data);
	}

	@Override
	public Object visit(ASTAssignmentOperator node, Object data) {
		if (TypeNode.class.isAssignableFrom(node.getClass()) && ((TypeNode) node).getType() != null) {
            Class<?> c = ((TypeNode) node).getType();
            int k = 0;
            k++;
		}
		return super.visit(node, data);
	}

	@Override
	public Object visit(ASTPrimaryExpression node, Object data) {
		if (TypeNode.class.isAssignableFrom(node.getClass()) && ((TypeNode) node).getType() != null) {
            Class<?> c = ((TypeNode) node).getType();
            int k = 0;
            k++;
		}
		return super.visit(node, data);
	}

	@Override
	public Object visit(ASTPrimarySuffix node, Object data) {
		if (TypeNode.class.isAssignableFrom(node.getClass()) && ((TypeNode) node).getType() != null) {
            Class<?> c = ((TypeNode) node).getType();
            int k = 0;
            k++;
		}
		return super.visit(node, data);
	}

	@Override
	public Object visit(ASTStatement node, Object data) {
		if (TypeNode.class.isAssignableFrom(node.getClass()) && ((TypeNode) node).getType() != null) {
            Class<?> c = ((TypeNode) node).getType();
            int k = 0;
            k++;
		}
		return super.visit(node, data);
	}

	@Override
	public Object visit(ASTStatementExpression node, Object data) {
		if (TypeNode.class.isAssignableFrom(node.getClass()) && ((TypeNode) node).getType() != null) {
            Class<?> c = ((TypeNode) node).getType();
            int k = 0;
            k++;
		}
		return super.visit(node, data);
	}
	
	
}
