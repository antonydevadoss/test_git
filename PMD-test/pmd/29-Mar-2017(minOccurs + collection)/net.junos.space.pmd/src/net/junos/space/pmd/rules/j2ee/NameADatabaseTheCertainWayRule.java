package net.junos.space.pmd.rules.j2ee;


import org.w3c.dom.Element;
import net.junos.space.pmd.rules.xml.AbstractXMLRule;
import net.junos.space.pmd.rules.xml.SimpleXpathRule;

public class NameADatabaseTheCertainWayRule extends SimpleXpathRule {
	
	

	@Override
	protected String getXpathExpression() {
		return "//resource-ref/res-ref-name";
	}

	public String getMessage() {
		return "Name a database like [Name]DB. The database name used by JDBC resource references must have a suffix DB.";
	}

	protected String getTargetXMLName() {
		return AbstractXMLRule.XML_NAME_EJB_JAR;
	}

	@Override
	protected boolean isValid(Element element) {		
		String str = element.getTextContent();
		if(str == null)
			return false ;
		
		return str.endsWith("DB") ;
	}
}
