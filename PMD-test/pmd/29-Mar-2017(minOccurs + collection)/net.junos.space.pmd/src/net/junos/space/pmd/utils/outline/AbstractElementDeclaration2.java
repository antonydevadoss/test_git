package net.junos.space.pmd.utils.outline;

import java.util.ArrayList;
import java.util.Collection;


public abstract class AbstractElementDeclaration2 extends AbstractElementDeclaration implements ICanBeGeneric, ICanBeAbstract {
	
	private boolean isAbstract ;
	
	private int genericParamsCount = 0;
	
	private ArrayList<String> paramNames ;
	
	
	private IJavaTypeDeclaration ownerClass ;
	
	@Override
	public boolean isAbstract() {
		return this.isAbstract ;
	}

	@Override
	public void setAbstract(boolean isAbstract) {
		this.isAbstract = isAbstract ;	
	}

	@Override
	public int getGenericParamsCount() {
		return genericParamsCount;
	}

	@Override
	public void setGenericParamsCount(int genericParamsCount ) {
		this.genericParamsCount = genericParamsCount;
	}

	@Override
	public ArrayList<String> getGenericParamNames() {
		return paramNames;
	}
	
	@Override
	public IJavaTypeDeclaration getOwner() {
		return ownerClass;
	}

	@Override
	public void setOwner(IJavaTypeDeclaration ownerClass) {
		this.ownerClass = (IJavaTypeDeclaration) ownerClass ;		
	}

	@Override
	public void setGenericParamNames(Collection<String> paramNames) {
		if( paramNames != null )
			this.paramNames = new ArrayList<String>(paramNames);
		else
			this.paramNames = null ;
	}
	
	@Override
	public int getOrder( String paramName ){
		if(paramNames == null)
			return -1 ;
		
		return paramNames.indexOf(paramName) ;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + genericParamsCount;
		result = prime * result + (isAbstract ? 1231 : 1237);
		result = prime * result
				+ ((paramNames == null) ? 0 : paramNames.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractElementDeclaration2 other = (AbstractElementDeclaration2) obj;
		if (genericParamsCount != other.genericParamsCount)
			return false;
		if (isAbstract != other.isAbstract)
			return false;
		if (paramNames == null) {
			if (other.paramNames != null)
				return false;
		} else if (!paramNames.equals(other.paramNames))
			return false;
		return true;
	}


}
