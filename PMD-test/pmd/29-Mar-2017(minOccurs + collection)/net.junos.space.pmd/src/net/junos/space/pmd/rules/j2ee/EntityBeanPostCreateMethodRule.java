package net.junos.space.pmd.rules.j2ee;

import java.util.ArrayList;
import java.util.List;

import net.junos.space.pmd.utils.outline.AbstractOutlineAccessor;
import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IJavaMethodDeclaration;
import net.junos.space.pmd.utils.outline.IJavaType;
import net.sourceforge.pmd.RuleContext;

public class EntityBeanPostCreateMethodRule extends AbstractOutlineAccessor {
	
	@Override
	public void doEnd(RuleContext ctx) {
		for (IClassOutline outline : getOutlineContainer().getAllOutlines()) {
			if ( !isEntityBeanEJB2(outline.getTypeDeclaration()))
				continue ;
			
			List<IClassOutline> definedSuperclasses = outline.getDefinedSuperclasses(getOutlineContainer());
			List<IClassOutline> hierarchy = new ArrayList<IClassOutline>() ;
			hierarchy.add(outline) ;
			hierarchy.addAll(definedSuperclasses) ;
			
			List<IJavaMethodDeclaration> ejbPostCreateMethods = new ArrayList<IJavaMethodDeclaration>() ;
			List<IJavaMethodDeclaration> ejbCreateMethods = new ArrayList<IJavaMethodDeclaration>() ;
			for( IClassOutline o : hierarchy ){				
				ejbPostCreateMethods.addAll( o.getMethodsBySimpleName("ejbPostCreate") );
				ejbCreateMethods.addAll( o.getMethodsBySimpleName("ejbCreate") );
			}
			
			for (IJavaMethodDeclaration ejbPostCreateMethod : ejbPostCreateMethods) {
				boolean hasMatched = false;
				
				for (IJavaMethodDeclaration ejbCreateMethod : ejbCreateMethods)
				{
					if (ejbPostCreateMethod.getParameterNames().size() == ejbCreateMethod.getParameters().size()) {
						boolean hasDifferentParams = false;
						for (int i = 0; i < ejbPostCreateMethod.getParameterNames().size(); i++) {
							IJavaType ejbPostCreateParam = ejbPostCreateMethod.getParameters().get(i);
							IJavaType ejbCreateParam = ejbCreateMethod.getParameters().get(i);
							boolean hasSameTypes = false;
							for (String ejbPostCreateParamQName : ejbPostCreateParam.getQualifiedName().getValues()) {
								for (String ejbCreateParamQName : ejbCreateParam.getQualifiedName().getValues()) {
									if (ejbCreateParamQName.equals(ejbPostCreateParamQName)) {
										hasSameTypes = true;
									}
								}
							}
							
							if (!hasSameTypes) {
								hasDifferentParams = true;
								break;
							}
						}
						if (!hasDifferentParams) {
						hasMatched = true;
							break;
						}
					}
				}
				if (!hasMatched) {
				reportError(ctx, outline, ejbPostCreateMethod);
			}
			}
		}
		super.doEnd(ctx);
	}
	
	@Override
	public String getMessage() {
		return "Match an ejbPostCreate method to an ejbCreate method for an Entity Bean";
	}
}
