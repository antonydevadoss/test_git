package net.junos.space.pmd.utils.outline;

import java.util.List;

public interface IOutlineContainer {
	
	IClassOutline getClassOutline( String qualifiedName ) ;
	
	List<IClassOutline> getMultipleOutlines( String simpleName ) ;
	
	void putClassOutline( IClassOutline outline ) ;
	
	IJavaTypeDeclaration getTypeDeclaration(String qualiFiedName) ;
	
	void putTypeDeclaration( IJavaTypeDeclaration type ) ;
	
	List<IJavaTypeDeclaration> getMultipleDeclarations( String simpleName ) ;
	
	List<IClassOutline> getAllOutlines() ;
	
	List<IJavaTypeDeclaration> getAllTypeDeclarations() ;
	
	void clear() ;

	void setPath(IClassOutline rootOutline, String path);
	
	IClassOutline getOutlineByPath(String path) ;
	
	List<IClassOutline> getPackageContent(String packageName) ;
	
	void setComplete(boolean isComplete) ;
	
	boolean isComplete() ;

	IJavaTypeDeclaration getTypeDeclaration(String simpleName,String contextClassQualifiedName);
	
	List<IJavaTypeDeclaration> getUnresolvedDeclarations() ;
}
