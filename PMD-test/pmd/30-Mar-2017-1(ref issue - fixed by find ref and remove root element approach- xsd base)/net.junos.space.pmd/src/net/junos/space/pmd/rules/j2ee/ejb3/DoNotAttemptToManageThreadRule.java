package net.junos.space.pmd.rules.j2ee.ejb3;

public class DoNotAttemptToManageThreadRule extends
			net.junos.space.pmd.rules.j2ee.DoNotAttemptToManageThreadRule {

	protected boolean mustBeChecked(String className) {		
		return isEnterpriseBeanSuperclassEJB3(className);
	}
}
