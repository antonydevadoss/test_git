package net.junos.space.pmd.rules.j2ee;


import org.w3c.dom.Element;
import net.junos.space.pmd.rules.xml.AbstractXMLRule;
import net.junos.space.pmd.rules.xml.SimpleXpathRule;

public class UseACorrectNameForAnEJBEnvironmentReferenceNameRule extends SimpleXpathRule {
	
	

	@Override
	protected String getXpathExpression() {
		return "//ejb-ref/ejb-ref-name";
	}

	public String getMessage() {
		return "Use A Correct Name For An EJB Environment Reference Name. All references to Enterprise Java Beans must be organized in the ejb subcontext of the application component's environment.";
	}

	protected String getTargetXMLName() {
		return AbstractXMLRule.XML_NAME_EJB_JAR;
	}

	@Override
	protected boolean isValid(Element element) {		
		String str = element.getTextContent();
		if(str == null)
			return false ;
		
		str = str.toLowerCase() ;
		return str.startsWith("ejb/");
	}
}
