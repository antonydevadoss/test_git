package net.junos.space.pmd.utils.outline;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


//This class can be used only with properly configured OutlineAccessor 
public class EjbEntity {
	
	
	public EjbEntity( IClassOutline outline )
	{
		this.outline = outline ;		
	}

	final IClassOutline outline;
	
//	private Boolean hasFileldAccess = null ;
//	
//	private Boolean hasPropertyAccess = null ;
	
	private Map<String,ArrayList<IJavaMethodDeclaration>> persistentPropertiesMap = null ;
	
	public String getQualifiedName() {
		return outline.getQualifiedNameString() ;
		
	}	
	
	public boolean hasPropertyAccess(){
		
		String accessType = getAccessTypeFromAnnotation();
		boolean result = (accessType!=null) && accessType.equals("AccessType.PROPERTY") ;
		return result ;
	}
	
	public boolean hasFieldAccess(){
		
		String accessType = getAccessTypeFromAnnotation();
		boolean result = (accessType==null) || accessType.equals("AccessType.FIELD") ;
		return result ;
	}
	
	public List<IJavaFieldDeclaration> getMappingFields(){
		
		ArrayList<IClassOutline> lst = new ArrayList<IClassOutline>() ;
		lst.add(outline) ;
		lst.addAll( outline.getDefinedSuperclasses( BasicOutlineContainer.getInstance() ) ) ;
		
		ArrayList<IJavaFieldDeclaration> fList = new ArrayList<IJavaFieldDeclaration>() ;
		for(IClassOutline o : lst){		
			fList.addAll(o.getFields() );
		}
		List<IJavaFieldDeclaration> mappingFields = hasMappingAnnotations(fList);		
		return mappingFields ;
	}
	
	public List<IJavaMethodDeclaration> getMappingMethods(){
		
		ArrayList<IClassOutline> lst = new ArrayList<IClassOutline>() ;
		lst.add(outline) ;
		lst.addAll( outline.getDefinedSuperclasses( BasicOutlineContainer.getInstance() ) ) ;
		
		ArrayList<IJavaMethodDeclaration> mList = new ArrayList<IJavaMethodDeclaration>() ;
		for(IClassOutline o : lst){		
			mList.addAll(o.getMethods() );
		}		
		List<IJavaMethodDeclaration> mappingMethods = hasMappingAnnotations(mList);		
		return mappingMethods ;
	}
	
	private <T extends IDeclaration>List<T> hasMappingAnnotations( List<T> declarations ){
		
		ArrayList<T> result = new ArrayList<T>();
		
		while(true){			
			if(declarations==null||declarations.isEmpty())
				break ;
	
			for( T decl : declarations ){
		
				List<IJavaAnnotation> fAnnotations = decl.getAnnotations();
				if(fAnnotations == null)
					continue ;
				
				for(IJavaAnnotation fAnn : fAnnotations){
					String annName = fAnn.getName();
					if( !AbstractEntityRule.fieldAndMethodMappingAnnotations.contains(annName) )
						continue ;
					
					result.add(decl) ;
				}
			}
			break;
		}
		return result ;
	}
	
	protected String getAccessTypeFromAnnotation()
	{
		String result = null ;
		while(true){
			List<IJavaAnnotation> annotations = this.outline.getTypeDeclaration().getAnnotations();
			if(annotations == null)
				break ;			
			
			for(IJavaAnnotation ann : annotations){
				if(!ann.getName().equals("Access"))
					continue ;
				
				IAnnotationMemberValue val = ann.getMemberValue("value");
				if(val == null)
					break ;
				
				result = val.getStringValue();
				break ;
			}
			break ;
		}
		return result ;
	}

	public ArrayList<IJavaFieldDeclaration> getPersistentFields() {
		
		ArrayList<IJavaFieldDeclaration> persistentFields = new ArrayList<IJavaFieldDeclaration>() ;
		while(true){
			
			if( !hasFieldAccess() )
				break ;
			
			ArrayList<IJavaFieldDeclaration> nonTransienFields = getNonTransientFields();
			persistentFields = nonTransienFields ;
			break ;
		}
		
		return persistentFields;
	}

	public ArrayList<IJavaFieldDeclaration> getNonTransientFields()
	{
		
		
		ArrayList<IClassOutline> lst = new ArrayList<IClassOutline>() ;
		lst.add(outline) ;
		lst.addAll( outline.getDefinedSuperclasses( BasicOutlineContainer.getInstance() ) ) ;

		ArrayList<IJavaFieldDeclaration> allFields = new ArrayList<IJavaFieldDeclaration>() ;
		for(IClassOutline o : lst){		
			allFields.addAll(o.getFields() );
		}
		
		ArrayList<IJavaFieldDeclaration> nonTransienFields = new ArrayList<IJavaFieldDeclaration>() ;
		for(IJavaFieldDeclaration fd : allFields){
				
			if(fd.isStatic())
				continue ;
				
			if(fd.isTransient())
				continue ;
				
			if( hasTransientAnnotation(fd) )
				continue ;
				
			nonTransienFields.add(fd) ;				
		}		
		return nonTransienFields;
	}

	
	public ArrayList<IJavaMethodDeclaration> getNonTransientProperties() {
		
		ArrayList<IJavaMethodDeclaration> persistentProperties = new ArrayList<IJavaMethodDeclaration>() ;
		while(true){
			
			if( !hasPropertyAccess() )
				break ;
			
			ArrayList<IClassOutline> lst = new ArrayList<IClassOutline>() ;
			lst.add(outline) ;
			lst.addAll( outline.getDefinedSuperclasses( BasicOutlineContainer.getInstance() ) ) ;

			ArrayList<IJavaMethodDeclaration> allMethods = new ArrayList<IJavaMethodDeclaration>() ; 
			for(IClassOutline o : lst){		
				allMethods.addAll(o.getMethods() );
			}
			
			persistentProperties = new ArrayList<IJavaMethodDeclaration>() ;
			for(IJavaMethodDeclaration md : allMethods){
				
				if( hasTransientAnnotation(md) )
					continue ;
				
				persistentProperties.add(md) ;
			}			
			break ;
		}
		return persistentProperties;
	}
	
	public Map<String,ArrayList<IJavaMethodDeclaration>> getNonTransientPropertiesMap() {
		
		if(persistentPropertiesMap != null)
			return persistentPropertiesMap ;
		
		persistentPropertiesMap = new HashMap<String, ArrayList<IJavaMethodDeclaration>>() ;
		for( IJavaMethodDeclaration mDecl : getNonTransientProperties() ){
			
			String name = mDecl.getSimpleName();
			ArrayList<IJavaMethodDeclaration> lst = persistentPropertiesMap.get(name);
			if(lst == null){
				lst = new ArrayList<IJavaMethodDeclaration>() ;
				persistentPropertiesMap.put(name, lst) ;
			}
			lst.add(mDecl) ;			
		}
		
		return persistentPropertiesMap ;
	}
	
	private boolean hasTransientAnnotation(IDeclaration decl) {
		
		boolean result = false ;
		while(true){
			List<IJavaAnnotation> annotations = decl.getAnnotations();
			if(annotations == null)
				break ;
			
			for(IJavaAnnotation ann : annotations){
				if(!ann.getName().equals("Transient"))
					continue ;
				
				result = true ;				
				break ;
			}
			break ;
		}
		return result ;
	}

	public IClassOutline getOutline() {
		return outline;
	}
}
