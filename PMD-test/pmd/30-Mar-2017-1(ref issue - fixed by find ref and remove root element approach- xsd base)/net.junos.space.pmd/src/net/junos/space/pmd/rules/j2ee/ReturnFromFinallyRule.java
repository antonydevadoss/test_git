package net.junos.space.pmd.rules.j2ee;

import net.junos.space.pmd.rules.AbstractJavaRuleWithUselessTypeResolution;
import net.sourceforge.pmd.lang.java.ast.ASTFinallyStatement;
import net.sourceforge.pmd.lang.java.ast.ASTReturnStatement;
import net.sourceforge.pmd.lang.java.ast.ASTTryStatement;

public class ReturnFromFinallyRule extends AbstractJavaRuleWithUselessTypeResolution {

	/**
	 * JAC_062
	 */
	
	@Override
	public Object visit(ASTReturnStatement node, Object data) {
		try {
			ASTFinallyStatement finallyStatement = node.getFirstParentOfType(ASTFinallyStatement.class);
			if (finallyStatement != null)
			{
				addViolationWithMessage(data, node, "Do not return from inside a finally block");
			}
			else
			{
				ASTTryStatement tryStatement = node.getFirstParentOfType(ASTTryStatement.class);
				if (tryStatement != null)
				{
					addViolationWithMessage(data, node, "Do not return from inside a try block");
				}
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return super.visit(node, data);
	}
}
