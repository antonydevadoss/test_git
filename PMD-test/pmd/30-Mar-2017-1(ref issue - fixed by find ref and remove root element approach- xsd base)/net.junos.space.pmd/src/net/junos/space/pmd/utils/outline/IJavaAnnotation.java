package net.junos.space.pmd.utils.outline;

import java.util.Collection;

public interface IJavaAnnotation {
	
	static int ANNOTATION_TYPE_STRING = 0 ;
	
	static int ANNOTATION_TYPE_INT = 1 ;

	static int ANNOTATION_TYPE_VARIABLE = 2;

	static int ANNOTATION_TYPE_CLASS_NAME = 4;
	
	String getName() ;
	
	Collection<String> getKeys() ;
	
	IAnnotationMemberValue getMemberValue( String key ) ;
	
	void setValue(String key, IAnnotationMemberValue value) ;
	
}
