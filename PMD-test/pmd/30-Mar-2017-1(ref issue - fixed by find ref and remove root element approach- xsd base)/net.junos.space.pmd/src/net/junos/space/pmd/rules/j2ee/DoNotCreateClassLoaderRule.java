package net.junos.space.pmd.rules.j2ee;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.junos.space.pmd.rules.AbstractJavaRuleWithUselessTypeResolution;
import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IDeclaration;
import net.junos.space.pmd.utils.reference.SimpleMethodReferenceRule;
import net.junos.space.pmd.utils.reference.SimpleReferenceRule;
import net.sourceforge.pmd.RuleContext;
import net.sourceforge.pmd.lang.java.ast.ASTCompilationUnit;

public class DoNotCreateClassLoaderRule extends AbstractJavaRuleWithUselessTypeResolution {
	
	
	private static final String message = "Allowing the enterprise bean to access information about other classes and to access the classes in a manner that is normally disallowed by the Java programming language could compromise security. The enterprise bean must not attempt to create a class loader; obtain the current class loader; set the context class loader; set security manager; create a new security manager; stop the JVM; or change the input, output, and error streams.";


	@Override
	public String getMessage() {		
		return message;
	}
	
	public class MethodReferenceRule extends SimpleMethodReferenceRule{
		private final String[] targetClasses = new String[]{
			"java.lang.System",
			"java.lang.ClassLoader",
			"java.lang.Thread",
			
		} ;
		
		private final String[] targetMethods = new String[]{
			"exit",
			"loadClass",
			"ClassLoader",
			"setContextClassLoader",
			"setSecurityManager",
			"setErr",
			"setIn",
			"setOut",
		} ;
		
		@Override
		protected void reportError(RuleContext ctx, IClassOutline outline,
				int beginLine, int endLine) {
			DoNotCreateClassLoaderRule.this.reportError(ctx, outline, beginLine, endLine);
		}
		
		@Override
		protected void reportError(RuleContext ctx, IClassOutline outline,
				IDeclaration decl) {
			DoNotCreateClassLoaderRule.this.reportError(ctx, outline, decl);
		}

		public String getMessage(){
			return message ;
		}

		protected List<String> getTargetClasses() {
			return new ArrayList<String>( Arrays.asList(targetClasses) ) ;
		}
		
		protected List<String> getTargetMethods() {
			return new ArrayList<String>( Arrays.asList(targetMethods) ) ;
		}

		protected boolean mustBeChecked(String className) {		
			return isEnterpriseBeanSuperclassEJB2(className) ;
		}
	}
	
	public class OutlineAccessorRule extends SimpleReferenceRule {
		
		protected boolean mustBeChecked(String qualifiedName) {
			return isEnterpriseBeanSuperclassEJB2(qualifiedName);
		}

		protected boolean isInvalid(String refQName)
		{
			boolean result = refQName.equals("java.lang.ClassLoader");
			
			return  result;
		}
		
		public String getMessage(){
			return message ;
		}
		
		@Override
		protected void reportError(RuleContext ctx, IClassOutline outline,
				int beginLine, int endLine) {
			DoNotCreateClassLoaderRule.this.reportError(ctx, outline, beginLine, endLine);
		}
		
		@Override
		protected void reportError(RuleContext ctx, IClassOutline outline,
				IDeclaration decl) {
			DoNotCreateClassLoaderRule.this.reportError(ctx, outline, decl);
		}
		
		public static final String classloaderQName = "java.lang.ClassLoader";

	}
	
	private MethodReferenceRule methodReferenceRule = new MethodReferenceRule();
	private OutlineAccessorRule outlineAccessorRule = new OutlineAccessorRule();
	
	
	@Override
	public Object visit(ASTCompilationUnit node, Object data) {
		
		methodReferenceRule.visit(node, data) ;
		outlineAccessorRule.visit(node, data) ;
		return null ;
	}
	
	@Override
	final public void start(RuleContext ctx) {
		methodReferenceRule.start(ctx) ;
		outlineAccessorRule.start(ctx) ;
	}

	@Override
	final public void end(RuleContext ctx) {
		methodReferenceRule.end(ctx) ;
		outlineAccessorRule.end(ctx) ;
	}
}
