package net.junos.space.pmd.utils.reference;

public class UnresolvedReference  extends AbstractReference {
	
	public UnresolvedReference(String simpleType, String contextType, int beginLine, int startLine) {
		super(beginLine, startLine);
		this.simpleType = simpleType;
		this.contextType = contextType;
	}

	private final String simpleType ;
	
	private final String contextType ;

	public String getSimpleType() {
		return simpleType;
	}

	public String getContextType() {
		return contextType;
	}

	@Override
	public String toString(){
		return "Unresolved reerence: type\"" + simpleType +"\" inside \"" + contextType +"\"." ;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((contextType == null) ? 0 : contextType.hashCode());
		result = prime * result
				+ ((simpleType == null) ? 0 : simpleType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		UnresolvedReference other = (UnresolvedReference) obj;
		if (contextType == null) {
			if (other.contextType != null)
				return false;
		} else if (!contextType.equals(other.contextType))
			return false;
		if (simpleType == null) {
			if (other.simpleType != null)
				return false;
		} else if (!simpleType.equals(other.simpleType))
			return false;
		return true;
	}
}
