package net.junos.space.pmd.rules.j2ee;

import java.util.List;

import net.junos.space.pmd.rules.AbstractJavaRuleWithUselessTypeResolution;
import net.sourceforge.pmd.lang.java.ast.ASTName;
import net.sourceforge.pmd.lang.java.ast.ASTPrimaryPrefix;

//JAC_065
public class SysoutRule extends AbstractJavaRuleWithUselessTypeResolution {

	@Override
	public Object visit(ASTPrimaryPrefix node, Object data) {
		List<ASTName> names = node.findChildrenOfType(ASTName.class);
		if (names != null)
		{
			for (ASTName name : names) {
				if (name.getImage() != null
						&& (name.getImage().equals("System.err.println")
								|| name.getImage().equals("System.out.println")))
				{
					addViolationWithMessage(data, name, "Do not unnecessary use the " +
							"System.out.print " +
							"or " +
							"System.err.print " +
							"Methods");
				}
			}
			
		}
		return super.visit(node, data);
	}
}
