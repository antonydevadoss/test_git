package net.junos.space.pmd.rules.j2ee.ejb3;

import java.util.ArrayList;
import java.util.List;

import net.junos.space.pmd.utils.outline.AbstractEntityRule;
import net.junos.space.pmd.utils.outline.CollectionAndMapDescendants;
import net.junos.space.pmd.utils.outline.EjbEntity;
import net.junos.space.pmd.utils.outline.IJavaFieldDeclaration;
import net.junos.space.pmd.utils.outline.IJavaMethodDeclaration;
import net.junos.space.pmd.utils.outline.IJavaType;
import net.sourceforge.pmd.RuleContext;

public class EntityCollectionFieldsAndPropertiesRule extends AbstractEntityRule {
	
	private final static String message =
			"Collection-valued persistent fields and properties must be defined in terms of one of the following collection-valued interfaces "
			+"regardless of whether the entity class otherwise adheres to the JavaBeans method conventions noted above "
			+"and whether field or property-based access is used: java.util.Collection, java.util.Set, java.util.List, java.util.Map." ;
	
	protected void processEntities(RuleContext ctx) {
		if (AbstractEntityRule.entityRegistry == null) {
			return;
		}
		for (EjbEntity ejbEntity : AbstractEntityRule.entityRegistry.values()) {
			
			if( ejbEntity.hasFieldAccess() ){
				
				ArrayList<IJavaFieldDeclaration> persistentFields = ejbEntity.getPersistentFields();
				if( persistentFields == null )
					continue ;
				
				for(IJavaFieldDeclaration fd : persistentFields ){
					if( checkField(fd) )
						continue ;
					
					reportError(ctx,fd,message) ;
				}
				
			}
			else if( ejbEntity.hasPropertyAccess() ){
				
				for( IJavaMethodDeclaration md : ejbEntity.getNonTransientProperties() ){
					
					if( checkMethod(md) )
						continue ;
					
					reportError(ctx,md,message) ;
				}
			}
		}
		super.processEntities(ctx);
	}

	private boolean checkField(IJavaFieldDeclaration fd) {
		
		IJavaType type = fd.getType();
		boolean result = checkType(type);		
		return result ;
	}

	protected boolean checkType(IJavaType type) {
		List<String> values = type.getQualifiedName().getValues();
		if(values.size()!=1)
			return true ;	
		
		String qName = values.iterator().next() ;
		
		boolean result = true ;
		if( CollectionAndMapDescendants.javaUtilCollectionAndMapDescendants.contains(qName) )
		{
			result = CollectionAndMapDescendants.allowedEntityFieldTypes.contains(qName) ;
		}
		return result;
	}
	
	private boolean checkMethod(IJavaMethodDeclaration md) {
		
		IJavaType type = null ;
		IJavaType returnType = md.getReturnType();
		List<IJavaType> params = md.getParameters();
		
		
		if(returnType == null){
			
			if(params == null || params.size() != 1 )
				return true ;
			
			type = params.get(0) ;
		}
		else{
			if(params != null && !params.isEmpty())
				return true ;
			
			type = returnType ;
		}
		
		boolean result = checkType(type);		
		return result ;
	}

}
