package net.junos.space.pmd.utils.outline;

import java.util.Arrays;
import java.util.HashSet;

public class ExceptionsCompleteList {
	
	public static boolean isKnownApplicationException(String qualifiedName){
		
		return java_lang_exception_application_subclasses.contains(qualifiedName);
	}
	
	public static boolean isKnownRuntimeException(String qualifiedName){
		
		return java_lang_runtime_exception_subclasses.contains(qualifiedName);
	}
	
	public static boolean isKnownRemoteException(String qualifiedName){
		
		return java_rmi_remote_exception_subclasses.contains(qualifiedName);
	}
	
	//an application exception is a java.lang.Exception subclass which is not
	//a java.rmi.RemoteException or java.lang.RuntimeException subclass
	private static final HashSet<String> java_lang_exception_application_subclasses =
			new HashSet<String>( Arrays.asList( new String[]{
				"java.lang.Exception",
				
					"java.security.acl.AclNotFoundException",
					
					"java.rmi.activation.ActivationException",
						"java.rmi.activation.UnknownGroupException",
						"java.rmi.activation.UnknownObjectException",
						
					"java.rmi.AlreadyBoundException",
					"org.omg.CORBA.portable.ApplicationException",
					"java.awt.AWTException",
					"java.util.prefs.BackingStoreException",
					"javax.management.BadAttributeValueExpException",
					"javax.management.BadBinaryOpValueExpException",
					"javax.swing.text.BadLocationException",
					"javax.management.BadStringOperationException",
					"java.util.concurrent.BrokenBarrierException",
					
					"javax.security.cert.CertificateException",
						"java.security.cert.CertificateEncodingException",
						"java.security.cert.CertificateExpiredException",
						"java.security.cert.CertificateNotYetValidException",
						"java.security.cert.CertificateParsingException",
						"java.security.cert.CertificateRevokedException",
					
					"java.lang.CloneNotSupportedException",
						"java.rmi.server.ServerCloneException",
					
					"java.util.zip.DataFormatException",
					"javax.xml.datatype.DatatypeConfigurationException",
					"javax.security.auth.DestroyFailedException",
					"java.util.concurrent.ExecutionException",
					"javax.swing.tree.ExpandVetoException",
					"java.awt.FontFormatException",
					
					"java.security.GeneralSecurityException",
						
						"javax.crypto.BadPaddingException",
							"javax.crypto.AEADBadTagException",
							
						
						"java.security.cert.CertificateException",
						"java.security.cert.CertPathBuilderException",
						"java.security.cert.CertPathValidatorException",
						"java.security.cert.CertStoreException",
						"java.security.cert.CRLException",
						"java.security.DigestException",
						"javax.crypto.ExemptionMechanismException",
						"javax.crypto.IllegalBlockSizeException",
						"java.security.InvalidAlgorithmParameterException",
						"java.security.spec.InvalidKeySpecException",
						"java.security.spec.InvalidParameterSpecException",
						
						"java.security.KeyException",
							"java.security.InvalidKeyException",
							"java.security.KeyManagementException",
						
						"java.security.KeyStoreException",
						
						"javax.security.auth.login.LoginException",							
							"javax.security.auth.login.AccountException",
								"javax.security.auth.login.AccountExpiredException",
								"javax.security.auth.login.AccountLockedException",
								"javax.security.auth.login.AccountNotFoundException",
							
							"javax.security.auth.login.CredentialException",
								"javax.security.auth.login.CredentialExpiredException",
								"javax.security.auth.login.CredentialNotFoundException",
							
							"javax.security.auth.login.FailedLoginException",
						
						"java.security.NoSuchAlgorithmException",
						"javax.crypto.NoSuchPaddingException",
						"java.security.NoSuchProviderException",
						"javax.crypto.ShortBufferException",
						"java.security.SignatureException",
						
						"java.security.UnrecoverableEntryException",
							"java.security.UnrecoverableKeyException",
					
					"org.ietf.jgss.GSSException",
					"java.lang.instrument.IllegalClassFormatException",
					"java.lang.InterruptedException",
					"java.beans.IntrospectionException",
					"javax.management.InvalidApplicationException",
					"javax.sound.midi.InvalidMidiDataException",
					"java.util.prefs.InvalidPreferencesFormatException",
					"javax.management.modelmbean.InvalidTargetObjectTypeException",
					
					"java.io.IOException",
						"javax.swing.text.ChangedCharSetException",
						
						"java.nio.charset.CharacterCodingException",
							"java.nio.charset.MalformedInputException",
							"java.nio.charset.UnmappableCharacterException",
						
						"java.io.CharConversionException",
						
						"java.nio.channels.ClosedChannelException",
							"java.nio.channels.AsynchronousCloseException",
								"java.nio.channels.ClosedByInterruptException",
								
						"java.io.EOFException",
						"java.nio.channels.FileLockInterruptionException",
						"java.io.FileNotFoundException",
						"javax.annotation.processing.FilerException",
						
						"java.nio.file.FileSystemException",
							"java.nio.file.AccessDeniedException",
							"java.nio.file.AtomicMoveNotSupportedException",
							"java.nio.file.DirectoryNotEmptyException",
							"java.nio.file.FileAlreadyExistsException",
							"java.nio.file.FileSystemLoopException",
							"java.nio.file.NoSuchFileException",
							"java.nio.file.NotDirectoryException",
							"java.nio.file.NotLinkException",
						
						"java.net.HttpRetryException",
						
						"javax.imageio.IIOException",
							"javax.imageio.metadata.IIOInvalidTreeException",
						
						"java.nio.channels.InterruptedByTimeoutException",
						
						"java.io.InterruptedIOException",
							"java.net.SocketTimeoutException",
						
						"java.util.InvalidPropertiesFormatException",
						"javax.management.remote.JMXProviderException",
						"javax.management.remote.JMXServerErrorException",
						"java.net.MalformedURLException",
						
						"java.io.ObjectStreamException",
							"java.io.InvalidClassException",
							"java.io.InvalidObjectException",
							"java.io.NotActiveException",
							"java.io.NotSerializableException",
							"java.io.OptionalDataException",
							"java.io.StreamCorruptedException",
							"java.io.WriteAbortedException",
							
						"java.net.ProtocolException",
						//"java.rmi.RemoteException",
						
						"javax.security.sasl.SaslException",
							"javax.security.sasl.AuthenticationException",
							
						"java.net.SocketException",
							"java.net.BindException",
							"java.net.ConnectException",
							"java.net.NoRouteToHostException",
							"java.net.PortUnreachableException",
						
						"javax.net.ssl.SSLException",
							"javax.net.ssl.SSLHandshakeException",
							"javax.net.ssl.SSLKeyException",
							"javax.net.ssl.SSLPeerUnverifiedException",
							"javax.net.ssl.SSLProtocolException",
						
						"java.io.SyncFailedException",
						"java.net.UnknownHostException",
						"java.net.UnknownServiceException",
						"javax.activation.UnsupportedDataTypeException",
						"java.io.UnsupportedEncodingException",
						"java.nio.file.attribute.UserPrincipalNotFoundException",
						"java.io.UTFDataFormatException",
						
						"java.util.zip.ZipException",
							"java.util.jar.JarException",
					
					"javax.xml.bind.JAXBException",
						"javax.xml.bind.MarshalException",
						"avax.xml.bind.PropertyException",
						"javax.xml.bind.UnmarshalException",
						"javax.xml.bind.ValidationException",
					
					"javax.management.JMException",
					"javax.xml.crypto.KeySelectorException",
					"java.security.acl.LastOwnerException",
					"javax.sound.sampled.LineUnavailableException",
					"javax.xml.crypto.MarshalException",
					"javax.sound.midi.MidiUnavailableException",
					"java.awt.datatransfer.MimeTypeParseException",
					"javax.activation.MimeTypeParseException",
					
					"javax.naming.NamingException",
						"javax.naming.directory.AttributeInUseException",
						"javax.naming.directory.AttributeModificationException",
						"javax.naming.CannotProceedException",
						"javax.naming.CommunicationException",
						"javax.naming.ConfigurationException",
						"javax.naming.ContextNotEmptyException",
						"javax.naming.InsufficientResourcesException",
						"javax.naming.InterruptedNamingException",
						"javax.naming.directory.InvalidAttributeIdentifierException",
						"javax.naming.directory.InvalidAttributesException",
						"javax.naming.directory.InvalidAttributeValueException",
						"javax.naming.InvalidNameException",
						"javax.naming.directory.InvalidSearchControlsException",
						"javax.naming.directory.InvalidSearchFilterException",
						
						"javax.naming.LimitExceededException",
							"javax.naming.SizeLimitExceededException",
							"javax.naming.TimeLimitExceededException",
						
						"javax.naming.LinkException",
							"javax.naming.LinkLoopException",
							"javax.naming.MalformedLinkException",
						
						"javax.naming.NameAlreadyBoundException",
						"javax.naming.NameNotFoundException",
						
						"javax.naming.NamingSecurityException",
							"javax.naming.AuthenticationException",
							"javax.naming.AuthenticationNotSupportedException",
							"javax.naming.NoPermissionException",
						
						"javax.naming.NoInitialContextException",
						"javax.naming.directory.NoSuchAttributeException",
						"javax.naming.NotContextException",
						"javax.naming.OperationNotSupportedException",
						"javax.naming.PartialResultException",
						
						"javax.naming.ReferralException",
							"javax.naming.ldap.LdapReferralException",
							
						"javax.naming.directory.SchemaViolationException",
						"javax.naming.ServiceUnavailableException",
					
					"java.awt.geom.NoninvertibleTransformException",
					"java.rmi.NotBoundException",
					"java.security.acl.NotOwnerException",
					"java.text.ParseException",
					"javax.xml.parsers.ParserConfigurationException",
					"java.awt.print.PrinterException",
					
					"javax.print.PrintException",
						"java.awt.print.PrinterAbortException",
						"java.awt.print.PrinterIOException",
						
					"java.security.PrivilegedActionException",
					"java.beans.PropertyVetoException",
					
					"java.lang.ReflectiveOperationException",
						"java.lang.IllegalAccessException",
						"java.lang.InstantiationException",
						"java.lang.reflect.InvocationTargetException",
						"java.lang.NoSuchFieldException",
						"java.lang.NoSuchMethodException",
					
					"javax.security.auth.RefreshFailedException",
					"org.omg.CORBA.portable.RemarshalException",
					//"java.lang.RuntimeException",
					
					"org.xml.sax.SAXException",
						"org.xml.sax.SAXNotRecognizedException",
						"org.xml.sax.SAXNotSupportedException",
						"org.xml.sax.SAXParseException",
						
					"javax.script.ScriptException",
					"java.rmi.server.ServerNotActiveException",
					"javax.xml.soap.SOAPException",
					
					"java.sql.SQLException",
						"java.sql.BatchUpdateException",
						"javax.sql.rowset.RowSetWarning",
						"javax.sql.rowset.serial.SerialException",
						"java.sql.SQLClientInfoException",
						
						"java.sql.SQLNonTransientException",
							"java.sql.SQLDataException",
							"java.sql.SQLFeatureNotSupportedException",
							"java.sql.SQLIntegrityConstraintViolationException",
							"java.sql.SQLInvalidAuthorizationSpecException",
							"java.sql.SQLNonTransientConnectionException",
							"java.sql.SQLSyntaxErrorException",
							
						"java.sql.SQLRecoverableException",
						
						"java.sql.SQLTransientException",
							"java.sql.SQLTimeoutException",
							"java.sql.SQLTransactionRollbackException",
							"java.sql.SQLTransientConnectionException",
						
						"java.sql.SQLWarning",
							"java.sql.DataTruncation",
							
						"javax.sql.rowset.spi.SyncFactoryException",
						"javax.sql.rowset.spi.SyncProviderException",
					
					"java.util.concurrent.TimeoutException",
					"java.util.TooManyListenersException",
					
					"javax.xml.transform.TransformerException",
						"javax.xml.transform.TransformerConfigurationException",
					
					"javax.xml.crypto.dsig.TransformException",
					"java.lang.instrument.UnmodifiableClassException",
					"javax.sound.sampled.UnsupportedAudioFileException",
					"javax.security.auth.callback.UnsupportedCallbackException",
					"java.awt.datatransfer.UnsupportedFlavorException",
					"javax.swing.UnsupportedLookAndFeelException",
					"javax.xml.crypto.URIReferenceException",
					"java.net.URISyntaxException",
					
					"org.omg.CORBA.UserException",
						"org.omg.PortableServer.POAPackage.AdapterAlreadyExists",
						"org.omg.PortableServer.POAManagerPackage.AdapterInactive",
						"org.omg.PortableServer.POAPackage.AdapterNonExistent",
						"org.omg.CosNaming.NamingContextPackage.AlreadyBound",
						"org.omg.CORBA.TypeCodePackage.BadKind",
						"org.omg.CORBA.Bounds",
						"org.omg.CORBA.TypeCodePackage.Bounds",
						"org.omg.CosNaming.NamingContextPackage.CannotProceed",
						"org.omg.PortableInterceptor.ORBInitInfoPackage.DuplicateName",
						"org.omg.IOP.CodecPackage.FormatMismatch",
						"org.omg.PortableServer.ForwardRequest",
						"org.omg.PortableInterceptor.ForwardRequest",
						"org.omg.CORBA.ORBPackage.InconsistentTypeCode",
						"org.omg.DynamicAny.DynAnyFactoryPackage.InconsistentTypeCode",
						"org.omg.CORBA.DynAnyPackage.Invalid",
						"org.omg.CosNaming.NamingContextExtPackage.InvalidAddress",
						"org.omg.CORBA.ORBPackage.InvalidName",
						"org.omg.CosNaming.NamingContextPackage.InvalidName",
						"org.omg.PortableInterceptor.ORBInitInfoPackage.InvalidName",
						"org.omg.PortableServer.POAPackage.InvalidPolicy",
						"org.omg.CORBA.DynAnyPackage.InvalidSeq",
						"org.omg.PortableInterceptor.InvalidSlot",
						"org.omg.IOP.CodecPackage.InvalidTypeForEncoding",
						"org.omg.CORBA.DynAnyPackage.InvalidValue",
						"org.omg.DynamicAny.DynAnyPackage.InvalidValue",
						"org.omg.PortableServer.CurrentPackage.NoContext",
						"org.omg.PortableServer.POAPackage.NoServant",
						"org.omg.CosNaming.NamingContextPackage.NotEmpty",
						"org.omg.CosNaming.NamingContextPackage.NotFound",
						"org.omg.PortableServer.POAPackage.ObjectAlreadyActive",
						"org.omg.PortableServer.POAPackage.ObjectNotActive",
						"org.omg.CORBA.PolicyError",
						"org.omg.PortableServer.POAPackage.ServantAlreadyActive",
						"org.omg.PortableServer.POAPackage.ServantNotActive",
						"org.omg.CORBA.DynAnyPackage.TypeMismatch",
						"org.omg.IOP.CodecPackage.TypeMismatch",
						"org.omg.DynamicAny.DynAnyPackage.TypeMismatch",
						"org.omg.IOP.CodecFactoryPackage.UnknownEncoding",
						"org.omg.CORBA.UnknownUserException",
						"org.omg.PortableServer.POAPackage.WrongAdapter",
						"org.omg.PortableServer.POAPackage.WrongPolicy",
						"org.omg.CORBA.WrongTransaction",
					
					"javax.transaction.xa.XAException",
					"javax.management.modelmbean.XMLParseException",
					"javax.xml.crypto.dsig.XMLSignatureException",
					"javax.xml.stream.XMLStreamException",
					
					"javax.xml.xpath.XPathException",
						"javax.xml.xpath.XPathFactoryConfigurationException",
						"javax.xml.xpath.XPathExpressionException",
							"javax.xml.xpath.XPathFunctionException",
			})) ;
	
	private static final HashSet<String> java_rmi_remote_exception_subclasses =
			new HashSet<String>( Arrays.asList( new String[]{
					
				"java.rmi.RemoteException",
				
					"java.rmi.AccessException",
					"java.rmi.activation.ActivateFailedException",
					"javax.activity.ActivityCompletedException",
					"javax.activity.ActivityRequiredException",
					"java.rmi.ConnectException",
					"java.rmi.ConnectIOException",
					
					"java.rmi.server.ExportException",
						"java.rmi.server.SocketSecurityException",
						
					"javax.activity.InvalidActivityException",
					"javax.transaction.InvalidTransactionException",
					"java.rmi.MarshalException",
					"java.rmi.NoSuchObjectException",
					"java.rmi.ServerError",
					"java.rmi.ServerException",
					"java.rmi.ServerRuntimeException",
					"java.rmi.server.SkeletonMismatchException",
					"java.rmi.server.SkeletonNotFoundException",
					"java.rmi.StubNotFoundException",
					"javax.transaction.TransactionRequiredException",
					"javax.transaction.TransactionRolledbackException",
					"java.rmi.UnexpectedException",
					"java.rmi.UnknownHostException",
					"java.rmi.UnmarshalException"
			})) ;
	
	private static final HashSet<String> java_lang_runtime_exception_subclasses =
			new HashSet<String>( Arrays.asList( new String[]{
					
				"java.lang.RuntimeException",
				
					"java.lang.annotation.AnnotationTypeMismatchException",
					"java.lang.ArithmeticException",
					"java.lang.ArrayStoreException",
					"java.nio.BufferOverflowException",
					"java.nio.BufferUnderflowException",
					"javax.swing.undo.CannotRedoException",
					"javax.swing.undo.CannotUndoException",
					"java.lang.ClassCastException",
					"java.awt.color.CMMException",
					"java.util.ConcurrentModificationException",
					"javax.xml.bind.DataBindingException",
					"org.w3c.dom.DOMException",
					"java.util.EmptyStackException",
					"java.lang.EnumConstantNotPresentException",
					"org.w3c.dom.events.EventException",
					
					"java.nio.file.FileSystemAlreadyExistsException",					
					"java.nio.file.FileSystemNotFoundException",
					
					"java.lang.IllegalArgumentException",
						"java.nio.channels.IllegalChannelGroupException",
						"java.nio.charset.IllegalCharsetNameException",
						
						"java.util.IllegalFormatException",
							"java.util.DuplicateFormatFlagsException",
							"java.util.FormatFlagsConversionMismatchException",
							"java.util.IllegalFormatCodePointException",
							"java.util.IllegalFormatConversionException",
							"java.util.IllegalFormatFlagsException",
							"java.util.IllegalFormatPrecisionException",
							"java.util.IllegalFormatWidthException",
							"java.util.MissingFormatArgumentException",
							"java.util.MissingFormatWidthException",
							"java.util.UnknownFormatConversionException",
							"java.util.UnknownFormatFlagsException",
							
						"java.nio.channels.IllegalSelectorException",
						"java.lang.IllegalThreadStateException",
						"javax.management.openmbean.InvalidKeyException",
						"javax.management.openmbean.InvalidOpenTypeException",
						"java.security.InvalidParameterException",
						"java.nio.file.InvalidPathException",
						"javax.management.openmbean.KeyAlreadyExistsException",
						"java.lang.NumberFormatException",
						"java.util.regex.PatternSyntaxException",
						"java.nio.file.ProviderMismatchException",
						"java.nio.channels.UnresolvedAddressException",
						"java.nio.channels.UnsupportedAddressTypeException",
						"java.nio.charset.UnsupportedCharsetException",
						
					"java.lang.IllegalMonitorStateException",
					"java.awt.geom.IllegalPathStateException",
					
					"java.lang.IllegalStateException",
						"java.nio.channels.AcceptPendingException",
						"java.nio.channels.AlreadyBoundException",
						"java.nio.channels.AlreadyConnectedException",
						"java.util.concurrent.CancellationException",
						"java.nio.channels.CancelledKeyException",
						"java.nio.file.ClosedDirectoryStreamException",
						"java.nio.file.ClosedFileSystemException",
						"java.nio.channels.ClosedSelectorException",
						"java.nio.file.ClosedWatchServiceException",
						"java.nio.channels.ConnectionPendingException",
						"java.util.FormatterClosedException",
						"java.nio.channels.IllegalBlockingModeException",
						"java.awt.IllegalComponentStateException",
						"java.awt.dnd.InvalidDnDOperationException",
						"java.nio.InvalidMarkException",
						"java.nio.channels.NoConnectionPendingException",
						"java.nio.channels.NonReadableChannelException",
						"java.nio.channels.NonWritableChannelException",
						"java.nio.channels.NotYetBoundException",
						"java.nio.channels.NotYetConnectedException",
						"java.nio.channels.OverlappingFileLockException",
						"java.nio.channels.ReadPendingException",
						"java.nio.channels.ShutdownChannelGroupException",
						"java.nio.channels.WritePendingException",
						
					"java.util.IllformedLocaleException",
					"java.awt.image.ImagingOpException",
					"java.lang.annotation.IncompleteAnnotationException",
					
					"java.lang.IndexOutOfBoundsException",
						"java.lang.ArrayIndexOutOfBoundsException",
						"java.lang.StringIndexOutOfBoundsException",
					
					"javax.management.JMRuntimeException",
						"javax.management.monitor.MonitorSettingException",
						"javax.management.RuntimeErrorException",
						"javax.management.RuntimeMBeanException",
						"javax.management.RuntimeOperationsException",
						
					"org.w3c.dom.ls.LSException",
					"java.lang.reflect.MalformedParameterizedTypeException",
					
					"javax.lang.model.type.MirroredTypesException",
						"javax.lang.model.type.MirroredTypeException",
					
					"java.util.MissingResourceException",
					"java.lang.NegativeArraySizeException",
					
					"java.util.NoSuchElementException",
						"java.util.InputMismatchException",
					
					"javax.xml.crypto.NoSuchMechanismException",
					"java.lang.NullPointerException",
					"java.awt.color.ProfileDataException",
					"java.security.ProviderException",
					"java.nio.file.ProviderNotFoundException",
					"java.awt.image.RasterFormatException",
					"java.util.concurrent.RejectedExecutionException",
					"java.lang.SecurityException",
					
					"org.omg.CORBA.SystemException",
						"class in org.omg.CORBA.ACTIVITY_COMPLETED",
						"org.omg.CORBA.ACTIVITY_REQUIRED",
						"org.omg.CORBA.BAD_CONTEXT",
						"org.omg.CORBA.BAD_INV_ORDER",
						"org.omg.CORBA.BAD_OPERATION",
						"org.omg.CORBA.BAD_PARAM",
						"org.omg.CORBA.BAD_QOS",
						"org.omg.CORBA.BAD_TYPECODE",
						"org.omg.CORBA.CODESET_INCOMPATIBLE",
						"org.omg.CORBA.COMM_FAILURE",
						"org.omg.CORBA.DATA_CONVERSION",
						"org.omg.CORBA.FREE_MEM",
						"org.omg.CORBA.IMP_LIMIT",
						"org.omg.CORBA.portable.IndirectionException",
						"org.omg.CORBA.INITIALIZE",
						"org.omg.CORBA.INTERNAL",
						"org.omg.CORBA.INTF_REPOS",
						"org.omg.CORBA.INV_FLAG",
						"org.omg.CORBA.INV_IDENT",
						"org.omg.CORBA.INV_OBJREF",
						"org.omg.CORBA.INV_POLICY",
						"org.omg.CORBA.INVALID_ACTIVITY",
						"org.omg.CORBA.INVALID_TRANSACTION",
						"org.omg.CORBA.MARSHAL",
						"org.omg.CORBA.NO_IMPLEMENT",
						"org.omg.CORBA.NO_MEMORY",
						"org.omg.CORBA.NO_PERMISSION",
						"org.omg.CORBA.NO_RESOURCES",
						"org.omg.CORBA.NO_RESPONSE",
						"org.omg.CORBA.OBJ_ADAPTER",
						"org.omg.CORBA.OBJECT_NOT_EXIST",
						"org.omg.CORBA.PERSIST_STORE",
						"org.omg.CORBA.REBIND",
						"org.omg.CORBA.TIMEOUT",
						"org.omg.CORBA.TRANSACTION_MODE",
						"org.omg.CORBA.TRANSACTION_REQUIRED",
						"org.omg.CORBA.TRANSACTION_ROLLEDBACK",
						"org.omg.CORBA.TRANSACTION_UNAVAILABLE",
						"org.omg.CORBA.TRANSIENT",
						"org.omg.CORBA.UNKNOWN",
						"org.omg.CORBA.portable.UnknownException",
					
					"javax.xml.bind.TypeConstraintException",
					"java.lang.TypeNotPresentException",
					"java.lang.reflect.UndeclaredThrowableException",
					"javax.lang.model.UnknownEntityException",
					"javax.print.attribute.UnmodifiableSetException",
					"java.lang.UnsupportedOperationException",
					"javax.xml.ws.WebServiceException",
					"java.lang.invoke.WrongMethodTypeException"
			})) ;

}
