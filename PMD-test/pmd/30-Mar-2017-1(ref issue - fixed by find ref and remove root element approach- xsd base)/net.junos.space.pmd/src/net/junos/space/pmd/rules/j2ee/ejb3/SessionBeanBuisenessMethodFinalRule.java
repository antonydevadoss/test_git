package net.junos.space.pmd.rules.j2ee.ejb3;

import java.util.ArrayList;
import java.util.HashSet;
import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IJavaMethodDeclaration;
import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;
import net.sourceforge.pmd.RuleContext;

public class SessionBeanBuisenessMethodFinalRule extends
		AbstractBusinessInterfacesInspector {
	private static final String message = "The business method of a session bean must not be declared as final or static.";

	@Override
	protected void inspectMethod(IJavaMethodDeclaration method,
			RuleContext ctx, HashSet<String> inspectedTypes) {
		if (method != null && method.isFinal() || method.isStatic()) {
			for (String ownerQualifiedName : method.getOwner().getQualifiedName().getValues()) {
				IClassOutline ownerOutline = getOutlineContainer().getClassOutline(ownerQualifiedName);
				if (ownerOutline != null ) {
					reportError(ctx, ownerOutline, method) ;
				}
			}
		}
	}
	
	public String getMessage() {
		return message;
	}
	
	@Override
	protected ArrayList<IClassOutline> extractTargetInterfaces(
			IJavaTypeDeclaration typeDeclaration) {
		return extractAllBusinessInterfaces(typeDeclaration) ;
	}
}
