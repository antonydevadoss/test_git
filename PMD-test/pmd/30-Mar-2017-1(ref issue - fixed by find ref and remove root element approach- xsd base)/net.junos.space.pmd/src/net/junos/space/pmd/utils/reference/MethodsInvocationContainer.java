package net.junos.space.pmd.utils.reference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.junos.space.pmd.utils.outline.IJavaType;
import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;
import net.junos.space.pmd.utils.outline.QualifiedName;

public class MethodsInvocationContainer {
	
	private static final MethodsInvocationContainer instance = new MethodsInvocationContainer() ;
	public static final String NULL_TYPE = "0";	

	public static MethodsInvocationContainer getInstance() {
		return instance;
	}
	
	HashMap<String,ClassMethodsInvocationRegistry> registryMap = new HashMap<String, ClassMethodsInvocationRegistry>() ;
	
	public void registerTargetMethod(String ownerFullName, String name, List<String> params){
		ClassMethodsInvocationRegistry reg = registryMap.get(ownerFullName) ;
		if( reg == null ){
			reg = new ClassMethodsInvocationRegistry(ownerFullName) ;
			registryMap.put(ownerFullName, reg) ;
		}
		reg.registerTarget(name, params) ;
	}
//	
//	public void registarMethodInvocation(
//			String methodOwnerQualifiedName, String methodName, List<String> params, String occurenceOwnerName, int line )
//	{
//		ClassMethodsInvocationRegistry reg = registryMap.get(methodOwnerQualifiedName);
//		if(reg == null){
//			reg = new ClassMethodsInvocationRegistry(methodOwnerQualifiedName) ;
//			registryMap.put(methodOwnerQualifiedName, reg) ;
//		}
//		reg.addInvocation(methodName, params, occurenceOwnerName, line) ;
//	}
	
	public void registarTargetMethodInvocation(
			IJavaType ownerType, String methodName, List<String> params, String occurenceOwnerName, IReference ref )
	{
		IJavaTypeDeclaration ownerDeclaration = ownerType.getDeclaration();
		if(ownerDeclaration==null)
			return ;
		
		ResolvedReference resolvedRef = new ResolvedReference(ownerType, ref.getBeginLine(), ref.getEndLine() ) ;
		List<IJavaTypeDeclaration> allInterfaces = ownerDeclaration.getAllInterfaces();
		List<IJavaTypeDeclaration> allSuperClasses = ownerDeclaration.getAllSuperClasses();
		ArrayList<IJavaTypeDeclaration> fullList = new ArrayList<IJavaTypeDeclaration>() ;
		fullList.add(ownerDeclaration) ;
		fullList.addAll(allSuperClasses) ;
		fullList.addAll(allInterfaces) ;
		
		for( IJavaTypeDeclaration decl : fullList ){
			QualifiedName qName = decl.getQualifiedName();
			if(qName==null)
				continue ;
			
			for(String s : qName.getValues()){
				ClassMethodsInvocationRegistry reg = registryMap.get( s );
				if(reg == null)
					continue ;
				
				reg.addTargetInvocation(methodName, params, occurenceOwnerName, resolvedRef) ;
			}			
		}
	}
	
	public ClassMethodsInvocationRegistry getInvocationRegistry(String qualifiedName ){
		return registryMap.get(qualifiedName) ;
	}

	public void clear() {
		registryMap.clear() ;		
	}
}
