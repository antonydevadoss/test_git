package net.junos.space.pmd.rules.j2ee.ejb3;

import java.util.List;

import net.junos.space.pmd.utils.outline.AbstractEntityRule;
import net.junos.space.pmd.utils.outline.AnnotationClassMemberValue;
import net.junos.space.pmd.utils.outline.EjbEntity;
import net.junos.space.pmd.utils.outline.IAnnotationMemberValue;
import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IJavaAnnotation;
import net.junos.space.pmd.utils.outline.IJavaFieldDeclaration;
import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;
import net.sourceforge.pmd.RuleContext;

public class EntityPrimaryKeyAccessRule extends AbstractEntityRule {

	
	private static final String message = "The composite primary key class must be serializable.";

	@Override
	protected void processEntities(RuleContext ctx) {
		if (AbstractEntityRule.entityRegistry == null) {
			return;
		}
		
		for (EjbEntity ejbEntity : AbstractEntityRule.entityRegistry.values()) {
			IClassOutline classOutline = ejbEntity.getOutline() ;

			IJavaTypeDeclaration typeDeclaration = classOutline.getTypeDeclaration();
			IJavaTypeDeclaration keyTypeDeclaration = null;
			List<IJavaAnnotation> annotations = typeDeclaration.getAnnotations();
			if (annotations != null) {
				for (IJavaAnnotation annotation : annotations) {
					if (annotation.getName().equals("IdClass")) {
						IAnnotationMemberValue memberValue = annotation.getMemberValue("value");
						if (memberValue instanceof AnnotationClassMemberValue) {
							AnnotationClassMemberValue classMemberValue = (AnnotationClassMemberValue)memberValue;
							if (classMemberValue.getType() != null && classMemberValue.getType().getDeclaration() != null ) {
								keyTypeDeclaration = classMemberValue.getType().getDeclaration();
								break;
							}
						}
					}
				}
			}
			
			if (keyTypeDeclaration == null) {
				List<IJavaFieldDeclaration> fields = ejbEntity.getPersistentFields();
				for (IJavaFieldDeclaration field : fields) {
					if (field.getAnnotations() != null) {
						for (IJavaAnnotation annotation : field.getAnnotations()) {
							if (annotation.getName().equals("EmbeddedId")) {
								if (field.getType() != null && field.getType().getDeclaration() != null) {
									keyTypeDeclaration = field.getType().getDeclaration();
									break;
								}
							}
						}
					}
				}
			}
			
			
			if (keyTypeDeclaration != null) {
				for (String recVal : keyTypeDeclaration.getQualifiedName().getValues()) {
					if( getOutlineContainer().getClassOutline(recVal) != null ){
						boolean isSerializable = true;
l0:						while(true){
							
							List<IJavaTypeDeclaration> undefinedInterfaces
									= keyTypeDeclaration.getUndefinedInterfaces(getOutlineContainer());
							
							if(!undefinedInterfaces.isEmpty())
								break ;
							
							List<IJavaTypeDeclaration> undefinedSuperClasses
									= keyTypeDeclaration.getUndefinedSuperClasses(getOutlineContainer());
							
							if(!undefinedSuperClasses.isEmpty())
								break ;
							
							
							for (IJavaTypeDeclaration keyInterface : keyTypeDeclaration.getAllInterfaces()) {
								for (String keyInterfaceQName : keyInterface.getQualifiedName().getValues()) {
									if (keyInterfaceQName.equals("java.io.Serializable")) {
										break l0;
									}
								}
							}
							isSerializable = false ;
							break ;
						}
						
						if (!isSerializable) {
							reportError(ctx,typeDeclaration,message) ;
						}
					}
				}
			}
		}
		super.processEntities(ctx);
	}
}
