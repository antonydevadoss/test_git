package net.junos.space.pmd.rules.j2ee.ejb3;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import net.junos.space.pmd.utils.outline.AbstractOutlineAccessor;
import net.junos.space.pmd.utils.outline.AnnotationClassMemberValue;
import net.junos.space.pmd.utils.outline.IAnnotationMemberValue;
import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IJavaAnnotation;
import net.junos.space.pmd.utils.outline.IJavaType;
import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;
import net.junos.space.pmd.utils.outline.MemberValueArray;
import net.junos.space.pmd.utils.outline.QualifiedName;
import net.sourceforge.pmd.RuleContext;

public class SessionBeanBusinessInterfaceExistenceRule extends AbstractOutlineAccessor {

	@Override
	public void doEnd(RuleContext ctx) {
		for( IClassOutline outline : getOutlineContainer().getAllOutlines() ){
			
			String qName = outline.getQualifiedNameString() ;
			if(!isSessionBeanEJB3(qName))
				continue ;
			
			IJavaTypeDeclaration jtd = outline.getTypeDeclaration();
			List<IJavaType> interfaces = jtd.getInterfaces();
			if(interfaces == null||interfaces.isEmpty()){
				
				if(!checkRemoteAndLocalAnnotations(jtd))
					reportError(ctx, outline,jtd );								

				continue ;
			}
			
			List<IJavaType> mInterfaces = extractMeaningfullInterfaces(interfaces) ;
			if(mInterfaces.isEmpty()){
				
				if(!checkRemoteAndLocalAnnotations(jtd))
					reportError(ctx, outline,jtd );

				continue ;
			}
			if(mInterfaces.size()==1)
				continue ;
			
			if(!checkRemoteAndLocalAnnotations(jtd))
				reportError(ctx, outline,jtd );
		}
		super.doEnd(ctx);
	}

	private boolean checkRemoteAndLocalAnnotations(IJavaTypeDeclaration jtd) {
		
		List<IJavaAnnotation> annotations = jtd.getAnnotations();
		if(annotations == null || annotations.isEmpty())
			return false ;
		
		HashSet<String> set = new HashSet<String>() ;
		{
			for( IJavaTypeDeclaration type : jtd.getAllInterfaces() ){
				QualifiedName qName = type.getQualifiedName();
				for(String s : qName.getValues()){
					if(!isMeaningful(s))
						continue ;
					
					set.add(s) ;
				}
			}
		}		
		
		boolean result = false ;
		for( IJavaAnnotation ann : annotations){
			
			String name = ann.getName();
			if(!(name.equals("Remote")||name.equals("Local")))
				continue ;
			
			IAnnotationMemberValue memberValue = ann.getMemberValue("value");
			List<String> lst = extractInterfacesFromAnnotations(memberValue) ;
			if(lst == null)
				continue ;
			
			result=true;
			break;
		}
		
		return result ;
	}

	private List<String> extractInterfacesFromAnnotations(IAnnotationMemberValue memberValue) {
		
		ArrayList<String> result = null ; 
		if(memberValue instanceof AnnotationClassMemberValue)
		{
			result = new ArrayList<String>() ;
			
			AnnotationClassMemberValue acmv = (AnnotationClassMemberValue) memberValue ;
			IJavaType type = acmv.getType();
			QualifiedName qName = type.getQualifiedName();
			for(String s : qName.getValues())
				result.add(s) ;
		}
		else if( memberValue instanceof MemberValueArray ){
			MemberValueArray mva = (MemberValueArray) memberValue ;
			ArrayList<IAnnotationMemberValue> values = mva.getValues();
			if(values != null){
				for(IAnnotationMemberValue amv : values){
					List<String> lst = extractInterfacesFromAnnotations(amv);
					if(lst != null){
						if(result == null){
							result = new ArrayList<String>() ;
						}
						result.addAll(lst) ;
					}
				}
				
			}
		}		
		return result;
	}

	private List<IJavaType> extractMeaningfullInterfaces(List<IJavaType> interfaces) {
		ArrayList<IJavaType> result = new ArrayList<IJavaType>() ;
l0:		for( IJavaType iType : interfaces ){
			
			QualifiedName qName = iType.getQualifiedName();
			for( String value : qName.getValues() ){
				if(!isMeaningful(value))
					continue l0;
			}
		
			result.add(iType) ;			
		}
		return result;
	}

	private boolean isMeaningful(String str) {
		
		if(str.equals("java.io.Serializable"))
			return false ;
		
		if(str.equals("java.io.Externalizable"))
			return false ;
		
		if(str.startsWith("javax.ejb"))
			return false ;
		
		return true ;
	}	
	
	public String getMessage(){
		return "A session bean must implement at least one business interface." ;
	}
}
