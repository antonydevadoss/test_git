package net.junos.space.pmd.rules.j2ee;

import net.junos.space.pmd.utils.outline.AbstractOutlineAccessor;
import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IJavaType;
import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;
import net.junos.space.pmd.utils.outline.QualifiedName;
import net.sourceforge.pmd.RuleContext;
import net.sourceforge.pmd.lang.java.rule.JavaRuleViolation;

public class ServletNameRule extends AbstractOutlineAccessor {
	
	public static final String message = "Name a Servlet like [Name]Servlet";
	
	@Override
	public void doEnd(RuleContext ctx) {
		for( IClassOutline outline : getOutlineContainer().getAllOutlines() ){
			IJavaTypeDeclaration jtd = outline.getTypeDeclaration();
			if (jtd != null)
			{
				IJavaType jt = jtd.getSuperClass();
				if (jt != null)
				{
					QualifiedName qualifiedName = jt.getQualifiedName();
					if(qualifiedName!=null){
						for(String s : qualifiedName.getValues())						
						{
							if ( !s.equals("javax.servlet.http.HttpServlet"))
								continue ;
							
							if (!jtd.getSimpleName().endsWith("Servlet"))
							{
								ctx.setSourceCodeFilename( outline.getFilePath() ) ;
								JavaRuleViolation violation
										= new JavaRuleViolation(this, 
												ctx, 
												null, 
												message,
												jtd.getLines()[0],
												jtd.getLines()[1]);
								ctx.getReport().addRuleViolation(violation) ;
							}
						}
					}
				}
			}
		}
		ctx.setSourceCodeFilename(null);
		super.doEnd(ctx);
	}
}
