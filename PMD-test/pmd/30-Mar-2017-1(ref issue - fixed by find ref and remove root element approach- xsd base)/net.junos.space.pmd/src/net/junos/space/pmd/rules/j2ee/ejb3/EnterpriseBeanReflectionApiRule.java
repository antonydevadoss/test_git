package net.junos.space.pmd.rules.j2ee.ejb3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.junos.space.pmd.rules.AbstractJavaRuleWithUselessTypeResolution;
import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IDeclaration;
import net.junos.space.pmd.utils.reference.SimpleMethodReferenceRule;
import net.junos.space.pmd.utils.reference.SimpleReferenceRule;
import net.sourceforge.pmd.RuleContext;
import net.sourceforge.pmd.lang.java.ast.ASTCompilationUnit;

public class EnterpriseBeanReflectionApiRule extends AbstractJavaRuleWithUselessTypeResolution {
	
	private static final String message = "The enterprise bean must not attempt to use the Reflection API.";

	private JavaLangReflectSearcher javaLangReflectSearcher = new JavaLangReflectSearcher() ; 
	
	private JavaLangClassInspector javaLangClassInspector = new JavaLangClassInspector() ;	
	
	public Object visit(ASTCompilationUnit node, Object data) {
		
		javaLangReflectSearcher.visit(node, data) ;
		javaLangClassInspector.visit(node, data) ;
		return null ;
	}
	
	@Override
	final public void start(RuleContext ctx) {
		javaLangReflectSearcher.start(ctx) ;
		javaLangClassInspector.start(ctx) ;
	}

	@Override
	final public void end(RuleContext ctx) {
		javaLangReflectSearcher.end(ctx) ;
		javaLangClassInspector.end(ctx) ;
	}
	
	public String getMessage(){
		return message ;
	}
	
	private class JavaLangReflectSearcher extends SimpleReferenceRule{
		
		protected boolean mustBeChecked(String qualifiedName) {
			return isEnterpriseBeanSuperclassEJB3(qualifiedName);
		}

		protected boolean isInvalid(String refQName)
		{
			boolean result = refQName.startsWith("java.lang.reflect") ;
			return  result;
		}
		
		public String getMessage(){
			return message ;
		}
		
		protected void reportError( RuleContext ctx, IClassOutline outline, IDeclaration decl ){
			EnterpriseBeanReflectionApiRule.this.reportError(ctx, outline, decl) ;
		}
		
		protected void reportError( RuleContext ctx, IClassOutline outline, int beginLine, int endLine ){
			EnterpriseBeanReflectionApiRule.this.reportError(ctx, outline, beginLine, endLine) ;
		}
	}
	
	private static final String[] targetClasses = new String[]{
		"java.lang.Class",
	} ;
	
	private static final String[] targetMethods = new String[]{
		"getAnnotation",
		"getAnnotations",
		"getClasses",
		"getComponentType",
        "getConstructor",
        "getConstructors",
        "getDeclaredAnnotations",
        "getDeclaredClasses",
        "getDeclaredConstructor",
        "getDeclaredConstructors",
        "getDeclaredField",
        "getDeclaredFields",
        "getDeclaredMethod",
        "getDeclaredMethods",
        "getDeclaringClass",
        "getEnclosingClass",
        "getEnclosingConstructor",
        "getEnclosingMethod",
        "getEnumConstants",
        "getField",
        "getFields",
        "getGenericInterfaces",
        "getGenericSuperclass",
        "getInterfaces",
        "getMethod",
        "getMethods",
        "getTypeParameters",
        "newInstance"
	} ;
	
	private class JavaLangClassInspector extends SimpleMethodReferenceRule {		
		
		public String getMessage(){
			return message ;
		}

		protected List<String> getTargetClasses() {
			return new ArrayList<String>( Arrays.asList(targetClasses) ) ;
		}
		
		protected List<String> getTargetMethods() {
			return new ArrayList<String>( Arrays.asList(targetMethods) ) ;
		}

		protected boolean mustBeChecked(String className) {		
			return isEnterpriseBeanSuperclassEJB3(className);
		}
		
		protected void reportError( RuleContext ctx, IClassOutline outline, IDeclaration decl ){
			EnterpriseBeanReflectionApiRule.this.reportError(ctx, outline, decl) ;
		}
		
		protected void reportError( RuleContext ctx, IClassOutline outline, int beginLine, int endLine ){
			EnterpriseBeanReflectionApiRule.this.reportError(ctx, outline, beginLine, endLine) ;
		}
	}
}
