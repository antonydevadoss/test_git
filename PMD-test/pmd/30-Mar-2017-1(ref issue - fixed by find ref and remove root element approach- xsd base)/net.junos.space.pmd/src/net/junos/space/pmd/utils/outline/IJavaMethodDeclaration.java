package net.junos.space.pmd.utils.outline;

import java.util.Collection;
import java.util.List;


public interface IJavaMethodDeclaration extends IDeclaration, ICanBeGeneric, ICanBeAbstract {
	
	IJavaType getReturnType() ;
	
	void setReturnType( IJavaType returnType ) ;
	
	List<IJavaType> getParameters() ;
	
	void addParameter( IJavaType param ) ;
	
	void setParameters( Collection<IJavaType> parameters  ) ;
	
	List<IJavaType> getExceptions() ;
	
	void setExceptions( List<IJavaType> exceptions ) ;

	void setParameterNames(List<String> paramNames);
	
	List<String> getParameterNames();
}
