package net.junos.space.pmd.rules.j2ee;

import net.junos.space.pmd.utils.outline.AbstractOutlineAccessor;
import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;
import net.sourceforge.pmd.RuleContext;

public class LocalInterfaceNameRule extends AbstractOutlineAccessor {
	
	public void doEnd(RuleContext ctx) {
		for (IClassOutline outline : getOutlineContainer().getAllOutlines()) {
	
			IJavaTypeDeclaration typeDeclaration = outline.getTypeDeclaration();
			if(!typeDeclaration.isInterface())
				continue ;
			
			boolean isHomeInterfase = false;
			for (IJavaTypeDeclaration interfaceType : typeDeclaration.getAllInterfaces()) {
				if (interfaceType.getQualifiedName().getValues().contains("javax.ejb.EJBLocalObject")) {
					isHomeInterfase = true;
					break;
				}
			}
			
			if (isHomeInterfase) {
				if (!outline.getQualifiedNameString().endsWith("Local")) {
					reportError(ctx, outline, typeDeclaration);
				}
			}
		}
		super.doEnd(ctx);
	}
	
	@Override
	public String getMessage() {
		return "Name a local interface like [Name]Local";
	}
}
