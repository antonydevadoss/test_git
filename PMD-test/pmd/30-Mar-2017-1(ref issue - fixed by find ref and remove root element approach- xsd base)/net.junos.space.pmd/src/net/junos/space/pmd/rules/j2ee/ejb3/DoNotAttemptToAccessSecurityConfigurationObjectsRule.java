package net.junos.space.pmd.rules.j2ee.ejb3;

import java.util.Arrays;
import java.util.HashSet;

import net.junos.space.pmd.utils.reference.SimpleReferenceRule;

public class DoNotAttemptToAccessSecurityConfigurationObjectsRule extends SimpleReferenceRule {
	
	private static final HashSet<String> targetClassesSet = new HashSet<String>(
			Arrays.asList( new String[]{
			
					"java.security.Policy",
					"java.security.Security",
					"java.security.Provider",
					"java.security.Signer",
					"java.security.Identity"
	} ) );
	
	protected boolean mustBeChecked(String qualifiedName) {
		return isEnterpriseBeanSuperclassEJB3(qualifiedName);
	}

	protected boolean isInvalid(String refQName)
	{
		boolean result = targetClassesSet.contains(refQName) ;
		return  result;
	}
	
	public String getMessage(){
		return "The enterprise bean must not attempt to access or modify the security configuration objects (Policy, Security, Provider, Signer, and Identity)." ;
	}
}
