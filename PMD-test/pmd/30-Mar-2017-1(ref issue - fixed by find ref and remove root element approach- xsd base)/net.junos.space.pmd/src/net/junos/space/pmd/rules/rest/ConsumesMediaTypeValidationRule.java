/*******************************************************************************
 * FILE NAME: ConsumesMediaTypeValidationRule.java
 * PURPOSE:  This is Rule-17 in document
 *
 *
 * Revision History: 
 * DATE:           				  AUTHOR:              	CHANGE:  
 * Tue Feb 18th Feb, 2011               Atul Bajpai       	Initial Version  
 * 
 * 
 ******************************************************************************/
package net.junos.space.pmd.rules.rest;

import java.util.List;import net.junos.space.pmd.utils.PMDConstants;import net.junos.space.pmd.utils.PMDUtils;import net.sourceforge.pmd.RuleContext;import net.sourceforge.pmd.lang.java.ast.ASTMethodDeclarator;import net.sourceforge.pmd.lang.java.rule.AbstractJavaRule;import org.apache.log4j.Logger;

/**
 * This class checks whether or not @Consumes annotation start with ' application/ '
 * while used on any Rest method if method expects DTO as input.
 */
public class ConsumesMediaTypeValidationRule extends AbstractJavaRule {
	
	/**
	 * Logger
	 */
	private static Logger log = Logger.getLogger(ConsumesMediaTypeValidationRule.class);
	
	/**
	 * This method visits all Rest methods in class and checks the @Consumes annotation,
	 * whether or not it is starting with 'application/' if return type is DTO
	 */
	public Object visit(ASTMethodDeclarator methodNode, Object data) {
		boolean isRestMethod = false;
		boolean isConsumesOnMethodExists = false;
		boolean isConsumesOnClassExists = false;
		boolean isValidConsumes = true;
		boolean isValidDTO = false;

		RuleContext ctx = (RuleContext) data;
		String methodName = methodNode.getImage();
		try{		
			// checks whether the method is REST or not.
			isRestMethod = PMDUtils.isRestMethod(methodNode);
			if (isRestMethod) {
				// checks if parameter is DTO type
				isValidDTO = PMDUtils.hasDTOParameters(methodNode);
				if (isValidDTO) {
					isConsumesOnMethodExists = PMDUtils.isMethodAnnotationExists(methodNode, PMDConstants.CONSUMES_ANNOTATION);
					isConsumesOnClassExists = PMDUtils.isClassAnnotationExists(methodNode, PMDConstants.CONSUMES_ANNOTATION);
					if (isConsumesOnMethodExists) {
						List<String> methodConsumesAnnotationValues = PMDUtils.getMethodAnnotationValue(methodNode, PMDConstants.CONSUMES_ANNOTATION);					
						isValidConsumes = PMDUtils.isMediaTypeStartWithApplication(methodConsumesAnnotationValues);					
					}
					if (!isConsumesOnMethodExists && isConsumesOnClassExists){
						List<String> classConsumesAnnotationValues = PMDUtils.getClassAnnotationValue(methodNode, PMDConstants.CONSUMES_ANNOTATION);					
						isValidConsumes = PMDUtils.isMediaTypeStartWithApplication(classConsumesAnnotationValues);
					}
				}
			}
			
			if(log.isDebugEnabled()){
				StringBuilder builder = new StringBuilder();
				builder.append("source file=").append(ctx.getSourceCodeFilename()).append(",method=").append(methodName);
				builder.append(",isRestMethod=").append(isRestMethod).append(",isValidDTO=").append(isValidDTO).append(",isConsumesOnMethodExists=");
				builder.append(isConsumesOnMethodExists).append(",isConsumesOnClassExists=").append(isConsumesOnClassExists).append(",isValidConsumes").append(isValidConsumes);
				log.debug(builder.toString());
			}
			
			if (!isValidConsumes)
				addViolation(data, methodNode);
		}catch(Exception e){
			log.error("Exception in executing method " + methodName + " of " + ctx.getSourceCodeFilename(), e);
		}
		return super.visit(methodNode, data);
	}
}
