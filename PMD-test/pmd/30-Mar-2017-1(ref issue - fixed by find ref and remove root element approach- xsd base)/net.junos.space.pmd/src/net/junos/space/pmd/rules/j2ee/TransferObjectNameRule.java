package net.junos.space.pmd.rules.j2ee;

import net.junos.space.pmd.rules.AbstractJavaRuleWithUselessTypeResolution;
import net.sourceforge.pmd.lang.java.ast.ASTClassOrInterfaceDeclaration;

public class TransferObjectNameRule extends AbstractJavaRuleWithUselessTypeResolution {
	@Override
	public Object visit(ASTClassOrInterfaceDeclaration node, Object data) {
		try {
			if (Utils.isClassOrInterfaceImplements(node, "Serializable"))
			{
				String name = node.getImage();
				if (!name.endsWith("TO"))
				{
					addViolationWithMessage(data, node, "Name a transfer object like [Name]TO");
				}
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return super.visit(node, data);
	}
}
