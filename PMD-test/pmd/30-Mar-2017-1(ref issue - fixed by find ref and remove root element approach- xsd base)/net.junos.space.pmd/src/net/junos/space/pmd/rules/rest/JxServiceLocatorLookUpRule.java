/*******************************************************************************
 * FILE NAME: JxServiceLocatorLookUpRule.java
 * PURPOSE:   This is Rule-24 in document
 *
 *
 * Revision History: 
 * DATE:           				  AUTHOR:              	CHANGE:  
 * 17th Mar, 2011                 Amit Arora	       	Initial Version  
 * 
 * Copyright (c) 2011 Juniper Networks. All rights reserved.
 ******************************************************************************/
package net.junos.space.pmd.rules.rest;

import java.util.List;import net.junos.space.pmd.utils.PMDConstants;import net.junos.space.pmd.utils.PMDUtils;import net.junos.space.pmd.utils.Project;import net.sourceforge.pmd.RuleContext;import net.sourceforge.pmd.lang.java.ast.ASTAssignmentOperator;import net.sourceforge.pmd.lang.java.ast.ASTClassOrInterfaceDeclaration;import net.sourceforge.pmd.lang.java.ast.ASTName;import net.sourceforge.pmd.lang.java.ast.ASTPrimaryPrefix;import net.sourceforge.pmd.lang.java.ast.ASTStatementExpression;import net.sourceforge.pmd.lang.java.ast.ASTVariableDeclarator;import net.sourceforge.pmd.lang.java.rule.AbstractJavaRule;import org.apache.log4j.Logger;

/**
 * Rule ensures that in REST implementation there should be JxServiceLocator.doLookup code.
 */
public class JxServiceLocatorLookUpRule extends AbstractJavaRule {

	/**
	 * Logger
	 */
	private Logger log = Logger.getLogger(JxServiceLocatorLookUpRule.class);
			
	/**
	 * Lookup expression
	 */
	private static final String JXSVCLOCATOR_LOOKUP_EXP = "JxServiceLocator.doLookup";
	
	/**
	 * Rule executes by: 
	 * 1. Gets all interfaces of this class.
	 * 2. Checks the one of the interfaces in step 2 is REST (Having @Path annotation) or not
	 * 3. If step 2 is true, get all expressions in this class to find out there is usage of 
	 *    JxServiceLocator.doLookup expression in it or not.
	 * 4. If step 3 is true, check whether is used in variable assignment or in statement exp.
	 * 5. If it is used in statement expression, verify that expression has usage of assignment operator.
	 * 6. If step 3 is not true, raise a violation.
	 * 7. If step 3 is true but if step 5 is not true then raise violation.
	 */
	public Object visit(ASTClassOrInterfaceDeclaration classNode, Object data) {
		RuleContext ctx = (RuleContext) data;
		String className = classNode.getImage();
		boolean isRestImplementation = false;
		boolean hasJxLookUpCode = false;
		boolean isUsedInStatement = false;
		boolean isUsedInVariable = false;
		ASTVariableDeclarator variableDeclaratorNode = null;
		ASTStatementExpression statementExpressionNode = null;
		try{
			//Is this rest implementation?
			List<Class<?>> impletmentsInterfaceList = PMDUtils.getInterfaceTypes(classNode, new Project(PMDConstants.WEB_SVC_PROJECT));
			for(Class<?> impletmentsInterface : impletmentsInterfaceList){
				isRestImplementation = PMDUtils.hasAnnotation(impletmentsInterface, PMDConstants.PATH_ANNOTATION);
				if(isRestImplementation)
					break;
			}
			
			if(isRestImplementation){
				List<ASTPrimaryPrefix> primaryPrefixNodeList = classNode.findChildrenOfType(ASTPrimaryPrefix.class);
				for(ASTPrimaryPrefix primaryPrefixNode : primaryPrefixNodeList){
					ASTName primaryPrefixNodeName = primaryPrefixNode.getFirstChildOfType(ASTName.class);
					if(primaryPrefixNodeName != null){
						String expression = primaryPrefixNodeName.getImage();
						
						//check if there is JxServiceLocatot.doLookup expression in code or not
						hasJxLookUpCode = expression.equals(JXSVCLOCATOR_LOOKUP_EXP);
						if(hasJxLookUpCode){
							variableDeclaratorNode = primaryPrefixNode.getFirstParentOfType(ASTVariableDeclarator.class);
							statementExpressionNode = primaryPrefixNode.getFirstParentOfType(ASTStatementExpression.class);
							isUsedInStatement = false;
							isUsedInVariable = variableDeclaratorNode != null;
							if(statementExpressionNode != null){
								ASTAssignmentOperator assignementOperatorNode = statementExpressionNode.getFirstChildOfType(ASTAssignmentOperator.class);
								if(assignementOperatorNode != null){
									isUsedInStatement = true; // check whether this expression assigned to some variable or not
								}
							}
							break;
						}
					}
				}
			}			
			if(log.isDebugEnabled()){
				StringBuilder builder = new StringBuilder();
				builder.append("source file=").append(ctx.getSourceCodeFilename()).append(",class=").append(className);
				builder.append(",isRestImplementation=").append(isRestImplementation).append(",hasJxLookUpCode=").append(hasJxLookUpCode);
				builder.append(",isUsedInStatement=").append(isUsedInStatement).append(",statementExpressionNode=").append(statementExpressionNode);
				builder.append(",isUsedInVariable=").append(isUsedInVariable).append(",variableDeclaratorNode=").append(variableDeclaratorNode);
				log.debug(builder.toString());
			}
			
			if(isRestImplementation){
				if(!hasJxLookUpCode)
					addViolation(data, classNode);
				else if(hasJxLookUpCode){
					if(!isUsedInStatement && !isUsedInVariable){
						if(variableDeclaratorNode != null)
							addViolation(data, variableDeclaratorNode);
						else if(statementExpressionNode != null) 
							addViolation(data, statementExpressionNode);						
					}
				}
			}
			
		} catch(Exception e){
			log.error("Exception in executing class " + className + " of " + ctx.getSourceCodeFilename(), e);
		}
		return super.visit(classNode, data);
		
	}
	
}
