package net.junos.space.pmd.rules.j2ee.ejb3;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import net.junos.space.pmd.utils.outline.AbstractEntityRule;
import net.junos.space.pmd.utils.outline.EjbEntity;
import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IJavaMethodDeclaration;
import net.sourceforge.pmd.RuleContext;

public class EntityPropertyAccessMethodsRule extends AbstractEntityRule {
	private static final String message = "PROPERTY access type requires public or protected accessor methods.";
	@Override
	protected void processEntities(RuleContext ctx) {
		if (AbstractEntityRule.entityRegistry == null) {
			return;
		}
		for (EjbEntity ejbEntity : AbstractEntityRule.entityRegistry.values()) {
			
			if(!ejbEntity.hasPropertyAccess())
				continue ;
			
			IClassOutline classOutline = getOutlineContainer().getClassOutline(ejbEntity.getQualifiedName());
			
			String path = classOutline.getFilePath();			
			ctx.setSourceCodeFilename( path ) ;
			
			List<IJavaMethodDeclaration> methods = classOutline.getMethods();
			for (IJavaMethodDeclaration method : methods) {
				String simpleName = method.getSimpleName();
				if (simpleName.startsWith("get") 
						|| simpleName.startsWith("is")
						|| simpleName.startsWith("set")) {
					if (!method.isPublic() && !method.isProtected()) {
						
						reportError(ctx,method,message) ;
					}
				}
			}
		}
		super.processEntities(ctx);
	}
	
	protected Set<String> getTargetAnnotations() {
		HashSet<String> set = new HashSet<String>();
		set.add("Entity") ;
		set.add("Embeddable") ;
		return set ;
	}
}
