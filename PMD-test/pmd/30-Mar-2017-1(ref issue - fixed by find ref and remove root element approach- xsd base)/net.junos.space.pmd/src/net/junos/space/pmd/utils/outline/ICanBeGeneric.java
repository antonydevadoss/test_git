package net.junos.space.pmd.utils.outline;

import java.util.Collection;
import java.util.List;

public interface ICanBeGeneric {
	
	int getGenericParamsCount() ;
	
	void setGenericParamsCount( int count ) ;
	
	List<String> getGenericParamNames() ;
	
	void setGenericParamNames( Collection<String> paramNames ) ;
	
	int getOrder( String paramName ) ;
}
