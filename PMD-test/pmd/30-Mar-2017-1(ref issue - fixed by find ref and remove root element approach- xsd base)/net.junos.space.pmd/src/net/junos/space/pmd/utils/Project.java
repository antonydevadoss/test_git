/*******************************************************************************
 * FILE NAME: Project.java
 * PURPOSE: Provides project information
 *
 *
 * Revision History: 
 * DATE:           				  AUTHOR:              	CHANGE:  
 * Tue March 9, 2011               Atul Bajpai       	Initial Version  
 * 
 * 
 ******************************************************************************/
package net.junos.space.pmd.utils;

import java.io.File;
import java.io.FileFilter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.launching.JavaRuntime;

/**
 * Class responsible for storing project information. Whether this project
 * is of type web svc or EJB.
 */
public class Project {
	
	/**
	 * Logger
	 */
	private Logger log = Logger.getLogger(Project.class);
	
	/**
	 * Class file extension
	 */
	private static final String CLASS = ".class";
	
	/**
	 * IProject type
	 */
	private IProject iProject;	
	
	/**
	 * Class loader
	 */
	private ClassLoader classLoader;
	
	/**
	 * Constructor accepting project type
	 * @param projectType Type of project
	 */
	public Project(String projectType){		
		try {
			IWorkspace ws = ResourcesPlugin.getWorkspace();			
			final IProject[] projects = ws.getRoot().getProjects();		
			if (projects != null && projects.length > 0) {
				for (IProject project : projects) {				
					if (project.getDescription().getComment().equals(projectType)) {
						iProject = project;
						initializeClassLoader();
						break;
					}
				}
			}
		}catch (CoreException e) {		
			log.info("Ignore it: " + e.getMessage());
		}
	}

	/**
	 * Initialize class loader for this project
	 */
	private void initializeClassLoader() {
		try {
			if (iProject != null) {
				IJavaProject project = JavaCore.create(iProject);
				String[] classPathEntries;
				classPathEntries = JavaRuntime.computeDefaultRuntimeClassPath(project);

				List<URL> urlList = new ArrayList<URL>();
				for (int i = 0; i < classPathEntries.length; i++) {
					String entry = classPathEntries[i];
					IPath path = new Path(entry);
					URL url = path.toFile().toURI().toURL();
					urlList.add(url);
				}
				ClassLoader parentClassLoader = project.getClass()
						.getClassLoader();
				URL[] urls = (URL[]) urlList.toArray(new URL[urlList.size()]);
				classLoader = new URLClassLoader(urls,
						parentClassLoader);				
			}
		} catch (CoreException e) {
			log.info("Ignore it: " + e.getMessage());
		} catch (MalformedURLException e) {
			log.info("Ignore it: " + e.getMessage());
		}
	}

	/**
	 * Load class by using project class loader
	 * @param className Name of class
	 * @return Class type
	 */
	public Class<?> loadClass(String className) {	
		Class<?> clazz = null;
		if(iProject != null && classLoader != null){
			try {				
				clazz = classLoader.loadClass(className);			
			} catch (ClassNotFoundException e) {
				log.info("Ignore it: class - " + className + " not present. ");
			}
		}
		return clazz;
	}
	
	/**
	 * Returns location of workspace.
	 * @return Workspace absolute path
	 */
	public String getWorkSpaceLocation(){
		IWorkspace ws = ResourcesPlugin.getWorkspace();		
		String path = new File(ws.getRoot().getLocation().toOSString()).getAbsolutePath();
		return path;
	}
	
	/**
	 * Gets project name
	 * @return Name of project
	 */
	public String getProjectName(){
		String projectName = "";
		if(iProject != null)
			projectName = iProject.getName();
		return projectName;
	}
	
	/**
	 * Returns the path of project
	 * 
	 * @return
	 */
	public String getProjectPath(){
		return getWorkSpaceLocation() + File.separator + getProjectName();
	}
	
	/**
	 * Find all implementations of a interface in class path. It does not scan for jar file.
	 * Get classpath of project
	 * For each classpath entry 
	 * If entry is directory then call findImplementationInDirectory method  
	 * 
	 * @param interfaceClass Interface type 
	 * @return List of implementing classes
	 */
	public List<Class<?>> findAllImplementingClasses(Class<?> interfaceClass){
		
		List<Class<?>> classFiles = new ArrayList<Class<?>>();
		try {		
			IJavaProject project = JavaCore.create(iProject);
			String[] classPathEntries = JavaRuntime.computeDefaultRuntimeClassPath(project);
			for(String classPathEntry : classPathEntries) {
				File file = new File(classPathEntry);
				if (file.isDirectory()) {
					classFiles.addAll(findImplementationInDirectory(file, "", interfaceClass));
				}				
			}
		} catch (CoreException e) {			
			log.info("Ignore it: " + e.getMessage());
		}
		return classFiles;
	}
	
	/**
	 * Find implementations of interface class in directory.
	 * List all files in directory:
	 *  If file is directory 
	 *  	Make the path by prepending input path
	 *  	Call recursively findImplementationInDirectory for this modified path
	 *  Else
	 *  	Replace / with . in file name
	 *  	Remove .class from it and load the class name
	 *  	Check this class is assignable to interface and it is not same as interface
	 *  	Then add this class as valid implementation
	 * 
	 * @param directory Directory
	 * @param path Path of directory relative to classpath entry
	 * @param interfaceClass interface class
	 * @return List of implementations
	 */
	private List<Class<?>> findImplementationInDirectory(File directory, String path, Class<?> interfaceClass){
		List<Class<?>> classFiles = new ArrayList<Class<?>>();
		try {
			File[] files = directory.listFiles(new ClassOrDirectoryFilter());
			for(File file : files){
				if(file.isDirectory()){
					String fullPath = "";
					if(path.equals(""))
						fullPath = file.getName();
					else	 
						fullPath = path + File.separator + file.getName(); 
					classFiles.addAll(findImplementationInDirectory(file, fullPath, interfaceClass));
				} else {
					String fullPath = path + File.separator + file.getName();
					String fullyQualifiedName = fullPath.replace(File.separator, ".").replace(CLASS, "");
					Class<?> fullyQualifiedClass = classLoader.loadClass(fullyQualifiedName);					
					if(interfaceClass.isAssignableFrom(fullyQualifiedClass) && interfaceClass != fullyQualifiedClass)
						classFiles.add(fullyQualifiedClass);
				}
			}
		} catch (ClassNotFoundException e) {					
			log.info("Ignore it: " + e.getMessage());
		}
		return classFiles;
	}
	
	/**
	 * Filter for accepting .class file and for directory
	 */
	private class ClassOrDirectoryFilter implements FileFilter{
		@Override
		public boolean accept(File pathname) {
			if(pathname.getName().contains(CLASS) || pathname.isDirectory())
				return true;
			else
				return false;
		} 		
	}
	
}
