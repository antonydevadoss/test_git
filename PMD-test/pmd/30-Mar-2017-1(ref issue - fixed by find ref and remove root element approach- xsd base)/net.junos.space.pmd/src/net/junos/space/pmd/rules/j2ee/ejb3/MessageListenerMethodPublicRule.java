package net.junos.space.pmd.rules.j2ee.ejb3;

import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IJavaMethodDeclaration;
import net.sourceforge.pmd.RuleContext;

public class MessageListenerMethodPublicRule extends AbstractMessageListenerMethodRule {
	
	public String getMessage(){
		return "The message listener methods of the message-driven bean must be declared as public." ;
	}
	
	protected void checkMethod(IClassOutline outline, IJavaMethodDeclaration md, RuleContext ctx) {
		
		if( md.isPublic() )
			return ;
		
		reportError(ctx, outline, md) ;		
	}

}
