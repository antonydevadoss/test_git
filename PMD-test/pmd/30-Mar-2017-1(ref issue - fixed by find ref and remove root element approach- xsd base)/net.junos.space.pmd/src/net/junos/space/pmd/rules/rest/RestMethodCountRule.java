package net.junos.space.pmd.rules.rest;



import net.junos.space.pmd.utils.PMDUtils;
import net.sourceforge.pmd.RuleContext;
import net.sourceforge.pmd.lang.java.ast.ASTMethodDeclarator;
import net.sourceforge.pmd.lang.java.rule.AbstractJavaRule;

import org.apache.log4j.Logger;



/**

 * This class checks whether or not @RBAC annotation present on every REST Method.

 */

public class RestMethodCountRule extends AbstractJavaRule {

	private static int count = 0;
	 public void start(RuleContext paramRuleContext) {
//		 Integer val = new Integer(0);
//		 paramRuleContext.setAttribute("count", val);
		 count = 0;
	 }

	/**

	 * Logger

	 */

	private static Logger log = Logger.getLogger(MissingRBACRule.class);



	/**

	 * Visits all the REST methods of this class and checks whether or 

	 * not @RBAC annotation present on every method.

	 */

	public Object visit(ASTMethodDeclarator methodNode, Object data) {

		RuleContext ctx = (RuleContext) data;

		String methodName = methodNode.getImage();

		try {

			boolean isRBACExists = false;

			boolean isRestMethod = false;



			// checks whether the method is REST or not.

			isRestMethod = PMDUtils.isRestMethod(methodNode);

			if (isRestMethod) {
				count++;
//				ctx.setAttribute("count", (Integer)ctx.getAttribute("count")+1);
				StringBuilder builder = new StringBuilder();

				builder.append("source file=").append(ctx.getSourceCodeFilename()).append(",method=").append(methodName).append(", Count:"+ctx.getAttribute("count"));
				log.debug(builder.toString());
			}

		} catch (Exception e) {

			log.error("Exception in executing method " + methodName + " of " + ctx.getSourceCodeFilename(), e);

		}

		return super.visit(methodNode, data);

	}
	
	
	@Override
	 public void end(RuleContext paramRuleContext) {
		 System.out.println("FINALLLLLLLLLLLLLLLLLL Count : "+count);
	 }
	

}

