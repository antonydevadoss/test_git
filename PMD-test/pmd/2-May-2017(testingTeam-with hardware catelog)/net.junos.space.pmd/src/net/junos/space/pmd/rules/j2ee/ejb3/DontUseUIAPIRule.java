package net.junos.space.pmd.rules.j2ee.ejb3;

public class DontUseUIAPIRule extends
			net.junos.space.pmd.rules.j2ee.DontUseUIAPIRule {
	
	protected boolean mustBeChecked(String qualifiedName) {
		return isEnterpriseBeanSuperclassEJB3(qualifiedName);
	}
}