package net.junos.space.pmd.utils.outline;

import java.util.Collection;
import java.util.List;


public interface IJavaTypeDeclaration extends IDeclaration, ICanBeAbstract, ICanBeGeneric {
	
	boolean isPrimitive() ;
	
	void setPrimitive( boolean isPrimitive ) ;	
	
	boolean isSimilar( IJavaTypeDeclaration other ) ;
	
	boolean isInterface() ;
	
	void setIsInterface( boolean isInterface ) ;
	
	IJavaType getSuperClass() ;
	
	void setSuperClass( IJavaType superClass ) ;
	
	List<IJavaType> getInterfaces() ;
	
	List<IJavaTypeDeclaration> getAllInterfaces() ;
	
	List<IJavaTypeDeclaration> getUndefinedInterfaces( IOutlineContainer container) ;
	
	List<IJavaTypeDeclaration> getAllSuperClasses() ;
	
	List<IJavaTypeDeclaration> getUndefinedSuperClasses( IOutlineContainer container) ;
	
	void setInterfaces( Collection<IJavaType> interfaces) ;
	
	List<IJavaTypeDeclaration> getNestedClasses() ;
	
	void setNestedClasses(Collection<IJavaTypeDeclaration> nestedClasses) ;
	
	void addSubclass(IJavaTypeDeclaration subclass);
	
	List<IJavaTypeDeclaration> getSubclasses() ;
	
	boolean isEnum() ;
	
	void setIsEnum( boolean isEnum ) ;
	
	List<IEnumConstant> getEnumConstants() ;
	
	void setEnumConstants(List<IEnumConstant> enumConstants) ;	
}
