package net.junos.space.pmd.utils.outline;

import java.util.ArrayList;

public class MemberValueArray implements IAnnotationMemberValue {
	
	public MemberValueArray(ArrayList<IAnnotationMemberValue> values) {
		super();
		this.values = values;
	}

	private final ArrayList<IAnnotationMemberValue> values ;

	@Override
	public String getStringValue() {
		
		if(values == null)
			return "" ;
		
		StringBuilder bld = new StringBuilder() ;
		
		int size = values.size() ;
		for(int i = 0 ; i < size ; i++){
			IAnnotationMemberValue mem = values.get(i);
			bld.append(mem.getStringValue()) ;
			if(i<size-1)
				bld.append("; ") ;
		}

		return bld.toString();
	}

	@Override
	public int getKind() {
		if(values == null || values.isEmpty())
			return IJavaAnnotation.ANNOTATION_TYPE_STRING;
		
		return values.get(0).getKind() ;
	}

	public ArrayList<IAnnotationMemberValue> getValues() {
		return values;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((values == null) ? 0 : values.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MemberValueArray other = (MemberValueArray) obj;
		if (values == null) {
			if (other.values != null)
				return false;
		} else if (!values.equals(other.values))
			return false;
		return true;
	}

}
