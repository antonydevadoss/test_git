package net.junos.space.pmd.rules.j2ee.ejb3;

import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;

public class EnterpriseBeanPassThisReferenceRule extends
	net.junos.space.pmd.rules.j2ee.EnterpriseBeanPassThisReferenceRule {

	protected boolean mustBeChecked(IJavaTypeDeclaration decl) {
		return isEnterpriseBeanEJB3(decl);
	}
	
	@Override
	protected String getOwnKey() {		
		return this.getClass().getCanonicalName() ;
	}
}
