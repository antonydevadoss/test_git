package net.junos.space.pmd.utils.reference;

import java.util.List;

public class MethodInvocationReference extends AbstractReference{
	
	private static final String LINE_SEPARATOR = System.getProperty("line.separator");


	public MethodInvocationReference(IReference headReference, String methodName, List<IReference> arguments, int beginLine, int startLine)
	{
		super(beginLine, startLine);
		this.headReference = headReference;
		this.methodName = methodName;
		this.arguments = arguments;
	}

	private final IReference headReference ;
	private final String methodName ;
	private final List<IReference> arguments ;
	
	
	public IReference getHeadReference() {
		return headReference;
	}

	public String getMethodName() {
		return methodName;
	}
	
	public List<IReference> getArguments() {
		return arguments;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((arguments == null) ? 0 : arguments.hashCode());
		result = prime * result
				+ ((headReference == null) ? 0 : headReference.hashCode());
		result = prime * result
				+ ((methodName == null) ? 0 : methodName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		MethodInvocationReference other = (MethodInvocationReference) obj;
		if (arguments == null) {
			if (other.arguments != null)
				return false;
		} else if (!arguments.equals(other.arguments))
			return false;
		if (headReference == null) {
			if (other.headReference != null)
				return false;
		} else if (!headReference.equals(other.headReference))
			return false;
		if (methodName == null) {
			if (other.methodName != null)
				return false;
		} else if (!methodName.equals(other.methodName))
			return false;
		return true;
	}
	
	@Override
	public String toString(){
		
		StringBuilder bld = new StringBuilder() ;
		bld.append("Method Invocation Reference: ").append(LINE_SEPARATOR) ;
		bld.append("Head: ").append(headReference.toString()).append(LINE_SEPARATOR);
		bld.append("Method name: ").append(methodName).append(LINE_SEPARATOR) ;
		
		if(arguments!=null){
			bld.append("Arguments: ").append(LINE_SEPARATOR);
			
			for(IReference ref : arguments)
				bld.append(ref.toString()).append(LINE_SEPARATOR);
		}
		
		String result = bld.toString();
		return result ;
	}

}
