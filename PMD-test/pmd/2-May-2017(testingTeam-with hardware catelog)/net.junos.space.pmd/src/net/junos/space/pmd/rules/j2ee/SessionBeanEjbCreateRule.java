package net.junos.space.pmd.rules.j2ee;

import net.junos.space.pmd.utils.outline.IClassOutline;

public class SessionBeanEjbCreateRule extends AbstractCreateMethodExistenceRule {

	@Override
	public boolean isProperBean(IClassOutline outline) {
		return isSessionBeanEJB2(outline.getTypeDeclaration());
	}
	
	@Override
	public String getMessage() {
		return "Provide a ejbCreate Method For A Session Bean";
	}
}
