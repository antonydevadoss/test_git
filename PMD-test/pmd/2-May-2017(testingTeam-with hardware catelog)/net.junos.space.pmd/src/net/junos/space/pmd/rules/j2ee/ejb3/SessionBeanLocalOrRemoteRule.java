package net.junos.space.pmd.rules.j2ee.ejb3;

import java.util.ArrayList;
import java.util.List;

import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;
import net.junos.space.pmd.utils.outline.QualifiedName;
import net.sourceforge.pmd.RuleContext;

public class SessionBeanLocalOrRemoteRule extends AbstractBusinessInterfacesInspector {
	
	@Override
	public String getMessage() {
		return "The same business interface cannot be both a local and a remote business interface of the bean.";
	}

	@Override
	public void doEnd(RuleContext ctx) {
		List<IClassOutline> allOutlines = getOutlineContainer().getAllOutlines();
		
		for (IClassOutline classOutline : allOutlines) {
			if (!isSessionBeanEJB3(classOutline.getQualifiedNameString()))
				continue ;
			
			IJavaTypeDeclaration typeDeclaration = classOutline.getTypeDeclaration();
				
			ArrayList<IJavaTypeDeclaration> localInterfaces = extractLocalDeclarationsFromAnnotation(typeDeclaration);
			ArrayList<IJavaTypeDeclaration> remoteInterfaces = extractRemoteDeclarationsFromAnnotation(typeDeclaration);
				
			if (localInterfaces == null || remoteInterfaces == null)
				continue ;
			
			
			for (IJavaTypeDeclaration rIDecl : remoteInterfaces) {
				
				QualifiedName rQName = rIDecl.getQualifiedName();
				List<String> rValues = rQName.getValues();
				if(rValues==null || rValues.isEmpty())
					continue ;
				
				for (IJavaTypeDeclaration lIDecl : localInterfaces) {
					
					QualifiedName lQName = lIDecl.getQualifiedName();
					List<String> lValues = lQName.getValues();
					if(lValues==null || lValues.isEmpty())
						continue ;
					
					for(String rName : rValues){
						for( String lName : lValues ){
							if(rName.equals(lName))
								reportError(ctx, classOutline, typeDeclaration) ;
						}
					}
				}
			}
		}

	}

	@Override
	protected ArrayList<IClassOutline> extractTargetInterfaces(
			IJavaTypeDeclaration typeDeclaration) {
		return null;
	}
}
