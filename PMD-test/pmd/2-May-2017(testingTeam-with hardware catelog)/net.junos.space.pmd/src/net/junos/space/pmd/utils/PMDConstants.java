/*******************************************************************************
 * FILE NAME: PMDConstants.java
 * PURPOSE: Provides the Constants used in this framework.
 *
 *
 * Revision History: 
 * DATE:           				  AUTHOR:              	CHANGE:  
 * Tue March 9, 2011               Atul Bajpai       	Initial Version  
 * 
 * 
 ******************************************************************************/
package net.junos.space.pmd.utils;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * This class provides all the constants values used in this framwork. 
 */
public class PMDConstants {

	public static enum Method {
		
		add(EnumSet.of(HttpMethod.POST), CRUDEnum.CREATE),
		get(EnumSet.of(HttpMethod.GET), CRUDEnum.READ),
		update(EnumSet.of(HttpMethod.PUT), CRUDEnum.UPDATE),
		delete(EnumSet.of(HttpMethod.DELETE), CRUDEnum.DELETE);
		
		private static final Map<String, Method> METHOD_MAP = new HashMap<String, Method>();
		
		static {
			for(Method method : Method.values()) {
				METHOD_MAP.put(method.toString(), method);
			}
		}
		
		CRUDEnum capabilityType;
		
		EnumSet<HttpMethod> method;
		
		Method(EnumSet<HttpMethod> method, CRUDEnum capabilityType) {
			this.method = method;
			this.capabilityType = capabilityType;
		}
		
		public static Method getMethod(String methodName) {
			Method method = null;
			Iterator<String> methodKeyItr = METHOD_MAP.keySet().iterator();
			while(methodKeyItr.hasNext()){
				String methodKey = methodKeyItr.next();
				if(methodName.indexOf(methodKey) != -1){
					method = METHOD_MAP.get(methodKey);
					break;
				}
			}
			return method;
		}
		
		public boolean isValidHttpMethod(String httpMethodName){
			boolean isValidHttpMethod = false;
			isValidHttpMethod = HttpMethod.isValidHttpMethod(httpMethodName);
			if(isValidHttpMethod){
				HttpMethod httpMethod = HttpMethod.valueOf(httpMethodName);				
				isValidHttpMethod = this.method.contains(httpMethod);				
			}
			return isValidHttpMethod;
		}
		
		public boolean isValidCapability(String capability){
			return this.capabilityType.toString().equals(capability);
		}
		
	}

	public static enum NonDTO {
		Boolean, Byte, Character, Double, Float, Integer, Long, Short, String, Response, PagingContext, ApiContextInterface, ScheduleContext, UriContext;

		private static final Map<String, NonDTO> DTO_MAP = new HashMap<String, NonDTO>();
		
		static {
			for(NonDTO dto : NonDTO.values()) {
				DTO_MAP.put(dto.toString(), dto);
			}
		}
			
		public static boolean isDTO(String name) {
			if(name.startsWith(JAVA_LANG)){
				name = name.substring(name.indexOf(JAVA_LANG)+ JAVA_LANG.length());
			}
			if(name.startsWith(JAVAX_RS)){
				name = name.substring(name.indexOf(JAVAX_RS)+ JAVAX_RS.length());
			}
			return DTO_MAP.get(name) == null ? true : false;
		}
	};

	public static enum CRUDEnum {
		READ, CREATE, DELETE, UPDATE;
	}
	
	public static enum HttpMethod {

		GET, POST, PUT, DELETE;

		private static final Map<String, HttpMethod> HTTP_METHOD_MAP = new HashMap<String, HttpMethod>();

		static {
			for (HttpMethod method : HttpMethod.values()) {
				HTTP_METHOD_MAP.put(method.toString(), method);
			}
		}

		public static boolean isValidHttpMethod(String name) {
			return HTTP_METHOD_MAP.get(name) == null ? false : true;
		}

	};

	public static final String PATH_ANNOTATION = "Path";
	public static final String RBAC_ANNOTATION = "RBAC";
	public static final String CAPABILITY = "capability";
	public static final String CONSUMES_ANNOTATION = "Consumes";
	public static final String PRODUCES_ANNOTATION = "Produces";
	public static final String PATHPARAM_ANNOTATION = "PathParam";
	public static final String MEDIA_TYPE_START_STRING = "application/";
	public static final String CHARSET_STRING = ";charset=UTF-8";
	public static final String SEMICOLON = ";";
	public static final String JAVA_LANG = "java.lang.";
	public static final String JAVAX_RS = "javax.ws.rs.core.";
	public static final String CURLYBRACE_OPENING = "{";
	public static final String CURLYBRACE_CLOSING = "}";
	public static final String SLASH = "\\";
	public static final String QUOTE = "\"";
	public static final String RBAC_TYPE = "type";
	public static final String MODULE_XML_DIR = "/EarContent/META-INF/paradigm/";
	public static final String MODULE_XML_NAME = "-module.xml";
	public static final String EAR_PROJECT = "EAR_PROJECT";
	public static final String ALL_CAPABILITY = "all-capabilities";
	public static final String NAME = "name";
	public static final String WEB_SVC_PROJECT = "WEBSVC_PROJECT";
	public static final String WEB_XML_DIR = "/web/WEB-INF/";
	public static final String WEB_XML_NAME = "web.xml";
	public static final String LISTENER_XPATH = "//listener/listener-class";
	public static final String CONTEXT_PARAM_XPATH = "//context-param";
	public static final String CONTEXT_PARAM_NAME = "param-name";
	public static final String CONTEXT_PARAM_VALUE = "param-value";
	public static final String CONTEXT_PARAM_REST_EASY_SCAN = "resteasy.scan";
	public static final String LISTENER_BOOT_STRAP = "org.jboss.resteasy.plugins.server.servlet.ResteasyBootstrap";
	public static final String LISTENER_INFO_SERVICE = "net.juniper.jmp.interceptors.infoservice.InfoServiceInterceptor";
	public static final String XML_ROOT_ELEMENT	= "XmlRootElement";
	public static final String XML_ACCESSOR_TYPE = "XmlAccessorType";
	public static final String XML_ELEMENT = "XmlElement";
	public static final String XML_ATTRIBUTE = "XmlAttribute";
	public static final String CONTEXT_ANNOTATION = "Context";
	public static final String FILTERABLE_ANNOTATION = "Filterable";
	public static final String SORTABLE_ANNOTATION = "Sortable";
	public static final String VALUE_ANNOTATION_METHOD = "value";
}	

