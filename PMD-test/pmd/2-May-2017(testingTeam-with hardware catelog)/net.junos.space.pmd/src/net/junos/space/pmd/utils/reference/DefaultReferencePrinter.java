package net.junos.space.pmd.utils.reference;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;

import net.junos.space.pmd.utils.outline.IDeclaration;
import net.junos.space.pmd.utils.outline.IJavaFieldDeclaration;
import net.junos.space.pmd.utils.outline.IJavaMethodDeclaration;



public class DefaultReferencePrinter {
	
	private final String lineSeparator = System.getProperty("line.separator") ;
	
	public void print( IReferenceContainer container, OutputStream os,
			String indent, String indentIncrement ) throws UnsupportedEncodingException, IOException{
		
		for( IClassReferenceStorage<IReference> cRefStorage : container.getAllTemporaryClassStorages() ){
			
			IClassReferenceStorage<SimpleClassReference> resolvedRefs
					= container.getResolvedReferenceListForClass(cRefStorage.getQualifiedName()) ;
			
			print( cRefStorage, resolvedRefs, os, indent, indentIncrement ) ;
			os.write((lineSeparator + lineSeparator).getBytes("UTF-8")) ;
		}
		
	}

	private void print(IClassReferenceStorage<IReference> cRefStorage,
			IClassReferenceStorage<SimpleClassReference> resolvedRefStorage, OutputStream os,
			String indent, String indentIncrement) throws UnsupportedEncodingException, IOException {
		
		os.write( (indent+"Class: "+cRefStorage.getQualifiedName()).getBytes("UTF-8") ) ;
		os.write((lineSeparator + lineSeparator).getBytes("UTF-8")) ;
		
		String methodIndent = indent + indentIncrement ;
		String refIndent = indent + indentIncrement + indentIncrement ;
		for( IDeclaration decl : cRefStorage.getMethods() ){
			
			String type ;
			if(decl instanceof IJavaMethodDeclaration){
				type = "Method" ;
			} else if(decl instanceof IJavaFieldDeclaration){
				type = "Field" ;
			} else
				continue ;				
			
			String methodName = decl.getSimpleName() ;
			os.write( (methodIndent+type+": "+methodName).getBytes("UTF-8") ) ;
			os.write(lineSeparator.getBytes("UTF-8")) ;
			
			List<IReference> methodRefs = cRefStorage.getMethodReferences(decl);
			List<SimpleClassReference> resolvedRefs = null ;
			if(resolvedRefStorage != null)
				resolvedRefs = resolvedRefStorage.getMethodReferences(decl);
			
			for( IReference ref : methodRefs ){
				print(ref, os, refIndent, indentIncrement) ;
				os.write(lineSeparator.getBytes("UTF-8")) ;
			}
			
			if( resolvedRefs != null && !resolvedRefs.isEmpty() ){				
				os.write( (refIndent+"ResolvedRefs: ").getBytes("UTF-8") ) ;
				os.write(lineSeparator.getBytes("UTF-8")) ;
				for(SimpleClassReference rr : resolvedRefs){
					os.write( (refIndent+rr.toString() + lineSeparator ).getBytes("UTF-8") ) ;					
				}
			}
			os.write(lineSeparator.getBytes("UTF-8")) ;
			os.write(lineSeparator.getBytes("UTF-8")) ;
		}
	}

	private void print(IReference ref, OutputStream os, String indent, String indentIncrement )
			throws UnsupportedEncodingException, IOException
	{
		if(ref instanceof AbstractReference){
			AbstractReference aRef = (AbstractReference) ref ;
			os.write( indent.getBytes("UTF-8") ) ;
			os.write( ("lines: " + aRef.getBeginLine() + "-" + aRef.getEndLine()).getBytes("UTF-8") ) ;
			os.write(lineSeparator.getBytes("UTF-8")) ;
		}
		
		if( ref instanceof MethodInvocationReference ){			
			
			MethodInvocationReference cr = (MethodInvocationReference) ref ;
			String indent1 = indent+"|" ;
			os.write( (indent1 + "head:" + lineSeparator).getBytes("UTF-8") ) ;
			print(cr.getHeadReference(), os, indent1+indentIncrement, indentIncrement) ;
			
			String methodName = cr.getMethodName();
			if(methodName != null)
				os.write( (indent1 + "method:" + methodName + lineSeparator).getBytes("UTF-8") ) ;
			
			List<IReference> arguments = cr.getArguments();
			if(arguments != null){
				os.write( (indent1 + "arguments:" + lineSeparator).getBytes("UTF-8") ) ;
				for(IReference arg : arguments){
					print(arg,os,indent1+indentIncrement,indentIncrement) ;
					os.write(lineSeparator.getBytes("UTF-8")) ;
				}
			}
		} else if( ref instanceof FieldAccessReference){
			
			FieldAccessReference fRef = (FieldAccessReference) ref ;
			String indent1 = indent+"|" ;
			os.write( (indent1 + "head:" + lineSeparator).getBytes("UTF-8") ) ;
			print(fRef.getHeadReference(), os, indent1+indentIncrement, indentIncrement) ;
			
			String fieldName = fRef.getFieldName() ;
			if(fieldName != null){
				os.write( (indent1 + "field:" + fieldName + lineSeparator).getBytes("UTF-8") ) ;			
			}			
			
		} else if( ref != null) {
			os.write( indent.getBytes("UTF-8") ) ;
			os.write( ref.toString().getBytes("UTF-8") ) ;
			os.write(lineSeparator.getBytes("UTF-8")) ;
		}
		else  {
			os.write( indent.getBytes("UTF-8") ) ;
			os.write( "null ref".getBytes("UTF-8") ) ;
			os.write(lineSeparator.getBytes("UTF-8")) ;
		}
	}

	public void writeDump(IReferenceContainer oc, String path) {
		File f = new File( path ) ;
		f.getParentFile().mkdirs() ;
		if(f.exists())
			f.delete() ;
		
		try {
			f.createNewFile() ;
			FileOutputStream fos = new FileOutputStream(f) ;
			print( oc, fos, "\t", "\t\t" ) ;
			fos.close() ;			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
