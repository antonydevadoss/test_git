package net.junos.space.pmd.utils.reference;

import java.util.List;

import net.junos.space.pmd.utils.outline.IJavaType;

public class AllocationReference extends AbstractReference {

	public AllocationReference(IJavaType type, List<IReference> arguments, int beginLine, int endLine)
	{
		super(beginLine, endLine);
		this.type = type ;
		this.arguments = arguments;
	}

	private final List<IReference> arguments ;
	private final IJavaType type ;
	
	
	public List<IReference> getArguments() {
		return arguments;
	}


	public IJavaType getType() {
		return type;
	}
	

}
