package net.junos.space.pmd.utils.reference;

import java.util.Collection;
import java.util.List;

import net.junos.space.pmd.utils.outline.IJavaType;
import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;
import net.junos.space.pmd.utils.outline.QualifiedName;

public class JavaTypeReducedImpl implements IJavaType {

	private QualifiedName qualifiedName;
	private String simpleName;

	public JavaTypeReducedImpl(String qualifiedName) {
		super();		
		int ind = qualifiedName.lastIndexOf(".") ;
		if(ind < 0)
			ind = -1 ;
		this.simpleName = qualifiedName.substring(ind+1) ;
		this.qualifiedName = new QualifiedName(qualifiedName );
		
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((qualifiedName == null) ? 0 : qualifiedName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JavaTypeReducedImpl other = (JavaTypeReducedImpl) obj;
		if (qualifiedName == null) {
			if (other.qualifiedName != null)
				return false;
		} else if (!qualifiedName.equals(other.qualifiedName))
			return false;
		return true;
	}

	@Override
	public IJavaTypeDeclaration getDeclaration() {
		return null;
	}

	@Override
	public void setDeclaration(IJavaTypeDeclaration declaration) {}

	@Override
	public boolean isArray() {

		return false;
	}

	@Override
	public void setIsArray(boolean isArray) {
	}

	@Override
	public List<IJavaType> getGenericParameters() {
		return null;
	}

	@Override
	public void setGenericParameters(Collection<IJavaType> params) {
	}

	@Override
	public int getArrayDepth() {
		return 0;
	}

	@Override
	public void setArrayDepth(int arrayDepth) {
	}

	@Override
	public String getSimpleName() {		
		return simpleName;
	}

	@Override
	public void setSimpleName(String simpleName) {
	}

	@Override
	public QualifiedName getQualifiedName() {
		return qualifiedName ;
	}

	@Override
	public void setQualifiedName(QualifiedName qualifiedName) {
		
	}
	
	@Override
	public String toString(){
		return getQualifiedName()!=null? getQualifiedName().toString() : getSimpleName() ;
	}

	@Override
	public boolean isSimilar(IJavaType another) {		
		return this.getSimpleName().equals(another.getSimpleName());
	}

}
