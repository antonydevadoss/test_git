package net.junos.space.pmd.rules.xml;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import net.sourceforge.pmd.RuleContext;

public class AbstractJSPRule extends JavaProjectCollector {
	
	private static boolean isComplete_JSP = false ;
	
	private static ArrayList<String> fileList = null ;
	
	public void doStart(RuleContext ctx){
		
		isComplete_JSP = false ;
		if(fileList!=null)
			fileList.clear() ;
	}
	
	public void doEnd(RuleContext ctx){
		
		if(!isComplete_JSP){
			isComplete_JSP = true ;
			extractJSPs() ;
		}
		processFiles(ctx) ;
	}	

	private void extractJSPs() {
		HashSet<File> projects = getProjects();
		if(projects == null || projects.isEmpty())
			return ;
		
		for( File project : projects )
			processProject(project) ;				
	}

	private void processProject(File project) {
				
		if(!project.isDirectory())
			return ;
		
		File webDir = new File(project,"web") ;
		if(!webDir.exists() || !webDir.isDirectory())
			return ;
		
		if(fileList == null)
			fileList = new ArrayList<String>() ;
		
		extractJSP( webDir ) ;		
	}

	private void extractJSP(File dir) {
		
		File[] listFiles = dir.listFiles();
		if(listFiles == null )
			return ;
		
		for(File f : listFiles){
			if(f.isDirectory()){
				extractJSP(f) ;
			}
			else if(f.getName().toLowerCase().endsWith(".jsp")){
				fileList.add( f.getAbsolutePath() ) ;
			}
		}		
	}
	
	private void processFiles(RuleContext ctx) {
		
		if(fileList == null)
			return ;
		
		for( String path : fileList ){						
			processSingleJSP( path, ctx ) ;
		}		
	}

	protected void processSingleJSP(String path, RuleContext ctx) {}

}