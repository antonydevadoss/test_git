package net.junos.space.pmd.utils.reference;

import java.util.Collection;
import java.util.List;

import net.junos.space.pmd.utils.outline.IDeclaration;
import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;



public interface IClassReferenceStorage<T> {

	String getQualifiedName();

	List<T> getClassReferences();

	List<T> getMethodReferences(IDeclaration method);

	void addMethodReferences(IDeclaration method, Collection<T> refs);

	List<IDeclaration> getMethods() ;
	
	IJavaTypeDeclaration getDeclaration() ;
}

