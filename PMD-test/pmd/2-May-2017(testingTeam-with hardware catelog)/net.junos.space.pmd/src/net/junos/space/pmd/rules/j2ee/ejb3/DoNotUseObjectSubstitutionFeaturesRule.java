package net.junos.space.pmd.rules.j2ee.ejb3;

public class DoNotUseObjectSubstitutionFeaturesRule extends 
			net.junos.space.pmd.rules.j2ee.DoNotUseObjectSubstitutionFeaturesRule
{
	public boolean isProperBean(String className) {
		return isEnterpriseBeanSuperclassEJB3(className);
	}
	
}
