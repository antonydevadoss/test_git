package net.junos.space.pmd.rules.j2ee.ejb3;


import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;
import net.junos.space.pmd.utils.outline.SpecialInspector;
import net.sourceforge.pmd.lang.java.ast.ASTMethodDeclaration;
import net.sourceforge.pmd.lang.java.ast.ASTSynchronizedStatement;

public class EnterpriseBeanSynchronizationRule extends SpecialInspector {
	
	public Object visit(ASTSynchronizedStatement node, Object data) {
		
		registerPossibleViolation(node) ;			
		return super.visit(node, data);
	}
	
	public Object visit(ASTMethodDeclaration node, Object data) {
		
		if( node.isSynchronized() )
			registerPossibleViolation(node) ;
		
		return super.visit(node, data);
	}

	@Override
	public String getMessage(){
		return "An enterprise bean must not use thread synchronization primitives." ;
	}
	
	protected boolean mustBeChecked(IJavaTypeDeclaration decl) {
		return isEnterpriseBeanEJB3(decl);
	}

	@Override
	protected String getOwnKey() {		
		return this.getClass().getCanonicalName() ;
	}
}
