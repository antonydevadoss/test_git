package net.junos.space.pmd.rules.j2ee.ejb3;

import java.util.ArrayList;
import java.util.List;

import net.junos.space.pmd.utils.reference.SimpleMethodReferenceRule;

public class SecurityPolicyInformationInEJBRule extends SimpleMethodReferenceRule{

	private static final String message = "The enterprise bean must not attempt to obtain the security policy "+
				"information for a particular code source. "+
			"Allowing the enterprise bean to access the security policy information "+
			"would create a security hole. (See EJB20SPEC - C.1.2 - Programming restrictions)" ;
	
	protected List<String> getTargetMethods() {
		ArrayList<String> lst = new ArrayList<String>();
		lst.add("getPermissions") ;
		return lst ;
	}
	
	public String getMessage(){
		return message ;
	}

	protected List<String> getTargetClasses() {
		ArrayList<String> lst = new ArrayList<String>();
		lst.add("java.security.Policy") ;
		return lst ;
	}
	
	protected List<String> getParams(String qName, String methodName) {
		ArrayList<String> lst = new ArrayList<String>();
		lst.add("CodeSource") ;
		return lst ;
	}

	protected boolean mustBeChecked(String className) {		
		return isEnterpriseBeanSuperclassEJB3(className);
	}
}
