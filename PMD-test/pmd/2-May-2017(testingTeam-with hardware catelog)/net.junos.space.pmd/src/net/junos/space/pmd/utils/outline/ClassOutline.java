package net.junos.space.pmd.utils.outline;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public class ClassOutline implements IClassOutline {
	
	private List<String> imports = new ArrayList<String>();
	
	private List<String> packageImports = new ArrayList<String>() ;

	private IJavaTypeDeclaration type ;
	
	private String filePath ;
	
	private ArrayList<IJavaFieldDeclaration> fields = new ArrayList<IJavaFieldDeclaration>() ;
	
	private ArrayList<IJavaMethodDeclaration> methods = new ArrayList<IJavaMethodDeclaration>();
	
	private ArrayList<IJavaMethodDeclaration> constructors = new ArrayList<IJavaMethodDeclaration>();

	public IJavaTypeDeclaration getTypeDeclaration() {
		return type;
	}

	public void setTypeDeclaration(IJavaTypeDeclaration type) {
		this.type = type;
	}

	@Override
	public List<IJavaMethodDeclaration> getMethods() {
		return methods ;
	}

	@Override
	public List<IJavaFieldDeclaration> getFields() {
		return fields;
	}

	@Override
	public void putMethod(IJavaMethodDeclaration method) {
		IJavaMethodDeclaration similarMethod = getSimilarMethod( method ) ;
		if( similarMethod != null )
			methods.remove(similarMethod) ;
		
		methods.add(method) ;
	}	

	@Override
	public void putField(IJavaFieldDeclaration field) {
		IJavaFieldDeclaration similarField = getSimilarField(field) ;
		if( similarField != null )
			fields.remove(similarField) ;
		
		fields.add(similarField) ;
	}
	
	private IJavaFieldDeclaration getSimilarField(IJavaFieldDeclaration field) {
		
		String name = field.getSimpleName();
//		IJavaTypeDeclaration fType = field.getType().getDeclaration() ;
		
		IJavaFieldDeclaration result = null ;
		
		for( IJavaFieldDeclaration fld : fields ){
			if( !name.equals(fld.getSimpleName()))
				continue ;
			
//			if(!fType.isSimilar(fld.getType().getDeclaration()))				
//				continue ;
			
			result = fld ;
			break ;
		}
		return result;
	}

	protected IJavaMethodDeclaration getSimilarMethod( IJavaMethodDeclaration method) {
		
		IJavaMethodDeclaration result = null ;
		//TODO use IJavaMethodParams
		//TODO mind isArray
//		String name = method.getSimpleName() ;
//		List<IJavaTypeDeclaration> params = method.getParameters();
//		
//		int size = params.size() ;
//		
//		
//l0:		for( IJavaMethodDeclaration md : methods ){
//			
//			if( !name.equals(md.getSimpleName()))
//				continue ;
//			
//			List<IJavaTypeDeclaration> otherParams = md.getParameters();
//			if(otherParams.size() != size)
//				continue ;
//			
//			for( int i = 0 ; i < size ; i++ ){
//				IJavaTypeDeclaration t0 = params.get(i);
//				IJavaTypeDeclaration t1 = otherParams.get(i); 
//			}
//			result = md ;
//			break ;
//		}
		return result ;
	}

	@Override
	public void setMethods(Collection<IJavaMethodDeclaration> methods) {
		if(methods != null)
			this.methods = new ArrayList<IJavaMethodDeclaration>(methods) ;
		else
			this.methods.clear() ;
	}

	@Override
	public void setFields(Collection<IJavaFieldDeclaration> fields) {
		if(fields != null)
			this.fields = new ArrayList<IJavaFieldDeclaration>(fields) ;
		else
			this.fields.clear() ;
	}

	@Override
	public List<String> getImports() {
		return imports;
	}

	@Override
	public void setImports(Collection<String> imports) {
		this.imports = new ArrayList<String>(imports);
	}

	@Override
	public String getFilePath() {
		return filePath;
	}

	@Override
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	@Override
	public IJavaFieldDeclaration getFieldBySimpleName(String name) {
		
		IJavaFieldDeclaration result = null ;
		for(IJavaFieldDeclaration fd : getFields() ){
			if( fd.getSimpleName().equals(name) ){
				result = fd ;
				break ;
			}
		}
		if(result != null)
			return result ;
		
		IJavaType superClass = getTypeDeclaration().getSuperClass();
		if(superClass != null){
			QualifiedName superQualifiedName = superClass.getQualifiedName();
			List<String> values = superQualifiedName.getValues();
			if(values.size()==1){
				String superName = values.iterator().next() ;
				IClassOutline superOutline = BasicOutlineContainer.getInstance().getClassOutline(superName);
				if(superOutline!=null)
					result = superOutline.getFieldBySimpleName(name) ;
			}
		}
		
		if(result != null)
			return result ;
		
		IJavaTypeDeclaration owner = getTypeDeclaration().getOwner();
		if(owner != null){
			//qualifiedNmae existence is guaranteed here
			QualifiedName ownerQualifiedName = owner.getQualifiedName();
			List<String> values = ownerQualifiedName.getValues();
			if(values.size()==1){
				String ownerName = values.iterator().next() ;
				IClassOutline ownerOutline = BasicOutlineContainer.getInstance().getClassOutline(ownerName);
				if(ownerOutline!=null)
					result = ownerOutline.getFieldBySimpleName(name) ;
			}
		}
		
		return result ;
	}

	@Override
	public List<IJavaMethodDeclaration> getMethodsBySimpleName(String name) {
		List<IJavaMethodDeclaration> result = new ArrayList<IJavaMethodDeclaration>() ;
		for(IJavaMethodDeclaration md : getMethods() ){
			if(md.getSimpleName().equals(name))
				result.add(md) ;
		}
		if(!result.isEmpty())
			return result;
		
		IJavaType superClass = getTypeDeclaration().getSuperClass();
		if(superClass != null){
			QualifiedName superQualifiedName = superClass.getQualifiedName();
			List<String> values = superQualifiedName.getValues();
			if(values.size()==1){
				String superName = values.iterator().next() ;
				IClassOutline superOutline = BasicOutlineContainer.getInstance().getClassOutline(superName);
				if(superOutline!=null)
					result = superOutline.getMethodsBySimpleName(name) ;
			}
		}
		
		if(!result.isEmpty())
			return result;
		
		IJavaTypeDeclaration owner = getTypeDeclaration().getOwner();
		if(owner != null){

			QualifiedName ownerQualifiedName = owner.getQualifiedName();
			List<String> values = ownerQualifiedName.getValues();
			if(values.size()==1){
				String ownerName = values.iterator().next() ;
				IClassOutline ownerOutline = BasicOutlineContainer.getInstance().getClassOutline(ownerName);
				if(ownerOutline!=null)
					result = ownerOutline.getMethodsBySimpleName(name) ;
			}
		}		
		return result;
	}

	@Override
	public void setConstructors(ArrayList<IJavaMethodDeclaration> constructors) {
		this.constructors = constructors ;		
	}

	@Override
	public List<IJavaMethodDeclaration> getConstructors() {
		return constructors ;
	}

	public List<String> getPackageImports() {
		return packageImports;
	}

	public void setPackageImports(List<String> packageImports) {
		this.packageImports = new ArrayList<String>(packageImports);
	}

	@Override
	public QualifiedName getQualifiedName() {
		return getTypeDeclaration().getQualifiedName() ;
	}

	@Override
	public IJavaMethodDeclaration getMethod(String methodName, IJavaType returnType, List<IJavaType> params)
	{
		List<IJavaMethodDeclaration> methodsBySimpleName = getMethodsBySimpleName(methodName);
		if( methodsBySimpleName == null || methodsBySimpleName.isEmpty() )
			return null ;
		
		boolean rtIsNull = returnType == null;
		boolean noParams = params == null || params.isEmpty() ;
		int paramsCount = noParams ? -1 : params.size() ;
		
		IJavaMethodDeclaration result = null ;
l0:		for( IJavaMethodDeclaration md : methodsBySimpleName ){
			
			IJavaType returnType2 = md.getReturnType();
			
			boolean rt2IsNull = returnType2 == null;
			if( rtIsNull != rt2IsNull){
				continue ;				
			}
			else if( !rtIsNull ){
				//check
			}
			
			List<IJavaType> params2 = md.getParameters();
			boolean noParams2 = params2 == null || params2.isEmpty() ;
			if(noParams != noParams2)
				continue ;
			
			if(noParams){
				result = md ;
				break ;
			}
			
			int paramsCount2 = params2.size() ;
			if(paramsCount != paramsCount2)
				continue ;
			
			for( int i = 0 ; i < paramsCount ; i++ ){
				IJavaType p1 = params.get(i);
				IJavaType p2 = params2.get(i);
				if(!p1.isSimilar(p2))
					continue l0;				
			}
			result = md ;
			break ;
		}
		
		return result ;
	}

	@Override
	public String getQualifiedNameString() {
		return getQualifiedName().getValues().iterator().next() ;
	}
	
	@Override
	public String toString() {	
		return "Outline: " + getQualifiedNameString() ;
	}

	@Override
	public List<IClassOutline> getDefinedSuperclasses(IOutlineContainer outlineContainer) {
		
		ArrayList<IClassOutline> result = new ArrayList<IClassOutline>() ;
		List<IJavaTypeDeclaration> allSuperClasses = getTypeDeclaration().getAllSuperClasses();
		
		for(IJavaTypeDeclaration decl : allSuperClasses){
			List<String> values = decl.getQualifiedName().getValues();
			for(String s : values){
				IClassOutline o = outlineContainer.getClassOutline(s);
				if(o!=null)
					result.add(o) ;
			}
		}		
		return result;
	}

	@Override
	public boolean hasAllSuperclassesDefined( IOutlineContainer container ) {
		int size1 = getDefinedSuperclasses(container).size();
		int size2 = getTypeDeclaration().getAllSuperClasses().size();
		boolean result = size1 == size2 ;
		return result;
	}
}
