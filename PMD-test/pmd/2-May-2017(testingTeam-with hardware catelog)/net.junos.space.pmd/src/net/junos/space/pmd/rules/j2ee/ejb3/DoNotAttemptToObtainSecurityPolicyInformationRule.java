package net.junos.space.pmd.rules.j2ee.ejb3;

public class DoNotAttemptToObtainSecurityPolicyInformationRule extends 
			net.junos.space.pmd.rules.j2ee.DoNotAttemptToObtainSecurityPolicyInformationRule {

	protected boolean mustBeChecked(String className) {		
		return isEnterpriseBeanSuperclassEJB3(className);
	}
}
