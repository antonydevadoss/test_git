package net.junos.space.pmd.rules.j2ee;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.junos.space.pmd.utils.reference.SimpleMethodReferenceRule;

public class DoNotAttemptToSetSocketFactoryRule extends SimpleMethodReferenceRule {
	private static final String[] targetClasses = new String[]{
		"javax.net.SocketFactory",
		"javax.net.DefaultSocketFactory",
		"javax.net.ssl.SSLSocketFactory",
		"javax.net.ssl.DefaultSSLSocketFactory",
		"com.sun.net.ssl.internal.ssl.SSLSocketFactoryImpl",
		"java.net.Socket",
		"javax.net.ssl.SSLSocket",
		"java.net.ServerSocket",
		"javax.net.ssl.SSLServerSocket",
		"java.net.URL",
		
	} ;
	
	private static final String[] targetMethods = new String[]{
		"createSocket",
		"setSocketImplFactory",
		"setSocketFactory",
		"setURLStreamHandlerFactory",
	} ;
	
	private static final String message = "Do not attempt to set the socket factory in an EJB"; 
	
	public String getMessage(){
		return message ;
	}

	protected List<String> getTargetClasses() {
		return new ArrayList<String>( Arrays.asList(targetClasses) ) ;
	}
	
	protected List<String> getTargetMethods() {
		return new ArrayList<String>( Arrays.asList(targetMethods) ) ;
	}

	protected boolean mustBeChecked(String className) {		
		return isEnterpriseBeanSuperclassEJB2(className);
	}
}
