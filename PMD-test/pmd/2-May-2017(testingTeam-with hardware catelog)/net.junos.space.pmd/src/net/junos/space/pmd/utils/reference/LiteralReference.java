package net.junos.space.pmd.utils.reference;

public class LiteralReference extends AbstractReference{
	
	public LiteralReference(String type, int beginLine, int startLine) {
		super(beginLine, startLine);
		this.type = type;
	}

	private final String type ;

	public String getType() {
		return type;
	}
	
	@Override
	public String toString(){
		return "Literal reference: " + type + "." ;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		LiteralReference other = (LiteralReference) obj;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}

}
