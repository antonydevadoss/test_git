package net.junos.space.pmd.rules.j2ee.ejb3;

import java.util.ArrayList;
import java.util.List;

import net.junos.space.pmd.utils.outline.AbstractOutlineAccessor;
import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IJavaMethodDeclaration;
import net.junos.space.pmd.utils.outline.IJavaType;
import net.sourceforge.pmd.RuleContext;

public class EnterpriseBeanTimeoutMethodExceptionsRule extends AbstractOutlineAccessor {

	
	@Override
	public String getMessage() {		
		return "A Timeout callback method of the enterprise bean must not throw application exceptions.";
	}

	@Override
	public void doEnd(RuleContext ctx) {
		
		for( IClassOutline outline : getOutlineContainer().getAllOutlines() ){
			
			String qualifiedName = outline.getQualifiedNameString();
			if(!isEnterpriseBeanEJB3(qualifiedName))
				continue ;
			
			ArrayList<IJavaMethodDeclaration> timeoutMethods = 
					EnterpriseBeanTimeoutMethodStaticFinalRule.extractAllTimeoutMethods(outline,getOutlineContainer()) ;
			
			for(IJavaMethodDeclaration md: timeoutMethods){
				List<IJavaType> exceptions = md.getExceptions();
				if(exceptions == null||exceptions.isEmpty())
					continue ;
				
				for(IJavaType s : exceptions){
					if( isKnownApplicationExceptionSubclass(s) ){
						reportError(ctx, outline, md) ;
						break ;
					}
				}
			}
		}		
	}
}
