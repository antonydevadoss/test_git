/*******************************************************************************
 * FILE NAME: InvalidDTORule.java
 * PURPOSE: This is Rule-19 of document 
 *
 *
 * Revision History: 
 * DATE:           				  AUTHOR:              	CHANGE:  
 * Tue March 9, 2011               Atul Bajpai       	Initial Version  
 * 
 * Copyright (c) 2011 Juniper Networks. All rights reserved. 
 ******************************************************************************/
package net.junos.space.pmd.rules.dto;

import java.util.List;import net.junos.space.pmd.utils.PMDConstants;import net.junos.space.pmd.utils.PMDUtils;import net.junos.space.pmd.utils.Project;import net.sourceforge.pmd.RuleContext;import net.sourceforge.pmd.lang.java.ast.ASTMethodDeclarator;import net.sourceforge.pmd.lang.java.rule.AbstractJavaRule;import org.apache.log4j.Logger;

/**
 * This class validates if DTO is not annotated with @XmlRootElement annotation.
 */
public class InvalidDTORule extends AbstractJavaRule {

	private static Logger log = Logger.getLogger(InvalidDTORule.class);

	/**
	 * This method checks all the methods of class and checks whether or not DTO annotated with @XmlRootElement annotation.
	 */
	public Object visit(ASTMethodDeclarator methodNode, Object data) {
		RuleContext ctx = (RuleContext) data;
		String methodName = methodNode.getImage();
		try {
			boolean isRestMethod = false;
			boolean hasDTOParameter = false;
			boolean isReturningDTO = false;
			boolean isValidParameterDTO = false;
			boolean isValidReturningDTO = false;

			// checks whether the method is REST or not.
			isRestMethod = PMDUtils.isRestMethod(methodNode);
			if (isRestMethod) {

				hasDTOParameter = PMDUtils.hasDTOParameters(methodNode);
				isReturningDTO = PMDUtils.isReturningDTO(methodNode);

				Project project = new Project(PMDConstants.WEB_SVC_PROJECT);
				if (hasDTOParameter) {
					Class<?> parameterDTOClass = PMDUtils.getParameterDTOClass(methodNode, project);
					if(!parameterDTOClass.isInterface())
						isValidParameterDTO = PMDUtils.hasAnnotation(parameterDTOClass, PMDConstants.XML_ROOT_ELEMENT);						
					else {
						List<Class<?>> implementations = project.findAllImplementingClasses(parameterDTOClass);
						for(Class<?> implementation : implementations){
							isValidParameterDTO = PMDUtils.hasAnnotation(implementation, PMDConstants.XML_ROOT_ELEMENT);
							if(isValidParameterDTO)
								break;
						}
					}
				}
 
				if (isReturningDTO) {
					Class<?> returningDTOClass = PMDUtils.getReturningDTOClass(methodNode, project);
					if(!returningDTOClass.isInterface())
						isValidReturningDTO = PMDUtils.hasAnnotation(returningDTOClass, PMDConstants.XML_ROOT_ELEMENT);
					else {
						List<Class<?>> implementations = project.findAllImplementingClasses(returningDTOClass);
						for(Class<?> implementation : implementations){
							isValidReturningDTO = PMDUtils.hasAnnotation(implementation, PMDConstants.XML_ROOT_ELEMENT);
							if(isValidReturningDTO)
								break;
						}
					}
				}
				
			}

			if (log.isDebugEnabled()) {
				StringBuilder builder = new StringBuilder();
				builder.append("source file=").append(ctx.getSourceCodeFilename()).append(",method=").append(methodName);
				builder.append(",isRestMethod=").append(isRestMethod).append(",hasDTOParameter=").append(hasDTOParameter).append(
						",isValidParameterDTO=").append(isValidParameterDTO).append(",isReturningDTO=").append(isReturningDTO)
						.append(",isValidReturningDTO=").append(isValidReturningDTO);
				log.debug(builder.toString());
			}

			if (isRestMethod && ((hasDTOParameter && !isValidParameterDTO) || (isReturningDTO && !isValidReturningDTO)))
				addViolation(data, methodNode);
		} catch (Exception e) {
			log.error("Exception in executing method " + methodName + " of " + ctx.getSourceCodeFilename(), e);
		}
		return super.visit(methodNode, data);
	}

}
