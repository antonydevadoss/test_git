package net.junos.space.pmd.rules.j2ee;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.junos.space.pmd.utils.reference.SimpleMethodReferenceRule;

public class DoNotAttemptToListenSocketRule extends SimpleMethodReferenceRule {
	private static final String[] targetClasses = new String[]{
		"java.net.ServerSocket",
		"java.net.MulticastSocket"
	} ;
	
	private static final String[] targetMethods = new String[]{
		"accept",
		"send"
	} ;
	
	private static final String message = "Do not attempt to listen on a Socket in an EJB"; 
	
	public String getMessage(){
		return message ;
	}

	protected List<String> getTargetClasses() {
		return new ArrayList<String>( Arrays.asList(targetClasses) ) ;
	}
	
	protected List<String> getTargetMethods() {
		return new ArrayList<String>( Arrays.asList(targetMethods) ) ;
	}

	protected boolean mustBeChecked(String className) {		
		return isEnterpriseBeanSuperclassEJB2(className);
	}
}
