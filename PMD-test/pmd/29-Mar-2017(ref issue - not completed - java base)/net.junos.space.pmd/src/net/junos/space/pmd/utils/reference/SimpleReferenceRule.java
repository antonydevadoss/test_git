package net.junos.space.pmd.utils.reference;

import java.util.List;

import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IDeclaration;
import net.sourceforge.pmd.RuleContext;

public class SimpleReferenceRule extends AbstractReferenceAccessor {
	
	@Override
	public final void doEnd(RuleContext ctx) {

		for( IClassReferenceStorage<SimpleClassReference> cs : getReferenceContainer().getAllResolvedClassStorages() ){
			
			String qualifiedName = cs.getQualifiedName();
			if(!mustBeChecked(qualifiedName))
				continue ;
			
			IClassOutline classOutline = getOutlineContainer().getClassOutline(qualifiedName);
			
			for (IDeclaration method : cs.getMethods())
			{
				List<SimpleClassReference> references = cs.getMethodReferences(method);
				for (SimpleClassReference reference : references) {
					String refQName = reference.getQualifiedName();					
					
					if ( !isInvalid(refQName) )
						continue ;
					
					int beginLine = reference.getBeginLine();
					int endLine = reference.getEndLine();
					
					reportError(ctx,classOutline,beginLine,endLine) ;
				}
			}
		}		
	}
	
	protected boolean mustBeChecked(String qualifiedName) {
		return true;
	}

	protected boolean isInvalid(String refQName) {
		return false;
	}
	
	public String getMessage(){
		return "" ;
	}
}
