package net.junos.space.pmd.rules.j2ee;


import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;

public class EntityBeanEjbPostCreateMethodReturnTypeRule extends
		AbstractEjbPostCreateReturnTypeRule {
	
	@Override
	public boolean isProperBean(IJavaTypeDeclaration decl) {
		return isEntityBeanEJB2(decl);
	}
	
	@Override
	public String getMessage() {
		return "Enterprise entity beans must declare ejbPostCreate methods that return void.";
	}
}
