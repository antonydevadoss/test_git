/*******************************************************************************
 * FILE NAME: CapabilityValidityRule.java
 * PURPOSE:   
 *
 *
 * Revision History: 
 * DATE:           				  AUTHOR:              	CHANGE:  
 * Tue Feb 05, 2011               Atul Bajpai       	Initial Version  
 * 
 * 
 ******************************************************************************/
package net.junos.space.pmd.rules.rest;

import java.util.List;import net.junos.space.pmd.utils.ModuleXMLReader;import net.junos.space.pmd.utils.PMDConstants;import net.junos.space.pmd.utils.PMDUtils;import net.sourceforge.pmd.RuleContext;import net.sourceforge.pmd.lang.java.ast.ASTMethodDeclarator;import net.sourceforge.pmd.lang.java.rule.AbstractJavaRule;import org.apache.log4j.Logger;

/**
 * CapabilityValidityRule class checks the validity of capabilities provided 
 * in @RBAC annotation on REST API. This class checks the availability of 
 * capabilities in moduleXML.
 */
public class CapabilityValidityRule extends AbstractJavaRule {

	/**
	 * Logger
	 */
	private static Logger log = Logger.getLogger(CapabilityValidityRule.class);

	/**
	 * This method Fetches out capability defined in module.xml and then fetches the
	 * capability defined in @RBAC annotation and verifies whether capability in 
	 * @RBAC is present in module.xml capabilities list.
	 */
	public Object visit(ASTMethodDeclarator methodNode, Object data) {
		RuleContext ctx = (RuleContext) data;
		String methodName = methodNode.getImage();
		try {
			List<String> XMLCapList = new ModuleXMLReader().getCapabilities();
			boolean isRBACExist = false;
			boolean isRestMethod = false;
			List<String> capList = null;
			// checks whether the method is REST or not.
			isRestMethod = PMDUtils.isRestMethod(methodNode);
			if (isRestMethod) {
				isRBACExist = PMDUtils.isMethodAnnotationExists(methodNode, PMDConstants.RBAC_ANNOTATION);
				if (isRBACExist) {
					capList = PMDUtils
							.getMethodAnnotationValue(methodNode, PMDConstants.RBAC_ANNOTATION, PMDConstants.CAPABILITY);
					capList.removeAll(XMLCapList);
				}
			}
			
			if(log.isDebugEnabled()){
				StringBuilder builder = new StringBuilder();
				builder.append("source file=").append(ctx.getSourceCodeFilename()).append(",method=").append(methodName);
				builder.append(",isRestMethod=").append(isRestMethod).append(",capList=").append(capList);
				log.debug(builder.toString());
			}
			
			if (capList != null && !capList.isEmpty())
				addViolation(data, methodNode);
		} catch (Exception e) {
			log.error("Exception in executing method " + methodName + " of " + ctx.getSourceCodeFilename(), e);
		}
		return super.visit(methodNode, data);

	}
}
