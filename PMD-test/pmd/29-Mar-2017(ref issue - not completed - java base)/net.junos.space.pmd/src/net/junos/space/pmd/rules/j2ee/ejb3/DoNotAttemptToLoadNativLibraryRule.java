package net.junos.space.pmd.rules.j2ee.ejb3;

public class DoNotAttemptToLoadNativLibraryRule extends 
			net.junos.space.pmd.rules.j2ee.DoNotAttemptToLoadNativLibraryRule {

	protected boolean mustBeChecked(String className) {		
		return isEnterpriseBeanSuperclassEJB3(className);
	}
}
