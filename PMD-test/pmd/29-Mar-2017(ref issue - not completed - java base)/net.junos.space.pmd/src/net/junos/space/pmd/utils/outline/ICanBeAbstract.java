package net.junos.space.pmd.utils.outline;

public interface ICanBeAbstract {
	
	boolean isAbstract() ;
	
	void setAbstract( boolean isAbstract ) ;

}
