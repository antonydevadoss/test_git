package net.junos.space.pmd.rules.j2ee.ejb3;

import java.util.ArrayList;
import java.util.List;

import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;
import net.sourceforge.pmd.RuleContext;

public class SessionBeanBusinessInterfaceOldInterfacesRule extends
		AbstractBusinessInterfacesInspector {

	@Override
	public String getMessage() {
		return "A business interface must not extend javax.ejb.EJBObject or javax.ejb.EJBLocalObject.";
	}
	

	@Override
	public void doEnd(RuleContext ctx) {
		List<IClassOutline> allOutlines = getOutlineContainer().getAllOutlines();
		
		for (IClassOutline classOutline : allOutlines) {
			if (isSessionBeanEJB3(classOutline.getQualifiedNameString())) {
				IJavaTypeDeclaration typeDeclaration = classOutline.getTypeDeclaration();
				ArrayList<IClassOutline> businessInterfaces = extractTargetInterfaces(typeDeclaration);
				for (IClassOutline businessInterfaceOutline : businessInterfaces) {
//					IClassOutline businessInterfaceOutline = getOutlineContainer().getClassOutline(businessInterface.getQualifiedName().getRecognisedValue());
					IJavaTypeDeclaration businessInterface = businessInterfaceOutline.getTypeDeclaration();
					for (IJavaTypeDeclaration biSuperclass : businessInterface.getAllSuperClasses()) {
						boolean isWrong = false;
						for (String biSuperclassQName : biSuperclass.getQualifiedName().getValues()) {
							if (biSuperclassQName.equals("javax.ejb.EJBObject")
									|| biSuperclassQName.equals("javax.ejb.EJBLocalObject")) {
								isWrong = true;
							}
						}
						if (isWrong) {
							reportError(ctx, classOutline, typeDeclaration) ;
						}
					}
				}
			}
		}
		super.doEnd(ctx);
	}

	@Override
	protected ArrayList<IClassOutline> extractTargetInterfaces(
			IJavaTypeDeclaration typeDeclaration) {
		return extractAllBusinessInterfaces(typeDeclaration) ;
	}
}
