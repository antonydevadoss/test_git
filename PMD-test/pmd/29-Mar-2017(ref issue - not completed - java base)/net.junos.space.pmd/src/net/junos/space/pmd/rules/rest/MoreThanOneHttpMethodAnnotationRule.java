/*******************************************************************************
 * FILE NAME: MoreThanOneHttpMethodAnnotationRule.java
 * PURPOSE:  This is Rule-22 in document 
 *
 *
 * Revision History: 
 * DATE:           				  AUTHOR:              	CHANGE:  
 * Tue Mar 03rd, 2011             Amit Arora	       	Initial Version  
 * 
 * 
 ******************************************************************************/
package net.junos.space.pmd.rules.rest;

import java.util.List;import net.junos.space.pmd.utils.PMDUtils;import net.sourceforge.pmd.RuleContext;import net.sourceforge.pmd.lang.java.ast.ASTMethodDeclarator;import net.sourceforge.pmd.lang.java.rule.AbstractJavaRule;import org.apache.log4j.Logger;

/**
 * This rule verifies that if more than one HTTP method annotations are present
 * on method raise a warning
 */
public class MoreThanOneHttpMethodAnnotationRule extends AbstractJavaRule {

	/**
	 * Logger
	 */
	private Logger log = Logger.getLogger(MoreThanOneHttpMethodAnnotationRule.class);
	
	/**
	 * This method :
	 *  a> Get all HTTP method annotations 
	 *  b> Count the size of annotations list
	 *  c> If more than one, raise warning
	 */
	public Object visit(ASTMethodDeclarator methodNode, Object data) {
		RuleContext ctx = (RuleContext) data;
		String methodName = methodNode.getImage();
		try{
			List<String> httpMethodList = PMDUtils.getHttpMethods(methodNode);
			if(httpMethodList.size() > 1){
				addViolation(data, methodNode);
			}
			
			if(log.isDebugEnabled()){
				StringBuilder builder = new StringBuilder();
				builder.append("source file=").append(ctx.getSourceCodeFilename()).append(",method=").append(methodName);
				builder.append(", RestMethod count=").append(httpMethodList.size());
				log.debug(builder.toString());
			}
			
		} catch (Exception e) {
			log.error("Exception in executing method " + methodName + " of " + ctx.getSourceCodeFilename(), e);
		}
		return super.visit(methodNode, data);
	}

}
