/*******************************************************************************
 * FILE NAME: PMDUtils.java
 * PURPOSE: Provides the Utility methods.
 *
 *
 * Revision History: 
 * DATE:           				  AUTHOR:              	CHANGE:  
 * Tue March 9, 2011               Atul Bajpai       	Initial Version  
 * 
 * 
 ******************************************************************************/
package net.junos.space.pmd.utils;

import java.lang.annotation.Annotation;import java.lang.reflect.Field;import java.lang.reflect.Method;import java.util.ArrayList;import java.util.List;import java.util.regex.Matcher;import java.util.regex.Pattern;import net.junos.space.pmd.utils.PMDConstants.HttpMethod;import net.junos.space.pmd.utils.PMDConstants.NonDTO;import net.sourceforge.pmd.lang.ast.Node;import net.sourceforge.pmd.lang.java.ast.ASTAnnotation;import net.sourceforge.pmd.lang.java.ast.ASTClassOrInterfaceBodyDeclaration;import net.sourceforge.pmd.lang.java.ast.ASTClassOrInterfaceDeclaration;import net.sourceforge.pmd.lang.java.ast.ASTClassOrInterfaceType;import net.sourceforge.pmd.lang.java.ast.ASTCompilationUnit;import net.sourceforge.pmd.lang.java.ast.ASTFormalParameter;import net.sourceforge.pmd.lang.java.ast.ASTFormalParameters;import net.sourceforge.pmd.lang.java.ast.ASTImplementsList;import net.sourceforge.pmd.lang.java.ast.ASTImportDeclaration;import net.sourceforge.pmd.lang.java.ast.ASTLiteral;import net.sourceforge.pmd.lang.java.ast.ASTMemberValue;import net.sourceforge.pmd.lang.java.ast.ASTMemberValueArrayInitializer;import net.sourceforge.pmd.lang.java.ast.ASTMemberValuePair;import net.sourceforge.pmd.lang.java.ast.ASTMethodDeclaration;import net.sourceforge.pmd.lang.java.ast.ASTMethodDeclarator;import net.sourceforge.pmd.lang.java.ast.ASTName;import net.sourceforge.pmd.lang.java.ast.ASTPackageDeclaration;import net.sourceforge.pmd.lang.java.ast.ASTResultType;import net.sourceforge.pmd.lang.java.ast.ASTType;import net.sourceforge.pmd.lang.java.ast.ASTTypeDeclaration;import org.apache.log4j.Logger;


/**
 * This class provide all the Utility methods required in this project.
 */
public class PMDUtils {

	/**
	 * Logger
	 */
	private static final Logger log = Logger.getLogger(PMDUtils.class);
	
	/**
	 * Tells whether a method is a REST method or not.
	 * 
	 * @param methodNode
	 *            - Method nodes
	 * @return true if a method is REST
	 */
	public static boolean isRestMethod(ASTMethodDeclarator methodNode) {
		List<ASTAnnotation> annotations = PMDUtils.getMethodAnnotations(methodNode);
		boolean isHttpMethodPresent = false;
		for (int i = 0; i < annotations.size(); i++) {			
			String annotationName = annotations.get(i).getFirstDescendantOfType(ASTName.class).getImage();
			if (HttpMethod.isValidHttpMethod(annotationName))
				isHttpMethodPresent = true;
		}
		return isHttpMethodPresent;
		
	}

	/**
	 * Returns the List of Http Annotation of a REST method.
	 * 
	 * @param methodNode
	 *            - Method nodes
	 * @return Annotation name
	 */
	public static List<String> getHttpMethods(ASTMethodDeclarator methodNode) {
		List<ASTAnnotation> annotations = PMDUtils.getMethodAnnotations(methodNode);
		List<String> lstAnnotation = new ArrayList<String>();
		for (int i = 0; i < annotations.size(); i++) {
			String annotationName = getAnnotationName(annotations.get(i));
			if (HttpMethod.isValidHttpMethod(annotationName)) {
				lstAnnotation.add(annotationName);
			}
		}
		return lstAnnotation;
	}

	/**
	 * Tells whether a given Annotation exists on method level or not.
	 * 
	 * @param methodNode
	 *            - AST methodDeclarator
	 * @param annotationName
	 *            - Annotation to be checked for existence on method
	 * @return true, if annotation exists on this method.
	 */
	public static boolean isMethodAnnotationExists(ASTMethodDeclarator methodNode, String annotationName) {
		boolean isAnnotationPresent = false;
		List<ASTAnnotation> methodAnnotations = getMethodAnnotations(methodNode);
		for (int i = 0; i < methodAnnotations.size(); i++) {
			if (getAnnotationName(methodAnnotations.get(i)).equals(annotationName)) {
				isAnnotationPresent = true;
				break;
			}
		}
		return isAnnotationPresent;
	}

	/**
	 * Tells whether a given Annotation exists on class level or not.
	 * 
	 * @param methodNode
	 *            - AST methodDeclarator
	 * @param annotationName
	 *            - Annotation to be checked for existence on class
	 * @return true, if annotation exists on this class level
	 */
	public static boolean isClassAnnotationExists(ASTMethodDeclarator methodNode, String annotationName) {
		boolean isAnnotation = false;
		List<ASTAnnotation> results = getClassAnnotations(methodNode);
		for (int i = 0; i < results.size(); i++) {
			ASTAnnotation annotation = results.get(i);
			if (getAnnotationName(annotation).equals(annotationName)) {
				isAnnotation = true;
				break;
			}
		}
		return isAnnotation;
	}

	/**

	 * Tells whether a given Annotation exists on class level or not.

	 * 

	 * @param classNode

	 *            - AST ClassOrInterfaceDeclaration

	 * @param annotationName

	 *            - Annotation to be checked for existence on class

	 * @return true, if annotation exists on this class level

	 */
	public static boolean isClassAnnotationExists(ASTClassOrInterfaceDeclaration classNode, String annotationName) {

		boolean isAnnotation = false;

		List<ASTAnnotation> results = getClassAnnotations(classNode);

		for (int i = 0; i < results.size(); i++) {

			ASTAnnotation annotation = results.get(i);

			if (getAnnotationName(annotation).equals(annotationName)) {

				isAnnotation = true;

				break;

			}

		}

		return isAnnotation;

	}

	/**
	 * Returns list of all annotations of a method.
	 * 
	 * @param methodNode
	 *            - AST methodDeclarator
	 * @return list of annotations
	 */
	public static List<ASTAnnotation> getMethodAnnotations(ASTMethodDeclarator methodNode) {
		ASTClassOrInterfaceBodyDeclaration methodBodyNode = methodNode
				.getFirstParentOfType(ASTClassOrInterfaceBodyDeclaration.class);
		return methodBodyNode.findChildrenOfType(ASTAnnotation.class);
	}

	/**
	 * Returns an annotation if it exists on a given method.
	 * 
	 * @param methodNode
	 *            - AST methodDeclarator
	 * @param annotationName
	 *            - Annotation Name
	 * @return the specified annotation
	 */
	public static ASTAnnotation getMethodAnnotation(ASTMethodDeclarator methodNode, String annotationName) {
		ASTAnnotation annotation = null;
		List<ASTAnnotation> methodAnnotations = getMethodAnnotations(methodNode);
		for (int i = 0; i < methodAnnotations.size(); i++) {
			if (getAnnotationName(methodAnnotations.get(i)).equals(annotationName)) {
				annotation = methodAnnotations.get(i);
			}
		}
		return annotation;
	}

	/**
	 * Returns all annotations of a class
	 * 
	 * @param methodNode
	 *            - AST methodDeclarator
	 * @return list of annotations
	 */
	public static List<ASTAnnotation> getClassAnnotations(ASTMethodDeclarator methodNode) {
		return getClassAnnotations(methodNode.getFirstParentOfType(ASTTypeDeclaration.class));
	}

	
	/**

	 * Returns all annotations of a class

	 * 

	 * @param classNode

	 *            - AST ClassOrInterfaceDeclaration

	 * @return list of annotations

	 */
	public static List<ASTAnnotation> getClassAnnotations(ASTClassOrInterfaceDeclaration classNode) {

		return getClassAnnotations(classNode.getFirstParentOfType(ASTTypeDeclaration.class));

	}
	/**
	 * Returns a list of all annotations present on class level
	 * 
	 * @param classNode
	 *            - AST TypeDeclaration
	 * @return list of annotations
	 */
	public static List<ASTAnnotation> getClassAnnotations(ASTTypeDeclaration classNode) {
		List<ASTAnnotation> results = new ArrayList<ASTAnnotation>();
		for (int i = 0; i < classNode.jjtGetNumChildren(); i++) {
			Node classChildNode = classNode.jjtGetChild(i);
			if (classChildNode instanceof ASTAnnotation) {
				results.add((ASTAnnotation) classChildNode);
			}
		}
		return results;
	}

	/**
	 * Returns an object of class level ASTAnnotation type, which name is provided by user.
	 * 
	 * @param methodNode
	 *            - AST methodDeclarator
	 * @param annotationName
	 *            - Name of Annotation
	 * @return ASTAnnotation object
	 */
	public static ASTAnnotation getClassAnnotation(ASTMethodDeclarator methodNode, String annotationName) {
		ASTAnnotation annotation = null;
		List<ASTAnnotation> classsAnnotations = getClassAnnotations(methodNode);
		for (int i = 0; i < classsAnnotations.size(); i++) {
			if (getAnnotationName(classsAnnotations.get(i)).equals(annotationName)) {
				annotation = classsAnnotations.get(i);
			}
		}
		return annotation;
	}

	/**
	 * Returns all the values of a Class level annotation which name is provide by the user.
	 * 
	 * @param methodNode
	 *            - AST methodDeclarator
	 * @param annotationName
	 *            - Name of Annotation
	 * @return A list of values of an Annotation
	 */
	public static List<String> getClassAnnotationValue(ASTMethodDeclarator methodNode, String annotationName) {
		ASTAnnotation annotation = getClassAnnotation(methodNode, annotationName);
		List<String> lstLiteral = new ArrayList<String>();
		if (annotation != null) {
			if (annotation.getFirstChildOfType(ASTMemberValueArrayInitializer.class) != null) {
				ASTMemberValueArrayInitializer memberValueArray = annotation
						.getFirstChildOfType(ASTMemberValueArrayInitializer.class);
				List<ASTMemberValue> memberValueArrayList = memberValueArray.findChildrenOfType(ASTMemberValue.class);
				for (int i = 0; i < memberValueArrayList.size(); i++) {
					ASTMemberValue memberValueItem = memberValueArrayList.get(i);
					ASTLiteral literal = memberValueItem.getFirstChildOfType(ASTLiteral.class);
					ASTName name = memberValueItem.getFirstChildOfType(ASTName.class);
					if (literal != null) {
						lstLiteral.add(removeQuotes(literal.getImage()));
					}
					if (name != null) {
						String val = getVariableValue(methodNode, name.getImage());
						lstLiteral.add(val);
					}
				}
			} else {
				lstLiteral.add(getAnnotationValue(methodNode, annotation));
			}
		}
		return lstLiteral;
	}

	/**

	 * Returns the value of String Variable passed in any annotation either class level or method level.

	 * 

	 * @param methodNode

	 *            - AST methodDeclarator

	 * @param annotationVariableName

	 *            - Variable passed to annotation

	 * @return String value of variable

	 */
	public static String getVariableValue(ASTMethodDeclarator methodNode, String annotationVariableName) {
		String val = null;
		String pckgName = null;
		ASTCompilationUnit compilationUnit = methodNode.getFirstParentOfType(ASTCompilationUnit.class);
		Class<?> varClass = null;
		Project project = new Project(PMDConstants.WEB_SVC_PROJECT);
		if(!annotationVariableName.contains(".")) {
			ASTClassOrInterfaceDeclaration classdecl = compilationUnit.getFirstChildOfType(ASTClassOrInterfaceDeclaration.class);
			ASTPackageDeclaration pckgDeclaration = compilationUnit.getFirstChildOfType(ASTPackageDeclaration.class);
			if(pckgDeclaration != null){
				pckgName = pckgDeclaration.getFirstChildOfType(ASTName.class).getImage();
			}
			String ClassName = classdecl.getImage();
			String fullyQualifiedClassName = pckgName + "." + ClassName;
			varClass = project.loadClass(fullyQualifiedClassName);
			val = getFieldValue(varClass, annotationVariableName);
			
		}else {
			int index = annotationVariableName.lastIndexOf(".");
			String varName = annotationVariableName.substring(index+1, annotationVariableName.length());
			String className = annotationVariableName.substring(0, index);
			varClass = getDTOClass(compilationUnit, className, project);
			if(varClass != null)
				val = getFieldValue(varClass, varName);
		}
		
		return val;
	}

	
	/**

	 * Returns the value of data members of a class.

	 * 

	 * @param className

	 *            - Class consisting the data members

	 * @param variableName

	 *            - data member variable

	 * @return String value of variable

	 */
	public static String getFieldValue(Class<?> className, String variableName) {
		String val = null;
		Field[] fields = className.getFields();
		for(int i=0;i<fields.length;i++) {
			Field fld = fields[i];
			if(fld.getName().equals(variableName)) {
				try {
					fld.setAccessible(true);
					Object object = fld.get(fld);
					if(object instanceof String)
						val = (String) object;					
					else 
						val = object.toString();
					break;
				} catch (IllegalArgumentException e) {
					log.warn("cannot get field value", e);
				} catch (IllegalAccessException e) {
					log.warn("cannot get field value", e);
				}
			}
		}
		return val;
		
	}
	
	/**
	 * Returns the annotation node corresponding to annotation name
	 * 
	 * @param annotationNode
	 *            - AST Annotation
	 * @return annotation node
	 */
	public static String getAnnotationName(ASTAnnotation annotationNode) {
		return annotationNode.getFirstDescendantOfType(ASTName.class).getImage();
	}

	/**
	 * Returns the value of an Annotation provided by the user.
	 * 
	 * @param annotationNode
	 *            - Name of Annotation
	 * @return String value of Annotation
	 */
	public static String getAnnotationValue(ASTMethodDeclarator methodNode, ASTAnnotation annotationNode) {
		String annotationVal = "";
		ASTMemberValue memValue = annotationNode.getFirstChildOfType(ASTMemberValue.class);
		
		ASTLiteral literal = null; 
		ASTName name = null;
			
		if(memValue != null){
			literal = memValue.getFirstChildOfType(ASTLiteral.class);
			name = memValue.getFirstChildOfType(ASTName.class);			
		} else if(annotationNode != null){
			literal = annotationNode.getFirstChildOfType(ASTLiteral.class);
			name = annotationNode.getFirstChildOfType(ASTName.class);					
		}
		
		if (literal != null) {
			annotationVal = literal.getImage();
		}
		if (name != null) {
			annotationVal = getVariableValue(methodNode, name.getImage());
		}
		
		if(literal == null && name == null) // check for empty String used in Annotation
			return annotationVal;
		
		return removeQuotes(annotationVal);
	}

	/**
	 * Removes the Quotes from a String
	 * 
	 * @param value
	 *            - Annotation value
	 * @return A string without Quotes
	 */
	private static String removeQuotes(String value) {
		if (value == null)
			value = "";
		value = value.replace("\"", "");
		return value;
	}

	/**
	 * Tells whether or not this method returns DTO.
	 * 
	 * @param methodNode
	 *            - AST methodDeclarator
	 * @return true if method is returning a DTO type
	 */
	public static boolean isReturningDTO(ASTMethodDeclarator methodNode) {
		boolean isReturningDTO = false;
		ASTMethodDeclaration methodDecl = methodNode.getFirstParentOfType(ASTMethodDeclaration.class);
		ASTResultType returnType = methodDecl.getFirstChildOfType(ASTResultType.class);
		if (returnType != null) { // void handling
			ASTClassOrInterfaceType refType = returnType.getFirstChildOfType(ASTClassOrInterfaceType.class);
			if (refType != null) { // primitive type handling

				ASTClassOrInterfaceType type = refType.getFirstChildOfType(ASTClassOrInterfaceType.class);
				if (type != null) { //valid for generic
					isReturningDTO = NonDTO.isDTO(type.getImage());
				} else {
					isReturningDTO = NonDTO.isDTO(refType.getImage());
				}
			}
		}
		return isReturningDTO;
	}

	/**
	 * Tells whether or not this method returning Void type
	 * 
	 * @param methodNode
	 *            - AST methodDeclarator
	 * @return true, if this method returns Void type
	 */
	public static boolean isReturningVoid(ASTMethodDeclarator methodNode) {
		boolean isReturningVoid = false;
		ASTMethodDeclaration methodDecl = methodNode.getFirstParentOfType(ASTMethodDeclaration.class);
		ASTResultType returnType = methodDecl.getFirstChildOfType(ASTResultType.class);
		if (returnType.getFirstChildOfType(ASTType.class) == null && returnType.getImage() == null) {
			isReturningVoid = true;
		}
		return isReturningVoid;
	}

	/**
	 * Returns all the formal parameters of this method
	 * 
	 * @param methodNode
	 *            - AST methodDeclarator
	 * @return list of all formal parameters of this method
	 */
	public static List<ASTFormalParameter> findAllFormalParametersOfMethod(ASTMethodDeclarator methodNode) {
		return methodNode.findChildrenOfType(ASTFormalParameter.class);
	}

	/**
	 * Checks if this method does not contain any formal parameter
	 * 
	 * @param methodNode
	 *            - AST methodDeclarator
	 * @return true, parameter list of this method is empty
	 */
	public static boolean hasNoParameter(ASTMethodDeclarator methodNode) {
		boolean hasNoParams = false;		ASTFormalParameters parameters = methodNode.getFirstChildOfType(ASTFormalParameters.class);		if (parameters == null)		{			hasNoParams = true;		}		else		{			List<ASTFormalParameter> paramList = parameters.findChildrenOfType(ASTFormalParameter.class);			if (paramList == null || paramList.isEmpty())			{				hasNoParams = true;			}		}				return hasNoParams;
	}

	/**
	 * Tells whether or not this method contains only annotation parameters
	 * 
	 * @param methodNode
	 *            - AST methodDeclarator
	 * @return true, if this method contains annotation parameter
	 */
	public static boolean hasOnlyAnnotationParameters(ASTMethodDeclarator methodNode) {
		boolean hasOnlyAnnotationParameters = true;
		List<ASTFormalParameter> lstOfParameter = findAllFormalParametersOfMethod(methodNode);
		if (lstOfParameter.isEmpty())
			hasOnlyAnnotationParameters = false;
		for (int i = 0; i < lstOfParameter.size(); i++) {
			if (lstOfParameter.get(i).getFirstChildOfType(ASTAnnotation.class) == null) {
				hasOnlyAnnotationParameters = false;
				break;
			}
		}
		return hasOnlyAnnotationParameters;
	}

	/**
	 * Tells whether or not, this method contains a valid DTO type parameter
	 * 
	 * @param methodNode
	 *            - AST methodDeclarator
	 * @return true, if this method contains a valid DTO type parameter
	 */
	public static boolean hasDTOParameters(ASTMethodDeclarator methodNode) {
		boolean isExpectingDTO = false;

		List<ASTFormalParameter> lstParameters = findAllFormalParametersOfMethod(methodNode);
		for (int i = 0; i < lstParameters.size(); i++) {
			ASTFormalParameter formalParameter = lstParameters.get(i);
			if (formalParameter.getFirstChildOfType(ASTAnnotation.class) == null) {// whether @PathParam/@QueryParam does not exists
				ASTClassOrInterfaceType type = formalParameter.getFirstChildOfType(ASTClassOrInterfaceType.class);
				if (type != null) {// avoiding primitive type

					ASTClassOrInterfaceType refType = type.getFirstChildOfType(ASTClassOrInterfaceType.class);
					if (refType != null) { // valid for Generic Type	
						isExpectingDTO = NonDTO.isDTO(refType.getImage());
						if (isExpectingDTO)
							break;
					} else {
						isExpectingDTO = NonDTO.isDTO(type.getImage());
						if (isExpectingDTO)
							break;
					}
				}
			}
		}
		return isExpectingDTO;
	}

	/**
	 * Tells whether or not this method contains this annotation as a formal parameter
	 * 
	 * @param methodNode
	 *            - AST methodDeclarator
	 * @param annotationName
	 *            - AST annotation
	 * @return true, if this annotation exists in this method a sa formal parameter
	 */
	public static boolean hasAnnotationParameter(ASTMethodDeclarator methodNode, String annotationName) {
		boolean isAnnotationExists = false;
		List<ASTFormalParameter> lstOfParameter = findAllFormalParametersOfMethod(methodNode);
		if (lstOfParameter.isEmpty())
			isAnnotationExists = false;
		for (int i = 0; i < lstOfParameter.size(); i++) {
			ASTAnnotation annotation = lstOfParameter.get(i).getFirstChildOfType(ASTAnnotation.class);
			if (annotation != null) {
				if (getAnnotationName(annotation).equals(annotationName)) {
					isAnnotationExists = true;
					break;
				}
			}
		}
		return isAnnotationExists;
	}

	/**
	 * Returns the number of DTO Parameters in this method
	 * 
	 * @param methodNode
	 *            - AST methodDeclarator
	 * @return number of DTO parameters of this method.
	 */
	public static int getNumberOfDTOs(ASTMethodDeclarator methodNode) {
		boolean isValidDTO = false;
		int count = 0;
		List<ASTFormalParameter> lstParameters = findAllFormalParametersOfMethod(methodNode);
		for (int i = 0; i < lstParameters.size(); i++) {
			ASTFormalParameter formalParameter = lstParameters.get(i);
			if (formalParameter.getFirstChildOfType(ASTAnnotation.class) == null) {
				ASTClassOrInterfaceType type = formalParameter.getFirstChildOfType(ASTClassOrInterfaceType.class);
				if (type != null) {// avoiding primitive type
					// Checks if the input parameter is a valid DTO or not
					isValidDTO = NonDTO.isDTO(type.getImage());
					if (isValidDTO)
						count++;
				}
			}
		}
		return count;
	}

	/**
	 * Returns the list of all literals of this annotation on method
	 * 
	 * @param annotation
	 *            - AST Annotations
	 * @return list of all literals of this annotation
	 */
	public static List<String> getMethodAnnotationValue(ASTMethodDeclarator methodNode, String annotationName) {
		ASTAnnotation annotation = getMethodAnnotation(methodNode, annotationName);
		List<String> lstLiteral = new ArrayList<String>();
		if (annotation != null) {
			if (annotation.getFirstChildOfType(ASTMemberValueArrayInitializer.class) != null) {
				ASTMemberValueArrayInitializer memberValueArray = annotation
						.getFirstChildOfType(ASTMemberValueArrayInitializer.class);
				List<ASTMemberValue> memberValueArrayList = memberValueArray.findChildrenOfType(ASTMemberValue.class);
				for (int i = 0; i < memberValueArrayList.size(); i++) {
					ASTMemberValue memberValueItem = memberValueArrayList.get(i);
					ASTLiteral literal = memberValueItem.getFirstChildOfType(ASTLiteral.class);
					ASTName name = memberValueItem.getFirstChildOfType(ASTName.class);
					if (name != null) {
						String val = getVariableValue(methodNode, name.getImage());
						lstLiteral.add(val);
					}
					if (literal != null) {
						lstLiteral.add(removeQuotes(literal.getImage()));
					}
				}
			} else {
				lstLiteral.add(getAnnotationValue(methodNode, annotation));
			}
		}
		return lstLiteral;
	}

	/**
	 * Returns the list of all literals of this annotation on method
	 * 
	 * @param annotation
	 *            - AST Annotations
	 * @return list of all literals of this annotation
	 */
	public static List<String> getMethodAnnotationValue(ASTMethodDeclarator methodNode, String annotationName,
			String attributeName) {
		ASTAnnotation annotation = getMethodAnnotation(methodNode, annotationName);
		List<String> lstLiteral = new ArrayList<String>();
		if (annotation != null) {
			List<ASTMemberValuePair> lstMember = annotation.findChildrenOfType(ASTMemberValuePair.class);
			if (lstMember != null) {
				for (int i = 0; i < lstMember.size(); i++) {
					ASTMemberValuePair memValuePair = lstMember.get(i);
					if (memValuePair.getImage().equals(attributeName)) {
						if (memValuePair.getFirstChildOfType(ASTMemberValueArrayInitializer.class) != null) {
							ASTMemberValueArrayInitializer memberValueArray = memValuePair
									.getFirstChildOfType(ASTMemberValueArrayInitializer.class);
							List<ASTMemberValue> memberValueArrayList = memberValueArray.findChildrenOfType(ASTMemberValue.class);
							for (int j = 0; j < memberValueArrayList.size(); j++) {
								ASTMemberValue memberValueItem = memberValueArrayList.get(j);
								ASTLiteral literal = memberValueItem.getFirstChildOfType(ASTLiteral.class);
								ASTName name = memberValueItem.getFirstChildOfType(ASTName.class);
								if (literal != null) {
									lstLiteral.add(removeQuotes(literal.getImage()));
								} else if (name != null) {
									String val = getVariableValue(methodNode, name.getImage());
									lstLiteral.add(val);
								}
							}
						} else {
							ASTName name = memValuePair.getFirstChildOfType(ASTName.class);
							ASTLiteral literal = memValuePair.getFirstChildOfType(ASTLiteral.class);
							if (literal != null)
								lstLiteral.add(removeQuotes(literal.getImage()));
							else if (name != null) {
								String val = getVariableValue(methodNode, name.getImage());
								lstLiteral.add(val);
							}
						}
						break;
					}
				}
			}
		}
		return lstLiteral;
	}

	/**
	 * Return all the values associated with given Annotation name.
	 * 
	 * @param methodNode
	 *            - ASTMethodDeclarator
	 * @param annotationName
	 *            - name of parameter Annotation
	 * @return List of values.
	 */
	public static List<String> getParameterAnnotationValue(ASTMethodDeclarator methodNode, String annotationName) {
		List<String> annotationValues = new ArrayList<String>();
		List<ASTFormalParameter> formalParameters = findAllFormalParametersOfMethod(methodNode);
		for (int i = 0; i < formalParameters.size(); i++) {
			ASTAnnotation annotationNode = formalParameters.get(i).getFirstChildOfType(ASTAnnotation.class);
			if (annotationNode != null) {
				if (getAnnotationName(annotationNode).equals(annotationName)) {
					annotationValues.add(getAnnotationValue(methodNode, annotationNode));
				}
			}
		}
		return annotationValues;
	}

	/**
	 * Returns a String after some modification.
	 * 
	 * @param methodNode
	 *            ASTMethodDeclarator
	 * @param annotationName
	 *            - Name of ASTAnnotation
	 * @param prepend
	 *            - String to prepend
	 * @param append
	 *            - String to append
	 * @return Modified string
	 */
	public static List<String> getModifiedParameterAnnotationValue(ASTMethodDeclarator methodNode, String annotationName,
			String prepend, String append) {
		List<String> annotationValues = new ArrayList<String>();
		List<ASTFormalParameter> formalParameters = findAllFormalParametersOfMethod(methodNode);
		for (int i = 0; i < formalParameters.size(); i++) {
			ASTAnnotation annotationNode = formalParameters.get(i).getFirstChildOfType(ASTAnnotation.class);
			if (annotationNode != null) {
				if (getAnnotationName(annotationNode).equals(annotationName)) {
					String annotationValue = getAnnotationValue(methodNode, annotationNode);
					if (prepend != null)
						annotationValue = prepend + annotationValue;
					if (append != null)
						annotationValue = annotationValue + append;
					annotationValues.add(annotationValue);
				}
			}
		}
		return annotationValues;
	}

	/**
	 * Fetch all the string values enclosed in curly braces.
	 * 
	 * @param literal
	 *            - String literal
	 * @return List of String
	 */
	public static List<String> getLiteralsseparatedByBraces(String literal) {
		List<String> lst = new ArrayList<String>();
		Pattern regex = Pattern.compile("\\{([^}]*)\\}");
		Matcher regexMatcher = regex.matcher(literal);
		while (regexMatcher.find())
			lst.add(regexMatcher.group());
		return lst;
	}

	/**
	 * Provides the Name of Parameter DTO provided by the user
	 * 
	 * @param methodNode
	 *            - ASTMethodDeclarator
	 * @return Parameter DTO name
	 */
	public static String getParameterDTOName(ASTMethodDeclarator methodNode) {
		boolean isExpectingDTO = false;
		String dtoName = null;
		List<ASTFormalParameter> lstParameters = findAllFormalParametersOfMethod(methodNode);
		for (int i = 0; i < lstParameters.size(); i++) {
			ASTFormalParameter formalParameter = lstParameters.get(i);
			if (formalParameter.getFirstChildOfType(ASTAnnotation.class) == null) {// whether @PathParam/@QueryParam does not exists
				ASTClassOrInterfaceType type = formalParameter.getFirstChildOfType(ASTClassOrInterfaceType.class);
				if (type != null) {// avoiding primitive type

					ASTClassOrInterfaceType refType = type.getFirstChildOfType(ASTClassOrInterfaceType.class);
					if (refType != null) { // valid for Generic Type							
						isExpectingDTO = NonDTO.isDTO(refType.getImage());
						if (isExpectingDTO)
							dtoName = refType.getImage();
					} else {
						isExpectingDTO = NonDTO.isDTO(type.getImage());
						if (isExpectingDTO)
							dtoName = type.getImage();
					}
				}
			}
		}
		return dtoName;
	}

	/**
	 * Provides the Name of Returning DTO provided by the user
	 * 
	 * @param methodNode
	 *            - ASTMethodDeclarator
	 * @return Returning DTO name
	 */
	public static String getReturningDTOName(ASTMethodDeclarator methodNode) {
		boolean isReturningDTO = false;
		String dtoName = null;
		ASTMethodDeclaration methodDecl = methodNode.getFirstParentOfType(ASTMethodDeclaration.class);
		ASTResultType returnType = methodDecl.getFirstChildOfType(ASTResultType.class);
		if (returnType != null) { // void handling
			ASTClassOrInterfaceType refType = returnType.getFirstChildOfType(ASTClassOrInterfaceType.class);
			if (refType != null) { // primitive type handling

				ASTClassOrInterfaceType type = refType.getFirstChildOfType(ASTClassOrInterfaceType.class);
				if (type != null) { // valid for Generic Type			
					isReturningDTO = NonDTO.isDTO(type.getImage());
					if (isReturningDTO)
						dtoName = type.getImage();
				} else {					
					isReturningDTO = NonDTO.isDTO(refType.getImage());
					if (isReturningDTO)
						dtoName = refType.getImage();
				}
			}
		}
		return dtoName;
	}

	/**
	 * This method checks whether or not all the Annotation values starts with "application/"
	 * 
	 * @param annotations
	 *            - List of Annotation values
	 * @return true if value starts with "application/"
	 */
	public static boolean isMediaTypeStartWithApplication(List<String> annotations) {
		boolean isMediaTypeStartsWithApplication = false;
		if (annotations == null || annotations.isEmpty())
			isMediaTypeStartsWithApplication = false;
		else {
			for (int i = 0; i < annotations.size(); i++) {
				String annotationValue = annotations.get(i);
				if (i == 0 && annotationValue != null && annotationValue.startsWith(PMDConstants.MEDIA_TYPE_START_STRING)) {
					isMediaTypeStartsWithApplication = true;
					break;
				}
			}
		}
		return isMediaTypeStartsWithApplication;
	}

	/**
	 * This method checks whether or not annotation values provided by the user consists ";charset=UTF-8"
	 * 
	 * @param annotations
	 *            - List of values of Annotation
	 * @return true if value consists with ";charset=UTF-8"
	 */
	public static boolean isMediaTypeContainsCharset(List<String> annotations) {
		boolean isMediaTypeContainsCharset = true;
		if (annotations == null || annotations.isEmpty()) {
			isMediaTypeContainsCharset = false;
		}
		for (int i = 0; i < annotations.size(); i++) {
			String annotationValue = annotations.get(i);
			if (annotationValue != null && annotationValue.indexOf(PMDConstants.SEMICOLON) > 0
					&& annotationValue.indexOf(PMDConstants.CHARSET_STRING) == -1) {
				isMediaTypeContainsCharset = false;
				break;
			}
		}
		return isMediaTypeContainsCharset;
	}

	/**
	 * Returns the fully Qualified Class of Parameter DTO
	 * 
	 * @param methodNode
	 *            - ASTMethodDeclarator
	 * @return fully Qualified Class of Parameter DTO
	 */
	public static Class<?> getParameterDTOClass(ASTMethodDeclarator methodNode, Project project) {
		boolean isDTO = false;
		List<ASTFormalParameter> lstParameters = findAllFormalParametersOfMethod(methodNode);
		Class<?> fullyQualifiedClass = null;
		for (int i = 0; i < lstParameters.size(); i++) {
			ASTFormalParameter formalParameter = lstParameters.get(i);
			if (formalParameter.getFirstChildOfType(ASTAnnotation.class) == null) {// whether @PathParam/@QueryParam does not exists
				ASTClassOrInterfaceType type = formalParameter.getFirstChildOfType(ASTClassOrInterfaceType.class);
				if (type != null) {// avoiding primitive type

					ASTClassOrInterfaceType refType = type.getFirstChildOfType(ASTClassOrInterfaceType.class);
					if (refType != null) { // valid for Generic Type
						isDTO = NonDTO.isDTO(refType.getImage());
						if (isDTO) {
							fullyQualifiedClass = getDTOClass(methodNode, refType.getImage(), project);
						}
					} else {
						isDTO = NonDTO.isDTO(type.getImage());
						if (isDTO) {
							fullyQualifiedClass = getDTOClass(methodNode, type.getImage(), project);
						}
					}
				}
			}
		}
		return fullyQualifiedClass;
	}

	/**
	 * Returns the fully Qualified Class of Returning DTO
	 * 
	 * @param methodNode
	 *            - ASTMethodDeclarator
	 * @return fully Qualified Class of Returning DTO
	 */
	public static Class<?> getReturningDTOClass(ASTMethodDeclarator methodNode, Project project) {
		boolean isDTO = false;
		Class<?> fullyQualifiedClass = null;
		ASTMethodDeclaration methodDecl = methodNode.getFirstParentOfType(ASTMethodDeclaration.class);
		ASTResultType returnType = methodDecl.getFirstChildOfType(ASTResultType.class);
		if (returnType != null) { // void handling
			ASTClassOrInterfaceType refType = returnType.getFirstChildOfType(ASTClassOrInterfaceType.class);
			if (refType != null) { // primitive type handling

				ASTClassOrInterfaceType type = refType.getFirstChildOfType(ASTClassOrInterfaceType.class);
				if (type != null) { // valid for Generic Type	
					isDTO = NonDTO.isDTO(type.getImage());
					if (isDTO)
						fullyQualifiedClass = getDTOClass(methodNode, type.getImage(), project);
				} else {
					isDTO = NonDTO.isDTO(refType.getImage());
					if (isDTO){
						fullyQualifiedClass = getDTOClass(methodNode, refType.getImage(), project);						
					}   
				}
			}
		}
		return fullyQualifiedClass;
	}

	/**
	 * Find the Fully Qualified class of DTO passed by the user
	 * 
	 * @param methodNode
	 *            - ASTMethodDeclarator
	 * @param dtoName
	 *            - name of DTO
	 * @param project
	 *            - Project in which you need to apply PMD
	 * @return Fully Qualified class of DTO passed by the user
	 */
	public static Class<?> getDTOClass(ASTMethodDeclarator methodNode, String dtoName, Project project) {		
		ASTCompilationUnit compilationUnit = methodNode.getFirstParentOfType(ASTCompilationUnit.class);
		return getDTOClass(compilationUnit, dtoName, project);
	}
	
	/**
	 * Find the Fully Qualified class of DTO passed by the user
	 * 
	 * @param compilationUnit
	 *            - ASTCompilationUnit
	 * @param dtoName
	 *            - name of DTO
	 * @param project
	 *            - Project in which you need to apply PMD
	 * @return Fully Qualified class of DTO passed by the user
	 */
	public static Class<?> getDTOClass(ASTCompilationUnit compilationUnit, String dtoName, Project project) {

		String qualifiedName = dtoName;
		String packageName = "";
		Class<?> fullyQualifiedClass = null;

		fullyQualifiedClass = project.loadClass(qualifiedName);		
		
		List<String> lstImportDecl = getImportDeclarations(compilationUnit);
		if (fullyQualifiedClass == null) {
			for (int i = 0; i < lstImportDecl.size(); i++) {
				qualifiedName = lstImportDecl.get(i);
				if (qualifiedName.indexOf(dtoName) != -1) {
					fullyQualifiedClass = project.loadClass(qualifiedName);
					if (fullyQualifiedClass != null)
						break;
				}
			}
		}
		if (fullyQualifiedClass == null) {
			for (int i = 0; i < lstImportDecl.size(); i++) {
				qualifiedName = lstImportDecl.get(i) + "." + dtoName;
				fullyQualifiedClass = project.loadClass(qualifiedName);
				if (fullyQualifiedClass != null)
					break;
			}
		}
		if (fullyQualifiedClass == null) {
			packageName = getPackageName(compilationUnit);
			qualifiedName = packageName + "." + dtoName;
			fullyQualifiedClass = project.loadClass(qualifiedName);
		}
		return fullyQualifiedClass;
	}

	/**
	 * Provides the package name of a Class
	 * 
	 * @param compilationUnit
	 *            - Compilation Unit
	 * @return Package name
	 */
	public static String getPackageName(ASTCompilationUnit compilationUnit) {
		String packageName = "";		
		for (int i = 0; i < compilationUnit.jjtGetNumChildren(); i++) {
			Node classChildNode = compilationUnit.jjtGetChild(i);
			if (classChildNode instanceof ASTPackageDeclaration) {
				packageName = ((ASTPackageDeclaration) classChildNode).getFirstChildOfType(ASTName.class).getImage();
				break;
			}
		}
		return packageName;
	}

	/**
	 * Provides List of all the import statments used in Class
	 * 
	 * @param compilationUnit
	 *            - Compilation Unit
	 * @return List of import statements
	 */
	public static List<String> getImportDeclarations(ASTCompilationUnit compilationUnit) {		
		List<String> results = new ArrayList<String>();
		for (int i = 0; i < compilationUnit.jjtGetNumChildren(); i++) {
			Node classChildNode = compilationUnit.jjtGetChild(i);
			if (classChildNode instanceof ASTImportDeclaration) {
				results.add(((ASTImportDeclaration) classChildNode).getFirstChildOfType(ASTName.class).getImage());
			}
		}
		return results;
	}
	
	/**
	 * Checks whether annotation is present on Class or not
	 * 
	 * @param classType - DTO class
	 * @param annotationName - annotation
	 * @return true if this field is having annotation.
	 */
	public static boolean hasAnnotation(Class<?> classType, String annotationName) {
		boolean isAnnotated = false;
		int i = 0;
		if (classType != null) {
			Annotation[] annos = classType.getDeclaredAnnotations();
			for (i = 0; i < annos.length; i++) {				
				if (annos[i].annotationType().getSimpleName().equals(annotationName)) {
					isAnnotated = true;
					break;
				}
			}
		}	
		return isAnnotated;
	}
	
	/**
	 * Checks whether annotation is present on field or not
	 * 
	 * @param field - Field in class
	 * @param annotationName - annotation
	 * @return true if this field is having annotation.
	 */
	public static boolean hasAnnotation(Field field, String annotationName) {
		boolean isAnnotated = false;
		int i = 0;
		if (field != null) {
			Annotation[] annos = field.getDeclaredAnnotations();
			for (i = 0; i < annos.length; i++) {				
				if (annos[i].annotationType().getSimpleName().equals(annotationName)) {
					isAnnotated = true;
					break;
				}
			}
		}
		return isAnnotated;
	}
	
	/**
	 * Checks whether annotation is present on method or not
	 * 
	 * @param method - method in class
	 * @param annotationName - annotation
	 * @return true if this field is having annotation.
	 */
	public static boolean hasAnnotation(Method method, String annotationName) {
		boolean isAnnotated = false;
		int i = 0;
		if (method != null) {
			Annotation[] annos = method.getDeclaredAnnotations();
			for (i = 0; i < annos.length; i++) {				
				if (annos[i].annotationType().getSimpleName().equals(annotationName)) {
					isAnnotated = true;
					break;
				}
			}
		}
		return isAnnotated;
	}
		
	/**
	 * Fetches all interfaces type which this class is implementing.
	 * 
	 * @param classNode Class Node
	 * @return List of implementation class
	 */
	public static List<Class<?>> getInterfaceTypes(ASTClassOrInterfaceDeclaration classNode, Project project){		
		List<Class<?>> impletmentsInterfaceList = new ArrayList<Class<?>>();
		ASTCompilationUnit compilationUnit = classNode.getFirstParentOfType(ASTCompilationUnit.class);
		ASTImplementsList implementsNode = classNode.getFirstChildOfType(ASTImplementsList.class);
		if(implementsNode != null){
			List<ASTClassOrInterfaceType> implementsInterfaceNodeList = implementsNode.findChildrenOfType(ASTClassOrInterfaceType.class);			
			for(ASTClassOrInterfaceType implementsInterfaceNode : implementsInterfaceNodeList){
				Class<?> interfaceClass = getDTOClass(compilationUnit, implementsInterfaceNode.getImage(), project);				
				impletmentsInterfaceList.add(interfaceClass);
			}
		}
		return impletmentsInterfaceList;
	}
	
	/**
	 * 1. Check all fields in class and find that it has any annotation defined in list or not
	 * 2. If step 2 is not true, repeat step 1 for all methods of DTO.
	 * 3. Check the same in inner class
	 * 4. If step 1 or 2 or 3 are not true, then this has no annotation in class
	 *  
	 * @param classType class
	 * @param annotations List of annotations to be looked in class
	 * @return Is this class has annotation defined in list	
	 */
	public static boolean hasAnnotationOnFieldOrMethod(Class<?> classType, List<String> annotations) {
		boolean hasAnnotation = false;
		Field[] fields = classType.getDeclaredFields();
		Method[] methods = classType.getDeclaredMethods();
		FIELD:
		for(Field field : fields){
			for(String annotation : annotations){
				if(hasAnnotation(field, annotation)){
					hasAnnotation = true;
					break FIELD;
				}
			}
		}
		
		if(!hasAnnotation){
			METHOD:
			for(Method method : methods){
				for(String annotation : annotations){
					if(hasAnnotation(method, annotation)){
						hasAnnotation = true;
						break METHOD;
					}
				}
			}
		}
		if(!hasAnnotation){
			Class<?>[] innerClasses = classType.getDeclaredClasses();
			for(Class<?> innerClass : innerClasses){
				hasAnnotation = hasAnnotationOnFieldOrMethod(innerClass,annotations);
				if(hasAnnotation)
					break;
			}
		}	
		return hasAnnotation;
	}
	
	/**
	 * Return the annotation value if there is no attribute in annotation name (for value only)
	 * @param method Method on which this annotation exists
	 * @param annotataionName Name of annotation
	 * @return List of Annotation value 
	 * @throws Exception If this method (value) does not exists in this annotation
	 */
	public static String[] getDefaultAnnotationValue(Method method, String annotataionName) throws Exception{
		Annotation[] annotations = method.getDeclaredAnnotations();
		return getDefaultAnnotationValue(annotations, annotataionName);
	}
	
	/**
	 * Return the annotation value if there is no attribute in annotation name (for value only)
	 * @param classInstance Class on which this annotation exists
	 * @param annotataionName Name of annotation
	 * @return List of Annotation value 
	 * @throws Exception If this method (value) does not exists in this annotation
	 */
	public static String[] getDefaultAnnotationValue(Class<?> classInstance, String annotataionName) throws Exception{
		Annotation[] annotations = classInstance.getDeclaredAnnotations();
		return getDefaultAnnotationValue(annotations, annotataionName);
	}
	
	/**
	 * Return the annotation value if there is no attribute in annotation name (for value only)
	 * @param Annotation Annotations 
	 * @param annotataionName Name of annotation
	 * @return List of Annotation value 
	 * @throws Exception If this method (value) does not exists in this annotation
	 */
	@SuppressWarnings("unchecked")
	public static String[] getDefaultAnnotationValue(Annotation[] annotations, String annotataionName) throws Exception{
		String[] annotationValues = null;
		if(annotations != null) {
			for(Annotation annotation : annotations){
				if(annotation.annotationType().getSimpleName().equals(annotataionName)){
					Method annotationMethod = annotation.annotationType().getDeclaredMethod(PMDConstants.VALUE_ANNOTATION_METHOD, (Class[]) null);
					if(annotationMethod != null ) {
						Object object = annotationMethod.invoke(annotation, (Object[]) null);
						if(object instanceof String){
							annotationValues = new String[1];
							annotationValues[0] = (String) object;
						} else if(object instanceof String[]){
							annotationValues = (String[]) object;
						} else if(object instanceof Enum){
							annotationValues = new String[1];
							annotationValues[0] = object.toString();
						}
					}
				}
			}
		}
		return annotationValues;
	}
}