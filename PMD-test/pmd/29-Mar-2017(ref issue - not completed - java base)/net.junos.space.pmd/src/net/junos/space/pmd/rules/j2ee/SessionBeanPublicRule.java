package net.junos.space.pmd.rules.j2ee;

import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;
import net.junos.space.pmd.utils.outline.SimpleInspector;

public class SessionBeanPublicRule extends SimpleInspector {

	@Override
	protected boolean mustBeChecked(IJavaTypeDeclaration typeDeclaration) {
		return isEnterpriseBeanEJB2(typeDeclaration);
	}
	
	public boolean haveError(IJavaTypeDeclaration decl, IClassOutline outline) {
		return !decl.isPublic();
	}

	@Override
	public String getMessage() {
		return "Make all EJB interfaces and classes public";
	}
}
