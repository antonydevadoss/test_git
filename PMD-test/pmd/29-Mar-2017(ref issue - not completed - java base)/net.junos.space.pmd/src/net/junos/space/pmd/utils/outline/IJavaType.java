package net.junos.space.pmd.utils.outline;

import java.util.Collection;
import java.util.List;

public interface IJavaType {
	
	IJavaTypeDeclaration getDeclaration() ;
	
	void setDeclaration( IJavaTypeDeclaration declaration ) ;
	
	boolean isArray() ;
	
	void setIsArray( boolean isArray ) ;
	
	List<IJavaType> getGenericParameters() ;
	
	void setGenericParameters( Collection<IJavaType> params ) ;
	
	int getArrayDepth() ;
	
	void setArrayDepth( int arrayDepth ) ;
	
	String getSimpleName() ;
	
	void setSimpleName( String simpleName ) ;
	
	QualifiedName getQualifiedName() ;
	
	void setQualifiedName( QualifiedName QualifiedName ) ;

	boolean isSimilar(IJavaType another);	
}
