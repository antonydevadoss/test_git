package net.junos.space.pmd.utils.outline;


public class JavaFieldDeclaration extends AbstractElementDeclaration implements IJavaFieldDeclaration {
	
	private IJavaType type ;
	
	private IJavaTypeDeclaration ownerClass ;
	
	private boolean isTransient ;

	@Override
	public IJavaType getType() {
		return this.type ;
	}

	@Override
	public void setType(IJavaType type) {
		this.type = type ;		
	}

	public IJavaTypeDeclaration getOwner() {
		return ownerClass;
	}

	public void setOwner(IJavaTypeDeclaration ownerClass) {
		this.ownerClass = (IJavaTypeDeclaration) ownerClass;
	}

	public boolean isTransient() {
		return isTransient;
	}

	public void setTransient(boolean isTransient) {
		this.isTransient = isTransient;
	}
}
