package net.junos.space.pmd.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.junos.space.pmd.utils.PMDConstants.HttpMethod;
import net.sourceforge.pmd.lang.ast.Node;
import net.sourceforge.pmd.lang.java.ast.ASTAdditiveExpression;
import net.sourceforge.pmd.lang.java.ast.ASTAnnotation;
import net.sourceforge.pmd.lang.java.ast.ASTClassOrInterfaceDeclaration;
import net.sourceforge.pmd.lang.java.ast.ASTCompilationUnit;
import net.sourceforge.pmd.lang.java.ast.ASTExpression;
import net.sourceforge.pmd.lang.java.ast.ASTFieldDeclaration;
import net.sourceforge.pmd.lang.java.ast.ASTLiteral;
import net.sourceforge.pmd.lang.java.ast.ASTMemberValue;
import net.sourceforge.pmd.lang.java.ast.ASTMemberValueArrayInitializer;
import net.sourceforge.pmd.lang.java.ast.ASTMemberValuePair;
import net.sourceforge.pmd.lang.java.ast.ASTMemberValuePairs;
import net.sourceforge.pmd.lang.java.ast.ASTMethodDeclarator;
import net.sourceforge.pmd.lang.java.ast.ASTName;
import net.sourceforge.pmd.lang.java.ast.ASTPackageDeclaration;
import net.sourceforge.pmd.lang.java.ast.ASTPrimaryExpression;

public class CustomUtils {
	
	public static String getPackageName(ASTCompilationUnit compilationUnit) {
		String packageName = "";		
		for (int i = 0; i < compilationUnit.jjtGetNumChildren(); i++) {
			Node classChildNode = compilationUnit.jjtGetChild(i);
			if (classChildNode instanceof ASTPackageDeclaration) {
				packageName = ((ASTPackageDeclaration) classChildNode).getFirstChildOfType(ASTName.class).getImage();
				break;
			}
		}
		return packageName;
	}
	
	public static Map<String, Object> getStaticVariablesOfClass(ASTClassOrInterfaceDeclaration classNode) {
		Map<String, Object> staticVariables = new HashMap<String, Object>();
		if(classNode != null) {
			List<ASTFieldDeclaration>  fieldList = classNode.findChildrenOfType(ASTFieldDeclaration.class);
			if(fieldList != null) {
				Map<String, Object> staticVars = null;
				for (ASTFieldDeclaration astField : fieldList) {
					if(astField.isStatic()) {
						
					}
				}
			}
		}
		return staticVariables;
	}
	
	public static String getClassAnnotationString(ASTClassOrInterfaceDeclaration classNode, String annotationName) {
		List<ASTAnnotation> annotationList = PMDUtils.getClassAnnotations(classNode);
		if(annotationList != null) {
			for (ASTAnnotation astAnnotation : annotationList) {
				String name = astAnnotation.getFirstDescendantOfType(ASTName.class).getImage();
				if(name.equals(annotationName)) {
					return getAnnotationMemberValue(astAnnotation.getFirstDescendantOfType(ASTMemberValue.class));
				}
			}
		}
		return null;
	}
	
	public static String getMethodAnnotationString(ASTMethodDeclarator methodNode, String annotationName) {
		ASTAnnotation annotation = PMDUtils.getMethodAnnotation(methodNode, annotationName);
		if(annotation != null) {
			return getAnnotationMemberValue((ASTMemberValue) annotation.getFirstDescendantOfType(ASTMemberValue.class));
		}
		return null;
	}
	
	public static Map<String, Object> getMethodAnnotationMap(ASTMethodDeclarator methodNode, String annotationName) {
		ASTAnnotation annotation = PMDUtils.getMethodAnnotation(methodNode, annotationName);
		if(annotation != null) {
			return getAnnotationValueMap((ASTMemberValuePairs) annotation.getFirstDescendantOfType(ASTMemberValuePairs.class));
		}
		return null;
	}
	
	public static Map<String, Object> getMethodAnnotationMap(ASTAnnotation annotation) {
		if(annotation != null) {
			return getAnnotationValueMap((ASTMemberValuePairs) annotation.getFirstDescendantOfType(ASTMemberValuePairs.class));
		}
		return null;
	}
	
	public static List<String> getMethodAnnotationList(ASTMethodDeclarator methodNode, String annotationName) {
		ASTAnnotation annotation = PMDUtils.getMethodAnnotation(methodNode, annotationName);
		if(annotation != null) {
			return getAnnotationMemberValueList((ASTMemberValue) annotation.getFirstDescendantOfType(ASTMemberValue.class));
		}
		return null;
	}
	
	private static String getAnnotationMemberValue(ASTMemberValue memberVal) {
		String val = null;
		if(memberVal != null) {
			Node primaryOrAdditiveExpr = memberVal.jjtGetChild(0);
			if(primaryOrAdditiveExpr != null) {
				if(primaryOrAdditiveExpr instanceof ASTPrimaryExpression) {
					val = getPrimaryExpressionValue((ASTPrimaryExpression)primaryOrAdditiveExpr);
				} else if(primaryOrAdditiveExpr instanceof ASTAdditiveExpression) {
					StringBuffer br = new StringBuffer();
					for(int i=0; i < primaryOrAdditiveExpr.jjtGetNumChildren(); i++) {
						br.append(getPrimaryExpressionValue((ASTPrimaryExpression) primaryOrAdditiveExpr.jjtGetChild(i)));
					}
				}
			}
		}
		return val;
	}
	
	private static Map<String, Object> getAnnotationValueMap(ASTMemberValuePairs memberVals) {
		Map<String, Object> val = null;
		if(memberVals != null ) {
			val = new HashMap<String, Object>();
			for(int i=0; i < memberVals.jjtGetNumChildren(); i++) {
				ASTMemberValuePair memValPair = (ASTMemberValuePair) memberVals.jjtGetChild(i);
				ASTMemberValue memVal = (ASTMemberValue) memValPair.jjtGetChild(0);
				if(memVal.jjtGetChild(0) instanceof ASTMemberValueArrayInitializer) {
					val.put(memValPair.getImage(), getAnnotationMemberValueList(memVal));
				} else {
					val.put(memValPair.getImage(), getAnnotationMemberValue(memVal));
				}
			}
		}
		return val;
	}
	
	
	private static List<String> getAnnotationMemberValueList(ASTMemberValue memberVal) {
		List<String> val = null;
		if(memberVal != null && memberVal.jjtGetChild(0) != null && memberVal.jjtGetChild(0) instanceof ASTMemberValueArrayInitializer ) {
			ASTMemberValueArrayInitializer memberArray = (ASTMemberValueArrayInitializer) memberVal.jjtGetChild(0);
			int count = memberArray.jjtGetNumChildren();
			val = new ArrayList<String>(count);
			for(int i=0; i < count; i++) {
				val.add(getAnnotationMemberValue((ASTMemberValue) memberArray.jjtGetChild(i)));
			}
		}
		return val;
	}
	
	private static String getPrimaryExpressionValue(ASTPrimaryExpression expr) {
		String val = null;
		Node literalOrName = expr.jjtGetChild(0).jjtGetChild(0);
		if(literalOrName instanceof ASTLiteral) {
			val = literalOrName.getImage();
			val = val.replaceAll("\"", "");
		} else if (literalOrName instanceof ASTName){
			val = new StringBuffer()/*.append("$$")*/.append(literalOrName.getImage())/*.append("#$")*/.toString();
		}
		return val;
	}

	public static boolean isWizardGeneratedMethod(ASTMethodDeclarator methodNode) {

		List<ASTAnnotation> annotations = PMDUtils.getMethodAnnotations(methodNode);
		boolean isWizardGeneratedMethod = false;
		for (int i = 0; i < annotations.size(); i++) {
			String annotationName = annotations.get(i).getFirstDescendantOfType(ASTName.class).getImage();
			if (annotationName.equals("Generated")){
				isWizardGeneratedMethod = true;
				break;
			}
		}
		return isWizardGeneratedMethod;
	}
}
