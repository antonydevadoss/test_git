package net.junos.space.pmd.utils.outline;



import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;

import net.junos.space.pmd.rules.AbstractJavaRuleWithUselessTypeResolution;
import net.junos.space.pmd.rules.j2ee.ejb3.EntityCompositePrimaryKeyFieldsRule;
import net.sourceforge.pmd.RuleContext;
import net.sourceforge.pmd.lang.java.ast.ASTClassOrInterfaceDeclaration;
import net.sourceforge.pmd.lang.java.ast.ASTCompilationUnit;
import net.sourceforge.pmd.lang.java.ast.ASTEnumDeclaration;
import net.sourceforge.pmd.lang.java.ast.ASTPackageDeclaration;
import net.sourceforge.pmd.lang.java.ast.ASTTypeDeclaration;
import net.sourceforge.pmd.lang.java.ast.AbstractJavaAccessTypeNode;

public class AbstractOutlineAccessor extends AbstractJavaRuleWithUselessTypeResolution {

	private OutlineCollector collector = new OutlineCollector();
	
	private IOutlineContainer outlineContainer ;
	
	static private boolean dumpWritten = false ;
	
	private IClassOutline currentOutline = null ;

	final public Object visit(ASTCompilationUnit node, Object data) {		
		
		currentOutline = null;
				
		setOutlineContainer(data) ;
		
		String packageName = "" ; 
		ASTPackageDeclaration packageDeclaration = node.getPackageDeclaration();
		if(packageDeclaration!=null)
			packageName = packageDeclaration.getPackageNameImage() ;
		
		AbstractJavaAccessTypeNode classNode = extractClassOrEnum(node) ;
		
		if(classNode != null){			
			String simpleName = classNode.getImage();
			String qualifiedName = packageName + "." + simpleName ;
			
			IClassOutline classOutline = getOutlineContainer().getClassOutline(qualifiedName);
			if( classOutline == null ){		
				collector.visit(node, data) ;
			}
			classOutline = getOutlineContainer().getClassOutline(qualifiedName);
			currentOutline = classOutline;
		}
		return doVisitCompilationUnit( node, data );
	}
	
	private AbstractJavaAccessTypeNode extractClassOrEnum(ASTCompilationUnit node)
	{
		ASTTypeDeclaration typeDeclNode = node.getFirstChildOfType(ASTTypeDeclaration.class);
		if(typeDeclNode==null)
			return null ;
		
		ASTClassOrInterfaceDeclaration classNode = typeDeclNode.getFirstChildOfType(ASTClassOrInterfaceDeclaration.class);
		if(classNode != null)
			return classNode ;
		
		ASTEnumDeclaration enumNode = typeDeclNode.getFirstChildOfType(ASTEnumDeclaration.class);
		
		return enumNode;
	}
	
	public Object doVisitCompilationUnit(ASTCompilationUnit node, Object data){
		return super.visit(node, data) ;
	}
	
	@Override
	public boolean usesTypeResolution() {
		return false ;
	}

	public void doStart(RuleContext ctx){} ;

	public void doEnd(RuleContext ctx){} ;

	@Override
	final public void start(RuleContext ctx) {
		setOutlineContainer(ctx) ;
		getOutlineContainer().clear() ;		
		dumpWritten = false ;
		doStart(ctx);
	}

	@Override
	final public void end(RuleContext ctx) {
		setOutlineContainer(ctx) ;
		collector.resolveUndefinedTypes() ;
		writeDump(ctx,this.outlineContainer) ;
		doEnd(ctx) ;
	}
	

	public IOutlineContainer getOutlineContainer() {
		return outlineContainer;
	}
	
	private void writeDump(RuleContext ctx, IOutlineContainer oc) {		
		
		Object pathObj = ctx.getAttribute("output.properties.path");
		if(pathObj == null)
			return;
		
		if(dumpWritten)
			return ;
		
		dumpWritten = true ;
		
		String path = pathObj.toString();
		Properties prop = new Properties() ;
		try {
			FileInputStream fis = new FileInputStream( new File(path));			
			prop.load(fis) ;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}		
		
		String dumpPath = prop.getProperty("dump.path.outline") ;
		if(dumpPath == null)
			return ;
		
		IOutlinePrinter printer = new DefaultOutlinePrinter() ;
		printer.writeDump(getOutlineContainer(), dumpPath) ;
	}
	
	private void setOutlineContainer(Object data) {
		
		this.outlineContainer = BasicOutlineContainer.getInstance() ;//null ;
		
//		RuleContext ctx = (RuleContext) data ;
//		Object obj = ctx.getAttribute( BasicOutlineContainer.BASIC_OUTLINE_CONTAINER_CONTEXT_KEY ) ;
//		if( obj == null ){
//			this.outlineContainer = new BasicOutlineContainer() ;
//			ctx.setAttribute(
//					BasicOutlineContainer.BASIC_OUTLINE_CONTAINER_CONTEXT_KEY, this.outlineContainer ) ;
//		}
//		else
//			this.outlineContainer = (IOutlineContainer) obj ;
		
		collector.setOutlineContainer( getOutlineContainer() ) ;
	}
	
	public boolean isEnterpriseBeanEJB3(String className) {
		return hasOneOfAnnotations(className, new String[]{"Stateless", "Stateful", "MessageDriven"}) ;
	}
	
	public boolean isSessionBeanEJB3(String className) {
		return hasOneOfAnnotations(className, new String[]{"Stateless", "Stateful"}) ;
	}
	
	public boolean isMessageDrivenBeanEJB3(String className) {
		return hasOneOfAnnotations(className, new String[]{"MessageDriven"}) ;
	}
	
	public boolean isEnterpriseBeanEJB2(String className) {
		return implementsOneOfInterfaces(className, new String[]{"javax.ejb.SessionBean","javax.ejb.EntityBean","javax.ejb.MessageDrivenBean"}) ;
	}
	
	public boolean isEntityBeanEJB2(String className) {
		return implementsOneOfInterfaces(className, new String[]{"javax.ejb.EntityBean"}) ;
	}
	
	public boolean isSessionBeanEJB2(String className) {
		return implementsOneOfInterfaces(className, new String[]{"javax.ejb.SessionBean"}) ;
	}
	
	public boolean isMessageDrivenBeanEJB2(String className) {
		return implementsOneOfInterfaces(className, new String[]{"javax.ejb.MessageDrivenBean"}) ;
	}
	
	public boolean isEnterpriseBeanEJB3(IJavaTypeDeclaration decl) {
		return hasOneOfAnnotations(decl, new String[]{"Stateless", "Stateful", "MessageDriven"}) ;
	}
	
	public boolean isSessionBeanEJB3(IJavaTypeDeclaration decl) {
		return hasOneOfAnnotations(decl, new String[]{"Stateless", "Stateful"}) ;
	}
	
	public boolean isMessageDrivenBeanEJB3(IJavaTypeDeclaration decl) {
		return hasOneOfAnnotations(decl, new String[]{"MessageDriven"}) ;
	}
	
	public boolean isEnterpriseBeanEJB2(IJavaTypeDeclaration decl) {
		return implementsOneOfInterfaces(decl, new String[]{"javax.ejb.SessionBean","javax.ejb.EntityBean","javax.ejb.MessageDrivenBean"}) ;
	}
	
	public boolean isEntityBeanEJB2(IJavaTypeDeclaration decl) {
		return implementsOneOfInterfaces(decl, new String[]{"javax.ejb.EntityBean"}) ;
	}
	
	public boolean isSessionBeanEJB2(IJavaTypeDeclaration decl) {
		return implementsOneOfInterfaces(decl, new String[]{"javax.ejb.SessionBean"}) ;
	}
	
	public boolean isMessageDrivenBeanEJB2(IJavaTypeDeclaration decl) {
		return implementsOneOfInterfaces(decl, new String[]{"javax.ejb.MessageDrivenBean"}) ;
	}
	
	public boolean isEnterpriseBeanSuperclassEJB3(String className) {
		return hasSubclassWithOneOfAnnotations(className, new String[]{"Stateless", "Stateful", "MessageDriven"}) ;
	}
	
	public boolean isSessionBeanSuperclassEJB3(String className) {
		return hasSubclassWithOneOfAnnotations(className, new String[]{"Stateless", "Stateful"}) ;
	}
	
	public boolean isMessageDrivenBeanSuperclassEJB3(String className) {
		return hasSubclassWithOneOfAnnotations(className, new String[]{"MessageDriven"}) ;
	}
	
	public boolean isEnterpriseBeanSuperclassEJB2(String className) {
		return hasSubclassWithOneOfInterfaces(className, new String[]{"javax.ejb.SessionBean","javax.ejb.EntityBean","javax.ejb.MessageDrivenBean"}) ;
	}
	
	public boolean isEntityBeanSuperclassEJB2(String className) {
		return hasSubclassWithOneOfInterfaces(className, new String[]{"javax.ejb.EntityBean"}) ;
	}
	
	public boolean isSessionBeanSuperclassEJB2(String className) {
		return hasSubclassWithOneOfInterfaces(className, new String[]{"javax.ejb.SessionBean"}) ;
	}
	
	public boolean isMessageDrivenBeanSuperclassEJB2(String className) {
		return hasSubclassWithOneOfInterfaces(className, new String[]{"javax.ejb.MessageDrivenBean"}) ;
	}
	
	public boolean isEnterpriseBeanSuperclassEJB3(IJavaTypeDeclaration decl) {
		return hasSubclassWithOneOfAnnotations(decl, new String[]{"Stateless", "Stateful", "MessageDriven"}) ;
	}
	
	public boolean isSessionBeanSuperclassEJB3(IJavaTypeDeclaration decl) {
		return hasSubclassWithOneOfAnnotations(decl, new String[]{"Stateless", "Stateful"}) ;
	}
	
	public boolean isMessageDrivenBeanSuperclassEJB3(IJavaTypeDeclaration decl) {
		return hasSubclassWithOneOfAnnotations(decl, new String[]{"MessageDriven"}) ;
	}
	
	public boolean isEnterpriseBeanSuperclassEJB2(IJavaTypeDeclaration decl) {
		return hasSubclassWithOneOfInterfaces(decl, new String[]{"javax.ejb.SessionBean","javax.ejb.EntityBean","javax.ejb.MessageDrivenBean"}) ;
	}
	
	public boolean isEntityBeanSuperclassEJB2(IJavaTypeDeclaration decl) {
		return hasSubclassWithOneOfInterfaces(decl, new String[]{"javax.ejb.EntityBean"}) ;
	}
	
	public boolean isSessionBeanSuperclassEJB2(IJavaTypeDeclaration decl) {
		return hasSubclassWithOneOfInterfaces(decl, new String[]{"javax.ejb.SessionBean"}) ;
	}
	
	public boolean isMessageDrivenBeanSuperclassEJB2(IJavaTypeDeclaration decl) {
		return hasSubclassWithOneOfInterfaces(decl, new String[]{"javax.ejb.MessageDrivenBean"}) ;
	}
	
	
	
	public boolean hasOneOfAnnotations( String className, String[] annotationsList){
		
		if(annotationsList == null || annotationsList.length == 0 || className == null)
			return false ;
		
		IClassOutline classOutline = getOutlineContainer().getClassOutline(className);
		if(classOutline == null)
			return false;
		
		IJavaTypeDeclaration decl = classOutline.getTypeDeclaration();		
		return hasOneOfAnnotations( decl, annotationsList );
	}

	private boolean hasOneOfAnnotations(IJavaTypeDeclaration decl, String[] annotationsList)
	{
		List<IJavaAnnotation> annotations = decl.getAnnotations();
		if( annotations == null )
			return false ;
		
		HashSet<String> set = new HashSet<String>(Arrays.asList(annotationsList)) ;
		
		boolean result = false ;
		for(IJavaAnnotation ann : annotations){
			String annName = ann.getName();
			if( set.contains(annName) )
			{
				result = true ;
				break ;
			}
		}		
		return result ;
	}
	
	private boolean hasSubclassWithOneOfAnnotations(IJavaTypeDeclaration decl, String[] annotationsList)
	{
		if(hasOneOfAnnotations(decl, annotationsList))
			return true ;
		
		List<IJavaTypeDeclaration> subclasses = decl.getSubclasses();
		if(subclasses==null)
			return false ;
		
		for(int i = 0 ; i < subclasses.size() ; i++ ){
			IJavaTypeDeclaration d = subclasses.get(i);
			if( hasOneOfAnnotations(d, annotationsList))
				return true ;
			
			List<IJavaTypeDeclaration> lst = d.getSubclasses();
			if(lst != null)
				subclasses.addAll(lst) ;
		}
		return false ;
	}
	
	public boolean hasSubclassWithOneOfAnnotations( String className, String[] annotationsList){
		
		if(annotationsList == null || annotationsList.length == 0 || className == null)
			return false ;
		
		IClassOutline classOutline = getOutlineContainer().getClassOutline(className);
		if(classOutline == null)
			return false;
		
		IJavaTypeDeclaration decl = classOutline.getTypeDeclaration();		
		return hasSubclassWithOneOfAnnotations( decl, annotationsList );
	}
	
	public boolean implementsOneOfInterfaces( String className, String[] interfacesList){
		
		if(interfacesList == null || interfacesList.length == 0 || className == null)
			return false ;
		
		IClassOutline classOutline = getOutlineContainer().getClassOutline(className);
		if(classOutline == null)
			return false;
		
		IJavaTypeDeclaration decl = classOutline.getTypeDeclaration();	
		return implementsOneOfInterfaces(decl, interfacesList);
	}

	private boolean implementsOneOfInterfaces(IJavaTypeDeclaration decl,
			String[] interfacesList) {
		List<IJavaTypeDeclaration> allInterfaces = decl.getAllInterfaces();
		if( allInterfaces == null || allInterfaces.isEmpty() )
			return false ;
		
		HashSet<String> set = new HashSet<String>(Arrays.asList(interfacesList)) ;
		
		boolean result = false ;
l0:		for(IJavaTypeDeclaration iDecl : allInterfaces ){
			QualifiedName qName = iDecl.getQualifiedName();
			List<String> values = qName.getValues();
			for( String s : values ){
				if(set.contains(s)){
					result = true ;
					break l0;
				}
			}
		}		
		return result ;
	}
	
	private boolean hasSubclassWithOneOfInterfaces(IJavaTypeDeclaration decl, String[] annotationsList)
	{
		if(implementsOneOfInterfaces(decl, annotationsList))
			return true ;
		
		List<IJavaTypeDeclaration> subclasses = decl.getSubclasses();
		if(subclasses==null)
			return false ;
		
		for(int i = 0 ; i < subclasses.size() ; i++ ){
			IJavaTypeDeclaration d = subclasses.get(i);
			if( implementsOneOfInterfaces(d, annotationsList))
				return true ;
			
			List<IJavaTypeDeclaration> lst = d.getSubclasses();
			if(lst != null)
				subclasses.addAll(lst) ;
		}
		return false ;
	}
	
	public boolean hasSubclassWithOneOfInterfaces( String className, String[] annotationsList){
		
		if(annotationsList == null || annotationsList.length == 0 || className == null)
			return false ;
		
		IClassOutline classOutline = getOutlineContainer().getClassOutline(className);
		if(classOutline == null)
			return false;
		
		IJavaTypeDeclaration decl = classOutline.getTypeDeclaration();		
		return hasSubclassWithOneOfInterfaces( decl, annotationsList );
	}
	
	public static class Property {
		
		private String name;
		private IJavaFieldDeclaration field;
		private IJavaMethodDeclaration getter;
		private IJavaMethodDeclaration setter;
		
		public IJavaFieldDeclaration getField() {
			return field;
		}
		public void setField(IJavaFieldDeclaration field) {
			this.field = field;
		}
		public IJavaMethodDeclaration getGetter() {
			return getter;
		}
		public void setGetter(IJavaMethodDeclaration getter) {
			this.getter = getter;
		}
		public IJavaMethodDeclaration getSetter() {
			return setter;
		}
		public void setSetter(IJavaMethodDeclaration setter) {
			this.setter = setter;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		
		
		public void initPropertyName() {
			if (field != null) {
				name = field.getSimpleName();
			} else {
				if (getter != null) {
					String getterName = getter.getSimpleName();
					if (getterName.startsWith("get")) {
						getterName = getterName.replace("get", "");
						getterName = getterName.substring(0, 0).toLowerCase() + getterName.substring(1);
					} else if (getterName.startsWith("is")) {
						getterName = getterName.replace("is", "");
						getterName = getterName.substring(0, 0).toLowerCase() + getterName.substring(1);
					}
					name = getterName;
				} else {
					if (setter != null) {
						if (setter != null) {
							String setterName = setter.getSimpleName();
							if (setterName.startsWith("set")) {
								setterName = setterName.replace("set", "");
								setterName = setterName.substring(0, 0).toLowerCase() + setterName.substring(1);
							} 
							name = setterName;
						}
					}
				}
			}
		}
		
		
		
	}
	
	private String getPropertyNameByMethod(IJavaMethodDeclaration method) {
		if (method.getSimpleName().startsWith("get") 
				|| method.getSimpleName().startsWith("set")
				|| method.getSimpleName().startsWith("is")) {
			String result = "";
			if (method.getSimpleName().startsWith("get")) {
				result = method.getSimpleName().replace("get", "");
			}
			if (method.getSimpleName().startsWith("set")) {
				result = method.getSimpleName().replace("set", "");
			}
			if (method.getSimpleName().startsWith("is")) {
				result = method.getSimpleName().replace("is", "");
			}
			result = result.substring(0, 0).toLowerCase() + result.substring(1);
			
			return result;
		}
		return null;
	}
	
	public HashMap<String, Property> getOutlineProperties(IClassOutline outline) {
		return getOutlineProperties(outline, true);
	}
	
	public HashMap<String, Property> getOutlineProperties(IClassOutline outline, boolean byFields) {
		HashMap<String, Property> propertiesMap = new HashMap<String, EntityCompositePrimaryKeyFieldsRule.Property>();
		
		
		List<IJavaFieldDeclaration> fields = outline.getFields();
		List<IJavaMethodDeclaration> methods = outline.getMethods();
		
		if (byFields) {
l0:			for (IJavaFieldDeclaration field : fields) {
				if (field.isStatic()) {
					continue;
				}
				
				if( field.isTransient() )
					continue ;
				
				List<IJavaAnnotation> annotations = field.getAnnotations();
				if(annotations != null)
					for( IJavaAnnotation ann : annotations ){
						if( ann.getName().equals("Transient") ||
							ann.getName().equals("javax.persistence.Transient") )
							continue l0;
					}
				
				Property property = new Property();
				property.field = field;
				property.initPropertyName();
				propertiesMap.put(property.getName(), property);
			}
		}
		
		for (IJavaMethodDeclaration method : methods) {
			
			if(method.isStatic())
				continue ;
			
			String propertyName = getPropertyNameByMethod(method);
			if (propertyName != null) {
				Property p = new Property();
				if (propertiesMap.containsKey(propertyName)) {
					p = propertiesMap.get(propertyName);
				} else {
					propertiesMap.put(propertyName, p);
				}
				if (method.getSimpleName().startsWith("get") || method.getSimpleName().startsWith("set")) {
					p.getter = method;
				} else {
					p.setter = method;
				}
			}
		}
		
		for (Property property : propertiesMap.values()) {
			property.initPropertyName();
		}
		
		return propertiesMap;
	}
	
	
	protected boolean isKnownApplicationExceptionSubclass( IJavaType type){
		
		IJavaTypeDeclaration decl = type.getDeclaration() ;
		if(decl==null){
			QualifiedName qName = type.getQualifiedName();
			return isKnownApplicationException(qName);				
		}
		
		ArrayList<IJavaTypeDeclaration> lst = new ArrayList<IJavaTypeDeclaration>() ;
		lst.add(decl) ;		
		List<IJavaTypeDeclaration> superClasses = decl.getAllSuperClasses();
		lst.addAll(superClasses) ;
		
		for(IJavaTypeDeclaration td : lst){
			
			QualifiedName qName = td.getQualifiedName();
			if(qName==null)
				continue ;
			
			if(isKnownApplicationException(qName))
				return true ;
		}		
		
		return false;
	}

	public boolean isKnownApplicationException(QualifiedName qName) {
		if(qName != null){
			for(String s : qName.getValues()){
				if( ExceptionsCompleteList.isKnownApplicationException(s) )
					return true ;
				
				if( ExceptionsCompleteList.isKnownRemoteException(s) )
					return false ;
				
				if(ExceptionsCompleteList.isKnownRuntimeException(s))
					return false ;
			}				
		}
		return false ;
	}
	
	protected boolean isKnownRemoteExceptionSubclass( IJavaType type){
		
		IJavaTypeDeclaration decl = type.getDeclaration() ;
		if(decl==null){
			QualifiedName qName = type.getQualifiedName();
			return isKnownRemoteException(qName);				
		}
		
		ArrayList<IJavaTypeDeclaration> lst = new ArrayList<IJavaTypeDeclaration>() ;
		lst.add(decl) ;		
		List<IJavaTypeDeclaration> superClasses = decl.getAllSuperClasses();
		lst.addAll(superClasses) ;
		
		for(IJavaTypeDeclaration td : lst){
			
			QualifiedName qName = td.getQualifiedName();
			if(qName==null)
				continue ;
			
			if(isKnownRemoteException(qName))
				return true ;
		}		
		
		return false;
	}
	
	public boolean isKnownRemoteException(QualifiedName qName) {
		if(qName != null){
			for(String s : qName.getValues()){
				if( ExceptionsCompleteList.isKnownRemoteException(s) )
					return true ;
			}				
		}
		return false ;
	}

	protected IClassOutline getCurrentOutline() {
		return currentOutline;
	}
	
}
