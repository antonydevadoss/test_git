package net.junos.space.pmd.rules.rest;



import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.SchemaOutputResolver;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import net.juniper.common.client.nativejson.JSONArray;
import net.juniper.common.client.nativejson.JSONObject;
import net.juniper.jmp.annotation.rbac.CRUDEnum;
import net.juniper.jmp.annotation.rbac.RBAC;
import net.juniper.jmp.interceptors.hateoas.CDATA;
import net.juniper.jmp.interceptors.hateoas.HATEOAS;
import net.juniper.jmp.parsers.annotations.Filterable;
import net.juniper.jmp.parsers.annotations.Sortable;
import net.juniper.patch.core.PATCH;
import net.junos.space.pmd.ClassContext;
import net.junos.space.pmd.MethodInfo;
import net.junos.space.pmd.PMDContext;
import net.junos.space.pmd.PmdData;
import net.junos.space.pmd.utils.CustomUtils;
import net.sourceforge.pmd.RuleContext;
import net.sourceforge.pmd.lang.java.ast.ASTAnnotation;
import net.sourceforge.pmd.lang.java.ast.ASTClassOrInterfaceBodyDeclaration;
import net.sourceforge.pmd.lang.java.ast.ASTClassOrInterfaceDeclaration;
import net.sourceforge.pmd.lang.java.ast.ASTClassOrInterfaceType;
import net.sourceforge.pmd.lang.java.ast.ASTCompilationUnit;
import net.sourceforge.pmd.lang.java.ast.ASTFieldDeclaration;
import net.sourceforge.pmd.lang.java.ast.ASTFormalParameters;
import net.sourceforge.pmd.lang.java.ast.ASTMethodDeclaration;
import net.sourceforge.pmd.lang.java.ast.ASTMethodDeclarator;
import net.sourceforge.pmd.lang.java.ast.ASTName;
import net.sourceforge.pmd.lang.java.ast.ASTPrimitiveType;
import net.sourceforge.pmd.lang.java.ast.ASTType;
import net.sourceforge.pmd.lang.java.ast.ASTVariableDeclaratorId;
import net.sourceforge.pmd.lang.java.ast.Comment;
import net.sourceforge.pmd.lang.java.rule.AbstractJavaRule;

import org.apache.log4j.Logger;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;



public class GenerateReportV2 extends AbstractJavaRule {

	public static final String CLASS_NAME = "CLASS_NAME";
	public static final String PACKAGE_NAME = "PACKAGE_NAME";
	public static final String PATH_VALUE = "PATH_VALUE";
	public static final String PMD_CTX = "PMD_CTX";
	public static final String CONTEXT_ROOT = "context-root";
	public static final String PATH = "Path";
	public static final String CONSUMES = "Consumes";
	public static final String PRODUCES = "Produces";
	public static final String RBAC = "RBAC";
	private static final String XS_ANNOTATION = "xs:annotation";
	private static final String XS_APPINFO = "xs:appinfo";
	private static final String SPACE_APPINFO = "space-appinfo";
	public static ThreadLocal<PMDContext> pmdCtxTL = new ThreadLocal<PMDContext>();
	public static ThreadLocal<ClassContext> classCtxTL = new ThreadLocal<ClassContext>();
	public static final String[] nonRecursibleClasses = {"java.lang.Boolean",
		"java.lang.Character",
		"java.lang.Byte",
		"java.lang.Short",
		"java.lang.Integer",
		"java.lang.Long",
		"java.lang.Float",
		"java.lang.Double",
		"java.lang.String",
		"java.sql.Timestamp",
		"java.util.Date",
		"net.juniper.jmp.interceptors.hateoas.HATEOASMethodObject",
		"net.juniper.jmp.cmp.deviceManager.DeviceDO"};
	
	
	/** 
	 * Rule start
	 * Initailize global params
	 */
	public void start(RuleContext paramRuleContext) {
		try {
			paramRuleContext.removeAttribute(PMD_CTX);
			paramRuleContext.setAttribute(PMD_CTX, PMDContext.getInstance());
		} catch(Exception e) {
			System.out.println("Error while initializing Report");
			e.printStackTrace();
		}
	}
	/**
	 * File Start
	 * Initialize params for a source file
	 */
    public void apply(List<? extends net.sourceforge.pmd.lang.ast.Node> paramList, RuleContext paramRuleContext) {
		try {
	    	pmdCtxTL.set((PMDContext) paramRuleContext.getAttribute(PMD_CTX)); 
	    	classCtxTL.remove();
	    	classCtxTL.set(new ClassContext());
	    	
	    	Map<String, String> contextRootMap = pmdCtxTL.get().getContextRootMap();
	    	
	    	String projectRoot = getProjectRootPath(paramRuleContext);
	    	classCtxTL.get().setProjectRoot(projectRoot);
	    	if(!contextRootMap.containsKey(projectRoot)) {
	    		contextRootMap.put(projectRoot, getContextRoot(projectRoot));
	    	}
	    	classCtxTL.get().setContextRoot(contextRootMap.get(projectRoot));
		} catch(Exception e) {
			System.out.println("Error while initializing File context");
			e.printStackTrace();
		}
		super.apply(paramList, paramRuleContext);
    }
    /**
     * Rule End
     * Finalize and print report
     */
	public void end(RuleContext paramRuleContext) {
		try {
			processClassesConcurrence(paramRuleContext);
			List<RestInfo> infoList = (List<RestInfo>) ((PMDContext)paramRuleContext.getAttribute(PMD_CTX)).getReportList();
			handleCSV(infoList);
			handleJSON(infoList);
		} catch(Exception e) {
			System.out.println("Error while finalizing Report");
			e.printStackTrace();
		}
		
	}
	
	public void processClassesConcurrence(final RuleContext paramRuleContext) {
		try {
			int noOfThreads = 3;
			
			PMDContext ctx = (PMDContext) paramRuleContext.getAttribute(PMD_CTX);
			Map<String, PmdData> classMap = ctx.getClassMap();
			if(classMap != null && !classMap.isEmpty()) {
				Object[] classArray = classMap.keySet().toArray();
				
				int threadLimit = 1;
				int arraySize = classArray.length;
				if(arraySize > noOfThreads) {
					threadLimit = arraySize/noOfThreads;
				}
				
				int index = 0;
				int i = 0;
				List<Thread> threads = new ArrayList<Thread>(noOfThreads);
				while(true) {
					++i;
					int endIndex = index+threadLimit;
					if(i == noOfThreads) {
						endIndex = arraySize + 1;
					}
					final Object[] classesPart = Arrays.copyOfRange(classArray, index, endIndex);
					index = endIndex;
					Runnable runnable = new Runnable() {
						public void run() {
							processClasses(classesPart, paramRuleContext);
						}
					};
					
					Thread t = new Thread(runnable,"Thread-"+i);
					t.start();
					threads.add(t);
					
					if(i == noOfThreads) {
						break;
					}
				}
				
				for (Thread thread : threads) {
					thread.join();
				}
			}
		} catch(Exception e) {
			System.out.println("Error in ::generateReport");
			e.printStackTrace();
		}
	}
	public static void processClasses(Object[] classesPart, RuleContext paramRuleContext) {
		try {
			pmdCtxTL.set((PMDContext) paramRuleContext.getAttribute(PMD_CTX));
			for (Object className : classesPart) {
				processClass((String) className, paramRuleContext);
			}
			
		} catch(Exception e) {
			System.out.println("Error in ::processClasses");
			e.printStackTrace();
		}
	}
	private static void processClass(String className, RuleContext paramRuleContext) {
		PmdData data = null;
		try {
			data = pmdCtxTL.get().getClassMap().get(className);
			if(data != null) {
				classCtxTL.remove();
				classCtxTL.set(data.getClassCtx());
				GenerateReportV2 obj = new GenerateReportV2();
				obj.crawlForRestInfo(Class.forName(classCtxTL.get().getPackageName() + "." + classCtxTL.get().getClassName()), data);
			}
		} catch(Throwable e) {
			System.out.println("Error in ::processClass "+data.getSourceCodeFilename());
			e.printStackTrace();
		}
	}
	private void handleCSV(List<RestInfo> infoList) {
		if(infoList != null) {
			StringBuffer br =new StringBuffer();
			br.append("HTTP-Method").append(",")
			.append("Project-Root").append(",")
			.append("API").append(",")
			.append("Consumes").append(",")
			.append("Produces").append(",")
			.append("Consumes-XSD-Valid").append(",")
			.append("Produces-XSD-Valid").append(",")
			.append("Deprecated").append(",")
			.append("Excluded-From-Discovery").append(",")
			.append("Sortable-Elements").append(",")
			.append("Filterable-Elements").append(",")
			.append("Pagination-Support").append(",")
			.append("Schedule-Support").append(",")
			.append("Version").append(",")
			.append("RBAC").append(",")
			.append("Java-Class").append(",")
			.append("Java-Method").append(",")
			.append("Source-Path");
			br.append("\n");
			for (RestInfo restInfo : infoList) {
				if(restInfo != null) {
					if (!(restInfo.isDeprecated() || restInfo.isExcludeFromDiscovery())) {
						br.append(csvEscape(restInfo.getHTTPMethodName())).append(",");
						br.append(csvEscape(restInfo.getServiceName())).append(",");
						br.append(csvEscape(restInfo.getApiURL())).append(",");
						br.append(csvEscape(join(restInfo.getConsumeTypes()))).append(",");
						br.append(csvEscape(join(restInfo.getProduceTypes()))).append(",");
						br.append(csvEscape(String.valueOf(restInfo.isHasValidConsumeXSD()))).append(",");
						br.append(csvEscape(String.valueOf(restInfo.isHasValidProduceXSD()))).append(",");
						br.append(csvEscape(String.valueOf(restInfo.isDeprecated()))).append(",");
						br.append(csvEscape(String.valueOf(restInfo.isExcludeFromDiscovery()))).append(",");
						br.append(csvEscape(join(restInfo.getSortableFields()))).append(",");
						br.append(csvEscape(join(restInfo.getFilterableFields()))).append(",");
						br.append(csvEscape(String.valueOf(restInfo.isPaginated()))).append(",");
						br.append(csvEscape(String.valueOf(restInfo.isSchedulable()))).append(",");
						br.append(csvEscape(restInfo.getVersion() != null ? restInfo.getVersion().toString() : null)).append(",");
						br.append(csvEscape(restInfo.getRbacInfo() != null ? restInfo.getRbacInfo().toString() : null)).append(",");
						br.append(csvEscape(restInfo.getJavaClassName())).append(",");
						br.append(csvEscape(restInfo.getJavaMethodName())).append(",");
						br.append(csvEscape(restInfo.getSourcePath()));
						br.append("\n");
					}
				}
			}
			br.deleteCharAt(br.length()-1);
			
			System.out.println(br.toString());
		}
	}
	
	
	private void handleJSON(List<RestInfo> infoList) {
		try {
			if(infoList != null) {
				JSONArray arr = new JSONArray();
				for (RestInfo restInfo : infoList) {
					if(restInfo != null) {
						JSONObject obj = new JSONObject();
						obj.put("http-method", restInfo.getHTTPMethodName());
						obj.put("api", restInfo.getApiURL());
						obj.put("consumes", toJSONArray(restInfo.getConsumeTypes()));
						obj.put("produces", toJSONArray(restInfo.getProduceTypes()));
						obj.put("version", restInfo.getVersion());
						obj.put("rbac", restInfo.getRbacInfo() != null ? restInfo.getRbacInfo().toJSONObject() : null);
						
						obj.put("java-class", restInfo.getJavaClassName());
						obj.put("java-method", restInfo.getJavaMethodName());
						obj.put("source-path", restInfo.getSourcePath());
						
						arr.put(obj);
					}
				}
				
				System.out.println(arr.toString());
			}
		} catch(Exception e) {
			System.out.println("Error while handleJSON");
			e.printStackTrace();
		}

	}
	
	private JSONArray toJSONArray(List<String> list) {
		JSONArray arr = new JSONArray();
		if(list != null) {
			for (String item : list) {
				arr.put(item);
			}
		}
		return arr;
	}
	
	private String join(List<String> stringList) {
		String res = null;
		if(stringList != null && !stringList.isEmpty()) {
			StringBuffer br = new StringBuffer();
			for (String string : stringList) {
				br.append(string).append(",");
			}
			br.deleteCharAt(br.length()-1);
			res=br.toString();
		}
		return res;
	}
	private String csvEscape(String input) {
		String res = "\"\"";
		if(input != null && !input.isEmpty()) {
			StringBuffer br  = new StringBuffer();
			br.append("\"").append(input.replaceAll("\"", "\"\"")).append("\"");
			res = br.toString();
		}
		return res;
	}

	private static Logger log = Logger.getLogger(GenerateReportRule.class);

	public Object visit(ASTCompilationUnit compilationUnit, Object data) {
		return super.visit(compilationUnit, data);
	}
	
	public Object visit(ASTClassOrInterfaceDeclaration classNode, Object data) {
		RuleContext ctx = (RuleContext) data;
		try {
			String className = getClassName(classNode);
			String packageName = CustomUtils.getPackageName(classNode.getFirstParentOfType(ASTCompilationUnit.class));
			
			ClassContext newCtx = classCtxTL.get().clone();
			
			classCtxTL.remove();
			classCtxTL.set(newCtx);
			
			classCtxTL.get().setClassName(className);
			classCtxTL.get().setPackageName(packageName);
			
			ASTCompilationUnit unit = classNode.getFirstParentOfType(ASTCompilationUnit.class);
			PmdData pmdData = new PmdData();
			pmdData.setAstClassOrInterfaceDeclaration(classNode);
			pmdData.setRuleCtx(ctx);
			pmdData.setClassCtx(classCtxTL.get());
			pmdData.setSourceCodeFilename(ctx.getSourceCodeFilename());
			pmdCtxTL.get().putInClassMap(packageName+"."+className, pmdData);
//			crawlForRestInfo(Class.forName(packageName+"."+className), data);
			
			
		} catch(Throwable e) {
			System.out.println("Error while initializing class context - "+ctx.getSourceCodeFilename());
			e.printStackTrace();
		}
		return super.visit(classNode, data);
		
	}
	
	private String getClassName(ASTClassOrInterfaceDeclaration classNode) {
		String className = "";
		if(classNode != null) {
			ASTClassOrInterfaceDeclaration parentClass = classNode.getFirstParentOfType(ASTClassOrInterfaceDeclaration.class);
			if(parentClass != null) {
				className = getClassName(parentClass)+ "$" + classNode.getImage();
			} else {
				className = classNode.getImage();
			}
		}
		return className;
	}
	public Object visit(ASTClassOrInterfaceBodyDeclaration classBodyNode, Object data) {
		if(classBodyNode.getFirstChildOfType(ASTClassOrInterfaceDeclaration.class) != null) {
			return super.visit(classBodyNode, data);
		} else {
			return data;
		}
	}
	
	private void crawlForRestInfo(Class<?> classObj, PmdData data) {
		try {
			classCtxTL.get().setPathAnnotationValue(getPathValue(classObj));
			classCtxTL.get().setServiceName(getServiceName(data.getAstClassOrInterfaceDeclaration()));
			
//			generateXSD(Class.forName("net.juniper.jmp.cmp.systemService.security.rest.v3.User"));
			
			boolean hasDeprecated = classObj.getAnnotation(Deprecated.class) != null;
			boolean hasExcludeFromDiscovery = hasExcludeFromDiscoveryAnnotation(classObj);
			
			Method[] methods = classObj.getDeclaredMethods();
			if(methods != null) {
				for (Method method : methods) {
					String restHTTPMethod = getRestHTTPMethod(method);
					//Rest Method
					if(restHTTPMethod != null) {
						RestInfo info = new RestInfo();
						String url = getRestUrl(method);
						info.setApiURL(url.replaceAll("/+", "/"));
						info.setHTTPMethodName(restHTTPMethod);
						info.setServiceName(classCtxTL.get().getServiceName());
						info.setConsumeTypes(getConsumesValueList(method));
						info.setProduceTypes(getProducesValueList(method));
						info.setRbacInfo(getRbacInfo(method));
						info.setVersion(getVersionFromMimeTypes(info));
						
						validateXSD(method, info);
						
						info.setDeprecated(hasDeprecated || hasDeprecatedAnnotation(method));
						info.setExcludeFromDiscovery(hasExcludeFromDiscovery || hasExcludeFromDiscoveryAnnotation(method));
						
						populateFilterableSortableFields(info, method);
						
						populatePaginationSchedulableInfo(info, method);
						
						info.setJavaClassName(classObj.getName());
						info.setJavaMethodName(method.toGenericString());
						info.setSourcePath(data.getSourceCodeFilename());
						
						pmdCtxTL.get().getReportList().add(info);
					}
				}
			}

			
		} catch(Exception e) {
			System.out.println("Error while processing class object");
			e.printStackTrace();
		}
		
	}
	
	private String getServiceName(ASTClassOrInterfaceDeclaration astClassOrInterfaceDeclaration) {
		String serviceName = null;
		try {
			if(astClassOrInterfaceDeclaration != null) {
				ASTCompilationUnit complUnit = astClassOrInterfaceDeclaration.getFirstParentOfType(ASTCompilationUnit.class);
				List<Comment> comments = complUnit.getComments();
				if(comments != null && !comments.isEmpty()) {
					for (Comment comment : comments) {
						String commentText = comment.getImage();
						if(commentText != null) {
							int serviceTextIndex = commentText.indexOf("@ServiceName");
							if(serviceTextIndex != -1) {
								int startIndex = serviceTextIndex+"@ServiceName".length()+1;
								int endIndex = commentText.indexOf("\n", startIndex);
								if(endIndex == -1) {
									endIndex = commentText.length();
								}
								serviceName = commentText.substring(startIndex, endIndex);
								break;
							}
						}
					}
				}
			}
		} catch(Exception e) {
			System.out.println("Error in getServiceName");
			e.printStackTrace();
		}
		return serviceName;
	}
	private void populatePaginationSchedulableInfo(RestInfo info, Method method) {
		try {
			if(method != null && method.getParameterTypes().length > 0) {
				Map<String, Object> generatedAnnotInfo = getGeneratedAnnotationInfo(method);
				if(generatedAnnotInfo != null && generatedAnnotInfo.containsKey("signature")) {
					String signature = (String) generatedAnnotInfo.get("signature");
					if(!signature.isEmpty() && !signature.equals("empty")) {
						int index = signature.indexOf(")");
						String params = signature.substring(1, index);
						String result = signature.substring(index+1, signature.length());
						
						if(params.contains("QPagingContext;") && result.contains("QPagingResult")) {
							info.setPaginated(true);
						}
						if(params.contains("QScheduleContext;") && result.contains("QJobInfoTO;")) {
							info.setSchedulable(true);
						}
						
					}
				}
			}
		} catch(Exception e) {
			System.out.println("Error in populatePaginationSchedulableInfo");
			e.printStackTrace();
		}
	}
	private void populateFilterableSortableFields(RestInfo info, Method method) {
		try {
			if(method != null && method.getReturnType() != void.class) {
				Class type = method.getReturnType();
				Class collType = getCollTypeIfCollectionWrapperModel(method.getReturnType());
				if(collType != null) {
					type = collType;
				}
				Field[] fields = type.getDeclaredFields();
				List<String> filterableFields = new ArrayList<String>();
				List<String> sortableFields = new ArrayList<String>();;
				for (Field field : fields) {
					MethodInfo methodInfo = extractMethodInfo(field, false);
					if(methodInfo != null){
						if(methodInfo.isFilterable()) {
							filterableFields.add(methodInfo.getName());
						}
						if(methodInfo.isSortable()) {
							sortableFields.add(methodInfo.getName());
						}
					}
				}
				if(!filterableFields.isEmpty()) {
					info.setFilterableFields(filterableFields);
				}
				if(!sortableFields.isEmpty()) {
					info.setSortableFields(sortableFields);
				}
			}
		} catch(Exception e) {
			System.out.println("Error in updateFilterableSortableFields");
			e.printStackTrace();
		}
	}
	
	private Class getCollTypeIfCollectionWrapperModel(Class classObj) {
		Class collType = null;
		if(classObj != null) {
			Field[] fieldArr = classObj.getDeclaredFields();
			
			int xmlElementCount = 0;
			for (Field field : fieldArr) {
				Method getterMethod = getGetterMethod(field);
				XmlElement xmlElement = getFieldOrMethodAnnotn(field, getterMethod, XmlElement.class);
				if(xmlElement != null && !field.getType().getName().equals("net.juniper.jmp.interceptors.hateoas.HATEOASMethodObject")) { 
					xmlElementCount++;
					if(xmlElementCount == 1) {
						collType = getSubTypeIfCollection(field);
						if(collType == null) {
							return null;
						}
					} else {
						return null;
					}
				}
			}
		}
		return collType;
	}
	
	private boolean hasExcludeFromDiscoveryAnnotation(Method method) {
		boolean hasExclude = false;
		if(method != null)
		{
			Annotation[] annots = method.getAnnotations();
			hasExclude = getAnnotationMatchingName(annots, "net.juniper.jmp.cmp.info.ExcludeFromDiscovery") != null;
		}
		return hasExclude;
	}
	
	private boolean hasExcludeFromDiscoveryAnnotation(Class classObj) {
		boolean hasExclude = false;
		if(classObj != null)
		{
			Annotation[] annots = classObj.getAnnotations();
			hasExclude = getAnnotationMatchingName(annots, "net.juniper.jmp.cmp.info.ExcludeFromDiscovery") != null;
		}
		return hasExclude;
	}
	
	private Annotation getAnnotationMatchingName(Annotation[] annots, String annotName) {
		Annotation annot = null;
		if(annots != null && annotName != null) {
			for (Annotation annotation : annots) {
				if(annotation.annotationType().getName().equals(annotName)) {
					annot = annotation;
					break;
				}
			}
		}
		return annot;
	}
	
	private boolean hasDeprecatedAnnotation(Method method) {
		boolean hasDeprecated = false;
		if(method != null)
		{
			Deprecated annot = method.getAnnotation(Deprecated.class);
			if(annot != null) {
				hasDeprecated = true;
			}
		}
		return hasDeprecated;
	}
	
	private void validateXSD(Method method, RestInfo info) {
		try {
			boolean produceValid = true;
			boolean consumeValid = true;
			String projectRoot = classCtxTL.get().getProjectRoot();
			if(method.getReturnType() != void.class && !method.getReturnType().isInterface()) {
				String mimeType = null;
				if(info.getProduceTypes() != null && !info.getProduceTypes().isEmpty()) {
					mimeType = info.getProduceTypes().get(0);
				}
				
				boolean validate = true;
				if(mimeType != null && mimeType.startsWith("application/vnd.net.juniper.space.job-management.task+") &&
						method.getReturnType().getName().equals("net.juniper.jmp.cmp.async.Task")) {
					if(!projectRoot.contains("CMP/jobManager-webSvc")) {
						validate = false;
					}
				}
				
				if(validate && !(info.isDeprecated() || info.isExcludeFromDiscovery()))
				produceValid = isXSDValid(method.getReturnType(),mimeType,projectRoot);
			}
			
			
			Class[] methodParams = method.getParameterTypes();
			Annotation[][] paramAnnotations = method.getParameterAnnotations();
			Class acceptClass = null; 
			for (int i=0;i<methodParams.length; i++) {
				if(paramAnnotations[i].length == 0){
					acceptClass = methodParams[i];
					break;
				}
			}
			if(acceptClass != null && !(info.isDeprecated() || info.isExcludeFromDiscovery())) {
				String mimeType = null;
				if(info.getConsumeTypes() != null && !info.getConsumeTypes().isEmpty()) {
					mimeType = info.getConsumeTypes().get(0);
				}
				
				consumeValid = isXSDValid(acceptClass,mimeType,projectRoot);
			}
			
			info.setHasValidProduceXSD(produceValid);
			info.setHasValidConsumeXSD(consumeValid);
		} catch(Exception e) {
			System.out.println("Error while validateXSD");
			e.printStackTrace();
		}
	}
	
	private boolean isXSDValid(Class classObj, String mimeType, String projectRoot) {
		boolean isValid = false;
		try {
			Document newXSDDoc = generateXSD(classObj);
			String generatedFileName = classObj.getName();
			if(mimeType != null && !mimeType.trim().isEmpty()) {
				String xsdFileName = getXSDFileNameFromMimeType(mimeType);
				if(xsdFileName != null) {
					generatedFileName = xsdFileName+"-"+generatedFileName;
					
					File xsdFile = new File(projectRoot+"web/WEB-INF/"+xsdFileName); 
					if(xsdFile.exists() && xsdFile.length() > 0) {
						Document existingXSDDoc = generateDoc(xsdFile);
						isValid = compareXSDDoc(existingXSDDoc, newXSDDoc);
					} else {
						return false;
					}
				} else {
					return false;
				}
			} else {
				return false;
			}

			
			if(!isValid) {
				DOMSource domSource = new DOMSource(newXSDDoc);
				StringWriter writer = new StringWriter();
				StreamResult result = new StreamResult(writer);
				TransformerFactory tf = TransformerFactory.newInstance();
				Transformer transformer = tf.newTransformer();
				transformer.setOutputProperty(OutputKeys.METHOD, "xml");
				transformer.setOutputProperty(OutputKeys.INDENT, "yes");
				transformer.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC, "yes");
				transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
				transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "3");
				transformer.transform(domSource, result);
				
				File newFile = new File("/home/antonydevadoss/Desktop/PMD-test/Generated/"+generatedFileName+ ".Generated.xsd");
				FileOutputStream ff = new FileOutputStream(newFile);
				ff.write(writer.toString().getBytes());
				ff.close();
			}
			
		} catch(Exception e) {
			System.out.println("Error while isXSDValid");
			e.printStackTrace();
		}
		return isValid;
	}
	
	
	private Document generateDoc(File xsdFile) {
		FileInputStream fis = null;
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setNamespaceAware(true);
			DocumentBuilder db = dbf.newDocumentBuilder();
			fis = new FileInputStream(xsdFile);
			return db.parse(fis);
		} catch(Exception e) {
			System.out.println("Error while generateDoc for file : "+xsdFile+e.getMessage());
		} finally {
			if(fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}
	private boolean compareXSDDoc(Document existingXSDDoc, Document newXSDDoc) {
		try {
			if(existingXSDDoc != null && existingXSDDoc.getDocumentElement() != null &&
					newXSDDoc != null && newXSDDoc.getDocumentElement() != null) {
				return matchXSDNode(existingXSDDoc.getDocumentElement(), newXSDDoc.getDocumentElement());
			}
		} catch(Exception e) {
			System.out.println("Error while compareDoc");
			e.printStackTrace();
		}
		return false;
	}
	private boolean matchXSDNode(Node lhsNode,Node rhsNode) {
		if(lhsNode != null && rhsNode != null) {
			//check type
			if(lhsNode.getNodeType() != rhsNode.getNodeType()) {
				return false;
			}
			
			//for Text node compare content
			if(lhsNode.getNodeType() == Node.TEXT_NODE) {
				return lhsNode.getTextContent().equals(rhsNode.getTextContent());
				
				//for Element node
			} else if(lhsNode.getNodeType() == Node.ELEMENT_NODE) {
				
				//Compare node name
				if(!lhsNode.getNodeName().equals(rhsNode.getNodeName())) {
					return false;
				}
				
				//Compare Attributes
					//Compare attribute count
				NamedNodeMap lAttrMap = lhsNode.getAttributes();
				NamedNodeMap rAttrMap = rhsNode.getAttributes();
				if(lAttrMap == null || rAttrMap == null || lAttrMap.getLength() != rAttrMap.getLength()) {
					return false;
				}
				
					//compare attribute name and value
				for(int i=0; i<lAttrMap.getLength(); i++) {
					Node lAttr = lAttrMap.item(0);
					Node rAttr = rAttrMap.getNamedItem(lAttr.getNodeName());
					if(rAttr == null) {
						return false;
					}
					String lAttrVal = lAttr.getNodeValue();
					String rAttrVal = rAttr.getNodeValue();
					if(!lAttrVal.equals(rAttrVal)) {
						return false;
					}
				}
				
				//Compare Child 
				
					//cleanup child for unwanted text nodes
				cleanUpChildFillerTextNodes(lhsNode);
				cleanUpChildFillerTextNodes(rhsNode);
					// Compare Child Count
				NodeList lChilds = lhsNode.getChildNodes();
				NodeList rChilds = rhsNode.getChildNodes();
				if(lChilds.getLength() != rChilds.getLength()) {
					return false;
				}
				
				
				//Compare Children
				for(int i=0; i<lChilds.getLength(); i++) {
					Node lChild = lChilds.item(i);
					for(int j=0; j<rChilds.getLength(); j++) {
						Node rChild = rChilds.item(j);
						
						//success
						if(matchXSDNode(lChild, rChild)) {
							break;
							
						  //failure
						} else {
							//if last child
							if(j == rChilds.getLength()-1) {
								return false;
							}
						}
						

					}
				}
				return true;
			}
		}
		return false;
	}
	private void cleanUpChildFillerTextNodes(Node node) {
		if(node != null) {
			NodeList nodes = node.getChildNodes();
			if(nodes.getLength() > 1) {
				List<Node> textNodes = new ArrayList<Node>(nodes.getLength());
				for(int i=0; i< nodes.getLength(); i++) {
					if(nodes.item(i).getNodeType() == Node.TEXT_NODE) {
						textNodes.add(nodes.item(i));
					}
				}
				
				for (Node textNode : textNodes) {
					node.removeChild(textNode);
				}
			}
		}
	}
	private String getXSDFileNameFromMimeType(String mimeType) {
		String fileName = null; 
		if(mimeType != null && !mimeType.isEmpty()) {
			String mimePattern = "^application/(?<main>[^+;]+)(\\+[^;]+)?;version=(?<version>\\d+)";
			Matcher matcher = Pattern.compile(mimePattern).matcher(mimeType);
			if(matcher.find()) {
				String mainPart = matcher.group("main");
				String versionPart = matcher.group("version");
				if(mainPart != null && versionPart != null) {
					fileName = mainPart+"_v"+versionPart+".xsd";
				}
			}
		}
		return fileName;
	}
	private Document generateXSD(Class classObj) {
		Document doc = null;
		try {
			JAXBContext jc = JAXBContext.newInstance(classObj);
			final ByteArrayOutputStream bos = new ByteArrayOutputStream();
			jc.generateSchema(new SchemaOutputResolver() {
	            @Override
	            public Result createOutput(String namespaceUri, String suggestedFileName) throws IOException {
	                StreamResult result = new StreamResult(bos);
	                result.setSystemId(suggestedFileName);
	                return result;
	            }
	        });
			
			ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
			
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setNamespaceAware(true);
			DocumentBuilder db = dbf.newDocumentBuilder();
			doc = db.parse(bis);
			XPathFactory xpathFactory = XPathFactory.newInstance();
			
			updateDocument(doc, xpathFactory, classObj, null, new HashMap<String, String>());
			
//			DOMSource domSource = new DOMSource(doc);
//			StringWriter writer = new StringWriter();
//			StreamResult result = new StreamResult(writer);
//			TransformerFactory tf = TransformerFactory.newInstance();
//			Transformer transformer = tf.newTransformer();
//			transformer.transform(domSource, result);
//			transformer.setOutputProperty(OutputKeys.METHOD, "xml");
//			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
//			transformer.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC, "yes");
//			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
//			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "3");
			
//			System.out.println(writer.toString());
			
			
			
		} catch(Exception e) {
			System.out.println("Error while generateXSD for class : "+classObj.getName());
			e.printStackTrace();
		}
		return doc;
	}

	@SuppressWarnings("unchecked")
	private void updateDocument(Document doc, XPathFactory xpathFactory, Class classObj, MethodInfo previousInfo, Map<String, String> visitedInfo) {
		try {		
			visitedInfo.put(classObj.getName(), "");   
			String typeName = extractTypeName(classObj); 
			
			//for root object
			if(previousInfo == null) {
				Class collectionType = getCollTypeIfCollectionWrapperModel(classObj);
				//if collection Wrapper
				if(collectionType != null) {
					patchCollectionForRootElement(doc, xpathFactory, typeName);
				}
			} else { 				
				if(previousInfo.getName().equals("ptp") && classObj.getName().equals("net.juniper.jmp.cems.inventoryManager.service.v1.JSPhysicalTerminationPoint")) {
					System.out.println("Debug");      		
				}
								
				Class xmlRootElement = Class.forName("javax.xml.bind.annotation.XmlRootElement");
				if(xmlRootElement != null && classObj.getAnnotation(xmlRootElement) != null)
				{
					List<Node> result = getNodeListByTypeAndRef(doc, xpathFactory, previousInfo.getFieldType(), previousInfo.getName());
					List<Node> result2 = getRootNodeListByTypeAndName(doc, xpathFactory, typeName, previousInfo.getFieldType(), previousInfo.getName());
					
					if(result != null &&  result.size() > 0 &&  result2 != null &&  result2.size() == 1) { 
						List<Node> nodes = result;
						int length = nodes.size();
						for (int i = 0; i < length; i++) {
							Node node = nodes.get(i);
							Attr nameAttr = doc.createAttribute("name");
							nameAttr.setNodeValue(previousInfo.getName());
							Attr typeAttr = doc.createAttribute("type");
							typeAttr.setNodeValue(typeName);
							((Element)node).setAttributeNode(nameAttr);
							((Element)node).setAttributeNode(typeAttr);
							((Element)node).removeAttribute("ref");
						}
						nodes = result2;
						length = nodes.size();
						for (int i = 0; i < length; i++) {
							Node node = nodes.get(i);	
							Node parentNode = node.getParentNode();
							parentNode.removeChild(node);
						}
					}	
				}				
			}
	
			Field[] f = classObj.getDeclaredFields();
			if(f != null) {
				for (Field field : f) {
					if(field.getName().equals("me")) {
						System.out.println("Debug"); 
					}
								
					MethodInfo info = extractMethodInfo(field, true);
					
					if(info.isCollectionType() && !info.isRequired()) {
						patchMinOccurs(doc, xpathFactory, typeName, info.getName());
					}				
					
					if(info.isPrimitive() && !info.isRequired()) {								
							patchPrimitiveMinOccurs(doc, xpathFactory, typeName, info.getFieldType(),  info.getName());
					}
					
					List<String> tags = new ArrayList<String>();
					if(info.isFilterable()) {
						tags.add("filterable");
					}
					if(info.isSortable()) {
						tags.add("sortable");
					}
					if(info.isHasCDATA()) {
						tags.add("cdata");
					}
					patchByTag(doc, xpathFactory, typeName, info.getFieldType(), info.getName(), tags);
					
					if(info.getGeneratedAnnotDetails() != null) {
						 patchRange(doc, xpathFactory, typeName, info);
						 patchDefaultValue(doc, xpathFactory, typeName, info);
						 if(info.getXmlElement() != null) {
							 patchReadOnly(doc, xpathFactory, typeName, info);
						 }
						 patchHATEOSMethod(doc, xpathFactory, typeName, info);
					}
					patchCollection(doc, xpathFactory, typeName, info, field);
					
					if(field.getName().equals("link")) {
						patchLink(doc, xpathFactory, typeName, info, previousInfo);
					}
					
					Class classType = getTypeIfRecursible(field, visitedInfo);
					if(classType != null) {
						updateDocument(doc, xpathFactory, classType, info, visitedInfo);
					}
					
				}
			}
		} catch(Exception e) {
			System.out.println("Error in updateDocument");
			e.printStackTrace();
		}
	}
	private void patchCollectionForRootElement(Document doc, XPathFactory xpathFactory, String typeName) {
		try {
			XPath xpath = xpathFactory.newXPath();
			xpath.setNamespaceContext(new NamespaceContext() {
				@Override
				public Iterator getPrefixes(String namespaceURI) {
					return null;
				}
				@Override
				public String getPrefix(String namespaceURI) {
					return "xs";
				}
				@Override
				public String getNamespaceURI(String prefix) {
					return "http://www.w3.org/2001/XMLSchema";
				}
			});
			XPathExpression expr = xpath.compile("/xs:schema/xs:element[@type='"+typeName+"']");
			List<Node> nodes = (List<Node>) expr.evaluate(doc, XPathConstants.NODESET);
			if(nodes != null && nodes.size() == 1) {
				Node collection = doc.createElement("collection");
				Node appInfo = getOrCreateSpaceAppInfoNode(doc, nodes.get(0));
				appInfo.appendChild(collection);
			}
		} catch(Exception e) {
			System.out.println("Error in patchCollectionForRootElement");
			e.printStackTrace();
		}
	}
	
	private void patchHATEOSMethod(Document doc, XPathFactory xpathFactory, String typeName, MethodInfo info) {
		try {
			Map<String, Object> generatedAnnotationMap = info.getGeneratedAnnotDetails();
			String isMethod = (String) generatedAnnotationMap.get("isMethod");
			if(isMethod != null && Boolean.parseBoolean(isMethod)) {
				List<Node> nodes = getNodeListByTypeAndName(doc, xpathFactory, typeName, info.getFieldType(), info.getName());
				if(nodes != null && nodes.size() == 1) {
					Node method = doc.createElement("method");
					Node appInfo = getOrCreateSpaceAppInfoNode(doc, nodes.get(0));
					appInfo.appendChild(method);
				}
			}
		} catch(Exception e) {
			System.out.println("Error in patchHATEOSMethod");
			e.printStackTrace();
		}
	}

	private void patchCollection(Document doc, XPathFactory xpathFactory, String typeName, MethodInfo info, Field field) {
		try {
			boolean isCollection = false;
			String ejbType = null;
			if (info.getGeneratedAnnotDetails() != null) {
				Map<String, Object> generatedAnnotationMap = info.getGeneratedAnnotDetails();
				ejbType = (String) generatedAnnotationMap.get("ejbType");
			}
			if (ejbType != null && !ejbType.isEmpty()) {

				int endIndex = ejbType.indexOf("<");
				if (endIndex == -1) {
					endIndex = ejbType.length();
				}
				Class ejbTypeClass = null;
				try {
					ejbTypeClass = Class.forName(ejbType.substring(0, endIndex));
				} catch (Throwable t) {

				}

				if (ejbTypeClass != null && Collection.class.isAssignableFrom(ejbTypeClass)) {
					isCollection = true;
				}
			} else {
				Class collType = getCollTypeIfCollectionWrapperModel(field.getType());
				if (collType != null) {
					isCollection = true;
				}
			}


			if (isCollection) {
				List<Node> nodes = getNodeListByTypeAndName(doc, xpathFactory, typeName, info.getFieldType(), info.getName());
				if (nodes != null && nodes.size() == 1) {
					Node method = doc.createElement("collection");
					Node appInfo = getOrCreateSpaceAppInfoNode(doc, nodes.get(0));
					appInfo.appendChild(method);
				}
			}

		} catch (Exception e) {
			System.out.println("Error in patchRange");
			e.printStackTrace();
		}
	}
	private Class getTypeIfRecursible(Field field, Map<String, String> visitedInfo) {
		Class classObj = null;
		if(field != null) {
			Class actualType = field.getType();
			Class collType = getSubTypeIfCollection(field);
			if(collType != null) {
				actualType = collType;
			}
			
			if(!actualType.isPrimitive() && !actualType.isEnum()) {
				boolean recurse = true;
				for (String noRecClass : nonRecursibleClasses) {
					if(actualType.getName().equals(noRecClass)) {
						recurse = false;
						break;
					}
				}
				if(recurse && !visitedInfo.containsKey(actualType.getName())) {
					classObj = actualType;
				}
			}
		}
		return classObj;
	}
	private void patchRange(Document doc, XPathFactory xpathFactory, String typeName, MethodInfo info) {
		try {
			Map<String, Object> generatedAnnotationMap = info.getGeneratedAnnotDetails();
			String rangeStr = (String) generatedAnnotationMap.get("hasRange");
			String min = "0";
			String max = "0";
			if(rangeStr != null && Boolean.parseBoolean(rangeStr)) {
				String annotMin = (String) generatedAnnotationMap.get("minRange");
				String annotMax = (String) generatedAnnotationMap.get("maxRange");
				if(annotMin != null) {
					min = annotMin;
				}
				if(annotMax != null) {
					max = annotMax;
				}
				
				List<Node> result = getNodeListByTypeAndName(doc, xpathFactory, typeName, info.getFieldType(), info.getName());
				if(result != null && result.size() == 1) {
					Node node = (Node) result.get(0);
					Element field = (Element) node;
					
					Node simpleTypeNode = getOrCreateChildElementNode(doc, node, "xs:simpleType");
					Node restrictionNode = getOrCreateChildElementNode(doc, simpleTypeNode, "xs:restriction");
					
					Element maxInclusive = doc.createElement("xs:maxInclusive");
					maxInclusive.setAttribute("value", max + "");
					Element minInclusive = doc.createElement("xs:minInclusive");
					minInclusive.setAttribute("value", min + "");
					
					String typeVal = field.getAttribute("type");
					if(typeVal != null) {
						field.removeAttribute("type");
						Element restrictionElement = (Element) restrictionNode;
						restrictionElement.setAttribute("base", typeVal);
					}
					restrictionNode.appendChild(maxInclusive);
					restrictionNode.appendChild(minInclusive);

				}
			
			}
			
		} catch(Exception e) {
			System.out.println("Error in patchRange");
			e.printStackTrace();
		}
	}
	
	private void patchDefaultValue(Document doc, XPathFactory xpathFactory, String typeName, MethodInfo info) {
		try {
			Map<String, Object> generatedAnnotationMap = info.getGeneratedAnnotDetails();
			String hasDefaultVal = (String) generatedAnnotationMap.get("hasDefaultValue");
			if(hasDefaultVal != null && Boolean.parseBoolean(hasDefaultVal)) {
				String defaultValue = (String) generatedAnnotationMap.get("defaultValue");
				List<Node> nodes = getNodeListByTypeAndName(doc, xpathFactory, typeName, info.getFieldType(), info.getName());
				if(nodes != null && nodes.size() == 1) {
					Attr attr = doc.createAttribute("default");
					attr.setNodeValue(defaultValue);
					((Element) nodes.get(0)).setAttributeNode(attr);
				}
			}
		} catch(Exception e) {
			System.out.println("Error in patchRange");
			e.printStackTrace();
		}
	}
	private void patchReadOnly(Document doc, XPathFactory xpathFactory, String typeName, MethodInfo info) {
		try {
			Map<String, Object> generatedAnnotationMap = info.getGeneratedAnnotDetails();
			String readOnly = (String) generatedAnnotationMap.get("readOnly");
			if(readOnly != null && Boolean.parseBoolean(readOnly)) {
				List<Node> nodes = getNodeListByTypeAndName(doc, xpathFactory, typeName, info.getFieldType(), info.getName());
				if(nodes != null && nodes.size() == 1) {
					Node stateData = doc.createElement("read-only");
					Node appInfo = getOrCreateSpaceAppInfoNode(doc, nodes.get(0));
					appInfo.appendChild(stateData);
				}
			}
		} catch(Exception e) {
			System.out.println("Error in patchRange");
			e.printStackTrace();
		}
	}
	private void patchLink(Document doc, XPathFactory xpathFactory, String typeName,  MethodInfo info, MethodInfo prevInfo) {
		try {
			String href = null;
			if(prevInfo != null && prevInfo.getHateoas() != null) {
				href = prevInfo.getHateoas().href();
			} else if(info != null && info.getHateoas() != null) {
				href = info.getHateoas().href();
			}
			if(href != null) {
				List<Node> nodes = getNodeListByTypeAndName(doc, xpathFactory, typeName, info.getFieldType(), info.getName());
				if(nodes != null && nodes.size() == 1) {
					Node spaceAppInfoNode = getOrCreateSpaceAppInfoNode(doc, nodes.get(0));
					Node linkNode = doc.createElement("link");
					linkNode.appendChild(doc.createTextNode(href));
					spaceAppInfoNode.appendChild(linkNode);
				}
			}
		} catch(Exception e) {
			System.out.println("Error in patchRange");
			e.printStackTrace();
		}
	}
	
	private List<Node> getNodeListByTypeAndName(Document doc, XPathFactory xpathFactory, String typeName, String type, String name) {
		List<Node> nodes= null;
		try {
			XPath xpath = xpathFactory.newXPath();
			xpath.setNamespaceContext(new NamespaceContext() {
				@Override
				public Iterator getPrefixes(String namespaceURI) {
					return null;
				}
				@Override
				public String getPrefix(String namespaceURI) {
					return "xs";
				}
				@Override
				public String getNamespaceURI(String prefix) {
					return "http://www.w3.org/2001/XMLSchema";
				}
			});
			XPathExpression expr = xpath.compile("//xs:complexType[@name='"+typeName+"']" +  "//"+type+"[@name='"+name+"']");
			nodes = (List<Node>) expr.evaluate(doc, XPathConstants.NODESET);
		} catch(Exception e) {
			System.out.println("Error in getNodeByTypeAndName");
			e.printStackTrace();
		}
		return nodes;
	}
	
	
	private List<Node> getNodeListByTypeAndRef(Document doc, XPathFactory xpathFactory, String type, String refName) {
		List<Node> nodes= null;
		try {
			XPath xpath = xpathFactory.newXPath();
			xpath.setNamespaceContext(new NamespaceContext() {
				@Override
				public Iterator getPrefixes(String namespaceURI) {
					return null;
				}
				@Override
				public String getPrefix(String namespaceURI) {
					return "xs";
				}
				@Override
				public String getNamespaceURI(String prefix) {
					return "http://www.w3.org/2001/XMLSchema";
				}
			});
			XPathExpression expr = xpath.compile("//"+type+"[@ref='"+refName+"']");
			nodes = (List<Node>) expr.evaluate(doc, XPathConstants.NODESET);
		} catch(Exception e) {
			System.out.println("Error in getNodeByTypeAndName");
			e.printStackTrace();
		}
		return nodes;
	}
	
	private List<Node> getRootNodeListByTypeAndName(Document doc, XPathFactory xpathFactory, String typeName, String type, String name) {
		List<Node> nodes= null;
		try {
			XPath xpath = xpathFactory.newXPath();
			xpath.setNamespaceContext(new NamespaceContext() {
				@Override
				public Iterator getPrefixes(String namespaceURI) {
					return null;
				}
				@Override
				public String getPrefix(String namespaceURI) {
					return "xs";
				}
				@Override
				public String getNamespaceURI(String prefix) {
					return "http://www.w3.org/2001/XMLSchema";
				}
			});
			XPathExpression expr = xpath.compile("//"+type+"[@name='"+name+"' and @type='"+typeName+"']"); 
			nodes = (List<Node>) expr.evaluate(doc, XPathConstants.NODESET);
		} catch(Exception e) {
			System.out.println("Error in getNodeByTypeAndName");
			e.printStackTrace();
		}
		return nodes;
	}
	
	private void patchByTag(Document doc, XPathFactory xpathFactory, String typeName, String fieldType, String name, List<String> tags) {
		try {
			if(tags != null && !tags.isEmpty()) {
				List<Node> result = getNodeListByTypeAndName(doc, xpathFactory, typeName, fieldType, name);
				if(result != null && result.size() > 0) {
					Node node = (Node) result.get(0);
					Node spaceAppInfoNode = getOrCreateSpaceAppInfoNode(doc, node);
					
					for (String tag : tags) {
						Node collectionNode = doc.createElement(tag);
						spaceAppInfoNode.appendChild(collectionNode);
					}
				}
			}
		} catch(Exception e) {
			System.out.println("Error in patchCDATA");
			e.printStackTrace();
		}
	}
	private Node getOrCreateSpaceAppInfoNode(Document doc, Node node) {
		Node spaceAppInfoNode = null;
		if(node != null) {
			Node xsAnnot = getOrCreateChildElementNode(doc, node, XS_ANNOTATION);
			Node xsappInfo = getOrCreateChildElementNode(doc, xsAnnot, XS_APPINFO);
			spaceAppInfoNode = getOrCreateChildElementNode(doc, xsappInfo, SPACE_APPINFO);
		}
		return spaceAppInfoNode;
	}
	
	private static final Node getOrCreateChildElementNode(Document doc, Node parent, String nodeName) {
		NodeList childNodes = parent.getChildNodes();
		Node result = null;
		for( int i = 0; i < childNodes.getLength(); i++){
			Node currentNode = childNodes.item(i);
			if( currentNode.getNodeType() == Node.ELEMENT_NODE && currentNode.getNodeName().equals(nodeName)){
				result = currentNode;
				break;
			}
		}
		if (result == null) {
			result = doc.createElement(nodeName);
			parent.insertBefore(result, parent.getFirstChild());
		}
		return result;
	}
	private void patchMinOccurs(Document doc, XPathFactory xpathFactory, String typeName, String name) {
		try {
			XPath xpath = xpathFactory.newXPath();
			xpath.setNamespaceContext(new NamespaceContext() {
				@Override
				public Iterator getPrefixes(String namespaceURI) {
					return null;
				}
				@Override
				public String getPrefix(String namespaceURI) {
					return "xs";
				}
				@Override
				public String getNamespaceURI(String prefix) {
					return "http://www.w3.org/2001/XMLSchema";
				}
			});
			XPathExpression expr = xpath.compile("//xs:complexType[@name='"+typeName+"']" + "/xs:sequence/xs:element[@maxOccurs='unbounded' and @name='"+name+"']");
			List<Node> result = (List<Node>) expr.evaluate(doc, XPathConstants.NODESET);
			if(result != null &&  result.size() > 0) {
				List<Node> nodes = result;
				int length = nodes.size();
				for (int i = 0; i < length; i++) {
					Node node = nodes.get(i);
					Attr attr = doc.createAttribute("minOccurs");
					attr.setNodeValue("0");
					((Element)node).setAttributeNode(attr);
				}
			}
		} catch(Exception e) {
			System.out.println("Error in patchMinOccurs");
			e.printStackTrace();
		}
	}
	
	private void patchPrimitiveMinOccurs(Document doc, XPathFactory xpathFactory, String typeName, String type,
			String name) {
		try {
			List<Node> result = getNodeListByTypeAndName(doc, xpathFactory, typeName, type, name);

			if (result != null && result.size() > 0) {
				List<Node> nodes = result;
				int length = nodes.size();
				for (int i = 0; i < length; i++) {
					Node node = nodes.get(i);
					Attr attr = doc.createAttribute("minOccurs");
					attr.setNodeValue("0");
					((Element) node).setAttributeNode(attr);
				}
			}
		} catch (Exception e) {
			System.out.println("Error in patchPrimitiveMinOccurs");
			e.printStackTrace();
		}
	}
	
	
	private MethodInfo extractMethodInfo(Field field, boolean needGeneratedInfo) {
		MethodInfo info = null;
		if(field != null) {
			info = new MethodInfo();
			try {
				
				Method getterMethod = getGetterMethod(field);
				
				String name = field.getName();
				XmlElement xmlElement = getFieldOrMethodAnnotn(field, getterMethod, XmlElement.class);
				XmlAttribute xmlAttribute = getFieldOrMethodAnnotn(field, getterMethod, XmlAttribute.class);
				if (xmlElement != null) {
					if (!xmlElement.name().equals("##default")) {
						name = xmlElement.name();
					}
					if (xmlElement.required())
						info.setRequired(true);
				}
				if (xmlAttribute != null) {
					if (!xmlAttribute.name().equals("##default")) {
						name = xmlAttribute.name();
					}
					info.setFieldType("xs:attribute");
					if (xmlAttribute.required())
						info.setRequired(true);
				}
				info.setName(name);
				info.setCollectionType(isCollectionType(field));
				info.setPrimitive(field.getType().isPrimitive());
				info.setXmlElement(xmlElement);
				info.setXmlAttribute(xmlAttribute);
				info.setHateoas(field.getAnnotation(HATEOAS.class));
				
				
				if(getterMethod != null) {
					if(getterMethod.getAnnotation(Filterable.class) != null) {
						info.setFilterable(true);
					}
					if(getterMethod.getAnnotation(Sortable.class) != null) {
						info.setSortable(true);
					}
					if(getterMethod.getAnnotation(CDATA.class) != null) {
						info.setHasCDATA(true);
					}
				}
				
				if(needGeneratedInfo) {
					Map<String, Object> generatedAnnotDetails = getGeneratedAnnotationInfo(field);
					info.setGeneratedAnnotDetails(generatedAnnotDetails);
				}
			} catch(Exception e) {
				System.out.println("Error in extractMethodInfo");
				e.printStackTrace();
			}
		}
		return info;
	}
	
	private <T extends Annotation> T getFieldOrMethodAnnotn(Field field, Method getterMethod, Class<T> annotClass) {
		T annot = null;
		if(field != null) {
			annot = field.getAnnotation(annotClass);
		}
		if(annot == null && getterMethod != null) {
			annot = getterMethod.getAnnotation(annotClass);
		}
		return annot;
	}
	private Method getGetterMethod(Field field) {
		Method getterMethod = null;
		if(field != null) {
			StringBuffer br = new StringBuffer(field.getName());
			br.insert(0, "get").setCharAt(3, Character.toUpperCase(br.charAt(3)));
			String methodName = br.toString();
			try {
				getterMethod = field.getDeclaringClass().getMethod(methodName, null);
			} catch (NoSuchMethodException e) {
				
			}
			if(getterMethod == null) {
				br = new StringBuffer(field.getName());
				br.insert(0, "is").setCharAt(2, Character.toUpperCase(br.charAt(2)));
				methodName = br.toString();
				try {
					getterMethod = field.getDeclaringClass().getMethod(methodName, null);
				} catch (NoSuchMethodException e) {
					
				}
			}
		}
		return getterMethod;
	}
	
	private Map<String, Object> getGeneratedAnnotationInfo(Field field) {
		Map<String, Object> generatedMap = null;
		String className = null;
		try {
			if(field != null) {
				className = field.getDeclaringClass().getName();
				PmdData data = pmdCtxTL.get().getClassMap().get(className);
				ASTClassOrInterfaceDeclaration astClassDeclaration = data.getAstClassOrInterfaceDeclaration();
				ASTAnnotation astAnnotation = getFieldAnnotationByName(astClassDeclaration, field.getName(), "Generated");
				if(astAnnotation != null) {
					generatedMap = new HashMap<String, Object>();
					Map<String, Object> annotMap = CustomUtils.getMethodAnnotationMap(astAnnotation);
					if(annotMap != null) {
						String comments = (String) annotMap.get("comments");
						generatedMap = extractInfoFrmGeneratedComments(comments);
					}
				}
			}
		} catch(Exception e) {
			System.out.println("Error while getting Generated Annotation for "+className);
		}
		return generatedMap;
	}
	
	
	private Map<String, Object> getGeneratedAnnotationInfo(Method method) {
		Map<String, Object> generatedMap = null;
		String className = null;
		try {
			if(method != null) {
				className = method.getDeclaringClass().getName();
				PmdData data = pmdCtxTL.get().getClassMap().get(className);
				ASTClassOrInterfaceDeclaration astClassDeclaration = data.getAstClassOrInterfaceDeclaration();
				
				Class[] paraClasses = method.getParameterTypes();
				List<String> parameterTypes = new ArrayList<String>();
				for (Class paramClass : paraClasses) {
					parameterTypes.add(paramClass.getName());
				}
				
				
				ASTAnnotation astAnnotation = getMethodAnnotationByName(astClassDeclaration, method.getName(), parameterTypes, "Generated");
				if(astAnnotation != null) {
					generatedMap = new HashMap<String, Object>();
					Map<String, Object> annotMap = CustomUtils.getMethodAnnotationMap(astAnnotation);
					if(annotMap != null) {
						String comments = (String) annotMap.get("comments");
						generatedMap = extractInfoFrmGeneratedComments(comments);
					}
				}
			}
		} catch(Exception e) {
			System.out.println("Error while getting Generated Annotation for "+className);
			e.printStackTrace();
		}
		return generatedMap;
	}
	
	private Map<String, Object> extractInfoFrmGeneratedComments(String comments) {
		 Map<String, Object>  genMap = new HashMap<String, Object>();
		 if(comments != null && !comments.trim().isEmpty()) {
			 String[] items = comments.trim().split(",");
			 for (String item : items) {
				 String[] kValArr = item.split(":");
				 if(kValArr.length == 2) {
					 String key = decrypt(kValArr[0]);
					 String val = decrypt(kValArr[1]);
					 if(val.startsWith("[") && val.endsWith("]")) {
						 genMap.put(key, val.substring(1, val.length() - 1).split(","));
					 } else {
						 genMap.put(key, val);
					 }
				 }
			}
		 }
		 return genMap;
	}

	private static String decrypt(String arg) {
		String quoteReplacement = "&#44;";
		String res = arg.replaceAll(quoteReplacement, "\\,");
		String quoteReplacement1 = "&#58;";
		res = res.replaceAll(quoteReplacement1, "\\:");
		return res;
	}
	
	private ASTAnnotation getFieldAnnotationByName(ASTClassOrInterfaceDeclaration unit, String fieldName, String annotName) {
		ASTAnnotation annot = null;
		List<ASTFieldDeclaration> fields = unit.findDescendantsOfType(ASTFieldDeclaration.class);
		if(fields != null) {
			for (ASTFieldDeclaration astFieldDeclaration : fields) {
				String varName = astFieldDeclaration.getFirstDescendantOfType(ASTVariableDeclaratorId.class).getImage();
				if(varName.equals(fieldName)) {
					List<ASTAnnotation> fieldAnnotations = astFieldDeclaration.getFirstParentOfType(ASTClassOrInterfaceBodyDeclaration.class).findChildrenOfType(ASTAnnotation.class);
					if(fieldAnnotations != null) {
						for (ASTAnnotation astAnnotation : fieldAnnotations) {
							String name = astAnnotation.jjtGetChild(0).getFirstChildOfType(ASTName.class).getImage();
							if(name.equals(annotName)) {
								annot = astAnnotation;
								break;
							}
						}
					}
					break;
				}
			}
		}
		return annot;
	}
	
	
	private ASTAnnotation getMethodAnnotationByName(ASTClassOrInterfaceDeclaration unit, String methodName, List<String> parameterTypes, String annotName) {
		ASTAnnotation annot = null;
		List<ASTMethodDeclaration> methods = unit.findDescendantsOfType(ASTMethodDeclaration.class);
		if(methods != null) {
			for (ASTMethodDeclaration astMethoddDeclaration : methods) {
				ASTMethodDeclarator methodDeclarator = astMethoddDeclaration.getFirstChildOfType(ASTMethodDeclarator.class);
				String mName = methodDeclarator.getImage();
				if(mName.equals(methodName)) {
					ASTFormalParameters params = (ASTFormalParameters) methodDeclarator.jjtGetChild(0);
					int paramCount = params.jjtGetNumChildren();
					
					if(paramCount == parameterTypes.size()) {
						boolean allMatches = true;
						for(int i=0; i<paramCount; i++) {
							String paramType = "";
							ASTType type = params.jjtGetChild(i).getFirstChildOfType(ASTType.class);
							if(type.jjtGetChild(0) instanceof ASTPrimitiveType) {
								paramType = type.jjtGetChild(0).getImage();
							} else {
								paramType = type.getFirstDescendantOfType(ASTClassOrInterfaceType.class).getImage();
							}
							if(!parameterTypes.get(i).endsWith(paramType)) {
								allMatches = false;
								break;
							}
						}
						if(allMatches) {
							List<ASTAnnotation> methodAnnotations = astMethoddDeclaration.getFirstParentOfType(ASTClassOrInterfaceBodyDeclaration.class).findChildrenOfType(ASTAnnotation.class);
							if(methodAnnotations != null) {
								for (ASTAnnotation astAnnotation : methodAnnotations) {
									String name = astAnnotation.jjtGetChild(0).getFirstChildOfType(ASTName.class).getImage();
									if(name.equals(annotName)) {
										annot = astAnnotation;
										break;
									}
								}
							}
							break;
						}
					}
					

				}
			}
		}
		return annot;
	}
	
	private boolean isCollectionType(Field field) {
		boolean isColl = false;
		if(field != null) {
			if(field.getType().isArray()) {
				isColl = true;
			} else if(Collection.class.isAssignableFrom(field.getType())){
				isColl = true;
			} else if(field.getType().getName().endsWith("PagingResult")) {
				isColl = true;
			}
		}
		return isColl;
	}
	
	private Class getSubTypeIfCollection(Field field) {
		Class obj = null;
		if(field != null) {
			if(field.getType().isArray()) {
				obj = field.getType().getComponentType();
			} else if(Collection.class.isAssignableFrom(field.getType())){
				obj = (Class) ((ParameterizedType)field.getGenericType()).getActualTypeArguments()[0];
			} else if(field.getType().getName().endsWith("PagingResult")) {
				obj = (Class) ((ParameterizedType)field.getGenericType()).getActualTypeArguments()[0];
			}
		}
		return obj;
	}
	private String extractTypeName(Class<?> classObj) {
		String typeName = null; 
		if(classObj != null) {
			if(classObj.getAnnotation(XmlType.class) != null && !classObj.getAnnotation(XmlType.class).name().equals("##default")) {
				typeName = classObj.getAnnotation(XmlType.class).name();
			} else {
				typeName = getTypeName(classObj);
				if (typeName.endsWith("_REF")) {
					typeName = typeName.substring(0, typeName.length() - 4) + "REF";
				} else if (typeName.endsWith("_REFs")) {
					typeName = typeName.substring(0, typeName.length() - 5) + "REFs";
				}
			}
			
		}
		return typeName;
	}
		
	private String getTypeName(Class classObj) {
		String str = classObj.getSimpleName();
		char[] chars = str.toCharArray();
		for (int i = 0; i < chars.length; i++) {
			char curChar = chars[i];
			if (Character.isLowerCase(curChar)) {
				if (i == 1) {
					str = str.substring(0, i).toLowerCase() + str.substring(i, str.length());
				} else if (i >= 2) {
					str = str.substring(0, i - 1).toLowerCase() + str.substring(i - 1, str.length());
				}
				break;
			}
		}
		return str;
	}
		
	private RBACInfo getRbacInfo(Method method) {
		RBACInfo info = null;
		try {
			if(method != null) {
				RBAC rbacAnnotation = method.getAnnotation(RBAC.class);
				if(rbacAnnotation != null) {
					info = new RBACInfo();
					if(rbacAnnotation.capability() != null) {
						info.setCapabilities(Arrays.asList(rbacAnnotation.capability()));
					}
					if(rbacAnnotation.task() != null) {
						info.setTasks(Arrays.asList(rbacAnnotation.task()));
					}
					CRUDEnum[] crudEnums = rbacAnnotation.type();
					if(crudEnums != null && crudEnums.length > 0) {
						List<String> crudTypes = new ArrayList<String>();
						for (CRUDEnum crudEnum : crudEnums) {
							switch (crudEnum) {
							case CREATE:
								crudTypes.add("CREATE");
								break;
							case ALL:
								crudTypes.add("ALL");
								break;
							case DELETE:
								crudTypes.add("DELETE");
								break;
							case READ:
								crudTypes.add("READ");
								break;
							case UPDATE:
								crudTypes.add("UPDATE");
								break;
							}
						}
						info.setCrudTypes(crudTypes);
					}
					
				}
			}
		} catch(Exception e) {
			System.out.println("Error while processing method annotation");
			e.printStackTrace();
		}
		return info;
	}
	private List<String> getConsumesValueList(Method method) {
		List<String> result = null;
		try {
			if(method != null) {
				Consumes consumes = method.getAnnotation(Consumes.class);
				if(consumes != null) {
					result = Arrays.asList(consumes.value());
				}
			}
		} catch(Exception e) {
			System.out.println("Error while processing consumes annotation");
			e.printStackTrace();
		}
		return result;
	}
	
	private List<String> getProducesValueList(Method method) {
		List<String> result = null;
		try {
			if(method != null) {
				Produces consumes = method.getAnnotation(Produces.class);
				if(consumes != null) {
					result = Arrays.asList(consumes.value());
				}
			}
		} catch(Exception e) {
			System.out.println("Error while processing produces annotation");
			e.printStackTrace();
		}
		return result;
	}
	private String getRestHTTPMethod(Method method) {
		String restHTTPMethod = null;
		try {
			if(method != null) {
				if(method.getAnnotation(GET.class) != null) {
					restHTTPMethod = "GET";
				} else if(method.getAnnotation(POST.class) != null) {
					restHTTPMethod = "POST";
				} else if(method.getAnnotation(PUT.class) != null) {
					restHTTPMethod = "PUT";
				} else if(method.getAnnotation(DELETE.class) != null) {
					restHTTPMethod = "DELETE";
				} else if(method.getAnnotation(PATCH.class) != null) {
					restHTTPMethod = "PATCH";
				}
			}
		} catch(Exception e) {
			System.out.println("Error while extracting http method type");
			e.printStackTrace();
		}
		return restHTTPMethod;
	}
	private String getPathValue(Class<?> classObj) {
		String pathVal = null;
		if(classObj != null) {
			Path p = classObj.getAnnotation(Path.class);
			if(p != null)
			pathVal = p.value();
		}
		return pathVal;
	}		
		private Integer getVersionFromMimeTypes(RestInfo info) {
			Integer version = null;
		try {
			if(info != null) {
				String consumeVersion = null;
				String produceVersion = null;
				List<String> consumeTypes = info.getConsumeTypes();
				List<String> produceTypes = info.getProduceTypes();
				if(consumeTypes != null && !consumeTypes.isEmpty()) {
					consumeVersion = extractVersion(consumeTypes.get(0));
				}
				if(produceTypes != null && !produceTypes.isEmpty()) {
					produceVersion = extractVersion(produceTypes.get(0));
				}
				
				if(consumeVersion != null && produceVersion != null) {
					if(produceTypes.get(0).contains("vnd.net.juniper.space.job-management.task+")) {
						version = Integer.parseInt(consumeVersion);
					} else {
						if(consumeVersion.equals(produceVersion)) {
							version = Integer.parseInt(consumeVersion);
						}
					}
				} else if(consumeVersion != null){
					version = Integer.parseInt(consumeVersion);
				} else if(produceVersion != null) {
					if(!produceTypes.get(0).contains("vnd.net.juniper.space.job-management.task+")){
						version = Integer.parseInt(produceVersion);
					}
				}
			}
		} catch(Exception e) {
			System.out.println("Error while getVersionFromMimeTypes");
			e.printStackTrace();
		}
		return version;
	}
	
	private String extractVersion(String mimeType) {
		String version=null;
		if(mimeType != null && !mimeType.isEmpty()) {
			try {
				String rootPath = "";
				String versionPattern = "version=(?<version>\\d+);";
				Pattern pattern = Pattern.compile(versionPattern);
		        Matcher matcher = pattern.matcher(mimeType);
		        if(matcher.find()) {
		        	version = matcher.group("version");
		        }
			} catch(Exception e) {
				System.out.println("Error while extracting version from mime type");
				e.printStackTrace();
			}
		}
		return version;
	}
	private String getProjectRootPath(RuleContext ctx) {
		String projectRootPath = null;
		try {
			String sourceFilePath = ctx.getSourceCodeFilename();
			String rootPath = "";
			String fileRootPattern = "(?<path>/.*/(CMP|CEMS)/[^/]+/)(.*)";
			Pattern pattern = Pattern.compile(fileRootPattern);
	        Matcher matcher = pattern.matcher(sourceFilePath);
	        if(matcher.find()) {
	        	rootPath = matcher.group("path");
	        	if(rootPath != null) {
	        		projectRootPath = rootPath;
	        	}
	        }
		} catch(Exception e) {
			System.out.println("Error while getting project root path");
			e.printStackTrace();
		}
		return projectRootPath;
	}
	
	private String getContextRoot(String projectRootPath) {
		String contextRoot = null;
		try {
        	if(projectRootPath != null) {
        		String jbossWebPath = projectRootPath + "web/WEB-INF/jboss-web.xml";
	            File jbossWebFile = new File(jbossWebPath);
	            
	            if(jbossWebFile.exists()) {
	                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	                DocumentBuilder builder = factory.newDocumentBuilder();
	                Document dd = builder.parse(jbossWebFile);
	                NodeList nodeList = dd.getElementsByTagName(CONTEXT_ROOT);
	                if(nodeList != null && nodeList.item(0) != null) {
	                	contextRoot = nodeList.item(0).getTextContent();
	                	if(!(contextRoot.charAt(0) == '/')) {
	                		contextRoot = "/" + contextRoot;
	                	}
	                	if(contextRoot.charAt(contextRoot.length()-1) == '/') {
	                		contextRoot = contextRoot.substring(0, contextRoot.length()-1);
	                	}
	                }
	            }
	        }
			
		} catch(Exception e) {
			System.out.println("Error while getting context root");
			e.printStackTrace();
		}
		return contextRoot;
	}
	
	private String getRestUrl(Method method) throws Exception {
		String url = null;
		try {
			String urlFromMethod = null;
			Path methodPath = method.getAnnotation(Path.class);
			if(methodPath != null) {
				urlFromMethod = methodPath.value();
			}
			
			StringBuffer br = new StringBuffer();
			if(classCtxTL.get().getContextRoot() != null) {
				br.append(classCtxTL.get().getContextRoot()).append("/");
			}
			if(classCtxTL.get().getPathAnnotationValue() != null) {
				br.append(classCtxTL.get().getPathAnnotationValue()).append("/");
			}
			if(urlFromMethod != null) {
				br.append(urlFromMethod);
			}
			url = br.toString();
		} catch(Exception e) {
			System.out.println("Error while constructing rest URL");
			e.printStackTrace();
		}
		return url;
	}
}

