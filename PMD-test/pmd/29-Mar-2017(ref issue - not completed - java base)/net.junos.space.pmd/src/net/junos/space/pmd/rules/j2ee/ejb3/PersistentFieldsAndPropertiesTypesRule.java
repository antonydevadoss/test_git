package net.junos.space.pmd.rules.j2ee.ejb3;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import net.junos.space.pmd.utils.outline.AbstractEntityRule;
import net.junos.space.pmd.utils.outline.CollectionAndMapDescendants;
import net.junos.space.pmd.utils.outline.EjbEntity;
import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IJavaAnnotation;
import net.junos.space.pmd.utils.outline.IJavaFieldDeclaration;
import net.junos.space.pmd.utils.outline.IJavaMethodDeclaration;
import net.junos.space.pmd.utils.outline.IJavaType;
import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;
import net.junos.space.pmd.utils.outline.QualifiedName;
import net.sourceforge.pmd.RuleContext;

public class PersistentFieldsAndPropertiesTypesRule extends AbstractEntityRule {
	
	private final static String message =
			"All user defined types of entity persistent fields and properties must be of the following types: Entities, Embeddable, Enums and Serializable." ;
	
	protected void processEntities(RuleContext ctx) {
		if (AbstractEntityRule.entityRegistry == null) {
			return;
		}
		for (EjbEntity ejbEntity : AbstractEntityRule.entityRegistry.values()) {
			
			if(ejbEntity.hasFieldAccess()){
				ArrayList<IJavaFieldDeclaration> persistentFields = ejbEntity.getPersistentFields();
				if(persistentFields != null){
					for( IJavaFieldDeclaration fd : persistentFields ){
						
						if(checkField(fd))
							continue ;
						
						reportError(ctx,fd,message) ;
					}
				}
			}
			
			if(ejbEntity.hasPropertyAccess()){
				ArrayList<IJavaMethodDeclaration> nonTransientProperties = ejbEntity.getNonTransientProperties();
				if(nonTransientProperties != null){
					for( IJavaMethodDeclaration md : nonTransientProperties ){
						
						if(checkMethod(md))
							continue ;
						
						reportError(ctx,md,message) ;
					}
				}
			}
		}
		super.processEntities(ctx);		
	}

	private boolean checkMethod(IJavaMethodDeclaration md) {
		
		IJavaType type = null ;
		IJavaType returnType = md.getReturnType();
		List<IJavaType> params = md.getParameters();
		if(returnType == null){
			if(params==null||params.size()!=1)
				return true ;
			
			type = params.get(0) ;
		}
		else{
			if(params != null && !params.isEmpty() )
				return true ;
			
			type = returnType ;
		}
		return checkType(type,true) ;		
	}

	

	private boolean checkField(IJavaFieldDeclaration fd) {
		
		IJavaType type = fd.getType();
		return checkType(type,true);
	}
	
	private boolean checkType(IJavaType type,boolean lookIntoGenerics) {
		
		IJavaTypeDeclaration tDecl = type.getDeclaration();
		if(tDecl!=null&&tDecl.isEnum())
			return true ;
		
		List<String> vals = type.getQualifiedName().getValues();
		if(vals.size()!=1)
			return true ;
		
		String qNameStr = vals.iterator().next() ;
		
		if(	CollectionAndMapDescendants.javaUtilCollectionAndMapDescendants.contains(qNameStr) ){
			
			boolean result = true ;
			List<IJavaType> gParams = type.getGenericParameters();
			for( IJavaType gType : gParams )
				result = ( result && checkType(gType, false) );
			
			return result ;			
		}
		IClassOutline classOutline = getOutlineContainer().getClassOutline(qNameStr);
		if(classOutline == null)
			return true ;
		
		
		IJavaTypeDeclaration decl = classOutline.getTypeDeclaration();
		List<IJavaAnnotation> annotations = decl.getAnnotations();
		if(annotations!= null){
			for( IJavaAnnotation ann : annotations ){
				String name = ann.getName();
				if(name.equals("Entity"))
					return true ;
				
				if(name.equals("Embeddable"))
					return true ;
			}
		}
		List<IJavaTypeDeclaration> undefinedInterfaces
				= decl.getUndefinedInterfaces(getOutlineContainer());
		
		if(!undefinedInterfaces.isEmpty())
			return true ;
		
		List<IJavaTypeDeclaration> undefinedSuperClasses
				= decl.getUndefinedSuperClasses(getOutlineContainer());

		if(!undefinedSuperClasses.isEmpty())
			return true ;
		
		List<IJavaTypeDeclaration> allInterfaces = decl.getAllInterfaces();
		for( IJavaTypeDeclaration iDecl : allInterfaces ){
			QualifiedName qName = iDecl.getQualifiedName();
			
			if(!qName.getSimpleName().equals(Serializable.class.getSimpleName()))
				continue ;
			
			List<String> values = qName.getValues();
			for( String val : values )
				if(val.equals( Serializable.class.getCanonicalName() ) )
					return true ;
		}
		
		return false ;
	}
	
}
