package net.junos.space.pmd.rules.xml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Stack;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import net.sourceforge.pmd.RuleContext;

public abstract class AbstractXMLRule extends JavaProjectCollector {
	
	protected static final String KEY_NAME_PROJECT_PATH = "projectPath";
	
	protected final static String KEY_NAME_LINE_NUMBER = "lineNumber";

	public static final String XML_NAME_WEB = "web.xml";

	public static final String XML_NAME_APPLICATION = "application.xml";

	public static final String XML_NAME_EJB_JAR = "ejb-jar.xml";

	private static HashSet<String> targetXMLs = null ;
	
	private static boolean isComplete_XML = false ;
	
	private static HashMap<String,HashMap<String,Document>> map ;
	
	public void doStart(RuleContext ctx){
		
		isComplete_XML = false ;
				
		if(targetXMLs == null)
			targetXMLs = new HashSet<String>() ;
		
		String name = getTargetXMLName();
		if(name != null)
			targetXMLs.add(name) ;
		
		map = null ;
	}
	
	public void doEnd(RuleContext ctx){
		if(!isComplete_XML){
			isComplete_XML = true ;
			extractXMLs() ;
			targetXMLs = null ;
		}
		processDocuments(ctx) ;
	}
	
	private void extractXMLs() {
		HashSet<File> projects = getProjects();
		if(projects == null || projects.isEmpty())
			return ;
		
		for( File project : projects )
			processProject(project) ;				
	}

	private void processProject(File project) {
		
		ArrayList<File> metaInf = null ;		
		File webInf = null ; 
				
		if( targetXMLs.contains(XML_NAME_EJB_JAR) ){
			if(metaInf == null)
				metaInf = findMetaInf(project,1) ; 
				
			if(metaInf != null){
				for(File metaInfFile : metaInf){
					File f = new File(metaInfFile, XML_NAME_EJB_JAR) ;
					addDocument( XML_NAME_EJB_JAR, f, project ) ;
				}
			}
		}
		
		if( targetXMLs.contains(XML_NAME_APPLICATION) ){
			if(metaInf == null)
				metaInf = findMetaInf(project,1) ; 
				
			if(metaInf != null){
				for(File metaInfFile : metaInf){
					File f = new File(metaInfFile, XML_NAME_APPLICATION) ;
					addDocument( XML_NAME_APPLICATION, f, project ) ;
				}
			}
		}
		
		if( targetXMLs.contains(XML_NAME_WEB) ){
			if(webInf == null)
				webInf = findWebInf(project) ; 
				
			if(webInf != null){
				File f = new File(webInf, XML_NAME_WEB) ;
				addDocument( XML_NAME_WEB, f, project ) ;
			}
		}
		
	}

	private File findWebInf(File project) {
		
		if(!project.isDirectory())
			return null ;
		
		File webDir = new File(project,"web") ;
		if(!webDir.exists() || !webDir.isDirectory())
			return null ;
		
		File webInf = new File(webDir, "WEB-INF") ;
		if(!webInf.exists() || !webInf.isDirectory())
			return null;
		
		return webInf;
	}

	private ArrayList<File> findMetaInf(File dir, int depth) {
		
		ArrayList<File> result = new ArrayList<File>() ;
		collectDirectories(dir,depth,result,"META-INF") ;
		return result ;		
	}
	
	private void collectDirectories(File dir, int depth, ArrayList<File> result, String string)
	{
		if( !dir.isDirectory() )
			return ;
		
		File metaInf = new File(dir,string) ;
		if( metaInf.exists() && metaInf.isDirectory() ){
			result.add(metaInf) ;
			return ;
		}
		
		File[] listFiles = dir.listFiles();
		if(listFiles == null)
			return ;
		
		for( File f : listFiles ){
			collectDirectories(f, depth-1, result, string) ;
		}		
	}

	private String projectName = null ;
	

	protected final void processDocuments(RuleContext ctx) {
		
		Map<String, Document> documents = getDocuments();
		if(documents == null)
			return ;
		
		for(Map.Entry<String,Document> entry : documents.entrySet() ){
			String path = entry.getKey() ;
			Document document = entry.getValue() ;
			
			updateProjectName(document) ;			
			processSingleDocument( path, document, ctx ) ;
		}
		
	}

	protected void updateProjectName(Document document) {
		
		this.projectName = null ;
		
		Object projectPathObj = document.getDocumentElement().getUserData(KEY_NAME_PROJECT_PATH);
		if(projectPathObj==null)
			return;
		
		String path = projectPathObj.toString() ;
		File f = new File(path) ;
		String name = f.getName() ;
		this.projectName = name ;
	}

	protected void processSingleDocument(String path, Document document, RuleContext ctx){}

	protected final Map<String,Document> getDocuments() {
		if(map == null)
			return null ;
		
		String tergetXMLName = getTargetXMLName();
		if( tergetXMLName == null )
			return null ;
		
		return map.get(tergetXMLName) ;
	}

	protected String getTargetXMLName(){
		return null ;
	}
	
	private void addDocument(String key, File f, File project) {
		if( f==null || !f.exists() )
			return ;
		
		Document doc = null ;
		try {
			doc = CustomXMLParser.parseXML( new FileInputStream(f) );
			doc.getDocumentElement().setUserData(KEY_NAME_PROJECT_PATH, project.getAbsolutePath(), null) ;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		}
		
		if(doc == null)
			return ;
		
		if( map == null )
			map = new HashMap<String, HashMap<String,Document>>() ;
		
		HashMap<String, Document> vol = map.get(key);
		if(vol == null){
			vol = new HashMap<String, Document>() ;
			map.put(key, vol) ;
		}
		vol.put( f.getAbsolutePath(), doc) ;
	}

	public static class CustomXMLParser {
	   

	    public static Document parseXML(final InputStream is) throws IOException, SAXException {
	        final Document doc;
	        SAXParser parser;
	        try {
	            final SAXParserFactory factory = SAXParserFactory.newInstance();
	            parser = factory.newSAXParser();
	            final DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
	            final DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
	            doc = docBuilder.newDocument();
	        } catch (final ParserConfigurationException e) {
	            throw new RuntimeException("Can't create SAX parser / DOM builder.", e);
	        }

	        final Stack<Element> elementStack = new Stack<Element>();
	        final StringBuilder textBuffer = new StringBuilder();
	        
	        final DefaultHandler handler = new DefaultHandler() {
	            private Locator locator;

	            @Override
	            public void setDocumentLocator(final Locator locator) {
	                this.locator = locator; // Save the locator, so that it can be used later for line tracking when traversing nodes.
	            }

	            @Override
	            public void startElement(final String uri, final String localName, final String qName, final org.xml.sax.Attributes attributes)
	                    throws SAXException {
	                addTextIfNeeded();
	                final Element el = doc.createElement(qName);
	                for (int i = 0; i < attributes.getLength(); i++) {
	                    el.setAttribute(attributes.getQName(i), attributes.getValue(i));
	                }
	                int lineNumber = this.locator.getLineNumber();
					el.setUserData(KEY_NAME_LINE_NUMBER, String.valueOf(lineNumber), null);
	                elementStack.push(el);
	            }

	            @Override
	            public void endElement(final String uri, final String localName, final String qName) {
	                addTextIfNeeded();
	                final Element closedEl = elementStack.pop();
	                if (elementStack.isEmpty()) { // Is this the root element?
	                    doc.appendChild(closedEl);
	                } else {
	                    final Element parentEl = elementStack.peek();
	                    parentEl.appendChild(closedEl);
	                }
	            }

	            @Override
	            public void characters(final char ch[], final int start, final int length) throws SAXException {
	                textBuffer.append(ch, start, length);
	            }

	            // Outputs text accumulated under the current node
	            private void addTextIfNeeded() {
	                if (textBuffer.length() > 0) {
	                    final Element el = elementStack.peek();
	                    final org.w3c.dom.Node textNode = doc.createTextNode(textBuffer.toString());
	                    el.appendChild(textNode);
	                    textBuffer.delete(0, textBuffer.length());
	                }
	            }
	        };
	        
	        parser.parse(is, handler);

	        return doc;
	    }
	}
	
	protected static int getLineNumber(Element element){
		
		int result = -1 ;
		while(true){
			Object lnObj = element.getUserData( KEY_NAME_LINE_NUMBER );
			if(lnObj == null)
				break ;
			
			String lnStr = lnObj.toString() ;
			Integer iVal = null ;
			try{
				iVal = Integer.parseInt(lnStr) ;
			}catch(NumberFormatException e){}
			
			if(iVal == null)
				break ;
			
			result = iVal ;			
			break ;		
		}
		return result ;
	}

	protected String getProjectName() {
		return projectName;
	}

}
