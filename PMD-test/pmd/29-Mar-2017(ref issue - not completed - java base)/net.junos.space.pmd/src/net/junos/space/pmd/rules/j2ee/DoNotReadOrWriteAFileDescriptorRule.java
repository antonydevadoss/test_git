package net.junos.space.pmd.rules.j2ee;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.junos.space.pmd.utils.reference.SimpleMethodReferenceRule;

public class DoNotReadOrWriteAFileDescriptorRule extends SimpleMethodReferenceRule {

	private static final String[] targetClasses = new String[]{
		"java.io.FileInputStream",
		"java.io.FileOutputStream",
		"java.io.FileReader",
		"java.io.FileWriter"
	} ;
	
	private static final String[] targetConstructors = new String[]{
		"FileInputStream",
		"FileOutputStream",
		"FileReader",
		"FileWriter"
	} ;
	
	private static final String message = "The enterprise bean must not attempt to directly read or write a file descriptor. "+
			"Allowing the enterprise bean to read and write file descriptors directly could compromise security. "+
			"(See EJB20SPEC - C.1.2 - Programming restrictions)" ;
	
	protected List<String> getTargetMethods() {
		return new ArrayList<String>( Arrays.asList(targetConstructors) ) ;
	}
	
	public String getMessage(){
		return message ;
	}

	protected List<String> getTargetClasses() {
		return new ArrayList<String>( Arrays.asList(targetClasses) ) ;
	}
	
	protected List<String> getParams(String qName, String methodName) {
		ArrayList<String> lst = new ArrayList<String>();
		lst.add("FileDescriptor") ;
		return lst ;
	}

	protected boolean mustBeChecked(String className) {		
		return isEnterpriseBeanSuperclassEJB2(className);
	}
}
