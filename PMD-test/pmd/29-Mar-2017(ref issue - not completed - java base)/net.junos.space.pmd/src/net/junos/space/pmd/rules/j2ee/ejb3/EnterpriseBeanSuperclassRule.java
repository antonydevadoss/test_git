package net.junos.space.pmd.rules.j2ee.ejb3;

import java.util.List;

import net.junos.space.pmd.utils.outline.AbstractOutlineAccessor;
import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;
import net.sourceforge.pmd.RuleContext;
import net.sourceforge.pmd.lang.java.rule.JavaRuleViolation;

public class EnterpriseBeanSuperclassRule extends
		AbstractOutlineAccessor {

	
	private static final String message = "A session (message-driven) bean class must not have a superclass that is itself a session (message-driven) bean class.";

	@Override
	public void doEnd(RuleContext ctx) {
		List<IClassOutline> allOutlines = getOutlineContainer().getAllOutlines();
		
		for (IClassOutline classOutline : allOutlines) {
			if (isMessageDrivenBeanEJB3(classOutline.getQualifiedNameString())
					|| isSessionBeanEJB3(classOutline.getQualifiedNameString())) {
				IJavaTypeDeclaration typeDeclaration = classOutline.getTypeDeclaration();
				List<IJavaTypeDeclaration> allSuperClasses = typeDeclaration.getAllSuperClasses();
				if (allSuperClasses != null) {
					for (IJavaTypeDeclaration superclassDeclaration : allSuperClasses) {
						for (String superclassQName : superclassDeclaration.getQualifiedName().getValues()) {
							if ((isMessageDrivenBeanEJB3(superclassQName) && isMessageDrivenBeanEJB3(classOutline.getQualifiedNameString()))
									|| (isSessionBeanEJB3(superclassQName) && isSessionBeanEJB3(classOutline.getQualifiedNameString()))) {
								String path = classOutline.getFilePath();			
								ctx.setSourceCodeFilename( path ) ;
								
								JavaRuleViolation violation
									= new JavaRuleViolation(this, ctx, null, message,typeDeclaration.getLines()[0],typeDeclaration.getLines()[1] ) ;
								
								ctx.getReport().addRuleViolation(violation) ;
								break;
							}
						}
					}
				}
			}
		}
		super.doEnd(ctx);
	}
}
