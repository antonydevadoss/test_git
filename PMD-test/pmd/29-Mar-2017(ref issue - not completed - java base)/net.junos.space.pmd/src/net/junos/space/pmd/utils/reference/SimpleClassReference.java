package net.junos.space.pmd.utils.reference;

public class SimpleClassReference extends AbstractReference {

	
	
	public SimpleClassReference( String qualifiedName, int beginLine, int endLine) {
		super(beginLine, endLine);
		this.qualifiedName = qualifiedName;
	}

	private final String qualifiedName ;

	public String getQualifiedName() {
		return qualifiedName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((qualifiedName == null) ? 0 : qualifiedName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SimpleClassReference other = (SimpleClassReference) obj;
		if (qualifiedName == null) {
			if (other.qualifiedName != null)
				return false;
		} else if (!qualifiedName.equals(other.qualifiedName))
			return false;
		return true;
	}
	
	@Override
	public String toString(){
		return qualifiedName ;
	}

}
