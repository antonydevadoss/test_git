package net.junos.space.pmd.utils.outline;

import java.util.Arrays;
import java.util.LinkedHashSet;

public class CollectionAndMapDescendants {
	
	public static final LinkedHashSet<String> allowedEntityFieldTypes =
			new LinkedHashSet<String>( Arrays.asList( new String[]{	
			"java.util.Collection",
			"java.util.Set",
			"java.util.List",
			"java.util.Map"
	} )) ;
	
	public static final LinkedHashSet<String> javaUtilCollectionAndMapDescendants =
			new LinkedHashSet<String>( Arrays.asList( new String[]{	
			
			"java.beans.beancontext.BeanContext",
			"java.beans.beancontext.BeanContextServices",
			"java.util.concurrent.BlockingDeque",
			"java.util.concurrent.BlockingQueue",
			"java.util.Deque",
			"java.util.NavigableSet",
			"java.util.Queue",
			"java.util.SortedSet",
			"java.util.Collection",
			"java.util.Set",
			"java.util.List",
			"java.util.Map",
		
			"java.util.AbstractCollection",
			"java.util.AbstractList",
			"java.util.AbstractQueue",
			"java.util.AbstractSequentialList",
			"java.util.AbstractSet",
			"java.util.ArrayBlockingQueue",
			"java.util.ArrayDeque",
			"java.util.ArrayList",
			"javax.management.AttributeList",
			"java.util.BeanContextServicesSupport",
			"java.beans.beancontext.BeanContextServicesSupport.html",
			"java.util.BeanContextSupport",
			"java.beans.beancontext.BeanContextSupport.html",
			"java.util.ConcurrentLinkedQueue",
			"java.util.concurrentConcurrentLinkedQueue.html",
			"java.util.ConcurrentSkipListSet",
			"java.util.concurrentConcurrentLinkedQueue.html",
			"java.util.CopyOnWriteArrayList",
			"java.util.concurrentConcurrentLinkedQueue.html",
			"java.util.CopyOnWriteArraySet",
			"java.util.concurrentConcurrentLinkedQueue.html",
			"java.util.DelayQueue",
			"java.util.concurrentConcurrentLinkedQueue.html",
			"java.util.EnumSet",
			"java.util.HashSet",
			"java.util.JobStateReasons",
			"javax.print.attribute.standard.JobStateReasons.html",
			"java.util.LinkedBlockingDeque",
			"java.util.concurrentConcurrentLinkedQueue.html",
			"java.util.LinkedBlockingQueue",
			"java.util.concurrentConcurrentLinkedQueue.html",
			"java.util.LinkedHashSet",
			"java.util.LinkedList",
			"java.util.PriorityBlockingQueue",
			"java.util.concurrentConcurrentLinkedQueue.html",
			"java.util.PriorityQueue",
			"java.util.RoleList",
			"javax.management.relation.RoleList.html",
			"java.util.RoleUnresolvedList",
			"javax.management.relation.RoleList.html",
			"java.util.Stack",
			"java.util.concurrent.SynchronousQueue",
			"java.util.TreeSet",
			"java.util.Vector",
		
			"javax.script.Bindings",
			"java.util.concurrent.ConcurrentMap",
			"java.util.concurrent.ConcurrentNavigableMap",
			"javax.xml.ws.handler.LogicalMessageContext",
			"javax.xml.ws.handler.MessageContext",
			"java.util.NavigableMap",
			"javax.xml.ws.handler.soap.SOAPMessageContext",
			"java.util.SortedMap",
		
			"java.util.AbstractMap",
			"java.util.jar.Attributes",
			"java.util.security.AuthProvider",
			"java.util.concurrent.ConcurrentHashMap",
			"java.util.concurrent.ConcurrentSkipListMap",
			"java.util.EnumMap",
			"java.util.HashMap",
			"java.util.Hashtable",
			"java.util.IdentityHashMap",
			"java.util.LinkedHashMap",
			"javax.print.attribute.standard.PrinterStateReasons",
			"java.util.Properties",
			"java.util.security.Provider",
			"java.awt.RenderingHints",
			"javax.script.SimpleBindings",
			"javax.management.openmbean.TabularDataSupport",
			"java.util.TreeMap",
			"java.util.javax.swing.UIDefaults",
			"java.util.WeakHashMap"
	} ) ) ;
}
