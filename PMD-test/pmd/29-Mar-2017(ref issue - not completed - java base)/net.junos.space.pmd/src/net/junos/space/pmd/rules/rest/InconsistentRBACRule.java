/*******************************************************************************
 * FILE NAME: InconsistentRBACRule.java
 * PURPOSE:  This is Rule-15 in document
 *
 *
 * Revision History: 
 * DATE:           				  AUTHOR:              	CHANGE:  
 * Tue Feb 22nd, 2011               Atul Bajpai       	Initial Version  
 * 
 * 
 ******************************************************************************/
package net.junos.space.pmd.rules.rest;

import java.util.List;import net.junos.space.pmd.utils.PMDConstants;import net.junos.space.pmd.utils.PMDUtils;import net.sourceforge.pmd.RuleContext;import net.sourceforge.pmd.lang.java.ast.ASTMethodDeclarator;import net.sourceforge.pmd.lang.java.rule.AbstractJavaRule;import org.apache.log4j.Logger;

/**
 * This class checks whether or not Rest method starts with 'get...'
 * and RBAC capability type is not 'READ'. Same could be defined
 * for 'get...' , 'delete...' , ' update...'.
 */
public class InconsistentRBACRule extends AbstractJavaRule {

	/**
	 * Logger
	 */
	private static Logger log = Logger.getLogger(InconsistentRBACRule.class);

	/**
	 * Visits all the Rest methods in class and checks the consistency of 
	 * method name RBAC type.
	 */
	public Object visit(ASTMethodDeclarator methodNode, Object data) {
		RuleContext ctx = (RuleContext) data;
		String methodName = methodNode.getImage();
		try {
			boolean isValidRBAC = false;
			boolean isRBACExist = false;
			boolean isRestMethod = false;
			PMDConstants.Method method = null;

			// checks whether the method is REST or not.
			isRestMethod = PMDUtils.isRestMethod(methodNode);
			if (isRestMethod) {
				// Returns httpMethod Annotation name
				method = PMDConstants.Method.getMethod(methodName);
				if (method != null) {
					// checks whether or not the method is Rest
					isRBACExist = PMDUtils.isMethodAnnotationExists(methodNode, PMDConstants.RBAC_ANNOTATION);
					if (isRBACExist) {
						List<String> array = PMDUtils.getMethodAnnotationValue(methodNode, PMDConstants.RBAC_ANNOTATION,
								PMDConstants.RBAC_TYPE);												if(array!=null&&!array.isEmpty()){							String name = array.get(0);	
							isValidRBAC = method.isValidCapability(name);						}
					}
				}
			}
			
			if(log.isDebugEnabled()){
				StringBuilder builder = new StringBuilder();
				builder.append("source file=").append(ctx.getSourceCodeFilename()).append(",method=").append(methodName);
				builder.append(",isRestMethod=").append(isRestMethod).append(",isRBACExist=").append(isRBACExist).append(
						",isValidRBAC=").append(isValidRBAC).append(",method=").append(method);
				log.debug(builder.toString());
			}
			
			if (isRestMethod && isRBACExist && !isValidRBAC && method != null)
				addViolation(data, methodNode);
		} catch (Exception e) {
			log.error("Exception in executing method " + methodName + " of " + ctx.getSourceCodeFilename(), e);
		}
		return super.visit(methodNode, data);
	}
}
