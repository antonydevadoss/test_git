package net.junos.space.pmd.rules.j2ee;

import java.util.List;

import net.junos.space.pmd.utils.outline.AbstractOutlineAccessor;
import net.junos.space.pmd.utils.outline.IClassOutline;
import net.sourceforge.pmd.RuleContext;

public abstract class AbstractCreateMethodExistenceRule extends AbstractOutlineAccessor {

	public AbstractCreateMethodExistenceRule() {
		super();
	}

	public abstract boolean isProperBean(IClassOutline outline);

	@Override
	public void doEnd(RuleContext ctx) {
		
l0:		for (IClassOutline outline : getOutlineContainer().getAllOutlines()) {
			if ( !isProperBean(outline) )
				continue ;
			
			if(!outline.hasAllSuperclassesDefined(getOutlineContainer()))
				continue ;
			
			List<IClassOutline> definedSuperclasses = outline.getDefinedSuperclasses(getOutlineContainer());
	
			definedSuperclasses.add(outline) ;
			for( IClassOutline o : definedSuperclasses){			
				if (o.getMethodsBySimpleName("ejbCreate") != null && !o.getMethodsBySimpleName("ejbCreate").isEmpty())
					continue l0;
			}
			
			reportError(ctx, outline, outline.getTypeDeclaration());
		
		}
		super.doEnd(ctx);
	}

}