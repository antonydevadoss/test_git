package net.junos.space.pmd.rules.j2ee;

import net.junos.space.pmd.utils.outline.AbstractOutlineAccessor;
import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;
import net.sourceforge.pmd.RuleContext;

public class SessionBeanFinalRule extends AbstractOutlineAccessor {
	
	public String getMessage() {
		return "Do not make an EJB implementation class final";
	}
	
	@Override
	public void doEnd(RuleContext ctx) {
		
		for(IClassOutline outline : getOutlineContainer().getAllOutlines() ){
			
			IJavaTypeDeclaration decl = outline.getTypeDeclaration();
			if(!isEnterpriseBeanEJB2(decl))
				continue ;
			
			if(!decl.isFinal())
				continue ;
			
			reportError(ctx, outline, decl);
		}
	}
}
