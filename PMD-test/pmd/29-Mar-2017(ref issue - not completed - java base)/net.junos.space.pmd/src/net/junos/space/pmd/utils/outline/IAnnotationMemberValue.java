package net.junos.space.pmd.utils.outline;

public interface IAnnotationMemberValue {

	public abstract String getStringValue();

	public abstract int getKind();

}