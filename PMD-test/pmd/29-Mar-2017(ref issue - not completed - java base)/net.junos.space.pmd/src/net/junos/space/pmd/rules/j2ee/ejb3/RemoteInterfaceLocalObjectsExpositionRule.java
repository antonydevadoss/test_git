package net.junos.space.pmd.rules.j2ee.ejb3;

import java.util.ArrayList;
import java.util.HashSet;

import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IJavaMethodDeclaration;
import net.junos.space.pmd.utils.outline.IJavaType;
import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;
import net.junos.space.pmd.utils.outline.QualifiedName;
import net.sourceforge.pmd.RuleContext;

public class RemoteInterfaceLocalObjectsExpositionRule extends AbstractBusinessInterfacesInspector  {

	HashSet<String> localInterfaces = new HashSet<String>() ;
	
	@Override
	public String getMessage() {
		return "If the interface is a remote business interface, its methods must not expose local interface types, timers or timer handles.";
	}
	
	@Override
	protected ArrayList<IClassOutline> extractTargetInterfaces(IJavaTypeDeclaration typeDeclaration)
	{
		ArrayList<IJavaTypeDeclaration> remoteInterfaces = super.extractRemoteDeclarationsFromAnnotation(typeDeclaration);
		if(remoteInterfaces == null || remoteInterfaces.isEmpty())
			return null ;
		
		localInterfaces.clear() ;
		ArrayList<IJavaTypeDeclaration> possibleBusinessInterfaces
			= super.extractPossibleBusinessInterfaces(typeDeclaration);

		if(possibleBusinessInterfaces.size() == 1){
			HashSet<String> remoteInterfacesNames = new HashSet<String>() ;
			for(IJavaTypeDeclaration tDecl : remoteInterfaces){
				
				QualifiedName qName = tDecl.getQualifiedName();
				for( String s : qName.getValues() )
					remoteInterfacesNames.add(s) ; 
			}
			
			IJavaTypeDeclaration bi = possibleBusinessInterfaces.get(0);
			QualifiedName qName = bi.getQualifiedName();
			boolean isRemote = false ;
			for( String s : qName.getValues() ){
				if(remoteInterfacesNames.contains(s)){
					isRemote = true ;
					break ;
				}					
			}
			if(!isRemote){
				for(String s : qName.getValues())
					localInterfaces.add(s) ;
			}
		}
		
		ArrayList<IJavaTypeDeclaration> localInterfacesFromAnnotation
			= super.extractLocalDeclarationsFromAnnotation(typeDeclaration);
		
		if(localInterfacesFromAnnotation != null){
			for(IJavaTypeDeclaration lid:localInterfacesFromAnnotation){
				QualifiedName qName = lid.getQualifiedName();
				if(qName == null)
					continue ;
				
				for(String s : qName.getValues())
					localInterfaces.add(s) ;							
			}
		}
		
		return extractRemoteInterfacesFromAnnotation(typeDeclaration) ;
	}
	
	protected void inspectMethod(IJavaMethodDeclaration method, RuleContext ctx, HashSet<String> inspectedTypes) {
		
		IJavaType returnType = method.getReturnType();
		if( returnType == null )
			return  ;
		
		boolean isInvalid = false ;
		QualifiedName qName = returnType.getQualifiedName();
		for(String s : qName.getValues()){
			if( isInvalid(s) )
				isInvalid = true ;							
		}
		if(!isInvalid)
			return ;
		
		String ownerQualifiedName = method.getOwner().getQualifiedName().getValues().iterator().next() ;
		IClassOutline ownerOutline = getOutlineContainer().getClassOutline(ownerQualifiedName);
		reportError(ctx, ownerOutline, method) ;
	}

	protected boolean isInvalid(String recVal) {
		if(recVal.equals("javax.ejb.Timer"))
			return true ;
		
		if(recVal.equals("javax.ejb.TimerHandle"))
			return true ;
		
		return localInterfaces.contains(recVal);
	}
	
	

}
