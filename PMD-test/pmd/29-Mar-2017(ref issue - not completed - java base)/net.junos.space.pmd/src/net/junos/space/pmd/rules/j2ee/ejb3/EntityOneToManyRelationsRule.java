package net.junos.space.pmd.rules.j2ee.ejb3;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import net.junos.space.pmd.utils.outline.AbstractEntityRule;
import net.junos.space.pmd.utils.outline.EjbEntity;
import net.junos.space.pmd.utils.outline.IDeclaration;
import net.junos.space.pmd.utils.outline.IJavaFieldDeclaration;
import net.junos.space.pmd.utils.outline.IJavaMethodDeclaration;
import net.junos.space.pmd.utils.outline.IJavaType;
import net.sourceforge.pmd.RuleContext;

public class EntityOneToManyRelationsRule extends AbstractEntityRule {
	
	@Override
	public String getMessage() {
		return "Relationship modeling annotations @OneToOne and @ManyToOne should be applied to fields or properties of @Entity types." ;
	}
	
	@Override
	protected void processEntities(RuleContext ctx) {
		if (AbstractEntityRule.entityRegistry == null) {
			return;
		}
		
		for (EjbEntity ejbEntity : AbstractEntityRule.entityRegistry.values()) {
			List<IJavaFieldDeclaration> fields = ejbEntity.getPersistentFields();
			
			for (IJavaFieldDeclaration field : fields) {
				if( !isProperMember(field) )
					continue ;
							
				validateType(ctx, ejbEntity, field.getType(), field );
			}
			for (IJavaMethodDeclaration method : ejbEntity.getNonTransientProperties()) {
				if( !isProperMember(method) )
					continue ;
				
				List<IJavaType> params = method.getParameters();
				if(params!=null){
					for(IJavaType type : params)
						validateType(ctx, ejbEntity, type, method );
				}
				if(method.getReturnType()!=null)
					validateType(ctx, ejbEntity, method.getReturnType(), method );
			}
		}
		super.processEntities(ctx);
	}

	public boolean isProperMember(IDeclaration field) {
		return field.hasAnnotation("OneToOne") ||
				field.hasAnnotation("javax.persistence.OneToOne") ||
				field.hasAnnotation("ManyToOne") ||
				field.hasAnnotation("javax.persistence.ManyToOne");
	}

	public void validateType(RuleContext ctx, EjbEntity ejbEntity, IJavaType type, IDeclaration decl)
	{
		
		for (String fieldQName : type.getQualifiedName().getValues()) {
			
			if( fieldQName.startsWith("java.lang.") ){
				reportError(ctx,ejbEntity.getOutline(),decl) ;
				break ;				
			}				
			
			if( getOutlineContainer().getClassOutline(fieldQName) == null )
				continue ;
			
			if (!AbstractEntityRule.entityRegistry.containsKey(fieldQName)) {
				reportError(ctx,ejbEntity.getOutline(),decl) ;
				break ;				
			}
		}
	}
	
	protected Set<String> getTargetAnnotations() {
		HashSet<String> set = new HashSet<String>();
		set.add("Entity") ;
		set.add("Embeddable") ;
		return set ;
	}
}
