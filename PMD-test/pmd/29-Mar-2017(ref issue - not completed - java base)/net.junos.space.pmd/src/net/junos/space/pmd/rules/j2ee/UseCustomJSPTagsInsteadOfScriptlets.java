package net.junos.space.pmd.rules.j2ee;

import net.junos.space.pmd.rules.xml.LineByLineJSPValidator;
import net.sourceforge.pmd.RuleContext;
import net.sourceforge.pmd.lang.java.rule.JavaRuleViolation;


//see for syntax details http://www.oracle.com/technetwork/java/card12-149784.pdf

public class UseCustomJSPTagsInsteadOfScriptlets extends LineByLineJSPValidator {

	private static final String message = "Use Custom JSP Tags Instead Of Scriptlets";

	protected void validateLine(String line, int lineNumber, String path, RuleContext ctx) {
		
		for( int ind = line.indexOf("<%") ; ind >= 0  ; ind = line.indexOf("<%",ind+1) ){
			
			if(line.startsWith("<%@", ind) ||line.startsWith("<%--", ind))
				continue ;
			
			String backup = ctx.getSourceCodeFilename();
			ctx.setSourceCodeFilename(path) ;
			JavaRuleViolation violation
				= new JavaRuleViolation(this, ctx, null, message, lineNumber, lineNumber );			
			ctx.getReport().addRuleViolation(violation) ;
			ctx.setSourceCodeFilename( backup ) ;
			
			break ;			
		}
	}

}
