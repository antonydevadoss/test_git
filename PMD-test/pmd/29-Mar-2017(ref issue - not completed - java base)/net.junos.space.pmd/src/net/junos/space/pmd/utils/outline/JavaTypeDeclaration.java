package net.junos.space.pmd.utils.outline;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;


public class JavaTypeDeclaration extends AbstractElementDeclaration2 implements IJavaTypeDeclaration {
	
	
	private boolean isPrimitive ;
	
	private boolean isInterface ;
	
	private IJavaType superClass = null ;
	
	private List<IJavaType> interfaces ;
	
	private List<IJavaTypeDeclaration> nestedClasses ;
	
	private List<IJavaTypeDeclaration> subclasses ;
	
	private boolean isEnum = false ;
	
	private List<IEnumConstant> enumConstants ;
	
	@Override
	public boolean isPrimitive() {
		return isPrimitive ;
	}

	public boolean isInterface() {
		return isInterface;
	}

	public void setIsInterface(boolean isInterface) {
		this.isInterface = isInterface;
	}

	@Override
	public void setPrimitive(boolean isPrimitive) {
		this.isPrimitive = isPrimitive ;		
	}

	@Override
	public IJavaType getSuperClass() {
		return superClass ;
	}

	@Override
	public void setSuperClass(IJavaType superClass) {		
		this.superClass = superClass ;
	}

	@Override
	public List<IJavaType> getInterfaces() {
		return this.interfaces ;
	}

	@Override
	public void setInterfaces(Collection<IJavaType> interfaces) {
		if( interfaces != null )
			this.interfaces = new ArrayList<IJavaType>(interfaces) ;		
	}

	public List<IJavaTypeDeclaration> getNestedClasses() {
		return nestedClasses;
	}

	public void setNestedClasses(Collection<IJavaTypeDeclaration> nestedClasses) {
		if( nestedClasses != null )
			this.nestedClasses = new ArrayList<IJavaTypeDeclaration>(nestedClasses);
		else
			this.nestedClasses = null ;
	}

	@Override
	public boolean isSimilar(IJavaTypeDeclaration other) {
		
		if( other == this )
			return true ;
		
		QualifiedName otherName = other.getQualifiedName();
		QualifiedName thisName = this.getQualifiedName();
		
		if( thisName.equals( otherName ) )
			return false ;

		if( this.getGenericParamsCount() != other.getGenericParamsCount() )
			return false ;
		
		return true;
	}

	@Override
	public void addSubclass(IJavaTypeDeclaration subclass) {
		if(subclasses == null)
			subclasses = new ArrayList<IJavaTypeDeclaration>() ;
		
		subclasses.add(subclass) ;		
	}

	@Override
	public List<IJavaTypeDeclaration> getSubclasses() {
		return subclasses;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (isInterface ? 1231 : 1237);
		result = prime * result + (isPrimitive ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		
		if(!(obj instanceof IJavaTypeDeclaration))
			return false ;
		
		
		IJavaTypeDeclaration other = (IJavaTypeDeclaration) obj ;
		boolean sameQName = this.qualifiedName.equals( other.getQualifiedName() ) ;		
		
		return sameQName;
	}

	@Override
	public List<IJavaTypeDeclaration> getAllInterfaces() {
		
		LinkedHashSet<IJavaTypeDeclaration> set = new LinkedHashSet<IJavaTypeDeclaration>() ;
		collectInterfaces(set,this) ;		
		return new ArrayList<IJavaTypeDeclaration>(set) ;
	}

	private static void collectInterfaces(LinkedHashSet<IJavaTypeDeclaration> set,	IJavaTypeDeclaration declaration)
	{
		if(declaration.isInterface())
			set.add(declaration) ;
		
		IJavaType superClass2 = declaration.getSuperClass();
		if(superClass2!= null)
			collectInterfaces(set,superClass2.getDeclaration()) ;
		
		if( declaration.getInterfaces() != null ){
			for( IJavaType iType : declaration.getInterfaces()){
				IJavaTypeDeclaration iDecl = iType.getDeclaration();				
				collectInterfaces(set,iDecl) ;
			}
		}		
	}

	@Override
	public List<IJavaTypeDeclaration> getAllSuperClasses()
	{
		if(superClass == null)
			return new ArrayList<IJavaTypeDeclaration>() ;
		
		IJavaTypeDeclaration superDeclaration = superClass.getDeclaration();
		List<IJavaTypeDeclaration> result = superDeclaration.getAllSuperClasses();
		result.add(superClass.getDeclaration()) ;
		return result ;
	}

	@Override
	public boolean isEnum() {
		return isEnum;
	}

	@Override
	public void setIsEnum(boolean isEnum) {
		this.isEnum = isEnum ;		
	}

	@Override
	public List<IEnumConstant> getEnumConstants() {
		return enumConstants ;
	}

	@Override
	public void setEnumConstants(List<IEnumConstant> enumConstants) {
		this.enumConstants = enumConstants ;		
	}

	@Override
	public List<IJavaTypeDeclaration> getUndefinedInterfaces( IOutlineContainer container) {
		
		List<IJavaTypeDeclaration> allInterfaces = getAllInterfaces();
		if(container == null)
			return allInterfaces ;
		
		ArrayList<IJavaTypeDeclaration> result = filterOutDefinedDeclarations( container, allInterfaces);			
		return result;
	}

	private ArrayList<IJavaTypeDeclaration> filterOutDefinedDeclarations(
			IOutlineContainer container,
			List<IJavaTypeDeclaration> allInterfaces) {
		ArrayList<IJavaTypeDeclaration> result = new ArrayList<IJavaTypeDeclaration>() ;
		for( IJavaTypeDeclaration iDecl : allInterfaces )
		{
			List<String> values = iDecl.getQualifiedName().getValues();
			for( String str : values ){
				IClassOutline classOutline = container.getClassOutline(str);
				if(classOutline != null)
					continue ;
				
				if( str.startsWith("java.lang") )
					continue ;
				
				result.add(iDecl) ;
			}
		}
		return result;
	}

	@Override
	public List<IJavaTypeDeclaration> getUndefinedSuperClasses( IOutlineContainer container) {
		List<IJavaTypeDeclaration> allInterfaces = getAllSuperClasses();
		if(container == null)
			return allInterfaces ;
		
		ArrayList<IJavaTypeDeclaration> result = filterOutDefinedDeclarations( container, allInterfaces);			
		return result;
	}
	
}
