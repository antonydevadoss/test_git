package net.junos.space.pmd.utils.outline;


public interface IHasJavaType {
	
	IJavaType getType() ;
	
	void setType( IJavaType type ) ;

}
