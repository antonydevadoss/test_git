/*******************************************************************************
 * FILE NAME: InvalidXMLAccessorTypeUsageRule.java
 * PURPOSE: This is Rule-23 of document 
 *
 *
 * Revision History: 
 * DATE:           				  AUTHOR:              	CHANGE:  
 * Tue March 22, 2011             Amit Arora	       	Initial Version  
 * 
 * Copyright (c) 2011 Juniper Networks. All rights reserved.
 ******************************************************************************/
package net.junos.space.pmd.rules.dto;

import java.lang.reflect.Field;import java.lang.reflect.Method;import net.junos.space.pmd.utils.PMDConstants;import net.junos.space.pmd.utils.PMDUtils;import net.junos.space.pmd.utils.Project;import net.sourceforge.pmd.RuleContext;import net.sourceforge.pmd.lang.java.ast.ASTMethodDeclarator;import net.sourceforge.pmd.lang.java.rule.AbstractJavaRule;import org.apache.log4j.Logger;

/**
 * This rules ensures that if there is @XmlAccessorType annotation on DTO with enum value as
 * XmlAccessType.NONE then there should be atleast one field or method in DTO which is annotated
 * with either @XmlElement or @XmlAttribute.
 */
public class InvalidXMLAccessorTypeUsageRule extends AbstractJavaRule {

	/**
	 * Logger
	 */
	private static Logger log = Logger.getLogger(InvalidXMLAccessorTypeUsageRule.class);

	/**
	 * Method executes by :
	 * 1. Check whether method is REST or not
	 * 2. If step 1 is true, check method is returning DTO or expecting DTO
	 * 3. If 2 is true, get the class of DTO
	 * 4. Check the validity of usage of XmlAccessorType on this DTO class by calling hasValidXMLAccessorUsage
	 * 5. If step 4 is not valid, then report error.
	 */
	public Object visit(ASTMethodDeclarator methodNode, Object data) {
		RuleContext ctx = (RuleContext) data;
		String methodName = methodNode.getImage();
		try {
			boolean isRestMethod = false;
			boolean hasDTOParameter = false;
			boolean isReturningDTO = false;			
			boolean isValidXMLAccessorTypeUsageForParamDTO = false;
			boolean isValidXMLAccessorTypeUsageForReturnDTO = false;

			// checks whether the method is REST or not.
			isRestMethod = PMDUtils.isRestMethod(methodNode);
			if (isRestMethod) {

				hasDTOParameter = PMDUtils.hasDTOParameters(methodNode);
				isReturningDTO = PMDUtils.isReturningDTO(methodNode);
				
				Project project = new Project(PMDConstants.WEB_SVC_PROJECT);				
				if (hasDTOParameter) {
					Class<?> parameterDTOClass = PMDUtils.getParameterDTOClass(methodNode, project);
					isValidXMLAccessorTypeUsageForParamDTO = hasValidXMLAccessorUsage(parameterDTOClass);
				}

				if (isReturningDTO) {
					Class<?> returningDTOClass = PMDUtils.getReturningDTOClass(methodNode, project);
					isValidXMLAccessorTypeUsageForReturnDTO = hasValidXMLAccessorUsage(returningDTOClass);
					
				}
			}

			if (log.isDebugEnabled()) {
				StringBuilder builder = new StringBuilder();
				builder.append("source file=").append(ctx.getSourceCodeFilename()).append(",method=").append(methodName);
				builder.append(",isRestMethod=").append(isRestMethod).append(",hasDTOParameter=").append(hasDTOParameter).append(
						",isValidXMLAccessorTypeUsageForParamDTO=").append(isValidXMLAccessorTypeUsageForParamDTO)
						.append(",isReturningDTO=").append(isReturningDTO).append(",isValidXMLAccessorTypeUsageForReturnDTO=").append(isValidXMLAccessorTypeUsageForReturnDTO);
				log.debug(builder.toString());
			}

			if (isRestMethod && ((hasDTOParameter && !isValidXMLAccessorTypeUsageForParamDTO) || (isReturningDTO && !isValidXMLAccessorTypeUsageForReturnDTO)))
				addViolation(data, methodNode);
		} catch (Exception e) {
			log.error("Exception in executing method " + methodName + " of " + ctx.getSourceCodeFilename(), e);
		}
		return super.visit(methodNode, data);
	}
	
	/**
	 * 1. Get XmlAccessorType annotation from class. 
	 * 2. Check whether the annotation value in annotation in step 1 is equal to XmlAccessType.NONE. 
	 * 3. If step 2 is true, check all fields in DTO and find that it has @XmlElement or @XmlAttribute annotation on it or not
	 * 4. If step 3 is not true, repeat step 3 for all methods of DTO.
	 * 5. Check the same in inner class
	 * 6. If step 3 or 4 or 5 are not true, then this is not valid usage of XmlAccessorType on DTO
	 * 7. If step 1 or step 2 is not true, it is valid.
	 *  
	 * @param classType DTO class
	 * @return Is this valid @XmlAccessorType usage	
	 * @throws Exception 
	 */
	private boolean hasValidXMLAccessorUsage(Class<?> classType) throws Exception{
		
		boolean isValidXMLAccessorTypeUsage = false; 		
		if(PMDUtils.hasAnnotation(classType, PMDConstants.XML_ROOT_ELEMENT) && PMDUtils.hasAnnotation(classType, PMDConstants.XML_ACCESSOR_TYPE)){
			String[] xmlAccessorValues = PMDUtils.getDefaultAnnotationValue(classType, PMDConstants.XML_ACCESSOR_TYPE);
			
			if(xmlAccessorValues != null && xmlAccessorValues[0] != null && xmlAccessorValues[0].equals("NONE")){
				Field[] fields = classType.getDeclaredFields();
				Method[] methods = classType.getDeclaredMethods();
				for(Field field : fields){
					if(PMDUtils.hasAnnotation(field, PMDConstants.XML_ELEMENT) || PMDUtils.hasAnnotation(field, PMDConstants.XML_ATTRIBUTE)){
						isValidXMLAccessorTypeUsage = true;
						break;
					}
				}
				if(!isValidXMLAccessorTypeUsage){
					for(Method method : methods){
						if(PMDUtils.hasAnnotation(method, PMDConstants.XML_ELEMENT) || PMDUtils.hasAnnotation(method, PMDConstants.XML_ATTRIBUTE)){
							isValidXMLAccessorTypeUsage = true;
							break;
						}
					}
				}
				if(!isValidXMLAccessorTypeUsage){
					Class<?>[] innerClasses = classType.getDeclaredClasses();
					for(Class<?> innerClass : innerClasses){
						isValidXMLAccessorTypeUsage = hasValidXMLAccessorUsage(innerClass);
						if(isValidXMLAccessorTypeUsage)
							break;
					}
				}	
			} else
				isValidXMLAccessorTypeUsage = true;
		} else
			isValidXMLAccessorTypeUsage = true;	
		return isValidXMLAccessorTypeUsage;
	}
}
