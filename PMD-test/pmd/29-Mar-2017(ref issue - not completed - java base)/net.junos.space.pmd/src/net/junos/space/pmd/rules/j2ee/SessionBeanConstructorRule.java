package net.junos.space.pmd.rules.j2ee;

import java.util.List;

import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IJavaMethodDeclaration;
import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;
import net.junos.space.pmd.utils.outline.SimpleInspector;

public class SessionBeanConstructorRule extends SimpleInspector {

	@Override
	protected boolean mustBeChecked(IJavaTypeDeclaration typeDeclaration) {
		return isEnterpriseBeanEJB2(typeDeclaration);
	}
	
	public boolean haveError(IJavaTypeDeclaration decl, IClassOutline outline) {
		
		List<IJavaMethodDeclaration> constructors = outline.getConstructors();
		if(constructors==null || constructors.isEmpty())
			return false ;
		
		for(IJavaMethodDeclaration mDecl : constructors){
			
			if(!mDecl.isPublic())
				continue ;
			
			if(mDecl.getParameters() != null && !mDecl.getParameters().isEmpty() )
				continue ;
			
			return false ;
		}
		return true ;
	}
	
	@Override
	public String getMessage() {
		return "Provide a public default constructor for an EJB implementation class";
	}
}
