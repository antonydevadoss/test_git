package net.junos.space.pmd.rules.j2ee;

import java.util.ArrayList;
import java.util.List;

import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;
import net.sourceforge.pmd.lang.java.ast.ASTClassOrInterfaceBody;
import net.sourceforge.pmd.lang.java.ast.ASTClassOrInterfaceBodyDeclaration;
import net.sourceforge.pmd.lang.java.ast.ASTClassOrInterfaceDeclaration;
import net.sourceforge.pmd.lang.java.ast.ASTClassOrInterfaceType;
import net.sourceforge.pmd.lang.java.ast.ASTCompilationUnit;
import net.sourceforge.pmd.lang.java.ast.ASTExtendsList;
import net.sourceforge.pmd.lang.java.ast.ASTFormalParameter;
import net.sourceforge.pmd.lang.java.ast.ASTFormalParameters;
import net.sourceforge.pmd.lang.java.ast.ASTImplementsList;
import net.sourceforge.pmd.lang.java.ast.ASTImportDeclaration;
import net.sourceforge.pmd.lang.java.ast.ASTMethodDeclaration;
import net.sourceforge.pmd.lang.java.ast.ASTMethodDeclarator;
import net.sourceforge.pmd.lang.java.ast.ASTName;
import net.sourceforge.pmd.lang.java.ast.ASTResultType;
import net.sourceforge.pmd.lang.java.ast.ASTType;

public class Utils {

	public static boolean isEntityBean(ASTClassOrInterfaceDeclaration node)
	{
		boolean isEntityBean = false;
		ASTImplementsList implementsList = node.getFirstChildOfType(ASTImplementsList.class);
		if (implementsList != null)
		{
			List<ASTClassOrInterfaceType> implementsTypes = implementsList.findChildrenOfType(ASTClassOrInterfaceType.class);
			for (ASTClassOrInterfaceType interfaceType : implementsTypes) {
				String image = interfaceType.getImage();
				if (image.equals("EntityBean"))
				{
					isEntityBean = true;
				}
			}
		}
		return isEntityBean;
	}
	
//	public static boolean isEnterpriseBeanEJB3(IClassOutline outline) {
//		IJavaTypeDeclaration typeDeclaration = outline.getTypeDeclaration();
//		if (typeDeclaration.getAnnotations() != null) {
//			for (IJavaAnnotation annotation : typeDeclaration.getAnnotations()) {
//				if (annotation.getName().equals("Stateless") 
//						|| annotation.getName().equals("javax.ejb.Stateless")
//						|| annotation.getName().equals("Stateful")
//						|| annotation.getName().equals("javax.ejb.Stateful")
//						|| annotation.getName().equals("MessageDriven")
//						|| annotation.getName().equals("javax.ejb.MessageDriven")) {
//					return true;
//				}
//			}
//		}
//		return false;
//	}
	
//	public static boolean isEnterpriseBeanEJB3(ASTClassOrInterfaceDeclaration node)
//	{
//		boolean isSessionBean = false;
//		ASTTypeDeclaration typeDeclaration = node.getFirstParentOfType(ASTTypeDeclaration.class);
//		List<ASTAnnotation> annotations = typeDeclaration.findChildrenOfType(ASTAnnotation.class);
//		ASTClassOrInterfaceDeclaration clazz = node;
//		
//		if (clazz != null && annotations != null && !annotations.isEmpty()) {
//			
//			for (ASTAnnotation astAnnotation : annotations) {
//				AbstractJavaNode annotationNode = null;
//				ASTNormalAnnotation normalAnnotation = astAnnotation.getFirstChildOfType(ASTNormalAnnotation.class);
//				if (normalAnnotation != null) {
//					annotationNode = normalAnnotation;
//				} else {
//					ASTMarkerAnnotation markerAnnotation = astAnnotation.getFirstChildOfType(ASTMarkerAnnotation.class);
//					if (markerAnnotation != null) {
//						annotationNode = markerAnnotation;
//					}
//				}
//				if (annotationNode != null) {
//					ASTName annotationName = annotationNode.getFirstChildOfType(ASTName.class);
//					if (annotationName != null && annotationName.getImage() != null) {
//						if (annotationName.getImage().equals("Stateless")
//								|| annotationName.getImage().equals("Stateful")
//								|| annotationName.getImage().equals("MessageDriven")) {
//							isSessionBean = true;
//						}
//					}
//					
//				}
//			}
//			
//						
//		}
	
//		boolean isSessionBean = false;
//		ASTImplementsList implementsList = node.getFirstChildOfType(ASTImplementsList.class);
//		if (implementsList != null)
//		{
//			List<ASTClassOrInterfaceType> implementsTypes = implementsList.findChildrenOfType(ASTClassOrInterfaceType.class);
//			for (ASTClassOrInterfaceType interfaceType : implementsTypes) {
//				String image = interfaceType.getImage();
//				if (image.equals("SessionBean"))
//				{
//					isSessionBean = true;
//				}
//			}
//		}
//		return isSessionBean;
//	}
	
//	public static boolean isEnterpriseBeanEJB2(ASTClassOrInterfaceDeclaration node){
//		return Utils.isClassOrInterfaceImplements(node, "EntityBean", "javax.ejb")
//				|| Utils.isClassOrInterfaceImplements(node, "SessionBean", "javax.ejb")
//				|| Utils.isClassOrInterfaceImplements(node, "MessageDrivenBean", "javax.ejb") ;
//	}
	
	public static boolean isClassOrInterfaceImplements(IJavaTypeDeclaration node, String interfaceQName){
		for (IJavaTypeDeclaration interfaceDeclaration : node.getAllInterfaces()) {
			if (interfaceDeclaration.getQualifiedName().getValues().contains(interfaceQName)) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean isClassOrInterfaceExtends(IJavaTypeDeclaration node, String superclassQName) {
		if (node.getAllSuperClasses() != null) {
			for (IJavaTypeDeclaration superclass : node.getAllSuperClasses()) {
				if (superclass.getQualifiedName().getValues().contains(superclassQName)) {
					return true;
				}
			}
		}
		return false;
	}
	
//	public static boolean isEnterpriseBeanEJB2(IClassOutline outline){
//		IJavaTypeDeclaration typeDeclaration = outline.getTypeDeclaration();
//		return isClassOrInterfaceImplements(typeDeclaration, "javax.ejb.EntityBean")
//				|| isClassOrInterfaceImplements(typeDeclaration, "javax.ejb.SessionBean")
//				|| isClassOrInterfaceImplements(typeDeclaration, "javax.ejb.MessageDrivenBean");
//	}
//	
//	public static boolean isSessionBeanEJB2(ASTClassOrInterfaceDeclaration node) {
//		return Utils.isClassOrInterfaceImplements(node, "SessionBean", "javax.ejb");
//	}
//	
//	public static boolean isSessionBeanEJB2(IClassOutline outline){
//		IJavaTypeDeclaration typeDeclaration = outline.getTypeDeclaration();
//		return isClassOrInterfaceImplements(typeDeclaration, "javax.ejb.SessionBean");
//	}
//	
//	public static boolean isMessageBeanEJB2(ASTClassOrInterfaceDeclaration node){
//		return Utils.isClassOrInterfaceImplements(node, "MessageDrivenBean", "javax.ejb") ;
//	}
//	
//	public static boolean isMessageBeanEJB2(IClassOutline outline){
//		IJavaTypeDeclaration typeDeclaration = outline.getTypeDeclaration();
//		return isClassOrInterfaceImplements(typeDeclaration, "javax.ejb.MessageDrivenBean");
//	}
	
	public static boolean isInterfaceExtendsEJBHome(ASTClassOrInterfaceDeclaration node)
	{
		boolean isInterfaceExtendsEJBHome = false;
		if (node.isInterface())
		{
			isInterfaceExtendsEJBHome = Utils.isClassOrInterfaceExtends(node, "EJBHome", "javax.ejb");
		}
		return isInterfaceExtendsEJBHome;
	}
	
	public static boolean isClassOrInterfaceImplements(ASTClassOrInterfaceDeclaration node, String implementsName)
	{
		boolean isExtends = false;
		ASTImplementsList implementsList = node.getFirstChildOfType(ASTImplementsList.class);
		if (implementsList != null)
		{
			List<ASTClassOrInterfaceType> implementsTypes = implementsList.findChildrenOfType(ASTClassOrInterfaceType.class);
			for (ASTClassOrInterfaceType interfaceType : implementsTypes) {
				String image = interfaceType.getImage();
				if (image.equals(implementsName))
				{
					isExtends = true;
				}
			}
		}
		return isExtends;
	}
	
	public static boolean isClassOrInterfaceImplements(ASTClassOrInterfaceDeclaration node, String implementsName, String packageName)
	{
		if(node==null)
			return false;
		
		boolean isExtends = false;
		
		String prefix = "";
		if (packageName != null) {
			prefix = packageName + ".";
			ASTCompilationUnit cu = node.getFirstParentOfType(ASTCompilationUnit.class);
			if (cu != null)
			{
				List<ASTImportDeclaration> ids = cu.findChildrenOfType(ASTImportDeclaration.class);
				if (ids != null)
				{
					for (ASTImportDeclaration importDeclaration : ids) {
						ASTName name = importDeclaration.getFirstChildOfType(ASTName.class);
						if (name != null 
								&& name.getImage() != null 
								&& (name.getImage().equals(prefix + "*") || 
										name.getImage().equals(prefix + implementsName)))
						{
							prefix = "";
						}
					}
				}
			}
		} else {
			prefix = "";
			packageName = "";
		}
		
		ASTImplementsList implementsList = node.getFirstChildOfType(ASTImplementsList.class);
		if (implementsList != null)
		{
			List<ASTClassOrInterfaceType> implementsTypes = implementsList.findChildrenOfType(ASTClassOrInterfaceType.class);
			for (ASTClassOrInterfaceType interfaceType : implementsTypes) {
				String image = interfaceType.getImage();
				if (image.equals(prefix + implementsName)
						|| image.equals(packageName + "." + implementsName))
				{
					isExtends = true;
					break;
				}
			}
		}
		return isExtends;
	}
	
	public static List<ASTMethodDeclaration> getClassOrInterfaceMethods(ASTClassOrInterfaceDeclaration node)
	{
		List<ASTMethodDeclaration> result = new ArrayList<ASTMethodDeclaration>();
		
		ASTClassOrInterfaceBody body = node.getFirstChildOfType(ASTClassOrInterfaceBody.class);
		if (body != null)
		{
			List<ASTClassOrInterfaceBodyDeclaration> bodyDeclarations = body.findChildrenOfType(ASTClassOrInterfaceBodyDeclaration.class);
			if (bodyDeclarations != null)
			{
				for (ASTClassOrInterfaceBodyDeclaration bodyDeclaration : bodyDeclarations) {
					ASTMethodDeclaration methodDeclaration = bodyDeclaration.getFirstChildOfType(ASTMethodDeclaration.class);
					if (methodDeclaration != null)
					{
						result.add(methodDeclaration);
					}
					
				}
			}
		}
		
		return result;
	}
	
	
	public static boolean isClassOrInterfaceExtends(ASTClassOrInterfaceDeclaration node, String extendsName)
	{
		boolean isExtends = false;
		ASTExtendsList extendsList = node.getFirstChildOfType(ASTExtendsList.class);
		if (extendsList != null)
		{
			List<ASTClassOrInterfaceType> extendsTypes = extendsList.findChildrenOfType(ASTClassOrInterfaceType.class);
			for (ASTClassOrInterfaceType interfaceType : extendsTypes) {
				String image = interfaceType.getImage();
				if (image.equals(extendsName))
				{
					isExtends = true;
					break;
				}
			}
		}
		return isExtends;
	}
	
	public static boolean isClassOrInterfaceExtends(ASTClassOrInterfaceDeclaration node, String extendsName, String packageName)
	{
		boolean isExtends = false;
		
		String prefix = packageName + ".";
		ASTCompilationUnit cu = node.getFirstParentOfType(ASTCompilationUnit.class);
		if (cu != null)
		{
			List<ASTImportDeclaration> ids = cu.findChildrenOfType(ASTImportDeclaration.class);
			if (ids != null)
			{
				for (ASTImportDeclaration importDeclaration : ids) {
					ASTName name = importDeclaration.getFirstChildOfType(ASTName.class);
					if (name != null 
							&& name.getImage() != null 
							&& (name.getImage().equals(prefix + "*") || 
									name.getImage().equals(prefix + extendsName)))
					{
						prefix = "";
					}
				}
			}
		}
		
		ASTExtendsList extendsList = node.getFirstChildOfType(ASTExtendsList.class);
		if (extendsList != null)
		{
			List<ASTClassOrInterfaceType> extendsTypes = extendsList.findChildrenOfType(ASTClassOrInterfaceType.class);
			for (ASTClassOrInterfaceType interfaceType : extendsTypes) {
				String image = interfaceType.getImage();
				if (image.equals(prefix + extendsName))
				{
					isExtends = true;
					break;
				}
			}
		}
		return isExtends;
	}
	
	public static boolean isVoidMethodReturnType(ASTMethodDeclaration node, Object data) {
		List<ASTResultType> resultTypes = node.findChildrenOfType(ASTResultType.class);
		boolean isVoid = false;
		for (ASTResultType astResultType : resultTypes) {
			List<ASTType> types = astResultType.findChildrenOfType(ASTType.class);
			if (types == null || types.size() == 0)
			{
				isVoid = true;
				break;
			}
		}
		return isVoid;
	}
	
	public static List<ASTFormalParameter> getMethodParameters(ASTMethodDeclaration method) {
		List<ASTFormalParameter> result = new ArrayList<ASTFormalParameter>();
		ASTMethodDeclarator methodDeclarator = method.getFirstChildOfType(ASTMethodDeclarator.class);
		if (methodDeclarator != null) {
			ASTFormalParameters formalParameters = methodDeclarator.getFirstChildOfType(ASTFormalParameters.class);
			if (formalParameters != null) {
				List<ASTFormalParameter> formalParametersList = formalParameters.findChildrenOfType(ASTFormalParameter.class);
				if (formalParametersList != null) {
					result.addAll(formalParametersList);
				}
			}
		}
		return result;
	}
	
}

