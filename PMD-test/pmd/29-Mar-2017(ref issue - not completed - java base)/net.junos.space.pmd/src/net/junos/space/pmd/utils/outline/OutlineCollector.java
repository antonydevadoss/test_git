package net.junos.space.pmd.utils.outline;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sourceforge.pmd.RuleContext;
import net.sourceforge.pmd.lang.java.ast.ASTAnnotation;
import net.sourceforge.pmd.lang.java.ast.ASTClassOrInterfaceBody;
import net.sourceforge.pmd.lang.java.ast.ASTClassOrInterfaceBodyDeclaration;
import net.sourceforge.pmd.lang.java.ast.ASTClassOrInterfaceDeclaration;
import net.sourceforge.pmd.lang.java.ast.ASTClassOrInterfaceType;
import net.sourceforge.pmd.lang.java.ast.ASTCompilationUnit;
import net.sourceforge.pmd.lang.java.ast.ASTConstructorDeclaration;
import net.sourceforge.pmd.lang.java.ast.ASTEnumBody;
import net.sourceforge.pmd.lang.java.ast.ASTEnumConstant;
import net.sourceforge.pmd.lang.java.ast.ASTEnumDeclaration;
import net.sourceforge.pmd.lang.java.ast.ASTExtendsList;
import net.sourceforge.pmd.lang.java.ast.ASTFieldDeclaration;
import net.sourceforge.pmd.lang.java.ast.ASTFormalParameter;
import net.sourceforge.pmd.lang.java.ast.ASTFormalParameters;
import net.sourceforge.pmd.lang.java.ast.ASTImplementsList;
import net.sourceforge.pmd.lang.java.ast.ASTImportDeclaration;
import net.sourceforge.pmd.lang.java.ast.ASTLiteral;
import net.sourceforge.pmd.lang.java.ast.ASTMarkerAnnotation;
import net.sourceforge.pmd.lang.java.ast.ASTMemberValue;
import net.sourceforge.pmd.lang.java.ast.ASTMemberValueArrayInitializer;
import net.sourceforge.pmd.lang.java.ast.ASTMemberValuePair;
import net.sourceforge.pmd.lang.java.ast.ASTMemberValuePairs;
import net.sourceforge.pmd.lang.java.ast.ASTMethodDeclaration;
import net.sourceforge.pmd.lang.java.ast.ASTName;
import net.sourceforge.pmd.lang.java.ast.ASTNameList;
import net.sourceforge.pmd.lang.java.ast.ASTNormalAnnotation;
import net.sourceforge.pmd.lang.java.ast.ASTPackageDeclaration;
import net.sourceforge.pmd.lang.java.ast.ASTPrimaryExpression;
import net.sourceforge.pmd.lang.java.ast.ASTPrimaryPrefix;
import net.sourceforge.pmd.lang.java.ast.ASTPrimitiveType;
import net.sourceforge.pmd.lang.java.ast.ASTReferenceType;
import net.sourceforge.pmd.lang.java.ast.ASTResultType;
import net.sourceforge.pmd.lang.java.ast.ASTSingleMemberAnnotation;
import net.sourceforge.pmd.lang.java.ast.ASTType;
import net.sourceforge.pmd.lang.java.ast.ASTTypeDeclaration;
import net.sourceforge.pmd.lang.java.ast.ASTTypeParameter;
import net.sourceforge.pmd.lang.java.ast.ASTTypeParameters;
import net.sourceforge.pmd.lang.java.ast.ASTVariableDeclaratorId;
import net.sourceforge.pmd.lang.java.ast.AbstractJavaAccessNode;
import net.sourceforge.pmd.lang.java.ast.AbstractJavaAccessTypeNode;
import net.sourceforge.pmd.lang.java.ast.AbstractJavaNode;

public class OutlineCollector {
	
	
	private String packageName ;
	private TypeResolvingService typeResolvingService = new TypeResolvingService() ;
	
	private IOutlineContainer outlineContainer ;
	private String filePath = null ;
	private ArrayList<String> packageImports = new ArrayList<String>() ;
	
	
	public Object visit(ASTCompilationUnit node, Object data){		
		
		extractPackageName(node);
			
		AbstractJavaAccessTypeNode classNode = extractClassOrEnum(node) ;
		if(classNode==null)
			return null ;
		
		String qualifiedName = composeQualifiedName(classNode, null);
		if( getOutlineContainer().getClassOutline(qualifiedName) != null )
			return null ;		
		
		this.typeResolvingService.setOutlineContainer(getOutlineContainer()) ;
		this.typeResolvingService.setContextQualifiedname(qualifiedName) ;
		
		clearAll();
		extractFilePath(data);		
		
		List<ASTImportDeclaration> imports = node.findDescendantsOfType(ASTImportDeclaration.class);
		extractImports(imports);			
		
		extractDeclaration( classNode, null );		
		
		ASTTypeDeclaration typeDeclNode = node.getFirstChildOfType(ASTTypeDeclaration.class);
		IClassOutline rootOutline = extractOutline(typeDeclNode, null);		
		
		if( this.filePath != null){
			getOutlineContainer().setPath(rootOutline, filePath) ;
		}
		return null ;
	}

	private AbstractJavaAccessTypeNode extractClassOrEnum(ASTCompilationUnit node)
	{
		ASTTypeDeclaration typeDeclNode = node.getFirstChildOfType(ASTTypeDeclaration.class);
		if(typeDeclNode==null)
			return null ;
		
		ASTClassOrInterfaceDeclaration classNode = typeDeclNode.getFirstChildOfType(ASTClassOrInterfaceDeclaration.class);
		if(classNode != null)
			return classNode ;
		
		ASTEnumDeclaration enumNode = typeDeclNode.getFirstChildOfType(ASTEnumDeclaration.class);
		
		return enumNode;
	}

	private List<IJavaAnnotation> extractAnnotations( AbstractJavaNode typeDeclNode, IDeclaration context) {
		
		List<ASTAnnotation> annotationNodes = typeDeclNode.findChildrenOfType(ASTAnnotation.class);
		if( annotationNodes == null || annotationNodes.isEmpty() )
			return null ;
		
		ArrayList<IJavaAnnotation> result = new ArrayList<IJavaAnnotation>() ;
		for(ASTAnnotation aNode : annotationNodes ){
			
			IJavaAnnotation annotation = null ;
			while(true){
				ASTSingleMemberAnnotation smAnn = aNode.getFirstChildOfType(ASTSingleMemberAnnotation.class);
				if(smAnn != null){
					annotation = extractSingleMemberAnnotation(smAnn,context) ;
					break ;
				}				
				
				ASTNormalAnnotation nAnn = aNode.getFirstChildOfType(ASTNormalAnnotation.class);
				if(nAnn != null){
					annotation = extractNormalAnnotation(nAnn,context) ;
					break ;
				}
				
				ASTMarkerAnnotation mAnn = aNode.getFirstChildOfType(ASTMarkerAnnotation.class) ;
				if(mAnn!= null){
					annotation = extractMarkerAnnotation( mAnn ) ;				
				}
				break ;
			}
			if(annotation == null)
				continue ;
			
			result.add(annotation) ;
		}
		return result;
	}

	private IJavaAnnotation extractMarkerAnnotation(ASTMarkerAnnotation mAnn) {
		
		ASTName nameNode = mAnn.getFirstChildOfType(ASTName.class);		
		String name = nameNode.getImage() ;
		IJavaAnnotation result = new JavaAnnotation(name) ;
		return result ;
	}

	private IJavaAnnotation extractNormalAnnotation(ASTNormalAnnotation aNode, IDeclaration context)
	{
		ASTMemberValuePairs pairsNode = aNode.getFirstDescendantOfType(ASTMemberValuePairs.class);
		if( pairsNode == null )
			return null ;
		
		List<ASTMemberValuePair> pairsList = pairsNode.findChildrenOfType(ASTMemberValuePair.class);
		if(pairsList == null || pairsList.isEmpty())
			return null ;
		
		ASTName nameNode = aNode.getFirstChildOfType(ASTName.class);
		String annotationName = nameNode.getImage() ;
		
		JavaAnnotation result = new JavaAnnotation(annotationName) ;
		
		for( ASTMemberValuePair pairNode : pairsList ){
			String name = pairNode.getImage() ;
			ASTMemberValue memberValueNode = pairNode.getFirstChildOfType(ASTMemberValue.class);
			IAnnotationMemberValue val = extractAnnotationMemberValues(memberValueNode,context) ;
			if(val==null)
				continue ;
			
			result.setValue(name, val) ;
		}
		return result ;
	}



	private IJavaAnnotation extractSingleMemberAnnotation( ASTSingleMemberAnnotation aNode, IDeclaration context)
	{
		ASTName nameNode = aNode.getFirstChildOfType(ASTName.class);
		String annotationName = nameNode.getImage() ;
		
		JavaAnnotation result = new JavaAnnotation(annotationName) ;
		
		ASTMemberValue memberValueNode = aNode.getFirstChildOfType(ASTMemberValue.class);
		IAnnotationMemberValue val = extractAnnotationMemberValues(memberValueNode,context) ;
		if(val!=null)
			result.setValue("value", val) ;
		
		return result;
	}
	
	private IAnnotationMemberValue extractAnnotationMemberValues(ASTMemberValue memberValueNode, IDeclaration context)
	{
		ASTMemberValueArrayInitializer arrayInitializerNode
			= memberValueNode.getFirstChildOfType(ASTMemberValueArrayInitializer.class);
		
		if(arrayInitializerNode == null)		
			return extractSingleAnnotationMemberValue(memberValueNode, context);
		
		List<ASTMemberValue> arrayMembers = arrayInitializerNode.findChildrenOfType(ASTMemberValue.class);
		ArrayList<IAnnotationMemberValue> arr = new ArrayList<IAnnotationMemberValue>() ;
		for( ASTMemberValue mv : arrayMembers ){
			IAnnotationMemberValue amv = extractAnnotationMemberValues(mv, context);
			if(amv==null)
				continue ;
			
			arr.add(amv) ;		
		}
		if(arr.isEmpty())
			return null ;		
		
		MemberValueArray result = new MemberValueArray(arr);
		return result ;
	}

	protected IAnnotationMemberValue extractSingleAnnotationMemberValue(
			ASTMemberValue memberValueNode, IDeclaration context) {
		ASTPrimaryExpression primExpr = memberValueNode.getFirstChildOfType(ASTPrimaryExpression.class);
		if(primExpr == null)
			return null ;
		
		ASTPrimaryPrefix primPrefix = primExpr.getFirstChildOfType(ASTPrimaryPrefix.class);
		if(primPrefix == null)
			return null ;
		
		ASTLiteral literalNode = primPrefix.getFirstChildOfType(ASTLiteral.class);
		if(literalNode != null)
			return extractLiteralValue( literalNode ) ;
		
		ASTName nameNode = primPrefix.getFirstChildOfType(ASTName.class);
		if(nameNode != null)
			return new AnnotationMemberValue(nameNode.getImage(), IJavaAnnotation.ANNOTATION_TYPE_VARIABLE ) ;
		
		ASTResultType resultTypeNode = primExpr.getFirstDescendantOfType(ASTResultType.class);
		if(resultTypeNode==null)
			return null ;
		
		ASTType typeNode = resultTypeNode.getFirstChildOfType(ASTType.class);
		if(typeNode != null)
			return extractTypeValue(typeNode,context) ;
		
		return null;
	}

	private IAnnotationMemberValue extractTypeValue(ASTType typeNode, IDeclaration context) {

		ASTReferenceType referenceTypeNode = typeNode.getFirstChildOfType( ASTReferenceType.class );
		if( referenceTypeNode != null ){
			
			ASTClassOrInterfaceType classOrInterfacTypeNode
					= referenceTypeNode.getFirstChildOfType(ASTClassOrInterfaceType.class);
			
			IJavaType type = this.typeResolvingService.obtainType(classOrInterfacTypeNode, context, false );			
			return new AnnotationClassMemberValue(type) ;
		}

		ASTPrimitiveType primitiveTypeNode = typeNode.getFirstChildOfType( ASTPrimitiveType.class );
		if(primitiveTypeNode != null){
			String typeName = primitiveTypeNode.getImage();
			return new AnnotationMemberValue(typeName, IJavaAnnotation.ANNOTATION_TYPE_CLASS_NAME) ;
		}

		return null;
	}

	private AnnotationMemberValue extractLiteralValue(ASTLiteral literalNode) {
		
		Class<?> clazz = literalNode.getType();		
		if(clazz == null)
			return null ;
		
		int kind = IJavaAnnotation.ANNOTATION_TYPE_STRING ;		
		if(clazz == int.class){
			kind = IJavaAnnotation.ANNOTATION_TYPE_INT ;
		}
		String value = literalNode.getImage();
		return new AnnotationMemberValue(value, kind);
	}

	private void extractFilePath(Object data) {
		if( data instanceof RuleContext ){
			File sourceCodeFile = ((RuleContext)data).getSourceCodeFile();
			if( sourceCodeFile != null )
				this.filePath = sourceCodeFile.getAbsolutePath() ;
		}
	}
	
	private IJavaTypeDeclaration extractDeclaration(
			AbstractJavaAccessTypeNode classNode, IJavaTypeDeclaration owner )
	{		
		IJavaTypeDeclaration decl = extractTypeDeclaration(classNode,owner);
		decl.setLines( classNode.getBeginLine(), classNode.getEndLine() ) ;
		if(owner != null){
			decl.setOwner(owner) ;
			List<IJavaTypeDeclaration> nestedClasses = owner.getNestedClasses();
			if(nestedClasses == null){
				nestedClasses = new ArrayList<IJavaTypeDeclaration>() ;
				owner.setNestedClasses(nestedClasses) ;
				nestedClasses = owner.getNestedClasses();
			}
			nestedClasses.add(decl) ;			
		}

		List<ASTClassOrInterfaceDeclaration> nestedClassNodes
				= getChildDeclarations( classNode, ASTClassOrInterfaceDeclaration.class ) ;
		
		for( ASTClassOrInterfaceDeclaration nestedClassNode : nestedClassNodes ){
			extractDeclaration(nestedClassNode, decl ) ;
		}
		
		List<ASTEnumDeclaration> nestedEnumNodes
				= getChildDeclarations( classNode, ASTEnumDeclaration.class ) ;
		
		for( ASTEnumDeclaration nestedClassNode : nestedEnumNodes ){
				extractDeclaration(nestedClassNode, decl ) ;
		}
		
		return decl ;
	}


	private IClassOutline extractOutline(
			AbstractJavaNode typeDeclNode, IJavaTypeDeclaration owner )
	{
		IClassOutline cOutline = new ClassOutline() ;
		AbstractJavaAccessTypeNode classNode = typeDeclNode.getFirstDescendantOfType( ASTClassOrInterfaceDeclaration.class );
		if(classNode == null)
			classNode = typeDeclNode.getFirstDescendantOfType( ASTEnumDeclaration.class );
		
		String qualifiedName = composeQualifiedName(classNode, owner) ;		
		IJavaTypeDeclaration decl
				= BasicOutlineContainer.getInstance().getTypeDeclaration(qualifiedName) ;

		
		List<IJavaAnnotation> annotations = extractAnnotations( typeDeclNode, decl ) ;
		decl.setAnnotations(annotations) ;
		
		
		List<ASTClassOrInterfaceBodyDeclaration> nestedClassNodes
				= getBodyDeclarations( classNode, ASTClassOrInterfaceDeclaration.class ) ;
				
		processNestedClasses( decl , nestedClassNodes) ;
		
		List<ASTClassOrInterfaceBodyDeclaration> nestedEnumNodes
				= getBodyDeclarations( classNode, ASTEnumDeclaration.class ) ;
		
		processNestedClasses( decl , nestedEnumNodes) ;
				
		List<ASTClassOrInterfaceBodyDeclaration> fieldNodes
				= getBodyDeclarations( classNode, ASTFieldDeclaration.class ) ;
				
		ArrayList<IJavaFieldDeclaration> fields = extractFields(fieldNodes, decl);
		
		List<ASTClassOrInterfaceBodyDeclaration> constructorNodes
				= getBodyDeclarations( classNode, ASTConstructorDeclaration.class ) ;
		
		ArrayList<IJavaMethodDeclaration> constructors = extractConstructors(constructorNodes, decl );
		
		List<ASTClassOrInterfaceBodyDeclaration> methodNodes
				= getBodyDeclarations( classNode, ASTMethodDeclaration.class ) ;
				
		ArrayList<IJavaMethodDeclaration> methods = extractMethods(methodNodes, decl );			
				
		cOutline.setImports( this.typeResolvingService.getImportsMap().values() ) ;
		cOutline.setPackageImports(this.packageImports) ;
		cOutline.setTypeDeclaration(decl) ;
		cOutline.setFields(fields) ;
		cOutline.setMethods(methods) ;
		cOutline.setConstructors(constructors) ;
		if(this.filePath != null)
			cOutline.setFilePath( this.filePath) ;
		
		this.outlineContainer.putClassOutline(cOutline) ;

		return cOutline ;
	}
	
	protected ArrayList<IJavaMethodDeclaration> extractConstructors(
			List<ASTClassOrInterfaceBodyDeclaration> bodyDeclNodes, IJavaTypeDeclaration owner) {
		
		ArrayList<IJavaMethodDeclaration> result = new ArrayList<IJavaMethodDeclaration>() ;
		if( bodyDeclNodes == null || bodyDeclNodes.isEmpty() )
			return result ;
		
		for( ASTClassOrInterfaceBodyDeclaration bodyDeclNode : bodyDeclNodes ){
			
			ASTConstructorDeclaration fd = bodyDeclNode.getFirstDescendantOfType(ASTConstructorDeclaration.class) ;			
			IJavaMethodDeclaration jmd = extractSingleConstructor(fd,owner);
			if(jmd== null)
				continue ;
			
			List<IJavaAnnotation> annotations = extractAnnotations(bodyDeclNode, owner);
			jmd.setAnnotations(annotations) ;
			
			jmd.setSimpleName( owner.getSimpleName() ) ;
			//qualifiedNmae existence is guaranteed here
			jmd.setQualifiedName( owner.getQualifiedName() ) ;
			result.add(jmd) ;
		}
		
		return result ;
	}


	private <T>List<T> getChildDeclarations(
			AbstractJavaNode classNode,
			Class<T> clazz )
	{
		ArrayList<T> result = new ArrayList<T>() ;
		AbstractJavaNode classBodyNode
				= classNode.getFirstChildOfType(ASTClassOrInterfaceBody.class);
		if(classBodyNode==null)
			classBodyNode=classNode.getFirstChildOfType(ASTEnumBody.class) ; 
		
		List<ASTClassOrInterfaceBodyDeclaration> declarations
				= classBodyNode.findChildrenOfType(ASTClassOrInterfaceBodyDeclaration.class);
		for(ASTClassOrInterfaceBodyDeclaration decl : declarations){
			T node = decl.getFirstChildOfType(clazz);
			if(node != null)
				result.add( node ) ;
		}
		return result;
	}
	
	private List<ASTClassOrInterfaceBodyDeclaration> getBodyDeclarations(
			AbstractJavaNode classNode,
			Class<? extends AbstractJavaNode> clazz )
	{
		ArrayList<ASTClassOrInterfaceBodyDeclaration> result
				= new ArrayList<ASTClassOrInterfaceBodyDeclaration>() ;
		
		AbstractJavaNode classBodyNode = classNode.getFirstChildOfType(ASTClassOrInterfaceBody.class);
		if(classBodyNode == null)
			classBodyNode = classNode.getFirstChildOfType(ASTEnumBody.class);
		
		List<ASTClassOrInterfaceBodyDeclaration> declarations
				= classBodyNode.findChildrenOfType(ASTClassOrInterfaceBodyDeclaration.class);
		for(ASTClassOrInterfaceBodyDeclaration decl : declarations){
			AbstractJavaNode node = decl.getFirstChildOfType(clazz);
			if(node != null)
				result.add( decl ) ;
		}
		return result;
	}



	private void processNestedClasses(
			IJavaTypeDeclaration decl, List<ASTClassOrInterfaceBodyDeclaration> nestedClassNodes )
	{
		if( nestedClassNodes == null || nestedClassNodes.isEmpty() )
			return ;

		ArrayList<IJavaTypeDeclaration> lst = new ArrayList<IJavaTypeDeclaration>() ;
		for( ASTClassOrInterfaceBodyDeclaration obj : nestedClassNodes ){		
			
			IClassOutline outline = extractOutline(obj, decl ) ;
			
			IJavaTypeDeclaration nestedTypeDeclaration = outline.getTypeDeclaration();
			lst.add(nestedTypeDeclaration) ;
		}
		decl.setNestedClasses(lst) ;		
	}


	protected ArrayList<IJavaMethodDeclaration> extractMethods(
			List<ASTClassOrInterfaceBodyDeclaration> methodDeclNodes, IJavaTypeDeclaration owner) {
		
		ArrayList<IJavaMethodDeclaration> result = new ArrayList<IJavaMethodDeclaration>() ;
		if( methodDeclNodes == null || methodDeclNodes.isEmpty() )
			return result ;
		
		for( ASTClassOrInterfaceBodyDeclaration bodyDeclNode : methodDeclNodes )
		{
			ASTMethodDeclaration md = bodyDeclNode.getFirstDescendantOfType(ASTMethodDeclaration.class) ;			
			IJavaMethodDeclaration jmd = extractSingleMethod(md,owner);
			
			if(jmd == null)
				continue ;
			
			List<IJavaAnnotation> annotations = extractAnnotations(bodyDeclNode, owner);
			jmd.setAnnotations(annotations) ;
			
			result.add(jmd) ;
		}
		
		return result ;
	}

	protected ArrayList<IJavaFieldDeclaration> extractFields(
			List<ASTClassOrInterfaceBodyDeclaration> fieldDeclNodes,
			IJavaTypeDeclaration owner ) {	
		
		ArrayList<IJavaFieldDeclaration> result = new ArrayList<IJavaFieldDeclaration>() ;
		if( fieldDeclNodes == null || fieldDeclNodes.isEmpty() )
			return result ;
		
		for( ASTClassOrInterfaceBodyDeclaration bodyDeclNode : fieldDeclNodes  ){
			
			ASTFieldDeclaration fd = bodyDeclNode.getFirstDescendantOfType(ASTFieldDeclaration.class) ;			
			IJavaFieldDeclaration jfd = extractSingleField(fd,owner);
			if(jfd == null)
				continue ;
			
			List<IJavaAnnotation> annotations = extractAnnotations(bodyDeclNode, owner);
			jfd.setAnnotations(annotations) ;
			result.add(jfd) ;
		}
		return result ;
	}


	private IJavaFieldDeclaration extractSingleField(
			ASTFieldDeclaration fd,
			IJavaTypeDeclaration owner ) {
		
		IJavaFieldDeclaration result = new JavaFieldDeclaration() ;
		result.setLines( fd.getBeginLine(), fd.getEndLine() ) ;
		result.setOwner(owner) ;
		
		result.setFinal(fd.isFinal()) ;
		result.setStatic( fd.isStatic() ) ;
		result.setTransient( fd.isTransient() ) ;
		
		if( fd.isPublic() )
			result.setPublic() ;
		else if ( fd.isProtected() )
			result.setProtected() ;
		else if (fd.isPrivate())
			result.setPrivate() ;
		else 
			result.setDefault() ;		
		
		String fieldName = fd.getVariableName() ;
		//qualifiedNmae existence is guaranteed here
		QualifiedName qName = new QualifiedName( owner.getQualifiedName().getValues().iterator().next()+"."+fieldName ) ;
		
		result.setSimpleName(fieldName) ;			
		result.setQualifiedName(qName) ;
		result.setPackage( this.packageName ) ;	
		
		IJavaType fieldType = null ;

		ASTType typeNode = fd.getFirstDescendantOfType(ASTType.class);
		fieldType = this.typeResolvingService.extractTypeFromNode(typeNode, owner );
		
		result.setType(fieldType) ;
		return result;
	}
	
	private IJavaMethodDeclaration extractSingleMethod(ASTMethodDeclaration md,
			IJavaTypeDeclaration owner) {		
		
		IJavaMethodDeclaration result = new JavaMethodDeclaration() ;
		result.setLines( md.getBeginLine(), md.getEndLine() ) ;
		result.setOwner(owner) ;
		
		result.setPackage(this.packageName) ;
		result.setFinal(md.isFinal()) ;
		result.setAbstract(md.isAbstract()) ;
		result.setStatic(md.isStatic()) ;
		
		if(md.isPublic())
			result.setPublic() ;
		else if( md.isPrivate() )
			result.setPrivate() ;
		else if( md.isProtected() )
			result.setProtected() ;
		else 
			result.setDefault() ;
		
		String simpleName = md.getMethodName() ;
		//qualifiedNmae existence is guaranteed here
		QualifiedName qualifiedName = new QualifiedName(owner.getQualifiedName().getValues().iterator().next() + "." + simpleName );
		
		result.setSimpleName(simpleName) ;
		result.setQualifiedName(qualifiedName) ;
		
		extractGenericParams( md, result) ;
		
		extractReturnTypeAndParameters( md, result ) ;		
		
		extractThrownExceptions( md, result ) ;
		return result;
	}
	
	private void extractThrownExceptions( AbstractJavaAccessNode md, IJavaMethodDeclaration result)
	{
		ASTNameList namesListNode = md.getFirstChildOfType(ASTNameList.class);
		if(namesListNode == null)
			return ;
		
		List<ASTName> namesList = namesListNode.findChildrenOfType(ASTName.class);
		if( namesList == null || namesList.isEmpty() )
			return ;
		
		ArrayList<IJavaType> exceptions = new ArrayList<IJavaType>() ;
		for( ASTName nameNode : namesList ){
			String exception = nameNode.getImage() ;
			IJavaType type = typeResolvingService.obtainType(exception, result, true);
			exceptions.add(type) ;
		}
		result.setExceptions(exceptions) ;
	}

	private IJavaMethodDeclaration extractSingleConstructor(ASTConstructorDeclaration fd,
			IJavaTypeDeclaration owner) {		
		
		IJavaMethodDeclaration result = new JavaMethodDeclaration() ;
		result.setLines( fd.getBeginLine(), fd.getEndLine() ) ;
		result.setOwner(owner) ;
		
		result.setPackage(this.packageName) ;
		result.setFinal(fd.isFinal()) ;
		result.setAbstract(fd.isAbstract()) ;
		result.setStatic(fd.isStatic()) ;
		
		if(fd.isPublic())
			result.setPublic() ;
		else if( fd.isPrivate() )
			result.setPrivate() ;
		else if( fd.isProtected() )
			result.setProtected() ;
		else 
			result.setDefault() ;
		
		//String simpleName = fd.getMethodName() ;
		//String qualifiedName = owner.getQualifiedName() + "." + simpleName ;
		
		//result.setSimpleName(simpleName) ;
		//result.setQualifiedName(qualifiedName) ;
		
		extractGenericParams( fd, result) ;
		
		extractReturnTypeAndParameters( fd, result ) ;
		
		extractThrownExceptions(fd, result) ;
		
		return result;
	}

	private void extractReturnTypeAndParameters(
			AbstractJavaAccessNode md,
			IJavaMethodDeclaration ownerMethod )
	{		
		ASTResultType resultNode
				= md.getFirstChildOfType(ASTResultType.class );
		
		if( resultNode != null ){
			//Constructors have their result nodes == null
			ASTType returnTypeNode
					= resultNode.getFirstChildOfType(ASTType.class);
		
			if(returnTypeNode != null){
				IJavaType returnType = this.typeResolvingService.extractTypeFromNode(returnTypeNode, ownerMethod);
				ownerMethod.setReturnType(returnType) ;
			}
		}
			
		ASTFormalParameters paramsNode
				= md.getFirstDescendantOfType(ASTFormalParameters.class);
			
		List<ASTFormalParameter> params
				= paramsNode.findChildrenOfType(ASTFormalParameter.class);
			
		ArrayList<IJavaType> paramTypes = new ArrayList<IJavaType>() ;
		ArrayList<String> paramNames = new ArrayList<String>() ;
		for( ASTFormalParameter paramNode : params ){
			ASTType paramTypeNode = paramNode.getFirstChildOfType(ASTType.class);			
			IJavaType paramType = this.typeResolvingService.extractTypeFromNode(paramTypeNode, ownerMethod);
			paramTypes.add(paramType) ;
			
			ASTVariableDeclaratorId declaratorNode = paramNode.getFirstChildOfType(ASTVariableDeclaratorId.class);
			String paramName = declaratorNode.getImage() ;
			paramNames.add(paramName) ;
		}
		ownerMethod.setParameters(paramTypes) ;
		ownerMethod.setParameterNames(paramNames) ;
	}

	
	private static HashMap<String,String> arrayNamingMap = new HashMap<String, String>() ;
	static{
		arrayNamingMap.put("Z", boolean.class.getSimpleName() ) ;
		arrayNamingMap.put("B", byte.class.getSimpleName() ) ;
		arrayNamingMap.put("C", char.class.getSimpleName() ) ;
		arrayNamingMap.put("D", double.class.getSimpleName() ) ;
		arrayNamingMap.put("F", float.class.getSimpleName() ) ;
		arrayNamingMap.put("I", int.class.getSimpleName() ) ;
		arrayNamingMap.put("J", long.class.getSimpleName() ) ;
		arrayNamingMap.put("S", short.class.getSimpleName() ) ;
	}
	
	protected void extractImports(List<ASTImportDeclaration> imports) {
		
		if(imports==null)
			return ;
		
		Map<String,String> importsMap = typeResolvingService.getImportsMap() ;
		for( ASTImportDeclaration imp : imports ){
				
			String iName = imp.getImportedName();
			int ind = iName.lastIndexOf(".") ;
				
				
			if(imp.isImportOnDemand()){
				this.packageImports.add(iName) ;
			}
			else{
				String sName = ind >= 0 ? iName.substring(ind+1) : iName ;
				importsMap.put(sName, iName) ;
			}
		}
	}

	private IJavaTypeDeclaration extractTypeDeclaration(
			AbstractJavaAccessTypeNode cd, IJavaTypeDeclaration parent)
	{		
		String simpleName = cd.getImage();
		String qualifiedNameString = composeQualifiedName(cd, parent);
		
		IJavaTypeDeclaration result
				= this.outlineContainer.getTypeDeclaration(qualifiedNameString) ;
		
		if( result == null ){
			
			result = new JavaTypeDeclaration() ;
			result.setSimpleName(simpleName) ;			
			result.setQualifiedName( new QualifiedName(qualifiedNameString)) ;
			this.outlineContainer.putTypeDeclaration(result) ;
		}
		
		if( cd.isPublic() )
			result.setPublic() ;
		else if ( cd.isProtected() )
			result.setProtected() ;
		else if (cd.isPrivate())
			result.setPrivate() ;
		else 
			result.setDefault() ;	
		
		result.setFinal(cd.isFinal()) ;
		result.setStatic( cd.isStatic() ) ;
		result.setPrimitive(false) ;
		result.setAbstract( cd.isAbstract() ) ;
		
		result.setPackage( packageName ) ;
		
		extractGenericParams(cd, result);
		if(cd instanceof ASTClassOrInterfaceDeclaration){
			
			ASTClassOrInterfaceDeclaration cd_ = (ASTClassOrInterfaceDeclaration) cd ;
			result.setIsInterface( cd_.isInterface() ) ;
			extractSuperClass(cd_,result) ;
			extractSuperInterfaces(cd_,result) ;			
		}
		else if (cd instanceof ASTEnumDeclaration){
			ASTEnumDeclaration ed = (ASTEnumDeclaration) cd ;
			result.setIsEnum(true) ;
			List<IEnumConstant> cList = extractEnumConstants(ed) ;
			result.setEnumConstants(cList) ;				
		}
		
		return result ;
	}

	private List<IEnumConstant> extractEnumConstants(ASTEnumDeclaration ed) {
		
		ArrayList<IEnumConstant> result = new ArrayList<IEnumConstant>() ;
		while(true){			
			ASTEnumBody bodyNode = ed.getFirstChildOfType(ASTEnumBody.class);
			if(bodyNode==null)
				break ;
			
			List<ASTEnumConstant> ecList = bodyNode.findChildrenOfType(ASTEnumConstant.class);
			if(ecList == null)
				break ;
			
			for(ASTEnumConstant ecNode : ecList){
				
				String im = ecNode.getImage() ;
				EnumConstant ec = new EnumConstant() ;
				ec.setName(im) ;
				result.add(ec) ;
			}
		
			break ;
		}
		return result;
	}

	private void extractSuperClass(
			ASTClassOrInterfaceDeclaration declNode,
			IJavaTypeDeclaration declaration )
	{
		ASTExtendsList extendsNode
				= declNode.getFirstChildOfType(ASTExtendsList.class);
		
		if(extendsNode == null)
			return ;
		
		ASTClassOrInterfaceType parentTypeNode
				= extendsNode.getFirstChildOfType( ASTClassOrInterfaceType.class) ;
		
		if( parentTypeNode == null)
			return ;
		
		IJavaType superType = this.typeResolvingService.obtainType( parentTypeNode, declaration, false );
		declaration.setSuperClass(superType) ;
		superType.getDeclaration().addSubclass(declaration) ;
	}

	private void extractSuperInterfaces(
			ASTClassOrInterfaceDeclaration declNode,
			IJavaTypeDeclaration declaration )
	{
		AbstractJavaNode implementsNode	= declaration.isInterface()
				? declNode.getFirstChildOfType(ASTExtendsList.class)
				: declNode.getFirstChildOfType(ASTImplementsList.class);

		if(implementsNode == null)
			return ;

		List<ASTClassOrInterfaceType> interfaceTypeNodes
				= implementsNode.findChildrenOfType( ASTClassOrInterfaceType.class);

		if(interfaceTypeNodes == null || interfaceTypeNodes.isEmpty())
			return ;

		ArrayList<IJavaType> interfaces =  new ArrayList<IJavaType>() ;
		for( ASTClassOrInterfaceType node : interfaceTypeNodes ){
			IJavaType interfaceType = this.typeResolvingService.obtainType( node, declaration, false );
			interfaceType.getDeclaration().setIsInterface(true) ;
			interfaces.add(interfaceType) ;			
		}
		
		if(interfaces.isEmpty())
			return ;
		
		declaration.setInterfaces(interfaces) ;
		for(IJavaType iFace : interfaces ){
			iFace.getDeclaration().addSubclass(declaration) ;
		}
	}

	private String composeQualifiedName(AbstractJavaAccessTypeNode classNode, IJavaTypeDeclaration parent)
	{
		String simpleName = classNode.getImage();
		
		String qualifiedName ;
		if( parent == null ){
			qualifiedName = packageName+ "." + simpleName ;
		}
		else{
			//qualifiedNmae existence is guaranteed here
			qualifiedName = parent.getQualifiedName().getValues().iterator().next() + "." + simpleName ;
		}
		return qualifiedName;
	}


	private void extractGenericParams( AbstractJavaAccessNode node,	ICanBeGeneric result) {
		ASTTypeParameters genericParams = node.getFirstDescendantOfType(ASTTypeParameters.class);
		if( genericParams != null ){
			List<ASTTypeParameter> params = genericParams.findChildrenOfType( ASTTypeParameter.class);
			if( params != null && !params.isEmpty() ){
				ArrayList<String> lst = new ArrayList<String>() ;
				for( ASTTypeParameter param : params ){
					String paramName = param.getImage();
					lst.add(paramName) ;					
				}
				result.setGenericParamNames(lst) ;
				result.setGenericParamsCount(lst.size()) ;
			}
		}
	}


	private void extractPackageName(ASTCompilationUnit node) {
		
		String pn = "" ;
		ASTPackageDeclaration packageDeclaration = node.getPackageDeclaration();
		if(packageDeclaration!=null)
			pn = packageDeclaration.getPackageNameImage() ;
		
		this.packageName = pn ;
	}

	private void clearAll() {
		this.typeResolvingService.getImportsMap().clear() ;
		this.filePath = null ;
		this.packageImports.clear() ;
	}	

	public IOutlineContainer getOutlineContainer() {
		return outlineContainer;
	}

	public void setOutlineContainer(IOutlineContainer outlineContainer) {
		this.outlineContainer = outlineContainer;
	}
	
	public void resolveUndefinedTypes(){
		if( outlineContainer.isComplete() )
			return ;
		
		outlineContainer.setComplete(true) ;
		
		List<IJavaTypeDeclaration> lst = outlineContainer.getUnresolvedDeclarations();
		for( IJavaTypeDeclaration dec: lst ){
			QualifiedName qName = dec.getQualifiedName();
			String simpleName = qName.getSimpleName();
			
			String ctxName = qName.getContextQualifiedName();
			
			IClassOutline outline = outlineContainer.getClassOutline(ctxName);
			String packageName = outline.getTypeDeclaration().getPackage();
			
			String possibleName = packageName+"."+qName.getSimpleName() ;
			IClassOutline ol = outlineContainer.getClassOutline(possibleName) ;
			if(ol != null ){
				ProxyTypeDeclaration proxy = (ProxyTypeDeclaration) dec ;
				proxy.setReferencedDeclaration(ol.getTypeDeclaration()) ;
				continue ;
			}
			
			List<String> packageImports2 = outline.getPackageImports();
			if(packageImports2 == null)
				continue ;
			
			ArrayList<String> values = new ArrayList<String>() ;
			for(String s : packageImports2)
				values.add(s+"."+simpleName) ;
			
			dec.setQualifiedName( new QualifiedName(simpleName, values)) ;
		}
	}
	

}
