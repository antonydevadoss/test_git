package net.junos.space.pmd.rules.j2ee;

import java.util.List;

import net.junos.space.pmd.utils.outline.AbstractOutlineAccessor;
import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IJavaMethodDeclaration;
import net.junos.space.pmd.utils.outline.IJavaType;
import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;
import net.junos.space.pmd.utils.outline.QualifiedName;
import net.sourceforge.pmd.RuleContext;

public class EJBHomeInterfaceCreateMethodRule extends AbstractOutlineAccessor {
	
	
	@Override
	public void doEnd(RuleContext ctx) {
		for (IClassOutline outline : getOutlineContainer().getAllOutlines()) {
			IJavaTypeDeclaration typeDeclaration = outline.getTypeDeclaration();
			boolean isHomeInterfase = false;
			for (IJavaTypeDeclaration interfaceType : typeDeclaration.getAllInterfaces()) {
				if (interfaceType.getQualifiedName().getValues().contains("javax.ejb.EJBHome")) {
					isHomeInterfase = true;
					break;
				}
			}
			
			if (isHomeInterfase) {
l0:				for (IJavaMethodDeclaration method : outline.getMethods()) {
					if (!method.getSimpleName().equals("create"))
						continue ;
					
					List<IJavaType> exceptions = method.getExceptions();
					if (exceptions == null ){
						reportError(ctx,outline,method);
						continue ;
					}
					
					for(IJavaType ex : exceptions){
						
						QualifiedName qName = ex.getQualifiedName();
						if(qName == null){
							if( "CreateException".equals(ex.getSimpleName()) )
								continue l0;								
						}
						else{
							if( qName.getValues().contains("javax.ejb.CreateException") )
								continue l0;									
						}
					}
					reportError(ctx, outline, method);
				}
			}
		}
		super.doEnd(ctx);
	}
	
	@Override
	public String getMessage() {		
		return "Make a create method of an EJB Home Interface throw an javax.ejb.CreateException";
	}
}
