package net.junos.space.pmd.rules.j2ee;


import org.w3c.dom.Element;
import net.junos.space.pmd.rules.xml.AbstractXMLRule;
import net.junos.space.pmd.rules.xml.SimpleXpathRule;

public class UseCorrectNameForAnEnterpriseApplicationDisplayNameRule extends SimpleXpathRule {
	
	

	@Override
	protected String getXpathExpression() {
		return "//application/display-name";
	}

	public String getMessage() {
		return "Use A Correct Name For An Enterprise Application Display Name. The enterprise application display name within the deployment descriptor is the application name, writtenin mixed cases, with a suffix EAR.";
	}

	protected String getTargetXMLName() {
		return AbstractXMLRule.XML_NAME_APPLICATION;
	}

	@Override
	protected boolean isValid(Element element) {
		
		String name = element.getTextContent();
		if (!name.endsWith("EAR")) {
			return false;
		}
		if (!Character.isUpperCase(name.toCharArray()[0])) {
			return false;
		}
		String pName = getProjectName() ;
		if(pName != null){
			String pName_lc = pName.toLowerCase() ;
			String name_lc = name.toLowerCase();
			if( !(name_lc.equals(pName_lc) || name_lc.equals(pName_lc+"ear")) ){
				return false ;				
			}
		}
		return true;
	}
}
