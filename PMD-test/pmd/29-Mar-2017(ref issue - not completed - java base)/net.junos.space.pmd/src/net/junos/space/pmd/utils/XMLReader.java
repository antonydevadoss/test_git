/*******************************************************************************
 * FILE NAME: XMLReader.java
 * PURPOSE:  This abstract class gets the location of XML file.
 * 			 
 *
 *
 * Revision History: 
 * DATE:           				  AUTHOR:              	CHANGE:  
 * Mar 03, 2011		              Amit Arora	       	Initial Version  
 * 
 * 
 ******************************************************************************/
package net.junos.space.pmd.utils;

/**
 * Abstract class responsible for getting location of XML file. This has one
 * abstract operation 'parseXML'. This has been implemented by WebXMLReader and
 * ModuleXMLReader.
 */
public abstract class XMLReader {

	/**
	 * This prepares the location of XML file from following parameters
	 * 
	 * @param projectType - Type of project
	 * @param directory - Directory
	 * @param fileName - Name of file
	 * @param includeProjectNameInFile - include project name in file
	 * @return
	 */
	protected String getXMLFilePath(String projectType, String directory, String fileName, boolean includeProjectNameInFile) {		
		Project project = new Project(projectType);
		String path = project.getWorkSpaceLocation();		
		String result = "";
		if (project != null) {
			if(includeProjectNameInFile)
				result = "/" + project.getProjectName() + directory + project.getProjectName() + fileName;
			else	
				result = "/" + project.getProjectName() + directory + fileName;						
		}
		if (!result.isEmpty()) {
			path = path + result;
		}
		return path;
	}
	
	/**
	 * Parse XML
	 */
	protected abstract void parseXML();
	
}
