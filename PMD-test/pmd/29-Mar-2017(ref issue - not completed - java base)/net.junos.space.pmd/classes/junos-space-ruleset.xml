<?xml version="1.0"?>

<ruleset name="Junos Space Rules"
    xmlns="http://pmd.sf.net/ruleset/1.0.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://pmd.sf.net/ruleset/1.0.0 http://pmd.sf.net/ruleset_xml_schema.xsd"
    xsi:noNamespaceSchemaLocation="http://pmd.sf.net/ruleset_xml_schema.xsd">
  
	<description>JunosSpace Rules are used to describe the rules for Junos Space SDK PMD plugin</description>

	<rule name="MissingHttpMethodRule"
			since="JS-0.1"
		  message="Missing Http Method Annotation - REST Method is not having any Http method annotation(@GET/@PUT/@DELETE/@POST)."
		  class="net.junos.space.pmd.rules.rest.MissingHttpMethodRule">          
	  <description>
		This rule verifies that Http Method annotation(@GET/@POST/@PUT/@DELETE) should be present on every rest method which is annotated with @Path/@Produces/@Consumes annotation.
	  </description>
	  <priority>1</priority>
	  <example>
		<![CDATA[
			@Path("/")
			public interface HelloWorldIf {

				//no @GET/@POST/@PUT/@DELETE exists
				@Path("/countries")
				public List<Country> getCountries(); 
			}
		]]>
	 </example>
	</rule>
	
	
	<rule name="PathExistenceRule"
			since="JS-0.1"
          message="Missing @Path Annotation - @Path annotation should present on enclosing rest Interface/Class."
          class="net.junos.space.pmd.rules.rest.PathExistenceRule">
      <description>
		This rule verifies that @Path annotation should be present on class/interface of every rest method.
      </description>
      <priority>1</priority>
      <example>
		<![CDATA[
			//no @Path exists
			public interface HelloWorldIf {

				@GET
				@Path("/countries")
				public List<Country> getCountries(); 
			}
		]]>
      </example>
    </rule>
	
	
	<rule name="MissingProducesRule"
    		 since="JS-0.1"
          message="Missing @Produces Annotation - @Produces Annotation should be present on method or on Interface/Class."
          class="net.junos.space.pmd.rules.rest.MissingProducesRule">
      <description>
		This rule verifies that if REST method is returning DTO, then @Produces annotation should be present either on REST method or on Interface/Class.
      </description>
      <priority>1</priority>
      <example>
		<![CDATA[			
			@Path("/")
			//no @Produce exists (either in class level or method level)
			public interface HelloWorldIf {

				@GET
				@Path("/countries")
				//no @Produce exists (either in class level or method level)
				public List<Country> getCountries(); 
			}
		]]>
      </example>
    </rule>
	
	
	<rule name="AvoidProducesRule"
    		 since="JS-0.1"
          message="Invalid @Produces Usage - No need to use @Produces annotation on REST method as it is not returning any DTO."
          class="net.junos.space.pmd.rules.rest.AvoidProducesRule">
      <description>
		This rule verifies that if REST method is not returning anything (return type is 'void'), then there is no need to use @Produces annotation on REST method.
      </description>
      <priority>3</priority>
      <example>
		<![CDATA[
			@Path("/")
			public interface HelloWorldIf {

				@GET
				@Path("/countries")
				@Produces("application/json") // no need to use @Produces here
				public void showDetails(); 
			}
		]]>
      </example>
    </rule>
	
	
	<rule name="MissingConsumesRule"
    		 since="JS-0.1"
          message="Missing @Consumes Annotation - @Consumes should be present on method or on Interface/Class."
          class="net.junos.space.pmd.rules.rest.MissingConsumesRule">
      <description>
		This rule verifies that if REST method is expecting one DTO as one of the input parameter, then @Consumes annotation should be present either on method or on Interface/Class.
      </description>
      <priority>1</priority>
      <example>
		<![CDATA[
			@Path("/")
			// no @Consumes annotation (either on class level or method level)
			public interface HelloWorldIf {

				@POST
				@Path("/country")
				// no @Consumes annotation (either on class level or method level)
				public void addCountry(Country param0); 
			}
		]]>
      </example>
    </rule>
	
	
	<rule name="AvoidConsumesForNoParameterRule"
    		 since="JS-0.1"
          message="Invalid @Consumes Usage - No need to use @Consumes annotation on method as method is not expecting any input."
          class="net.junos.space.pmd.rules.rest.AvoidConsumesForNoParameterRule">
      <description>
		This rule verifies that if REST method is not expecting any input, then there is no need to have @Consumes annotation on REST method.
      </description>
      <priority>3</priority>
      <example>
		<![CDATA[
			@Path("/")
			public interface HelloWorldIf {

				@POST
				@Path("/country")
				@Consumes("application/xml") //no need to use @Consumes here
				public void country(); 
			}
		]]>
      </example>
    </rule>
	
	
	<rule name="AvoidConsumesForAnnotationParameterOnlyRule"
    		 since="JS-0.1"
          message="Invalid @Consumes Usage - No need to use @Consumes annotation as method is expecting only parameter annotations as input."
          class="net.junos.space.pmd.rules.rest.AvoidConsumesForAnnotationParameterOnlyRule">
      <description>
		This rule verifies that if REST method is expecting only @PathParam or/and @QueryParam as input parameter(s), then there is no need to have @Consumes annotation on REST method.
      </description>
      <priority>3</priority>
      <example>
		<![CDATA[
			@Path("/")
			public interface HelloWorldIf {
				
				@POST
				@Path("/country/{countryid}")
				@Consumes("application/xml") // no need to use @Consumes here
				public void country(@PathParam("countryid") int param0); 
			}
		]]>
      </example>
    </rule>
	
	
	<rule name="InvalidRESTMethodRule"
    		 since="JS-0.1"
          message="Invalid REST method - Only one DTO is allowed as input parameter of REST method."
          class="net.junos.space.pmd.rules.rest.InvalidRESTMethodRule">
      <description>
		This rule verifies that REST method should not have more than one DTO as input parameter.
      </description>
      <priority>1</priority>
      <example>
		<![CDATA[
			@Path("/")
			public interface HelloWorldIf {

				@POST
				@Path("/country/{countryid}")
				@Consumes("application/xml") 
				public void addCountry(Country param0, State param1); //more than one DTO! 
			}
		]]>
      </example>
    </rule>
	
	
	<rule name="InvalidPathParamRule"
    		 since="JS-0.1"
          message="Invalid @PathParam annotation - @Pathparam attribute is not present in @Path annotation of method or interface/class."
          class="net.junos.space.pmd.rules.rest.InvalidPathParamRule">
      <description>
		This rule verifies that if REST method is expecting @PathParam as input parameter, then attribute of @PathParam should be also present within curly braces of @Path annotation of REST method or class.
      </description>
      <priority>1</priority>
      <example>
		<![CDATA[
			@Path("/")	//no @Path("/{countryid}") exists (either on class level or method level)			
			public interface HelloWorldIf {

				@GET 
				@Path("/country") //no @Path("/country/{countryid}") exists (either on class level or method level)
				@Produces("application/json")				
				public Country getCountry(@PathParam("countryid") int param0); 
			}
		]]>
      </example>
    </rule>
	
	
	<rule name="InvalidPathRule"
    		 since="JS-0.1"
          message="Invalid @Path annotation - @PathParam's attribute should not be used more than once in @Path annotation of method or class/interface."
          class="net.junos.space.pmd.rules.rest.InvalidPathRule">
      <description>
		This rule verifies that if REST method is expecting @PathParam as input parameter, then the attribute should not be repeated in @Path annotation.
      </description>
      <priority>3</priority>
      <example>
		<![CDATA[
			@Path("/helloworld/{id}") // Path Param identifier is repeated (either on class level or method level)
			public interface HelloWorldIf {

				@GET 
				@Path("/country/{id}") // Path Param identifier is repeated (either on class level or method level)
				@Produces("application/json")	
				public Country getCountry(@PathParam("id") int param0); 
				
				@GET 
				@Path("/country/{id}/state/{id}") // Path Param identifier is repeated (either on class level or method level) 
				@Produces("application/json")	
				public State getState(@PathParam("id") int param0); 
			}
		]]>
      </example>
    </rule>
	
	
	<rule name="MissingPathParamRule"
    		 since="JS-0.1"
          message="Missing @PathParam Annotation - @PathParam annotation may be defined as one of input parameter of method."
          class="net.junos.space.pmd.rules.rest.MissingPathParamRule">
      <description>
		This rule verifies that if REST method is not expecting @PathParam as input parameter, then attribute in @Path (on method or class) should not have path param identifier with curly braces.
      </description>
      <priority>3</priority>
      <example>
		<![CDATA[
			@Path("/helloworld/{stateId}") // there is no @PathParam in method matching identifier in method
			public interface HelloWorldIf {

				@GET 
				@Path("/country/{countryId}") // there is no @PathParam in method matching identifier in method
				@Produces("application/json")	
				public Country getDefaultCountry(); 
			}
		]]>
      </example>
    </rule>
	
	
	<rule name="CapabilityValidityRule"
    		 since="JS-0.1"
          message="Invalid capability - Capability defined in @RBAC annotation on REST method is not present in module.xml."
          class="net.junos.space.pmd.rules.rest.CapabilityValidityRule">
      <description>
		This rule verifies that if @RBAC annotation is defined with capability on REST method, then that capability should exists in module.xml.
      </description>
      <priority>3</priority>
      <example>
		<![CDATA[
			@Path("/")
			public interface HelloWorldIf {

				@GET
				@Path("/countries") 
				@Produces("application/json")
				@RBAC(type = { CRUDEnum.CREATE }, capability = { "HelloWorldCap" }) //wrong usage if capability "HelloWorldCap" does not exist in module.xml
				public List<Country> getCountries();  
			}
		]]>
      </example>
    </rule>
	
	
	<rule name="MissingRBACRule"
    		 since="JS-0.1"
          message="Missing @RBAC annotation - @RBAC annotation may be present on method."
          class="net.junos.space.pmd.rules.rest.MissingRBACRule">
      <description>
		This rule verifies that @RBAC annotation should be present on REST method.
      </description>
      <priority>5</priority>
      <example>
		<![CDATA[
			@Path("/")
			public interface HelloWorldIf {

				@GET 
				@Path("/countries") 
				@Produces("application/json")
				// no @RBAC(type = { CRUDEnum.CREATE }, capability = { "HelloWorldCap" }) exists
				public List<Country> getCountries(); 
			}
		]]>
      </example>
    </rule>
	
	
	<rule name="InvalidMethodRule"
    		 since="JS-0.1"
          message="Inconsistency in method name and HTTP method type."
          class="net.junos.space.pmd.rules.rest.InvalidMethodRule">
      <description>
		This rule verifies that 
			if REST method starts with 'add...' then Http method would be POST 
			if REST method starts with 'get...' then Http method would be GET
			if REST method starts with 'update...' then Http method would be PUT
			if REST method starts with 'delete...' then Http method would be DELETE
      </description>
      <priority>5</priority>
      <example>
		<![CDATA[
			@Path("/")
			public interface HelloWorldIf {

				@POST // should be @GET
				@Path("/countries") 
				@Produces("application/json")
				public List<Country> getCountries(); 
				
				@GET // should be @POST
				@Path("/country")
				@Consumes("application/json")
				public void addCountry(Country country);
				
				@PUT // should be @DELETE
				@Path("/country/{id}")
				public void deleteCountry(@PathParam("id") int param0);
				
				@DELETE // should be @PUT
				@Path("/country/{id}")
				@Consumes("application/json")
				public void updateCountry(Country param0, @PathParam("id") int param1);
			}
		]]>
      </example>
    </rule>
	
	
	<rule name="InconsistentRBACRule"
    		 since="JS-0.1"
          message="Inconsistency in method name and RBAC type."
          class="net.junos.space.pmd.rules.rest.InconsistentRBACRule">
      <description>
		This rule verifies that 
			if REST method starts with 'add...', then type in @RBAC would be CRUDEnum.CREATE 
			if REST method starts with 'get...', then type in @RBAC would be CRUDEnum.READ
			if REST method starts with 'update...', then type in @RBAC would be CRUDEnum.UPDATE
			if REST method starts with 'delete...', then type in @RBAC would be CRUDEnum.DELETE
      </description>
      <priority>5</priority>
      <example>
		<![CDATA[
			@Path("/")
			public interface HelloWorldIf {
				
				@GET
				@Path("/countries") 
				@Produces("application/json")
				@RBAC(type = { CRUDEnum.CREATE }, capability = { <someCapability> }) // should have CRUDEnum.READ
				public List<Country> getCountries(); 
				
				@POST
				@Path("/country")
				@Consumes("application/json")
				@RBAC(type = { CRUDEnum.READ }, capability = { <someCapability> }) // should have CRUDEnum.CREATE
				public void addCountry(Country country);
				
				@DELETE
				@Path("/country/{id}")
				@RBAC(type = { CRUDEnum.READ }, capability = { <someCapability> }) // should have CRUDEnum.DELETE
				public void deleteCountry(@PathParam("id") int param0);
				
				@PUT
				@Path("/country/{id}")
				@Consumes("application/json")
				@RBAC(type = { CRUDEnum.READ }, capability = { <someCapability> }) // should have CRUDEnum.UPDATE
				public void updateCountry(Country param0, @PathParam("id") int param1);
				
			}
		]]>
      </example>
    </rule>
	
	
	<rule name="ProducesMediaTypeValidationRule"
    		 since="JS-0.1"
          message="Invalid Media Type - Media Type defined in @Produces annotation should start with 'application/' for DTO 
		  return type."
          class="net.junos.space.pmd.rules.rest.ProducesMediaTypeValidationRule">
      <description>
		This rule verifies that if @Produces annotation defined on REST method and method is returning DTO, then media type in @Produces annotation should starts with 'application/'.
      </description>
      <priority>3</priority>
      <example>
		<![CDATA[
			@Path("/")
			public interface HelloWorldIf {

				@GET 
				@Path("/countries") 
				@Produces("app/json")	// media type should starts with 'application/'			
				public List<Country> getCountries(); 
			}
		]]>
      </example>
    </rule>
	
	
	<rule name="ConsumesMediaTypeValidationRule"
    		 since="JS-0.1"
          message="Invalid Media Type - Media Type defined in @Consumes annotation should start with 'application/' for DTO 
		  input parameter."
          class="net.junos.space.pmd.rules.rest.ConsumesMediaTypeValidationRule">
      <description>
		This rule verifies that if @Consumes annotation defined on REST method and method is expecting DTO as input, then media type in @Consumes annotation should starts with 'application/'.		
      </description>
      <priority>3</priority>
      <example>
		<![CDATA[
			@Path("/")
			public interface HelloWorldIf {

				@POST
				@Path("/country")
				@Consumes("app/json") // media type should starts with 'application/'				
				public void addCountry(Country param0);
			}
		]]>
      </example>
    </rule>
	
	
	<rule name="MissingCharsetInMediaTypeRule"
    		 since="JS-0.1"
          message="Invalid Media Type - Media Type defined in @Consumes annotation should have 'charset = UTF-8' encoding."
          class="net.junos.space.pmd.rules.rest.MissingCharsetInMediaTypeRule">
      <description>
		This rule verifies that if @Consumes annotation is defined on REST method and and media type contains semicolon (;), then media type should have ';charset="UTF-8"''.
      </description>
      <priority>5</priority>
      <example>
		<![CDATA[
			@Path("/")
			public interface HelloWorldIf {

				@POST
				@Path("/country")
				@Consumes("application/vnd.net.juniper.space.hello-world.state+json;version=1") // media type should be "application/vnd.net.juniper.space.hello-world.state+json;version=1;charset=UTF-8"
				public void addCountry(Country param0); 
			}
		]]>
      </example>
    </rule>
	
	<rule name="InvalidDTORule"
    		 since="JS-0.1"
          message="Invalid DTO - DTO should have @XmlRootElement annotation."
          class="net.junos.space.pmd.rules.dto.InvalidDTORule">
      <description>
		This rule states that if REST method is expecting or returning DTO, then DTO should have @XmlRootElement annotation
      </description>
      <priority>3</priority>
      <example>
		<![CDATA[
			@Path("/")
			public interface HelloWorldIf {

				@POST
				@Path("/country")
				@Consumes("application/vnd.net.juniper.space.hello-world.state+json;version=1;charset=UTF-8")
				public void addCountry(Country param0);  
			}
			
			//no @XmlRootElement exists
			public class Country {
			
			}
		]]>
      </example>
    </rule>
	
	<rule name="InvalidDeleteUsageRule"
    		 since="JS-0.1"
          message="Invalid @DELETE usage - @DELETE REST method neither requires DTO as input parameter nor requires @Consumes annotation."
          class="net.junos.space.pmd.rules.rest.InvalidDeleteUsageRule">
      <description>
		This rule verifies that if REST method has @DELETE annotation, then REST method should neither expect DTO nor have @Consumes annotation.
      </description>
      <priority>3</priority>
      <example>
		<![CDATA[
			@Path("/")
			public interface HelloWorldIf {

				@Path("country/{id}")				
				@Consumes("application/json) // no need to use @Consumes annotation
				@DELETE
				public void deleteCounntry(@PathParam("id") int param0, Country param1); // no need to use DTO as input 
			}
		]]>
      </example>
    </rule>
    
    <rule name="RestResourceNotDiscoverable"
    		 since="JS-0.1"
          message="Invalid REST Resource - REST Resource not discoverable by well known URL. InfoServiceInterceptor listener should be present after RestEasyBootStrap listener and resteasy.scan should be true in AppWebSvc/WEB-INF/web.xml."
          class="net.junos.space.pmd.rules.rest.RestResourceNotDiscoverableRule">
      <description>
		This rule verifies that web.xml in websvc project should have InfoServiceInterceptor listener defined after RestEasyBootStrap listener and value of resteasy.scan context parameter should be true.
      </description>
      <priority>3</priority>
      <example>
		<![CDATA[
			<web-app version="2.4">
			    <context-param>
			        <param-name>resteasy.scan</param-name>
			        <param-value>false</param-value>  <!-- should be there with true value --> 
			    </context-param>
			    
			    <listener>
			         <listener-class>net.juniper.jmp.interceptors.infoservice.InfoServiceInterceptor</listener-class>
			    </listener>
			    
			    <listener>
			        <listener-class>org.jboss.resteasy.plugins.server.servlet.ResteasyBootstrap</listener-class>
			    </listener>  <!-- should exists before InfoServiceInterceptor listener -->
			    
			</web-app>			
		]]>
      </example>
    </rule>
    
     <rule name="MoreThanOneHttpMethodRule"
    		 since="JS-0.1"
          message="Invalid REST Resource - More than one Http Method Annotation is present on REST method."
          class="net.junos.space.pmd.rules.rest.MoreThanOneHttpMethodAnnotationRule">
      <description>
		This rule verifies that more than one Http method annotation should not be present on REST method.
      </description>
      <priority>3</priority>
      <example>
		<![CDATA[
			@Path("/")
			public interface HelloWorldIf {

				@POST
				@GET   //more than one annotation exists (@POST and @GET exists)				 
				@Path("/country")
				@Consumes("application/vnd.net.juniper.space.hello-world.state+json;version=1;charset=UTF-8")
				public void addCountry(Country param0);  
			}
		]]>
      </example>
    </rule>
    
    <rule name="InvalidXMLAccessorTypeUsageRule"
    		 since="JS-0.1"
          message="Invalid @XMLAccessorType usage - There should be either @XmlElement or @XmlAttribute on any field or method of DTO, provided DTO is annotated with @XmlAccessorType with access type 'NONE' and with @XmlRootElement."
          class="net.junos.space.pmd.rules.dto.InvalidXMLAccessorTypeUsageRule">
      <description>
		This rule verifies that for every REST method which is expecting or returning DTO and that DTO is annotated with @XmlAccessorType(XmlAccessType.NONE) annotation, then that DTO should have atleast one field or method having @XmlElement or @XmlAttribute annotation on it.
	  </description>
      <priority>3</priority>
      <example>
		<![CDATA[
			@Path("/")
			public interface HelloWorldIf {

				@POST
				@Path("/country")
				@Consumes("application/vnd.net.juniper.space.hello-world.state+json;version=1;charset=UTF-8")
				public void addCountry(Country param0);  
			}
			
			@XmlRootElement
			@XmlAccessorType(XmlAccessType.NONE)
			public class Country {  // field or method not annotated with either @XmlElement or @XmlAttribute
			
				private Integer totalSize;

				public Integer getTotalSize() {
					return totalSize;
				}
			
			}
		]]>
      </example>
    </rule>
    
    <rule name="JxServiceLocatorLookupRule"
    		 since="JS-0.1"
          message="Bean not initialized appropriately - Use JxServiceLocator.doLookup to look up EJB."
          class="net.junos.space.pmd.rules.rest.JxServiceLocatorLookUpRule">
      <description>
		This rule verifies that REST implementation should have used JxServiceLocator.doLookup code for initialization of bean.
      </description>
      <priority>3</priority>
      <example>
		<![CDATA[
			@Path("/")
			public class HelloWorldImpl implements HelloWorldRest {
				net.juniper.jmp.helloworld.ejb.HelloWorld helloworld = null;
				//helloworld bean not initialized using JxServiceLocator.doLookup code.
				private vnd.jssdk.helloworld.HelloWorld getBean() {					if (helloworld == null) {						helloworld = JxServiceLocator								.doLookup("HelloWorld/HelloWorldEJB/remote");									return helloworld;				}				
			}
		]]>
      </example>
    </rule>
    
     <rule name="MissingSortableFilterableUsageRule"
    		 since="JS-0.1"
          message="Missing @Sortable/@Filterable usage in DTO - DTO does not have @Filterable or @Sortable annotation on any field or method, whereas REST API is supporting this feature on this DTO."
          class="net.junos.space.pmd.rules.dto.MissingSortableFilterableUsageRule">
      <description>
		This rule verifies that if REST method is supporting Sorting/Filtering/Paging and method is returning some DTO, then that DTO should have @Sortable or @Filterable annotation on any method or field.
      </description>
      <priority>3</priority>
      <example>
		<![CDATA[
			@Path("/")
			public interface HelloWorldIf {

				@GET
				@Path("/countries") 
				@Produces("application/json")
				@RBAC(type = { CRUDEnum.READ }, capability = { <someCapability> }) 
				public List<Country> getCountries(); 
			}
			
			@XmlRootElement
			@XmlAccessorType(XmlAccessType.NONE)
			public class Country {  // field or method not annotated with either @Sortable or @Filterable annotation
								
				private Integer totalSize; 

				public Integer getTotalSize() {
					return totalSize;
				}
			
			}
		]]>
      </example>
    </rule>
   
	<rule name="MissingRootLevelSchemaRule"
    	since="JS-0.1"
        message="Missing Root Level Schema - Root level schema is not present at appropriate location for this REST implementation."
        class="net.junos.space.pmd.rules.rest.MissingRootLevelSchemaRule">
      	<description>
			This rule verifies that for each REST implementation which has @XmlRootElement annotation and its REST interface has a root method, then there should be root level schema defined for this REST implementation.
	    </description>
      	<priority>3</priority>
      	<example>
		<![CDATA[

			@XmlRootElement
			public class HelloWorldImpl implements HelloWorldIf {
			
				public HelloWorld getRoot() {
					return new HelloWorldImpl();
				} 
			
			}

			@Path("/")
			public interface HelloWorldIf {

				@Path("/")
				@GET				
				@Produces({ "application/vnd.net.juniper.space.hello-world+json;version=1",
					"application/vnd.net.juniper.space.hello-world+xml;version=1" })
				public HelloWorld getRoot(); 

			}

			//There must be schema named as vnd.net.juniper.space.hello-world_v1.xsd located at WebSvcProject/web/WEB-INF/.			

		]]>
      </example>
    </rule>    </ruleset>	
