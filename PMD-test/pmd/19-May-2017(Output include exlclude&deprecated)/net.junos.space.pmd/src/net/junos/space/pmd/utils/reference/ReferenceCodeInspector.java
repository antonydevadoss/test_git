package net.junos.space.pmd.utils.reference;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;


import net.junos.space.pmd.utils.outline.AbstractOutlineAccessor;
import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IDeclaration;
import net.junos.space.pmd.utils.outline.IJavaFieldDeclaration;
import net.junos.space.pmd.utils.outline.IJavaMethodDeclaration;
import net.junos.space.pmd.utils.outline.IJavaType;
import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;
import net.junos.space.pmd.utils.outline.JavaType;
import net.junos.space.pmd.utils.outline.TypeResolvingService;
import net.junos.space.pmd.utils.reference.VariablesStack.VariableInfo;
import net.sourceforge.pmd.RuleContext;
import net.sourceforge.pmd.lang.ast.Node;
import net.sourceforge.pmd.lang.java.ast.ASTAllocationExpression;
import net.sourceforge.pmd.lang.java.ast.ASTArgumentList;
import net.sourceforge.pmd.lang.java.ast.ASTArguments;
import net.sourceforge.pmd.lang.java.ast.ASTBlock;
import net.sourceforge.pmd.lang.java.ast.ASTClassOrInterfaceBody;
import net.sourceforge.pmd.lang.java.ast.ASTClassOrInterfaceType;
import net.sourceforge.pmd.lang.java.ast.ASTExpression;
import net.sourceforge.pmd.lang.java.ast.ASTFieldDeclaration;
import net.sourceforge.pmd.lang.java.ast.ASTForStatement;
import net.sourceforge.pmd.lang.java.ast.ASTLiteral;
import net.sourceforge.pmd.lang.java.ast.ASTLocalVariableDeclaration;
import net.sourceforge.pmd.lang.java.ast.ASTName;
import net.sourceforge.pmd.lang.java.ast.ASTNullLiteral;
import net.sourceforge.pmd.lang.java.ast.ASTPrimaryExpression;
import net.sourceforge.pmd.lang.java.ast.ASTPrimaryPrefix;
import net.sourceforge.pmd.lang.java.ast.ASTPrimarySuffix;
import net.sourceforge.pmd.lang.java.ast.ASTPrimitiveType;
import net.sourceforge.pmd.lang.java.ast.ASTReferenceType;
import net.sourceforge.pmd.lang.java.ast.ASTStatementExpression;
import net.sourceforge.pmd.lang.java.ast.ASTType;
import net.sourceforge.pmd.lang.java.ast.ASTVariableDeclaratorId;
import net.sourceforge.pmd.lang.java.ast.AbstractJavaAccessNode;
import net.sourceforge.pmd.lang.java.ast.AbstractJavaNode;
import net.sourceforge.pmd.lang.java.ast.AbstractJavaTypeNode;

public class ReferenceCodeInspector {
	
	private static final String LINE_SEPARATOR = System.getProperty("line.separator");

	private static final String ERROR_MESSAGE_MISSING_PRIMARY_EXPRESSION_NODE = "Missing Primary Expression node.";

	private static final String ERROR_MESSAGE_UNKNOWN_LITERAL_CLASS = "Unknown literal class.";

	private static final String ERROR_MESSAGE_UNABLE_TO_MATCH_OWNER_CLASS = "Unable to match owner class";

	private static final String ERROR_MESSAGE_NO_NAME_OR_ALLOCATION_EXPRESSION = "Got PrimaryPrefix, no Name or AllocationExpression.";

	private static final String ERROR_MESSAGE_NO_PRIMARY_PREFIX = "Got PrimaryExpression, no PrimaryPrefix.";

	private static final String ERROR_MESSAGE_NO_LITERAL = "No PrimaryExpression, no Literal.";

	private TypeResolvingService typeResolvingService = new TypeResolvingService() ;
	
	private AbstractOutlineAccessor outlineAccessor ;
	
	private VariablesStack stack = new VariablesStack();
	
	private IClassOutline classOutline ;
	
	private IDeclaration declaration ;
	
	private RuleContext ruleContext ;
	
	private String errLogPath ;
	
	private HashSet<IReference> refSet = new HashSet<IReference>();
	
	public TypeResolvingService getTypeResolvingService() {
		return typeResolvingService;
	}

	public void setTypeResolvingService(TypeResolvingService typeResolvingService) {
		this.typeResolvingService = typeResolvingService;
	}

	public AbstractOutlineAccessor getOutlieAccessor() {
		return outlineAccessor;
	}

	public void setOutlieAccessor(AbstractOutlineAccessor outlieAccessor) {
		this.outlineAccessor = outlieAccessor;
	}

	public HashSet<IReference> getRefSet() {
		return refSet;
	}

	public void setRefSet(HashSet<IReference> refSet) {
		this.refSet = refSet;
	}

	public IClassOutline getClassOutline() {
		return classOutline;
	}

	public void setClassOutline(IClassOutline classOutline) {
		this.classOutline = classOutline;
	}

	public IDeclaration getDeclaration() {
		return declaration;
	}

	private void processVariable(ASTLocalVariableDeclaration varNode)
	{
		int beginLine = varNode.getBeginLine() ;
		int endLine = varNode.getEndLine() ;
		
		ASTVariableDeclaratorId declId = varNode.getFirstDescendantOfType(ASTVariableDeclaratorId.class);
		String varName = declId.getImage() ;
		
		ASTType typeNode = varNode.getFirstChildOfType(ASTType.class);		
		
		IJavaType type = resolveType(typeNode, declaration) ;
		IReference var ;
		VariableInfo info = null;
		if( type != null){
			var = new ResolvedReference( type, beginLine, endLine ) ;
			info = new VariableInfo(type.isArray()) ;
		}
		else{
			String simpleName
					= typeNode.getFirstDescendantOfType(ASTClassOrInterfaceType.class).getImage() ;
			
			var = new UnresolvedReference(simpleName.intern(),
							classOutline.getQualifiedNameString(), beginLine, endLine ) ;
		}		
		
		ASTExpression exprNode = varNode.getFirstDescendantOfType(ASTExpression.class);
		IReference expr = processExpression(exprNode);
		
		if(expr != null){
			refSet.add(expr) ;
		}
		if( expr instanceof AllocationReference ){
			stack.addVariable(varName, expr, info) ;
			refSet.add(expr) ;
		}
		else
			stack.addVariable(varName, var, info) ;
		
		refSet.add(var) ;
	}
	
	public void processSingleMethod(AbstractJavaAccessNode methodNode)
	{
		int beginLine = methodNode.getBeginLine() ;
		declaration = null ; 
		for( IJavaMethodDeclaration decl : classOutline.getMethods() ){
			if( beginLine != decl.getLines()[0] )
				continue ;
			
			declaration = decl ;
			break ;
		}
		if( declaration == null )
			return ;
		
		if(!(declaration instanceof IJavaMethodDeclaration))
			return ;
		
		IJavaMethodDeclaration md = (IJavaMethodDeclaration) declaration ;
		List<IJavaType> params = md.getParameters();
		List<String> paramNames = md.getParameterNames();
		int size = Math.min(paramNames.size(), params.size()) ;
		
		stack.push() ;
		
		for( int i = 0 ; i < size ; i++ ){
			IJavaType paramType = params.get(i) ;
			String paramName = paramNames.get(i) ;
			
			VariableInfo info = new VariableInfo(paramType.isArray()) ;
			IReference paramRef = new ResolvedReference( paramType, beginLine, beginLine ) ;
			stack.addVariable(paramName, paramRef, info) ;
		}
		
		ASTBlock blockNode = methodNode.getFirstChildOfType(ASTBlock.class);
		if(blockNode != null)
			processBlock(blockNode ) ;
		
		stack.pop() ;
	}
	
	private void processBlock( AbstractJavaNode blockNode )
	{
		stack.push();
		
		ArrayList<Node> targetNodes = extractNearestTargetNodes( blockNode ) ;
		
		for( Node node : targetNodes ){
			
			if( node instanceof ASTLocalVariableDeclaration ){
				ASTLocalVariableDeclaration varNode = (ASTLocalVariableDeclaration) node ;
				processVariable(varNode) ;
				
			}
			else if( node instanceof ASTExpression || node instanceof ASTStatementExpression ){
				AbstractJavaTypeNode exprNode = (AbstractJavaTypeNode) node ;
				IReference expr = processExpression(exprNode);
				if( expr != null )
					refSet.add(expr) ;				
			}
			else if( node instanceof ASTBlock || node instanceof ASTForStatement ){
				AbstractJavaNode childBlockNode = (AbstractJavaNode) node ;
				processBlock(childBlockNode) ;				
			}			
		}	
		stack.pop() ;
	}
	
	private IReference processExpression(AbstractJavaTypeNode exprNode)
	{
		try{
			
			
		
		if( exprNode == null )
			return null ;
		
		int beginLine = exprNode.getBeginLine() ;
		
		int endLine = exprNode.getEndLine() ;
		
		ASTPrimaryExpression primExprNode = exprNode.getFirstDescendantOfType(ASTPrimaryExpression.class);		
		
		if(primExprNode == null){
			return new InvalidReference(ERROR_MESSAGE_MISSING_PRIMARY_EXPRESSION_NODE, beginLine, endLine);			
		}
		
		ASTPrimaryPrefix prefixNode = primExprNode.getFirstChildOfType(ASTPrimaryPrefix.class);
		if( prefixNode == null){
			return new InvalidReference(ERROR_MESSAGE_NO_PRIMARY_PREFIX, beginLine, endLine);
		}
		
		ASTAllocationExpression allocNode = prefixNode.getFirstChildOfType(ASTAllocationExpression.class);
		ASTLiteral literalNode = prefixNode.getFirstChildOfType(ASTLiteral.class);
		
		if(allocNode == null){
			if( literalNode != null )
				return processLiteral( literalNode ) ;
			
			return processMethodInvocationAndFieldAccess(primExprNode, prefixNode );
		}
		return processAllocation(primExprNode, allocNode );	
		
		
		} catch(Throwable e) {
			reportReferenceExtractionError( exprNode, "Expression", e ) ;
			return null ;
		}
	}	

	private IReference processMethodInvocationAndFieldAccess( ASTPrimaryExpression primExprNode,ASTPrimaryPrefix prefixNode)
	{		
		int beginLine = primExprNode.getBeginLine() ;		
		int endLine = primExprNode.getEndLine() ;
		
		String methodName = null ;
		ASTName nameNode = prefixNode.getFirstChildOfType(ASTName.class);
		List<ASTPrimarySuffix> suffixNodes = primExprNode.findChildrenOfType(ASTPrimarySuffix.class);	
		IReference headReference = null ;
		
		String nameImage = null ;
		if(nameNode != null){		
			nameImage = nameNode.getImage() ;
		}
		else{
			String prefixImage = prefixNode.getImage() ;
			if( prefixImage != null	)
				nameImage = prefixImage ;
		}
			
		if( nameImage != null ){
			
			String complexString = nameImage ;
			if( suffixNodes.iterator().hasNext() &&
				suffixNodes.iterator().next().getFirstChildOfType(ASTArguments.class)!= null ){
				
				int ind = nameImage.lastIndexOf(".");
				if( ind < 0 ){
					methodName = nameImage ;
					complexString = null ;
				}
				else{
					complexString = nameImage.substring(0, ind) ;
					methodName = nameImage.substring(ind+1) ;
				}			
			}			
			headReference = buildHead(complexString, methodName, beginLine, endLine );
		}
		else{
			//this.*****
			JavaType type = new JavaType() ;
			type.setDeclaration( classOutline.getTypeDeclaration() ) ;
			headReference = new ResolvedReference(type, beginLine, endLine ) ;							
		}		
		
		IReference result = processSuffixArray(
				headReference,
				methodName,
				suffixNodes,
				beginLine,
				endLine);
		
		return result;	
	}

	private IReference buildHead(String complexString, String methodName, int beginLine, int endLine)
	{
		IReference headReference = null ;
		String fieldAccessString = null ;
		while(true)
		{
			if(complexString == null)
				break ;			
			
			String segment1 ;
			int dotInd = complexString.indexOf(".") ;
			segment1 = dotInd < 0 ? complexString : complexString.substring(0, dotInd) ;
			
			headReference = findFieldOrVariableType( segment1, stack, classOutline, beginLine, endLine ) ;
			
			if( headReference == null && ( dotInd >= 0 || methodName != null) ){			
				
				int l = complexString.length();
				int ucInd = 0 ;
				for( ;ucInd<l&&!Character.isUpperCase(complexString.charAt(ucInd)) ; ucInd++ ) ;
				dotInd = complexString.indexOf(".", ucInd) ;
				if(dotInd<0) {					
					dotInd = l ;
				} 
					
				String typeName = complexString.substring(0, dotInd) ;
				
				//static class access
				IJavaType type = resolveType( typeName, declaration) ;
				if(type != null)
					headReference = new ResolvedReference(type, beginLine, endLine ) ;
				else
					headReference = new UnresolvedReference(
							typeName.intern(), classOutline.getQualifiedNameString(), beginLine, endLine ) ;
			}
			
			if( headReference != null ){				
				if(dotInd >=0 && dotInd+1 < complexString.length() )
					fieldAccessString = complexString.substring( dotInd+1) ;				
				break ;
			}			
			
			headReference = new UnrecognizedAccessSequence(complexString.intern(), beginLine, endLine ) ;
			break ;
		}
		
		if(headReference==null && methodName != null){
			JavaType type = new JavaType() ;
			type.setDeclaration( classOutline.getTypeDeclaration() ) ;
			headReference = new ResolvedReference( type, beginLine, endLine ) ;
		}
		
		if( fieldAccessString != null ){
			ArrayList<String> fieldList = new ArrayList<String>( Arrays.asList( fieldAccessString.split("\\.")  ) ) ;
			headReference = appendFieldSequence( headReference, fieldList, beginLine, endLine );
		}
		return headReference;
	}


	private IReference appendFieldSequence(IReference headReference, List<String> fields, int beginLine, int endLine ) {
		
		for(String field : fields){			
			headReference = new FieldAccessReference(headReference, field, beginLine, endLine) ;			
		}		
		return headReference ;
	}

	private IReference processAllocation( ASTPrimaryExpression primExprNode, ASTAllocationExpression allocNode )
	{
		int beginLine = primExprNode.getBeginLine() ;		
		int endLine = primExprNode.getEndLine() ;
		
		ASTClassOrInterfaceType typeNode = allocNode.getFirstChildOfType(ASTClassOrInterfaceType.class);
		IJavaType type = null ;
		String name ;
		if(typeNode != null){
			type= resolveType(typeNode, declaration);
			name = typeNode.getImage() ;
		}
		else{
			ASTReferenceType refNode = allocNode.getFirstDescendantOfType(ASTReferenceType.class);
			ASTPrimitiveType primitiveType = allocNode.getFirstChildOfType(ASTPrimitiveType.class);
			type = resolvePrimitiveType( primitiveType, refNode ) ;
			name = primitiveType.getImage() ;
		}
		
		if(type == null){			
			if(name.contains(".")){
				IJavaTypeDeclaration decl = getOutlieAccessor().getOutlineContainer().getTypeDeclaration(name);
				if(decl!=null){
					type=new JavaType() ;
					type.setDeclaration(decl) ;
				}
				else{
					type = new JavaTypeReducedImpl(name) ;	
				}				
			}
			else{
				type = resolveType(name, declaration) ;
			}
		}
		
		ASTArguments argsNode = allocNode.getFirstChildOfType(ASTArguments.class);		
		List<IReference> argList = extractArguments(argsNode);
		
		IReference headReference = null ;
		
		if( type != null )
			headReference = new AllocationReference(type, argList, beginLine, endLine ) ;		
		else{
			headReference = new UnresolvedReference(
					name.intern(), classOutline.getQualifiedNameString(), beginLine, endLine ) ;
		}		
		
		ASTClassOrInterfaceBody bodyNode = allocNode.getFirstChildOfType(ASTClassOrInterfaceBody.class);
		if( bodyNode != null ){
			//TODO process class here
		}
		List<ASTPrimarySuffix> suffixNodes = primExprNode.findChildrenOfType(ASTPrimarySuffix.class);
		if(suffixNodes == null || suffixNodes.isEmpty() )
			return headReference ;
		
		IReference result = processSuffixArray(
				headReference,
				null,
				suffixNodes,
				beginLine,
				endLine) ;
		
		return result;
	}
	
	

	private IReference processSuffixArray(
			IReference parentHeadReference,
			String methodName,
			List<ASTPrimarySuffix> suffixNodes,
			int beginLine,
			int endLine)
	{
		int ind = 0 ;
		
		ASTArguments argsNode = null ;
		
		IReference methodOwnerRef = parentHeadReference ;
		
		int suffixCout = suffixNodes.size();
		if( methodName != null ){
			argsNode = suffixNodes.iterator().next().getFirstChildOfType(ASTArguments.class);						
		}
		else{			
			ArrayList<String> imageList = new ArrayList<String>() ;			
			for(  ; ind < suffixCout ; ind++ ){
				ASTPrimarySuffix suffix = suffixNodes.get(ind) ;			
				argsNode = suffix.getFirstChildOfType(ASTArguments.class);
				if(argsNode != null)
					break ;			
				
				imageList.add(suffix.getImage()) ;
			}
			
			List<String> fields=null ;
			if(ind == suffixCout){
				fields = imageList ;
			}
			else{
				int s = imageList.size()-1;
				methodName = imageList.get( s) ;
				if(s>0){
					fields = imageList.subList(0, s) ;					
				}
			}
			if(fields != null){
				methodOwnerRef = appendFieldSequence(methodOwnerRef, fields, beginLine, endLine ) ;
			}
		}
		
		if(argsNode == null)
			return methodOwnerRef ;
		
		List<IReference> argList = extractArguments(argsNode);
		
		IReference headReference = new MethodInvocationReference(
				methodOwnerRef, methodName.intern(), argList, beginLine, endLine) ;
		
		int nextStart = ind+1 ;
		if( nextStart == suffixCout )
			return headReference ;
		
		IReference result = processSuffixArray(
				headReference,
				null,
				suffixNodes.subList(nextStart, suffixCout ), beginLine, endLine ) ;
		
		return result;
	}

	private List<IReference> extractArguments(ASTArguments argsNode) {
		if(argsNode == null)
			return null ;
		ASTArgumentList argListNode = argsNode.getFirstDescendantOfType( ASTArgumentList.class ) ;
		
		List<IReference> argList = null ;		
		if( argListNode != null){
			List<ASTExpression> argExpressions = argListNode.findDescendantsOfType(ASTExpression.class);
			if( argExpressions != null && !argExpressions.isEmpty() ){
				argList = new ArrayList<IReference>() ;
				for( ASTExpression exprNode : argExpressions  ){
					IReference argExpr = processExpression(exprNode);
					argList.add(argExpr) ;
				}
			}
		}
		return argList;
	}

	private IReference findFieldOrVariableType(String name, VariablesStack stack, IClassOutline classOutline, int beginLine, int endLine)
	{
		IReference var = stack.getVariable(name);
		if(var != null)
			return var ;
		
		IClassOutline co = classOutline ;
		IJavaFieldDeclaration fd = null ;
		IJavaTypeDeclaration owner = co.getTypeDeclaration() ;
		while(owner != null){
			String ownerQualifiedName = owner.getQualifiedName().getValues().iterator().next() ;
			co = outlineAccessor.getOutlineContainer().getClassOutline(ownerQualifiedName) ;
			
			fd = co.getFieldBySimpleName( name );
			if(fd != null)
				break ;
			
			owner = co.getTypeDeclaration().getOwner();			
		}
		if(fd != null)
			return new ResolvedReference(fd.getType(), beginLine, endLine ) ;
		
		return null;
	}
	
	private IReference processLiteral(ASTLiteral literalNode) {
		
		int beginLine = literalNode.getBeginLine() ;
		int endLine = literalNode.getEndLine() ;
		
		if(literalNode.getFirstDescendantOfType(ASTNullLiteral.class) != null )
			return new NullLiteralReference( beginLine, endLine ) ;
		
		String canonicalName = null ;
		if( literalNode.isStringLiteral() ){
			canonicalName = String.class.getCanonicalName() ;
		}
		else if( literalNode.isIntLiteral() ){
			canonicalName = int.class.getCanonicalName() ;
		}
		else if( literalNode.isCharLiteral() ){
			canonicalName = char.class.getCanonicalName() ;
		}
		else if( literalNode.isFloatLiteral() ){
			canonicalName = float.class.getCanonicalName() ;
		}
		else if( literalNode.isSingleCharacterStringLiteral() ){
			canonicalName = String.class.getCanonicalName() ;
		}
		
		if( canonicalName == null )
			return new InvalidReference(ERROR_MESSAGE_UNKNOWN_LITERAL_CLASS, beginLine, endLine ) ;
		
		return new LiteralReference(canonicalName, beginLine, endLine ) ;
	}
	
	private IJavaType resolveType(ASTClassOrInterfaceType typeNode, IDeclaration owner) {
		
		IJavaType type = this.typeResolvingService.obtainType(typeNode, owner, true );
		return type ;
	}

	private IJavaType resolveType(ASTType typeNode, IDeclaration owner) {
		
		IJavaType type = this.typeResolvingService.extractTypeFromNode(typeNode, owner );
		return type ;
	}
	
	private IJavaType resolveType(String simpleName, IDeclaration owner) {
		
		IJavaType type = this.typeResolvingService.obtainType(simpleName, owner, true );		
		return type ;
	}
	
	private IJavaType resolvePrimitiveType(ASTPrimitiveType primitiveType, ASTReferenceType refNode) {
		IJavaType type = this.typeResolvingService.obtainPrimitiveType(primitiveType, refNode) ;		
		return type ;		
	}
	
	private ArrayList<Node> extractNearestTargetNodes( Node node ) {		
		ArrayList<Node> list = new ArrayList<Node>() ;
		extractNearestTargetNodes(node, list) ;
		return list ;
	}
	
	private final HashSet<Class<?>> targetNodes = new HashSet<Class<?>>( Arrays.asList( new Class<?>[]{
			ASTLocalVariableDeclaration.class,
			ASTExpression.class,
			ASTStatementExpression.class,
			ASTBlock.class,
			ASTForStatement.class
	})) ;

	private void extractNearestTargetNodes( Node node, List<Node> list ) {
		
		int count = node.jjtGetNumChildren() ;
		for( int i = 0 ; i < count ; i++ ){
			Node child = node.jjtGetChild(i);
			if( targetNodes.contains(child.getClass()) ){
				list.add(child) ;
				continue ;
			}			
			extractNearestTargetNodes(child, list) ;
		}		
	}

	public void processField(ASTFieldDeclaration fieldNode) {
		
		int beginLine = fieldNode.getBeginLine() ;
		IJavaFieldDeclaration fDecl = null; 
		for( IJavaFieldDeclaration decl : classOutline.getFields() ){
			if( beginLine != decl.getLines()[0] )
				continue ;
			
			fDecl = decl ;
			break ;
		}
		if( fDecl == null )
			return ;
		
		this.declaration = fDecl ;
		
		this.refSet.add( new ResolvedReference( fDecl.getType(), beginLine, beginLine) ) ;

		ASTExpression exprNode = fieldNode.getFirstDescendantOfType(ASTExpression.class);
		if(exprNode == null)
			return ;
		
		IReference ref = processExpression(exprNode) ;
		if(ref != null)
			this.refSet.add(ref) ;
	}
	
	private void reportReferenceExtractionError(AbstractJavaNode node, String operationKind, Throwable exception)
	{
		if(errLogPath == null){		
			String errLogPath = obtainErrorLogPath();
			if(errLogPath == null)
				return ;
		}		
		
		try {
			
			File f = new File( errLogPath ) ;
			if (!f.exists())
				f.createNewFile();
			
			FileOutputStream fos = new FileOutputStream(f,true) ;
			PrintStream printStream = new PrintStream(fos);
			
			printStream.println( operationKind ) ;
			
			StringBuilder bld = new StringBuilder() ;
			
			bld.append("node type: "+node.getClass().getSimpleName()).append(LINE_SEPARATOR) ;
			bld.append("lines: ["+node.getBeginLine() +","+node.getEndLine()+"]").append(LINE_SEPARATOR) ;
			bld.append("columns: ["+node.getBeginColumn() +","+node.getEndColumn()+"]").append(LINE_SEPARATOR) ;
			
			printStream.println( bld.toString() );
			
			exception.printStackTrace( printStream ) ;
			printStream.println();
			
			fos.close() ;
			
		} catch (IOException e) {

		}
	}

	public String obtainErrorLogPath()
	{
		if (ruleContext==null)
			return null;
		
		Object pathObj = ruleContext.getAttribute("output.properties.path");
		if(pathObj==null)
			return null;
		
		String outputPropertiesPath = pathObj.toString();
		
		Properties prop = new Properties() ;
		try {
			FileInputStream fis = new FileInputStream( new File(outputPropertiesPath));			
			prop.load(fis) ;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}		
		
		String errLogPath = prop.getProperty("err.path.reference.code.inspector") ;
		return errLogPath;
	}

	public RuleContext getRuleContext() {
		return ruleContext;
	}

	public void setRuleContext(RuleContext ruleContext) {
		this.ruleContext = ruleContext;
	}

}
