package net.junos.space.pmd.rules.rest;

import java.util.List;

public class RestInfo {
	private String apiURL;
	private String HTTPMethodName;
	private String serviceName;
	private List<String> consumeTypes;
	private List<String> produceTypes;
	
	private boolean hasValidProduceXSD;
	private boolean hasValidConsumeXSD;
	private RBACInfo rbacInfo;
	private Integer version;
	private boolean deprecated;
	private boolean excludeFromDiscovery;
	
	private List<String> filterableFields;
	private List<String> sortableFields;
	private boolean isPaginated;
	private boolean isSchedulable;
	
	
	private String javaClassName;
	private String javaMethodName;
	private String sourcePath;
	
	
	
	public String getApiURL() {
		return apiURL;
	}
	public void setApiURL(String apiURL) {
		this.apiURL = apiURL;
	}
	public String getHTTPMethodName() {
		return HTTPMethodName;
	}
	public void setHTTPMethodName(String hTTPMethodName) {
		HTTPMethodName = hTTPMethodName;
	}
	public List<String> getConsumeTypes() {
		return consumeTypes;
	}
	public void setConsumeTypes(List<String> consumeTypes) {
		this.consumeTypes = consumeTypes;
	}
	public List<String> getProduceTypes() {
		return produceTypes;
	}
	public void setProduceTypes(List<String> produceTypes) {
		this.produceTypes = produceTypes;
	}
	public RBACInfo getRbacInfo() {
		return rbacInfo;
	}
	public void setRbacInfo(RBACInfo rbacInfo) {
		this.rbacInfo = rbacInfo;
	}
	public Integer getVersion() {
		return version;
	}
	public void setVersion(Integer version) {
		this.version = version;
	}
	public String getJavaClassName() {
		return javaClassName;
	}
	public void setJavaClassName(String javaClassName) {
		this.javaClassName = javaClassName;
	}
	public String getJavaMethodName() {
		return javaMethodName;
	}
	public void setJavaMethodName(String javaMethodName) {
		this.javaMethodName = javaMethodName;
	}
	public String getSourcePath() {
		return sourcePath;
	}
	public void setSourcePath(String sourcePath) {
		this.sourcePath = sourcePath;
	}
	public boolean isHasValidProduceXSD() {
		return hasValidProduceXSD;
	}
	public void setHasValidProduceXSD(boolean hasValidProduceXSD) {
		this.hasValidProduceXSD = hasValidProduceXSD;
	}
	public boolean isHasValidConsumeXSD() {
		return hasValidConsumeXSD;
	}
	public void setHasValidConsumeXSD(boolean hasValidConsumeXSD) {
		this.hasValidConsumeXSD = hasValidConsumeXSD;
	}
	public List<String> getFilterableFields() {
		return filterableFields;
	}
	public void setFilterableFields(List<String> filterableFields) {
		this.filterableFields = filterableFields;
	}
	public List<String> getSortableFields() {
		return sortableFields;
	}
	public void setSortableFields(List<String> sortableFields) {
		this.sortableFields = sortableFields;
	}
	public boolean isDeprecated() {
		return deprecated;
	}
	public void setDeprecated(boolean deprecated) {
		this.deprecated = deprecated;
	}
	public boolean isExcludeFromDiscovery() {
		return excludeFromDiscovery;
	}
	public void setExcludeFromDiscovery(boolean excludeFromDiscovery) {
		this.excludeFromDiscovery = excludeFromDiscovery;
	}
	public boolean isPaginated() {
		return isPaginated;
	}
	public void setPaginated(boolean isPaginated) {
		this.isPaginated = isPaginated;
	}
	public boolean isSchedulable() {
		return isSchedulable;
	}
	public void setSchedulable(boolean isSchedulable) {
		this.isSchedulable = isSchedulable;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
}
