package net.junos.space.pmd.utils.reference;

public class UnrecognizedAccessSequence  extends AbstractReference {
	
	public UnrecognizedAccessSequence(String sequence, int beginLine, int startLine){
		super(beginLine, startLine);
		this.sequence = sequence;
	}

	private final String sequence ;
	
	public String getSequence() {
		return sequence;
	}

	@Override
	public String toString(){
		return "Unrecognized access sequence: " + sequence + " ." ; 
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((sequence == null) ? 0 : sequence.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		UnrecognizedAccessSequence other = (UnrecognizedAccessSequence) obj;
		if (sequence == null) {
			if (other.sequence != null)
				return false;
		} else if (!sequence.equals(other.sequence))
			return false;
		return true;
	}

}
