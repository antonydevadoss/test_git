package net.junos.space.pmd.utils.outline;

import java.util.ArrayList;
import java.util.Collection;


public class JavaType implements IJavaType{
	
	private IJavaTypeDeclaration declaration ;
	
	private ArrayList<IJavaType> genericParameters ;
	
	private boolean isArray ;
	
	private  int arrayDepth ;

	public IJavaTypeDeclaration getDeclaration() {
		return declaration;
	}

	public void setDeclaration(IJavaTypeDeclaration declaration) {
		this.declaration = declaration;
	}

	public ArrayList<IJavaType> getGenericParameters() {
		return genericParameters;
	}

	public void setGenericParameters(Collection<IJavaType> genericParameters) {
		if(genericParameters != null)
			this.genericParameters = new ArrayList<IJavaType>(genericParameters);
		else
			this.genericParameters = null ;
	}

	public boolean isArray() {
		return isArray;
	}

	public void setIsArray(boolean isArray) {
		this.isArray = isArray;
	}

	public int getArrayDepth() {
		return arrayDepth;
	}

	public void setArrayDepth(int arrayDepth) {
		this.arrayDepth = arrayDepth;
	}

	@Override
	public String getSimpleName() {
		if( getDeclaration() == null )
			return null ;
		
		return getDeclaration().getSimpleName() ;
	}

	@Override
	public void setSimpleName(String simpleName) {
		if( getDeclaration() == null )
			return ;
		
		getDeclaration().setSimpleName(simpleName) ;
		
	}

	@Override
	public QualifiedName getQualifiedName() {
		if( getDeclaration() == null )
			return null ;
		
		return getDeclaration().getQualifiedName() ;
	}

	@Override
	public void setQualifiedName(QualifiedName qualifiedName) {
		if( getDeclaration() == null )
			return ;
		
		getDeclaration().setQualifiedName(qualifiedName) ;	
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + arrayDepth;
		result = prime * result
				+ ((declaration == null) ? 0 : declaration.hashCode());
		result = prime
				* result
				+ ((genericParameters == null) ? 0 : genericParameters
						.hashCode());
		result = prime * result + (isArray ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JavaType other = (JavaType) obj;
		if (arrayDepth != other.arrayDepth)
			return false;
		if (declaration == null) {
			if (other.declaration != null)
				return false;
		} else if (!declaration.equals(other.declaration))
			return false;
		if (genericParameters == null) {
			if (other.genericParameters != null)
				return false;
		} else if (!genericParameters.equals(other.genericParameters))
			return false;
		if (isArray != other.isArray)
			return false;
		return true;
	}
	
	@Override
	public String toString(){
		return getQualifiedName()!=null? getQualifiedName().toString() : getSimpleName() ;
	}

	@Override
	public boolean isSimilar(IJavaType another) {
		
		return getSimpleName().equals(another.getSimpleName());
	}

}
