package net.junos.space.pmd.rules.j2ee.ejb3;


import java.util.HashSet;
import java.util.Set;

import net.junos.space.pmd.utils.outline.AbstractEntityRule;
import net.junos.space.pmd.utils.outline.EjbEntity;
import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;
import net.sourceforge.pmd.RuleContext;

public class EntityFinalRule extends AbstractEntityRule {

	
	private static final String message = "The entity or embeddedable class must not be final.";
	@Override
	protected void processEntities(RuleContext ctx) {
		if (AbstractEntityRule.entityRegistry == null) {
			return;
		}
		for (EjbEntity ejbEntity : AbstractEntityRule.entityRegistry.values()) {
			IClassOutline classOutline = getOutlineContainer().getClassOutline(ejbEntity.getQualifiedName());
			IJavaTypeDeclaration typeDeclaration = classOutline.getTypeDeclaration();
			if (typeDeclaration.isFinal()) {
				reportError(ctx,typeDeclaration,message) ;
			}
		}
		super.processEntities(ctx);
	}
	
	protected Set<String> getTargetAnnotations() {
		HashSet<String> set = new HashSet<String>();
		set.add("Entity") ;
		set.add("Embeddable") ;
		return set ;
	}
}
