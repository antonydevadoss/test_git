/*******************************************************************************
 * FILE NAME: AvoidProducesRule.java
 * PURPOSE: This is Rule-4 in document  
 *
 *
 * Revision History: 
 * DATE:           				  AUTHOR:              	CHANGE:  
 * Tue Feb 09, 2011               Atul Bajpai       	Initial Version  
 * 
 * 
 ******************************************************************************/
package net.junos.space.pmd.rules.rest;

import net.junos.space.pmd.utils.PMDConstants;import net.junos.space.pmd.utils.PMDUtils;import net.sourceforge.pmd.RuleContext;import net.sourceforge.pmd.lang.java.ast.ASTMethodDeclarator;import net.sourceforge.pmd.lang.java.rule.AbstractJavaRule;import org.apache.log4j.Logger;

/**
 * This class provides a warning if @Producess Annotation is used in a method, 
 * while the method is not returning anything i.e. return type is "void()" type
 */
public class AvoidProducesRule extends AbstractJavaRule {

	/**
	 * Logger
	 */
	private static Logger log = Logger.getLogger(AvoidProducesRule.class);

	/*
	 * This method checks if @Produces Annotation is used in a method which return "void()" type
	 */
	public Object visit(ASTMethodDeclarator methodNode, Object data) {
		RuleContext ctx = (RuleContext) data;
		String methodName = methodNode.getImage();
		try {
			boolean isProduceAnnotationExists = false;
			boolean isRest = false;

			isRest = PMDUtils.isRestMethod(methodNode);

			// check if return type is void type
			if (isRest && PMDUtils.isReturningVoid(methodNode)) {
				isProduceAnnotationExists = PMDUtils.isMethodAnnotationExists(methodNode, PMDConstants.PRODUCES_ANNOTATION);
			}
			
			if(log.isDebugEnabled()){
				StringBuilder builder = new StringBuilder();
				builder.append("source file=").append(ctx.getSourceCodeFilename()).append(",method=").append(methodName);
				builder.append(",isRest=").append(isRest).append(",isProduceAnnotationExists=").append(isProduceAnnotationExists);
				log.debug(builder.toString());
			}
			
			if (isRest && isProduceAnnotationExists)
				addViolation(data, methodNode);
		} catch (Exception e) {
			log.error("Exception in executing method " + methodName + " of " + ctx.getSourceCodeFilename(), e);
		}
		return super.visit(methodNode, data);
	}
}
