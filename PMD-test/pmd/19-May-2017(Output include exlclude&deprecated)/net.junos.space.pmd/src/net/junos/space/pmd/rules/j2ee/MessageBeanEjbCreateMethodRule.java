package net.junos.space.pmd.rules.j2ee;


import net.junos.space.pmd.utils.outline.IClassOutline;

public class MessageBeanEjbCreateMethodRule extends AbstractCreateMethodExistenceRule {

	@Override
	public boolean isProperBean(IClassOutline outline) {
		return isMessageDrivenBeanEJB2(outline.getTypeDeclaration());
	}
	
	@Override
	public String getMessage() {
		return "Provide a ejbCreate method for a Message Bean";
	}
}
