/*******************************************************************************
 * FILE NAME: InvalidRESTMethodRule.java
 * PURPOSE: This is Rule-8 of document 
 *
 *
 * Revision History: 
 * DATE:           				  AUTHOR:              	CHANGE:  
 * Tue Feb 09, 2011               Atul Bajpai       	Initial Version  
 * 
 * 
 ******************************************************************************/
package net.junos.space.pmd.rules.rest;

import net.junos.space.pmd.utils.PMDUtils;import net.sourceforge.pmd.RuleContext;import net.sourceforge.pmd.lang.java.ast.ASTMethodDeclarator;import net.sourceforge.pmd.lang.java.rule.AbstractJavaRule;import org.apache.log4j.Logger;

/**
 * This class checks whether or not any Rest method is having two or more DTOs.
 */
public class InvalidRESTMethodRule extends AbstractJavaRule {

	/**
	 * Logger
	 */
	private static Logger log = Logger.getLogger(InvalidRESTMethodRule.class);

	/**
	 * Visits all REST methods of class and checks whether or not any method is 
	 * having two or more DTOs.
	 */
	public Object visit(ASTMethodDeclarator methodNode, Object data) {
		RuleContext ctx = (RuleContext) data;
		String methodName = methodNode.getImage();
		try {
			boolean isRest = false;

			// checks whether the method is REST or not.
			isRest = PMDUtils.isRestMethod(methodNode);
			int count = 0;
			if (isRest) {
				// count total numbers of valid DTO members of a method
				count = PMDUtils.getNumberOfDTOs(methodNode);
			}
			
			if(log.isDebugEnabled()){
				StringBuilder builder = new StringBuilder();
				builder.append("source file=").append(ctx.getSourceCodeFilename()).append(",method=").append(methodName);
				builder.append(",isRest=").append(isRest).append(",count=").append(count);
				log.debug(builder.toString());
			}
			
			if (count >= 2)
				addViolation(data, methodNode);
		} catch (Exception e) {
			log.error("Exception in executing method " + methodName + " of " + ctx.getSourceCodeFilename(), e);
		}
		return super.visit(methodNode, data);
	}
}
