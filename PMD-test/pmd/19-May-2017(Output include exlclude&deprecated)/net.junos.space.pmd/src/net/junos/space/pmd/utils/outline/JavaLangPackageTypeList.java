package net.junos.space.pmd.utils.outline;

import java.util.HashSet;

public class JavaLangPackageTypeList {
	
	static public String resolveSimpleName(String simpleName) {
		if( _set.contains(simpleName) )
			return "java.lang."+simpleName ;
		
		return null;
	}

	private static HashSet<String> _set = new HashSet<String>();
	static{
		_set.add("Appendable") ;
		_set.add("CharSequence") ;
		_set.add("Cloneable") ;
		_set.add("Comparable") ;
		_set.add("Iterable") ;
		_set.add("Readable") ;
		_set.add("Runnable") ;
		_set.add("Thread.UncaughtExceptionHandler") ;
		_set.add("UncaughtExceptionHandler") ;

		_set.add("Boolean") ;
		_set.add("Byte") ;
		_set.add("Character") ;
		_set.add("Character.Subset") ;
		_set.add("Subset") ;
		_set.add("Character.UnicodeBlock") ;
		_set.add("UnicodeBlock") ;
		_set.add("Class") ;
		_set.add("ClassLoader") ;
		_set.add("Compiler") ;
		_set.add("Double") ;
		_set.add("Enum") ;
		_set.add("Float") ;
		_set.add("InheritableThreadLocal") ;
		_set.add("Integer") ;
		_set.add("Long") ;
		_set.add("Math") ;
		_set.add("Number") ;
		_set.add("Object") ;
		_set.add("Package") ;
		_set.add("Process") ;
		_set.add("ProcessBuilder") ;
		_set.add("Runtime") ;
		_set.add("RuntimePermission") ;
		_set.add("SecurityManager") ;
		_set.add("Short") ;
		_set.add("StackTraceElement") ;
		_set.add("StrictMath") ;
		_set.add("String") ;
		_set.add("StringBuffer") ;
		_set.add("StringBuilder") ;
		_set.add("System") ;
		_set.add("Thread") ;
		_set.add("ThreadGroup") ;
		_set.add("ThreadLocal") ;
		_set.add("Throwable") ;
		_set.add("Void") ;
	}
	

}
