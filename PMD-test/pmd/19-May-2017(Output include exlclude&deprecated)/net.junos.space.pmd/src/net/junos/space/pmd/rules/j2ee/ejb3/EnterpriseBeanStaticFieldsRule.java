package net.junos.space.pmd.rules.j2ee.ejb3;

import java.util.List;

import net.junos.space.pmd.utils.outline.AbstractOutlineAccessor;
import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IJavaFieldDeclaration;
import net.sourceforge.pmd.RuleContext;
import net.sourceforge.pmd.lang.java.rule.JavaRuleViolation;

public class EnterpriseBeanStaticFieldsRule extends AbstractOutlineAccessor {

	
	private static final String message = "Static fields in the enterprise bean should be declared as final.";

	@Override
	public void doEnd(RuleContext ctx) {
		List<IClassOutline> allOutlines = getOutlineContainer().getAllOutlines();
		
		for (IClassOutline classOutline : allOutlines) {
			if (isEnterpriseBeanSuperclassEJB3(classOutline.getQualifiedNameString())) {
				List<IJavaFieldDeclaration> fields = classOutline.getFields();
				for (IJavaFieldDeclaration field : fields) {
					if (field.isStatic() && !field.isFinal()) {
						String path = classOutline.getFilePath();			
						ctx.setSourceCodeFilename( path ) ;
						
						JavaRuleViolation violation
								= new JavaRuleViolation(this, ctx, null, message,field.getLines()[0],field.getLines()[1] ) ;
						
						ctx.getReport().addRuleViolation(violation) ;
					}
				}
			}
		}
		super.doEnd(ctx);
	}
}
