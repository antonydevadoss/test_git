package net.junos.space.pmd.rules.j2ee;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.junos.space.pmd.utils.reference.SimpleMethodReferenceRule;

public class DoNotAttemptToObtainSecurityPolicyInformationRule extends SimpleMethodReferenceRule {
	private static final String[] targetClasses = new String[]{
		"java.security.Policy",
	} ;
	
	private static final String[] targetMethods = new String[]{
		"getPermissions",
	} ;
	
	private static final String message = "The enterprise bean must not attempt to obtain the security policy "+
										  "information for a particular code source. "+
										  "Allowing the enterprise bean to access the security policy information "+
										  "would create a security hole. (See EJB20SPEC - C.1.2 - Programming restrictions)"; 
	
	public String getMessage(){
		return message ;
	}

	protected List<String> getTargetClasses() {
		return new ArrayList<String>( Arrays.asList(targetClasses) ) ;
	}
	
	protected List<String> getTargetMethods() {
		return new ArrayList<String>( Arrays.asList(targetMethods) ) ;
	}

	protected boolean mustBeChecked(String className) {		
		return isEnterpriseBeanSuperclassEJB2(className);
	}
	
	protected List<String> getParams(String qName, String methodName) {
		ArrayList<String> lst = new ArrayList<String>();
		lst.add("CodeSource") ;
		return lst ;
	}
}
