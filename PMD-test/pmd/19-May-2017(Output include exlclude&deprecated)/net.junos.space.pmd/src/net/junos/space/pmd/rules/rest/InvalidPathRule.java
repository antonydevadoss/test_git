/*******************************************************************************
 * FILE NAME: InvalidPathRule.java
 * PURPOSE: This is Rule-10 of document 
 *
 *
 * Revision History: 
 * DATE:           				  AUTHOR:              	CHANGE:  
 * Tue Feb 23, 2011               Atul Bajpai       	Initial Version  
 * 
 * 
 ******************************************************************************/
package net.junos.space.pmd.rules.rest;

import java.util.List;import net.junos.space.pmd.utils.PMDConstants;import net.junos.space.pmd.utils.PMDUtils;import net.sourceforge.pmd.RuleContext;import net.sourceforge.pmd.lang.java.ast.ASTMethodDeclarator;import net.sourceforge.pmd.lang.java.rule.AbstractJavaRule;import org.apache.log4j.Logger;

/**
 * This rule verifies that if @PathParam is present as input of method and the
 * attribute in the annotation is present more than once in @Path annotation on
 * method and class, then raise violation.
 */
public class InvalidPathRule extends AbstractJavaRule {

	/**
	 * Logger
	 */
	private static Logger log = Logger.getLogger(InvalidPathRule.class);

	/**
	 * Method checks:
	 * a> Method is REST or not
	 * b> Method has @PathParam annotation
	 * c> Get attributes defined in all @PathParam and store them.
	 * d> @Path exists on method or class
	 * e> For each attribute fetched in c>, check its existence in @Path annotation
	 * 		on method and class, and find out whether this is repeated in the 
	 * 		concatenated path.
	 */
	public Object visit(ASTMethodDeclarator methodNode, Object data) {
		RuleContext ctx = (RuleContext) data;
		String methodName = methodNode.getImage();
		try {
			boolean isRestMethod = false;
			boolean isPathParamExist = false;
			boolean isMethodPathExists = false;
			boolean isClassPathExists = false;
			boolean isRepeated = false;

			// checks whether the method is REST or not.
			isRestMethod = PMDUtils.isRestMethod(methodNode);
			if (isRestMethod) {

				// checks if @PathParam exists in method parameter
				isPathParamExist = PMDUtils.hasAnnotationParameter(methodNode, PMDConstants.PATHPARAM_ANNOTATION);
				if (isPathParamExist) {

					String methodPathAnnotationValue = "";
					String classPathAnnotationValue = "";
					int methodIndex = 0;
					int methodLastIndex = 0;
					int classIndex = 0;
					int classLastIndex = 0;

					// List of all annotation value used in a method parameter
					List<String> annotationValues = PMDUtils.getModifiedParameterAnnotationValue(methodNode,
							PMDConstants.PATHPARAM_ANNOTATION, PMDConstants.CURLYBRACE_OPENING, PMDConstants.CURLYBRACE_CLOSING);

					// checks if @Path exists as a method annotation
					isMethodPathExists = PMDUtils.isMethodAnnotationExists(methodNode, PMDConstants.PATH_ANNOTATION);
					// checks if @Path exists as a class annotation
					isClassPathExists = PMDUtils.isClassAnnotationExists(methodNode, PMDConstants.PATH_ANNOTATION);
					if (isMethodPathExists || isClassPathExists) {

						if (isMethodPathExists)
							methodPathAnnotationValue = PMDUtils.getAnnotationValue(methodNode,PMDUtils.getMethodAnnotation(methodNode,
									PMDConstants.PATH_ANNOTATION));

						if (isClassPathExists)
							classPathAnnotationValue = PMDUtils.getAnnotationValue(methodNode,PMDUtils.getClassAnnotation(methodNode,
									PMDConstants.PATH_ANNOTATION));

						for (int k = 0; k < annotationValues.size(); k++) {
							String annotationValue = annotationValues.get(k);
							methodIndex = methodPathAnnotationValue.indexOf(annotationValue);
							methodLastIndex = methodPathAnnotationValue.lastIndexOf(annotationValue);
							classIndex = classPathAnnotationValue.indexOf(annotationValue);
							classLastIndex = classPathAnnotationValue.lastIndexOf(annotationValue);
							if (methodIndex != -1 || classIndex != -1) {
								if (methodIndex != -1 && methodLastIndex != -1 && methodIndex != methodLastIndex) {
									isRepeated = true;
									break;
								}
								if (classIndex != -1 && classLastIndex != -1 && classIndex != classLastIndex) {
									isRepeated = true;
									break;
								}
								if (methodIndex != -1 && classIndex != -1) {
									isRepeated = true;
									break;
								}
							}// End if for methodIndex or classIndex
						}// End for
					}// End if for isMethodPathExists or isClassPathExists
				}// End if for isPathParamExist
			}// End if for isRestMethod
			
			if(log.isDebugEnabled()){
				StringBuilder builder = new StringBuilder();
				builder.append("source file=").append(ctx.getSourceCodeFilename()).append(",method=").append(methodName);
				builder.append(",isRestMethod=").append(isRestMethod).append(",isRepeated=").append(isRepeated);
				log.debug(builder.toString());
			}
			
			if (isRepeated)
				addViolation(data, methodNode);
		} catch (Exception e) {
			log.error("Exception in executing method " + methodName + " of " + ctx.getSourceCodeFilename(), e);
		}
		return super.visit(methodNode, data);
	}
}
