package net.junos.space.pmd.utils.outline;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public class JavaMethodDeclaration extends AbstractElementDeclaration2 implements IJavaMethodDeclaration {

	private boolean isAbstract ;
	
	private IJavaType returnType ;
	
	private List<IJavaType> parameters = new ArrayList<IJavaType>() ;
	
	private List<IJavaType> exceptions = new ArrayList<IJavaType>() ;
	
	private List<String> parameterNames = new ArrayList<String>() ;	
	
	@Override
	public boolean isAbstract() {
		return this.isAbstract ;
	}

	@Override
	public void setAbstract(boolean isAbstract) {
		this.isAbstract = isAbstract ;	
	}

	public IJavaType getReturnType() {
		return returnType;
	}

	public void setReturnType(IJavaType returnType) {
		this.returnType = returnType;
	}

	@Override
	public List<IJavaType> getParameters() {
		return this.parameters ;
	}

	@Override
	public void addParameter(IJavaType param) {
		this.parameters.add(param) ;		
	}

	@Override
	public void setParameters(Collection<IJavaType> parameters) {
		if(parameters != null)
			this.parameters = new ArrayList<IJavaType>(parameters) ;
		else
			this.parameters.clear() ;
	}

	public List<IJavaType> getExceptions() {
		return exceptions;
	}

	public void setExceptions(List<IJavaType> exceptions) {
		this.exceptions = exceptions;
	}

	public List<String> getParameterNames() {
		return parameterNames;
	}

	public void setParameterNames(List<String> parameterNames) {
		this.parameterNames = parameterNames;
	}

}
