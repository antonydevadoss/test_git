/*******************************************************************************
 * FILE NAME: MissingConsumesRule.java
 * PURPOSE:  This is Rule-5 as mentioned in document. 
 *
 *
 * Revision History: 
 * DATE:           				  AUTHOR:              	CHANGE:  
 * Tue Feb 09, 2011               Atul Bajpai       	Initial Version  
 * 
 * 
 ******************************************************************************/
package net.junos.space.pmd.rules.rest;

import net.junos.space.pmd.utils.PMDConstants;import net.junos.space.pmd.utils.PMDUtils;import net.sourceforge.pmd.RuleContext;import net.sourceforge.pmd.lang.java.ast.ASTMethodDeclarator;import net.sourceforge.pmd.lang.java.rule.AbstractJavaRule;import org.apache.log4j.Logger;

/**
 * This class validates, any REST method having at least one DTO as one of the Input parameter,
 * must have @Consumes Annotation, either in method level or Class level
 */
public class MissingConsumesRule extends AbstractJavaRule {

	/**
	 * Logger
	 */
	private static Logger log = Logger.getLogger(MissingConsumesRule.class);

	/**
	 * This method visits all methods in Class and checks if any REST method having DTO as Input parameter do not have @Consumes
	 * Annotation. In this case It checks whether the Class is having @Consumes Annotation or not.
	 */
	public Object visit(ASTMethodDeclarator methodNode, Object data) {
		RuleContext ctx = (RuleContext) data;
		String methodName = methodNode.getImage();
		try {
			boolean isConsumesAnnotationExists = false;
			boolean isRest = false;
			boolean isDTO = false;

			// checks whether the method is REST or not.
			isRest = PMDUtils.isRestMethod(methodNode);
			if (isRest) {
				// Checks the Result Type of method (DTO type)
				isDTO = PMDUtils.hasDTOParameters(methodNode);

				if (isDTO) {
					isConsumesAnnotationExists = PMDUtils.isMethodAnnotationExists(methodNode, PMDConstants.CONSUMES_ANNOTATION);
					if (!isConsumesAnnotationExists)
						isConsumesAnnotationExists = PMDUtils.isClassAnnotationExists(methodNode,
								PMDConstants.CONSUMES_ANNOTATION);
				}
			}
			
			if(log.isDebugEnabled()){
				StringBuilder builder = new StringBuilder();
				builder.append("source file=").append(ctx.getSourceCodeFilename()).append(",method=").append(methodName);
				builder.append(",isRest=").append(isRest).append(",isDTO=").append(isDTO).append(",isConsumesAnnotationExists=")
						.append(isConsumesAnnotationExists);
				log.debug(builder.toString());
			}
			
			if (isRest && isDTO && !isConsumesAnnotationExists)
				addViolation(data, methodNode);
		} catch (Exception e) {
			log.error("Exception in executing method " + methodName + " of " + ctx.getSourceCodeFilename(), e);
		}
		return super.visit(methodNode, data);
	}
}
