package net.junos.space.pmd.utils.outline;


public interface IGenericParam extends IJavaType {
	
	boolean ownerIsClass() ;
	
	void setOwnerIsClass() ;
	
	boolean ownerIsMethod() ;
	
	void setOwnerIsMethod() ;
	
	QualifiedName getOwnerQualifiedName() ;

	void setOwnerQualifiedName( QualifiedName name ) ;
	
	int getOrder() ;
	
	void setOrder(int order) ;
}
