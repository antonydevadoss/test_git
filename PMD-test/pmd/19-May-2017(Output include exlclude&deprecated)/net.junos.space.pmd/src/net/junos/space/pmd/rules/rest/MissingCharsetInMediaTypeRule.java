/*******************************************************************************
 * FILE NAME: MissingCharsetInMediaTypeRule.java
 * PURPOSE:   This is Rule-18 in document
 *
 *
 * Revision History: 
 * DATE:           				  AUTHOR:              	CHANGE:  
 * Tue Feb 21st Feb, 2011               Atul Bajpai       	Initial Version  
 * 
 * 
 ******************************************************************************/
package net.junos.space.pmd.rules.rest;

import java.util.List;import net.junos.space.pmd.utils.PMDConstants;import net.junos.space.pmd.utils.PMDUtils;import net.sourceforge.pmd.RuleContext;import net.sourceforge.pmd.lang.java.ast.ASTMethodDeclarator;import net.sourceforge.pmd.lang.java.rule.AbstractJavaRule;import org.apache.log4j.Logger;

/**
 * This class checks whether or not @Consumes annotation defined on REST method,
 * and the media type contains semicolon (;) but does not contain ';Charset=UTF-8'
 */
public class MissingCharsetInMediaTypeRule extends AbstractJavaRule {

	/**
	 * Logger
	 */
	private static Logger log = Logger.getLogger(MissingCharsetInMediaTypeRule.class);
	
	/**
	 * visits all the REST methods of this class and checks the validity of @Consumes Annotation and it's media Type
	 */
	public Object visit(ASTMethodDeclarator methodNode, Object data) {
		boolean isRestMethod = false;
		boolean isConsumesOnMethodExists = false;
		boolean isConsumesOnClassExists = false;
		boolean isValidConsumes = false;
		boolean hasDTO = false;
		
		RuleContext ctx = (RuleContext) data;
		String methodName = methodNode.getImage();
		try{
			// checks whether the method is REST or not.
			isRestMethod = PMDUtils.isRestMethod(methodNode);
			if (isRestMethod) {
				hasDTO = PMDUtils.hasDTOParameters(methodNode);
				if(hasDTO){
					isConsumesOnMethodExists = PMDUtils.isMethodAnnotationExists(methodNode, PMDConstants.CONSUMES_ANNOTATION);
					isConsumesOnClassExists = PMDUtils.isClassAnnotationExists(methodNode, PMDConstants.CONSUMES_ANNOTATION);				
					if (isConsumesOnMethodExists) {
						List<String> methodConsumesValues = PMDUtils.getMethodAnnotationValue(methodNode, PMDConstants.CONSUMES_ANNOTATION);
						isValidConsumes = PMDUtils.isMediaTypeContainsCharset(methodConsumesValues);
					}
					else if(!isConsumesOnMethodExists && isConsumesOnClassExists){
						List<String> classConsumesValues = PMDUtils.getClassAnnotationValue(methodNode, PMDConstants.CONSUMES_ANNOTATION);
						isValidConsumes = PMDUtils.isMediaTypeContainsCharset(classConsumesValues);
					} else {
						isValidConsumes = true;
					}
				}else
					isValidConsumes = true;			
			}else
				isValidConsumes = true;
			
			if(log.isDebugEnabled()){
				StringBuilder builder = new StringBuilder();
				builder.append("source file=").append(ctx.getSourceCodeFilename()).append(",method=").append(methodName);
				builder.append(",isRestMethod=").append(isRestMethod).append(",hasDTO=").append(hasDTO).append(",isConsumesOnMethodExists=");
				builder.append(isConsumesOnMethodExists).append(",isConsumesOnClassExists=").append(isConsumesOnClassExists).append(",isValidConsumes=").append(isValidConsumes);
				log.debug(builder.toString());
			}
			
			if (!isValidConsumes)
				addViolation(data, methodNode);
		} catch(Exception e){
			log.error("Exception in executing method " + methodName + " of " + ctx.getSourceCodeFilename(), e);
		}
		return super.visit(methodNode, data);
	}
	
}
