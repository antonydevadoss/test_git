package net.junos.space.pmd.utils.reference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.junos.space.pmd.utils.outline.AbstractOutlineAccessor;
import net.junos.space.pmd.utils.outline.IClassOutline;
import net.sourceforge.pmd.RuleContext;
import net.sourceforge.pmd.lang.ast.AbstractNode;
import net.sourceforge.pmd.lang.java.ast.ASTClassOrInterfaceBody;
import net.sourceforge.pmd.lang.java.ast.ASTClassOrInterfaceBodyDeclaration;
import net.sourceforge.pmd.lang.java.ast.ASTClassOrInterfaceDeclaration;
import net.sourceforge.pmd.lang.java.ast.ASTCompilationUnit;
import net.sourceforge.pmd.lang.java.ast.ASTConstructorDeclaration;
import net.sourceforge.pmd.lang.java.ast.ASTEnumBody;
import net.sourceforge.pmd.lang.java.ast.ASTEnumDeclaration;
import net.sourceforge.pmd.lang.java.ast.ASTFieldDeclaration;
import net.sourceforge.pmd.lang.java.ast.ASTMethodDeclaration;
import net.sourceforge.pmd.lang.java.ast.ASTPackageDeclaration;
import net.sourceforge.pmd.lang.java.ast.ASTTypeDeclaration;
import net.sourceforge.pmd.lang.java.ast.AbstractJavaAccessNode;
import net.sourceforge.pmd.lang.java.ast.AbstractJavaAccessTypeNode;
import net.sourceforge.pmd.lang.java.ast.AbstractJavaNode;

public class ReferenceCollector {
	
	AbstractOutlineAccessor outlieAccessor = new AbstractOutlineAccessor() ;
	
	private ReferenceCodeInspector codeInspector = new ReferenceCodeInspector() ;
	
	private IReferenceContainer referenceContainer ;
	
	public void start(RuleContext ctx){
		outlieAccessor.start(ctx) ;
	}
	
	public void end(RuleContext ctx){
		outlieAccessor.end(ctx) ;
	}
	
	public void visit(ASTCompilationUnit node, Object data){
		
		try{
			
		
		outlieAccessor.visit(node, data) ;
		
		if(data instanceof RuleContext){
			codeInspector.setRuleContext((RuleContext) data) ;
		}
		
		String packageName = "" ;
		ASTPackageDeclaration packageDeclaration = node.getPackageDeclaration();
		if(packageDeclaration!=null)
			packageName=packageDeclaration.getPackageNameImage() ;
		
		AbstractJavaAccessTypeNode classNode = extractClassOrEnum(node) ;
		if(classNode==null){
			//annotations lead here
//			System.err.println( getClass().getSimpleName() +", null cls node: " +
//		((RuleContext)data).getSourceCodeFilename());
			return ;
		}
		
		String simpleName = classNode.getImage() ;
		String qualifiedName = packageName + "." + simpleName ;
		
		processClass( classNode, qualifiedName ) ;
		
		
		}catch(Exception e){
			System.err.println( "NPE: " +
					((RuleContext)data).getSourceCodeFilename());
			e.printStackTrace(System.err) ;
		}
	}
	
	private AbstractJavaAccessTypeNode extractClassOrEnum(ASTCompilationUnit node)
	{
		ASTTypeDeclaration typeDeclNode = node.getFirstChildOfType(ASTTypeDeclaration.class);
		if(typeDeclNode==null)
			return null ;
		
		ASTClassOrInterfaceDeclaration classNode = typeDeclNode.getFirstChildOfType(ASTClassOrInterfaceDeclaration.class);
		if(classNode != null)
			return classNode ;
		
		ASTEnumDeclaration enumNode = typeDeclNode.getFirstChildOfType(ASTEnumDeclaration.class);
		
		return enumNode;
	}
	
	private void processClass( AbstractNode classNode, String qualifiedName) {
		
		IClassOutline outline = prepareServices(qualifiedName);
		
		prepareTypeResolvingService( qualifiedName ) ;
		
		ArrayList<ASTFieldDeclaration> fieldNodes = new ArrayList<ASTFieldDeclaration>() ;		
		ArrayList<AbstractJavaAccessNode> constructorNodes = new ArrayList<AbstractJavaAccessNode>() ;
		ArrayList<AbstractJavaAccessNode> methodNodes = new ArrayList<AbstractJavaAccessNode>() ;
		ArrayList<AbstractJavaAccessTypeNode> classNodes = new ArrayList<AbstractJavaAccessTypeNode>() ;
		
		fillMemberArrays(classNode, fieldNodes,constructorNodes, methodNodes, classNodes);
		
		processFields( fieldNodes, qualifiedName ) ;
		
		processConstructors( outline, constructorNodes, qualifiedName ) ;
		
		processMethods( outline, methodNodes, qualifiedName ) ;
		
		for( AbstractJavaAccessTypeNode nestedClass : classNodes ){
			
			String simpleName = nestedClass.getImage();
			String qName = qualifiedName + "." + simpleName ;
			processClass(nestedClass, qName) ;
		}
	}

	private IClassOutline prepareServices(String qualifiedName) {
		
		IClassOutline outline = outlieAccessor.getOutlineContainer().getClassOutline(qualifiedName) ;
		codeInspector.setClassOutline(outline) ;
		codeInspector.getTypeResolvingService().setOutlineContainer(outlieAccessor.getOutlineContainer()) ;
		List<String> imports = outline.getImports();
		if( imports != null && !imports.isEmpty() ){
			HashMap<String,String> importsMap = new HashMap<String, String>() ;
			for( String str : imports ){
				int ind = imports.lastIndexOf(".") ;
				String name = str.substring(ind+1) ;
				importsMap.put(name, str) ;
			}
			codeInspector.getTypeResolvingService().setImportsMap(importsMap) ;
		}
		return outline;
	}
	
	private void prepareTypeResolvingService(String qualifiedName) {
		
		this.codeInspector.setOutlieAccessor(outlieAccessor) ;
		Map<String, String> importsMap
				= this.codeInspector.getTypeResolvingService().getImportsMap();
		importsMap.clear() ;
		
		IClassOutline classOutline
				= this.outlieAccessor.getOutlineContainer().getClassOutline(qualifiedName);
		for( String iName : classOutline.getImports() ){
			int ind = iName.lastIndexOf(".") ;
			String sName = ind >= 0 ? iName.substring(ind+1) : iName ;
			importsMap.put(sName, iName) ;
		}		
	}

	private void processFields(	ArrayList<ASTFieldDeclaration> fieldNodes, String qualifiedName )
	{
		IClassOutline ol = outlieAccessor.getOutlineContainer().getClassOutline(qualifiedName) ;
		
		for( ASTFieldDeclaration fieldNode : fieldNodes ){
			processSingleField( fieldNode, ol ) ;
		}
	}

	private void processSingleField(
			ASTFieldDeclaration fieldNode,
			IClassOutline outline )
	{
		codeInspector.getRefSet().clear() ;			
		codeInspector.processField(fieldNode) ;
		if( codeInspector.getRefSet().isEmpty() )
			return ;
		
		getReferenceContainer().register( codeInspector.getDeclaration(), codeInspector.getRefSet() ) ;
	}

	private void processMethods(
			IClassOutline outline, ArrayList<AbstractJavaAccessNode> methodNodes, String qualifiedName )
	{
		for( AbstractJavaAccessNode methodNode : methodNodes ){
			
			codeInspector.getRefSet().clear() ;			
			codeInspector.processSingleMethod(methodNode) ;
			if( codeInspector.getRefSet().isEmpty() )
				continue ;
			
			getReferenceContainer().register(codeInspector.getDeclaration(), codeInspector.getRefSet() ) ;
		}		
	}
	
	private void processConstructors(
			IClassOutline outline, ArrayList<AbstractJavaAccessNode> constructorNodes, String qualifiedName )
	{
		for( AbstractJavaAccessNode methodNode : constructorNodes ){
			
			codeInspector.getRefSet().clear() ;			
			codeInspector.processSingleMethod(methodNode) ;
			if( codeInspector.getRefSet().isEmpty() )
				continue ;
			
			getReferenceContainer().register(codeInspector.getDeclaration(), codeInspector.getRefSet() ) ;
		}		
	}


	public static void fillMemberArrays(
			AbstractNode classNode,
			ArrayList<ASTFieldDeclaration> fieldNodes,
			ArrayList<AbstractJavaAccessNode> constructorNodes,
			ArrayList<AbstractJavaAccessNode> methodNodes,
			ArrayList<AbstractJavaAccessTypeNode> classNodes)
	{
		AbstractJavaNode bodyNode
				= classNode.getFirstDescendantOfType(ASTClassOrInterfaceBody.class);
		if(bodyNode==null)
			bodyNode = classNode.getFirstDescendantOfType(ASTEnumBody.class);

		List<ASTClassOrInterfaceBodyDeclaration> bodyDeclarationNodes
				= bodyNode.findChildrenOfType(ASTClassOrInterfaceBodyDeclaration.class);
		
		for( ASTClassOrInterfaceBodyDeclaration decl : bodyDeclarationNodes ){
			
			ASTMethodDeclaration methodNode
					= decl.getFirstChildOfType(ASTMethodDeclaration.class) ;
			
			if( methodNode != null ){
				methodNodes.add(methodNode) ;
				continue ;
			}
			
			ASTConstructorDeclaration constructorNode
					= decl.getFirstChildOfType(ASTConstructorDeclaration.class) ;
			if(constructorNode != null){
				constructorNodes.add(constructorNode) ;
				continue ;
			}
			
			ASTFieldDeclaration fieldNode
					= decl.getFirstChildOfType(ASTFieldDeclaration.class) ;
			
			if(fieldNode != null){
				fieldNodes.add(fieldNode) ;
				continue ;
			}
			
			ASTClassOrInterfaceDeclaration nestedClassNode
					= decl.getFirstChildOfType(ASTClassOrInterfaceDeclaration.class) ;
			
			if( nestedClassNode != null ){
				classNodes.add(nestedClassNode) ;
				continue ;
			}
			
			ASTEnumDeclaration nestedEnumNode
					= decl.getFirstChildOfType(ASTEnumDeclaration.class) ;
	
			if( nestedEnumNode != null ){
				classNodes.add(nestedEnumNode) ;
				continue ;
			}
		}
	}

	public IReferenceContainer getReferenceContainer() {
		return referenceContainer;
	}

	public void setReferenceContainer(IReferenceContainer referenceContainer) {
		this.referenceContainer = referenceContainer;
	}

	public AbstractOutlineAccessor getOutlieAccessor() {
		return outlieAccessor;
	}
	

	

	
}
