package net.junos.space.pmd.rules.j2ee.ejb3;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import net.junos.space.pmd.utils.outline.BasicOutlineContainer;
import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IJavaFieldDeclaration;
import net.junos.space.pmd.utils.outline.IJavaType;
import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;
import net.junos.space.pmd.utils.outline.QualifiedName;

public class RemoteInterfaceMethodsSignatureRule extends AbstractBusinessInterfacesInspector {
	
	@Override
	protected ArrayList<IClassOutline> extractTargetInterfaces(IJavaTypeDeclaration typeDeclaration){
		return extractRemoteInterfacesFromAnnotation(typeDeclaration);
	}

	public String getMessage(){
		return "Remote business interface methods must have argument and return value types which are legal types for RMI/IIOP." ;
	}
	
	@Override
	protected boolean isLegal(IJavaType returnType, HashSet<String> inspectedTypes) {
		
		boolean result = false ;
		String recognisedQualifiedName = null ;
		
l0:		while(true){
			IJavaTypeDeclaration decl = returnType.getDeclaration();
			if(decl == null){
				result = true ;
				break ;
			}
			
			QualifiedName qName = decl.getQualifiedName();
			if(qName == null){
				result = true ;
				return true ;
			}
			
			if(isCollection(qName)){
				
				List<IJavaType> genericParameters = returnType.getGenericParameters();
				if(genericParameters==null){
					result = true ;
					break ;
				}
				
				for(IJavaType gParamType : genericParameters){
					if(!isLegal(gParamType,inspectedTypes))
						break l0 ;
				}
				
				result = true ;
				break ;				
			}
			
			if(qName.hasValue("java.lang.Object"))
				break ;
			
			if(qName.getValues().size()!=1){
				result = true ;
				break ;
			}
			
			IClassOutline outline = getOutlineContainer().getClassOutline(recognisedQualifiedName);
			if(outline == null){
				result = true ;
				break ;
			}
			
			boolean implementsSerializable = mayImplementSerializable(decl);
			if(!implementsSerializable)
				break ;
			
			if(!inspectedTypes.contains(recognisedQualifiedName)){
				inspectedTypes.add(recognisedQualifiedName) ;
				
				ArrayList<IClassOutline> lst = new ArrayList<IClassOutline>() ;
				lst.add(outline) ;
				lst.addAll( outline.getDefinedSuperclasses( BasicOutlineContainer.getInstance() ) ) ;

				ArrayList<IJavaFieldDeclaration> allFields = new ArrayList<IJavaFieldDeclaration>() ; 
				for(IClassOutline o : lst){		
					allFields.addAll(o.getFields() );
				}

				for( IJavaFieldDeclaration fd : allFields ){
					if(!isLegal(fd.getType(),inspectedTypes))
						break l0 ;
				}
			}
			result = true ;
			break ;
		}
		if(recognisedQualifiedName != null)
			inspectedTypes.add(recognisedQualifiedName) ;
		
		return result ;
	}

}
