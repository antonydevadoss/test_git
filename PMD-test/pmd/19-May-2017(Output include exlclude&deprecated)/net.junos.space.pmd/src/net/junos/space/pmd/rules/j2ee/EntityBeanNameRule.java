package net.junos.space.pmd.rules.j2ee;

import net.junos.space.pmd.utils.outline.AbstractOutlineAccessor;
import net.junos.space.pmd.utils.outline.IClassOutline;
import net.sourceforge.pmd.RuleContext;

public class EntityBeanNameRule extends AbstractOutlineAccessor {

	@Override
	public void doEnd(RuleContext ctx) {
		for (IClassOutline outline : getOutlineContainer().getAllOutlines()) {
			if (isEnterpriseBeanEJB2(outline.getTypeDeclaration())) {
				if (!outline.getQualifiedNameString().endsWith("Bean")
						&& !outline.getQualifiedNameString().endsWith("EJB")) {
					reportError(ctx, outline, outline.getTypeDeclaration());
				}
			}
		}
		super.doEnd(ctx);
	}
	
	@Override
	public String getMessage() {
		return "Name an EJB Bean class like [Name]EJB or [Name]Bean";
	} 
}
