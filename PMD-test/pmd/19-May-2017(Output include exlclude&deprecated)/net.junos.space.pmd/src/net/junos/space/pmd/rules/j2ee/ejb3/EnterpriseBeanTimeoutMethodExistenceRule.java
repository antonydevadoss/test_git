package net.junos.space.pmd.rules.j2ee.ejb3;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IJavaAnnotation;
import net.junos.space.pmd.utils.outline.IJavaMethodDeclaration;
import net.junos.space.pmd.utils.outline.IJavaType;
import net.junos.space.pmd.utils.outline.QualifiedName;
import net.junos.space.pmd.utils.reference.SimpleReferenceRule;

public class EnterpriseBeanTimeoutMethodExistenceRule extends SimpleReferenceRule {
	
	private static final HashSet<String> targetClassesSet = new HashSet<String>(
			Arrays.asList( new String[]{
			
					"javax.ejb.TimerService",
					"javax.ejb.Timer"
	} ) );
	
	protected boolean mustBeChecked(String qualifiedName) {
		
		IClassOutline outline = getOutlineContainer().getClassOutline(qualifiedName);
		if(outline == null)
			return false ;
		
		if(!outline.hasAllSuperclassesDefined(getOutlineContainer()))
			return false ;
		
		if(!isEnterpriseBeanEJB3(qualifiedName))
			return false ;		
		
		return !hasATimeotMethod(qualifiedName);
	}	

	protected boolean isInvalid(String refQName)
	{
		boolean result = targetClassesSet.contains(refQName) ;
		return  result;
	}
	
	public String getMessage(){
		return "The enterprise bean that uses the timer service must provide a timeout callback method." ;
	}	
	
	private boolean hasATimeotMethod(String qualifiedName) {
		
		IClassOutline outline = getOutlineContainer().getClassOutline(qualifiedName);
		List<IJavaMethodDeclaration> methods = outline.getMethods();
		if(methods == null)
			return false ;
		
		for(IJavaMethodDeclaration md : methods){
			List<IJavaAnnotation> annotations = md.getAnnotations();
			if(annotations == null)
				continue ;
			
			for(IJavaAnnotation ann : annotations){
				if(ann.getName().equals("Timeout"))
					return true ;
			}
		}
		
		//javax.ejb.TimedObject method
		List<IJavaMethodDeclaration> methodsBySimpleName = outline.getMethodsBySimpleName("ejbTimeout");
		if(methodsBySimpleName==null)
			return false ;
		
		for( IJavaMethodDeclaration md : methodsBySimpleName ){
			if( md.getReturnType() != null )
				continue ;
			
			List<IJavaType> params = md.getParameters();
			if(params == null || params.size() != 1 )
				continue ;
			
			IJavaType paramType = params.get(0);
			QualifiedName qn = paramType.getQualifiedName();
			if( qn.getSimpleName().equals("Timer") )
				return true ;
		}

		return false;
	}

}
