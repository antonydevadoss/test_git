package net.junos.space.pmd.rules.j2ee.ejb3;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import net.junos.space.pmd.utils.outline.AbstractOutlineAccessor;
import net.junos.space.pmd.utils.outline.AnnotationClassMemberValue;
import net.junos.space.pmd.utils.outline.CollectionAndMapDescendants;
import net.junos.space.pmd.utils.outline.IAnnotationMemberValue;
import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IJavaAnnotation;
import net.junos.space.pmd.utils.outline.IJavaMethodDeclaration;
import net.junos.space.pmd.utils.outline.IJavaType;
import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;
import net.junos.space.pmd.utils.outline.MemberValueArray;
import net.junos.space.pmd.utils.outline.QualifiedName;
import net.sourceforge.pmd.RuleContext;

public abstract class AbstractBusinessInterfacesInspector extends
		AbstractOutlineAccessor {

	public AbstractBusinessInterfacesInspector() {
		super();
	}
	
	protected abstract ArrayList<IClassOutline> extractTargetInterfaces(IJavaTypeDeclaration typeDeclaration);

	protected boolean isLegal(IJavaType returnType, HashSet<String> inspectedTypes){
		return true ;
	};	

	@Override
	public void doEnd(RuleContext ctx) {
		for( IClassOutline outline : getOutlineContainer().getAllOutlines() ){
			
			IJavaTypeDeclaration typeDeclaration = outline.getTypeDeclaration();
			String qualifiedname = outline.getQualifiedNameString() ;
			if(!isSessionBeanEJB3(qualifiedname) )
				continue ;
			
			ArrayList<IClassOutline> targetInterfaces_ = extractTargetInterfaces(typeDeclaration);			
			if(targetInterfaces_ == null || targetInterfaces_.isEmpty())
				continue ;
			
			List<IClassOutline> targetInterfaces = includeSuperClasses(targetInterfaces_) ;
			
			HashSet<IJavaMethodDeclaration> methodsToInspect = new HashSet<IJavaMethodDeclaration>() ; 
			for( IClassOutline interfaceOutline : targetInterfaces ){
				List<IJavaMethodDeclaration> interfaceMethods = interfaceOutline.getMethods();
				for(IJavaMethodDeclaration interfaceMethod : interfaceMethods){
					
					if(inspectInterfaceMethods())
						methodsToInspect.add(interfaceMethod) ;
					
					String methodName = interfaceMethod.getSimpleName();
					IJavaType returnType = interfaceMethod.getReturnType();
					List<IJavaType> params = interfaceMethod.getParameters();
					
					IJavaMethodDeclaration methodImplementation = null ;
					ArrayList<IClassOutline> outlineList = new ArrayList<IClassOutline>() ;
					outlineList.add(outline) ;
					for( int i = 0 ; i < outlineList.size() ; i++ ){
						
						IClassOutline o = outlineList.get(i) ;
						methodImplementation = o.getMethod( methodName, returnType, params);
						if(methodImplementation!=null)
							break ;
						
						IJavaType superClass = o.getTypeDeclaration().getSuperClass();
						if(superClass==null)
							continue  ;
						
						QualifiedName superQName = superClass.getQualifiedName();
						if(superQName==null)
							continue ;
						
						for(String s : superQName.getValues()){
							IClassOutline o1 = getOutlineContainer().getClassOutline(s);
							if(o1==null)
								continue ;
							
							outlineList.add(o1) ;
						}
					}
					if(methodImplementation == null)
						continue ;
					
					methodsToInspect.add(methodImplementation) ;
				}				
			}
			if(methodsToInspect.isEmpty())
				continue ;
			
			HashSet<String> inspectedTypes = new HashSet<String>() ;
			for( IJavaMethodDeclaration method : methodsToInspect ){
				inspectMethod( method, ctx, inspectedTypes ) ;
			}			
		}
		super.doEnd(ctx);
	}
	
	protected boolean inspectInterfaceMethods(){
		return false ;
	}

	protected void inspectMethod(IJavaMethodDeclaration method, RuleContext ctx, HashSet<String> inspectedTypes) {
		
		IJavaType returnType = method.getReturnType();
		inspectedTypes.clear() ;
		if( returnType != null && !isLegal(returnType,inspectedTypes)){
			String ownerQualifiedName = method.getOwner().getQualifiedName().getValues().iterator().next() ;
			IClassOutline ownerOutline = getOutlineContainer().getClassOutline(ownerQualifiedName);
			reportError(ctx, ownerOutline, method) ;
			return ;
		}
		
		List<IJavaType> params = method.getParameters();
		if(params == null || params.isEmpty())
			return ;
		
		for(IJavaType paramType : params){
			inspectedTypes.clear() ;
			if(paramType != null && !isLegal(paramType,inspectedTypes)){
				String ownerQualifiedName = method.getOwner().getQualifiedName().getValues().iterator().next() ;
				IClassOutline ownerOutline = getOutlineContainer().getClassOutline(ownerQualifiedName);
				reportError(ctx, ownerOutline, method) ;
				return ;
			}
		}
	}

	protected boolean isCollection(QualifiedName qName) {
		
		for(String s : qName.getValues()){
			if(CollectionAndMapDescendants.allowedEntityFieldTypes.contains(s)
					|| CollectionAndMapDescendants.javaUtilCollectionAndMapDescendants.contains(s))
				return true ;
		}
		return false;
	}

	protected boolean mayImplementSerializable(IJavaTypeDeclaration decl)
	{
		List<IJavaTypeDeclaration> undefinedInterfaces = decl.getUndefinedInterfaces(getOutlineContainer());
		if(!undefinedInterfaces.isEmpty())
			return true ;
		
		List<IJavaTypeDeclaration> undefinedSuperClasses = decl.getUndefinedSuperClasses(getOutlineContainer());
		if(!undefinedSuperClasses.isEmpty())
			return true ;
		
		List<IJavaTypeDeclaration> allInterfaces = decl.getAllInterfaces();
		boolean implementsSerializable = false ;
		for(IJavaTypeDeclaration iType : allInterfaces){
			QualifiedName iQName = iType.getQualifiedName();
			if(iQName == null)
				continue ;
			
			implementsSerializable = iQName.hasValue("java.io.Serializable") ;
		}
		return implementsSerializable;
	}

	protected ArrayList<IClassOutline> extractRemoteInterfacesFromAnnotation(IJavaTypeDeclaration typeDeclaration) {
		return extractBIOutlinesFromAnnotations(typeDeclaration, new String[]{"Remote"}) ;
	}

	protected ArrayList<IClassOutline> extractLocalInterfacesFromAnnotation(IJavaTypeDeclaration typeDeclaration) {
		return extractBIOutlinesFromAnnotations(typeDeclaration, new String[]{"Local"}) ;
	}
	
	protected ArrayList<IJavaTypeDeclaration> extractRemoteDeclarationsFromAnnotation(IJavaTypeDeclaration typeDeclaration) {
		return extractBIDeclarationsFromAnnotations(typeDeclaration, new String[]{"Remote"}) ;
	}

	protected ArrayList<IJavaTypeDeclaration> extractLocalDeclarationsFromAnnotation(IJavaTypeDeclaration typeDeclaration) {
		return extractBIDeclarationsFromAnnotations(typeDeclaration, new String[]{"Local"}) ;
	}

	protected ArrayList<IClassOutline> extractBIOutlinesFromAnnotations(
			IJavaTypeDeclaration typeDeclaration, String[] kinds) {
		
		ArrayList<IClassOutline> remoteInterfaces = new ArrayList<IClassOutline>() ;
		
		ArrayList<IJavaTypeDeclaration> declarations =
				extractBIDeclarationsFromAnnotations(typeDeclaration, kinds) ;

		if(declarations == null)
			return remoteInterfaces ;
		
		for(IJavaTypeDeclaration decl : declarations){
				
			QualifiedName qName = decl.getQualifiedName();
			if(qName == null)
				continue ;
				
			if(qName.getValues().size()!=1)
				continue ;
			
			String recVal = qName.getValues().iterator().next() ;				
			IClassOutline remoteInterfaceOutline
				= this.getOutlineContainer().getClassOutline(recVal);
				
			if(remoteInterfaceOutline==null)						
				continue ;
			
			remoteInterfaces.add(remoteInterfaceOutline) ;					
		}		
		return remoteInterfaces;
	}
	
	protected ArrayList<IJavaTypeDeclaration> extractBIDeclarationsFromAnnotations(
			IJavaTypeDeclaration typeDeclaration, String[] kinds) {
		
		ArrayList<String> kindsList = new ArrayList<String>() ;
		if(kinds == null){
			kindsList.add("Local") ;
			kindsList.add("Remote") ;
		}
		else{
			for( String s : kinds)
				kindsList.add(s) ;
		}
		
		ArrayList<IJavaTypeDeclaration> remoteInterfaces = null ;
		while(true){
			
			List<IJavaAnnotation> annotations = typeDeclaration.getAnnotations();
			if(annotations == null)
				break ;
			
			IJavaAnnotation remoteAnnotation = null ;
			
			for(IJavaAnnotation ann : annotations){
				String name = ann.getName();
				if( !kindsList.contains(name) )
					continue ;
				
				remoteAnnotation = ann;
				break ;					
			}
			
			if(remoteAnnotation == null)
				break ;
			
			ArrayList<AnnotationClassMemberValue> lst = new ArrayList<AnnotationClassMemberValue>() ;
			IAnnotationMemberValue val = remoteAnnotation.getMemberValue("value");
			if(val instanceof AnnotationClassMemberValue){
				lst.add((AnnotationClassMemberValue) val) ;
			}
			else if(val instanceof MemberValueArray){
				MemberValueArray mva = (MemberValueArray) val ;
				ArrayList<IAnnotationMemberValue> values = mva.getValues();
				if(values != null){
					for(IAnnotationMemberValue arrMember : values){
						if(!(arrMember instanceof AnnotationClassMemberValue))
							continue ;
						
						lst.add((AnnotationClassMemberValue) arrMember) ;
					}
				}
			}
			if(lst.isEmpty())
				break ;
			
			for( AnnotationClassMemberValue acmv : lst ){
				IJavaType type = acmv.getType();
				if(type == null)
					continue ;
				
				IJavaTypeDeclaration decl = type.getDeclaration();
				if(decl == null)
					continue ;
				
				if(remoteInterfaces==null)
					remoteInterfaces = new ArrayList<IJavaTypeDeclaration>() ;
				
				remoteInterfaces.add(decl) ;					
			}			
			break ;
		}
		return remoteInterfaces;
	}
	
	protected ArrayList<IJavaTypeDeclaration> extractPossibleBusinessInterfaces(IJavaTypeDeclaration typeDeclaration){
		
		ArrayList<IJavaTypeDeclaration> result = new ArrayList<IJavaTypeDeclaration>() ;
		List<IJavaAnnotation> annotations = typeDeclaration.getAnnotations();
		if(annotations != null){
			for(IJavaAnnotation ann : annotations)
			{
				if(ann.getName().equals("Remote"))
					return result ;
				
				if(ann.getName().equals("Loacl"))
					return result ;
				
				if(ann.getName().equals("javax.ejb.Remote"))
					return result ;
				
				if(ann.getName().equals("javax.ejb.Remote"))
					return result ;				
			}			
		}
		
		List<IJavaType> interfaces = typeDeclaration.getInterfaces();
		if(interfaces==null)
			return result ;
		
l0:		for(IJavaType iType : interfaces){
			QualifiedName qName = iType.getQualifiedName();
			if(qName==null)
			{
				//according to OutlineCollecro dataflow, qualified name always !=null
				//in fact, this case is some strange error
				result.clear() ;
				break ;
			}
			
			List<String> values = qName.getValues();
			for(String s : values){
				if(!isValibBusinessInterfaceName(s))
					continue l0;
			}			
			IJavaTypeDeclaration declaration = iType.getDeclaration();
			if(declaration==null){
				//according to OutlineCollecro dataflow, declaration always !=null
				//in fact, this case is some strange error
				result.clear();
				break ;
			}
			result.add(declaration) ;
		}
		
		return result ;
	}

	protected boolean isValibBusinessInterfaceName(String str) {
		
		if(str.equals("java.io.Serializable"))
			return false ;
		
		if(str.equals("java.io.Externalizable"))
			return false ;
		
		if(str.startsWith("javax.ejb"))
			return false ;
		
		return true ;
	}
	
	protected List<IClassOutline> includeSuperClasses(List<IClassOutline> lst){
		HashSet<IClassOutline> set = new HashSet<IClassOutline>() ;
		for(IClassOutline outline : lst){
			set.add(outline) ;
			
			List<IJavaTypeDeclaration> allInterfaces = outline.getTypeDeclaration().getAllInterfaces();
			for(IJavaTypeDeclaration iDecl : allInterfaces){
				
				List<String> values = iDecl.getQualifiedName().getValues();
				if(values.size()!=1)
					continue ;
				
				String recVal = values.iterator().next() ;
				IClassOutline o = getOutlineContainer().getClassOutline(recVal);
				if(o == null)
					continue ;
				
				set.add(o) ;
			}
		}
		ArrayList<IClassOutline> resut = new ArrayList<IClassOutline>(set);
		return resut ;
	}
	
	protected ArrayList<IClassOutline> extractAllBusinessInterfaces(IJavaTypeDeclaration typeDeclaration)
	{		
		ArrayList<IClassOutline> result = new ArrayList<IClassOutline>();
		
		ArrayList<IClassOutline> localInterfacesFromAnnotation = extractLocalInterfacesFromAnnotation(typeDeclaration);
		ArrayList<IClassOutline> remoteInterfacesFromAnnotation = extractRemoteInterfacesFromAnnotation(typeDeclaration);
		
		if (localInterfacesFromAnnotation != null) {
			result.addAll(localInterfacesFromAnnotation);
		}
		
		if (remoteInterfacesFromAnnotation != null) {
			result.addAll(remoteInterfacesFromAnnotation);
		}
		
		if(!result.isEmpty())
			return result ;
		
		ArrayList<IJavaTypeDeclaration> possibleBusinessInterfaces = extractPossibleBusinessInterfaces(typeDeclaration);
		if (possibleBusinessInterfaces.size() == 1) {
			IJavaTypeDeclaration businessInterfaceTD = possibleBusinessInterfaces.get(0);
			for (String interfaceQName : businessInterfaceTD.getQualifiedName().getValues()) {
				IClassOutline classOutline = getOutlineContainer().getClassOutline(interfaceQName);
				if (classOutline != null) {
					result.add(classOutline);
				}
			}
		}		
		return result;
	}

}