package net.junos.space.pmd.utils.outline;

public interface IEnumConstant {
	
	String getName();

}
