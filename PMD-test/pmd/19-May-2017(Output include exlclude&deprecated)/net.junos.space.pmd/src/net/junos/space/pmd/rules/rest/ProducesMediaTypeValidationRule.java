/*******************************************************************************
 * FILE NAME: ProduceValidationRule.java
 * PURPOSE:  This is Rule-16 in document 
 *
 *
 * Revision History: 
 * DATE:           				  AUTHOR:              	CHANGE:  
 * Tue Feb 18th Feb, 2011               Atul Bajpai       	Initial Version  
 * 
 * 
 ******************************************************************************/
package net.junos.space.pmd.rules.rest;

import java.util.List;import net.junos.space.pmd.utils.PMDConstants;import net.junos.space.pmd.utils.PMDUtils;import net.sourceforge.pmd.RuleContext;import net.sourceforge.pmd.lang.java.ast.ASTMethodDeclarator;import net.sourceforge.pmd.lang.java.rule.AbstractJavaRule;import org.apache.log4j.Logger;

/**
 * This class checks whether or not @Produces annotation defined on REST method
 * or class and method is returning DTO, but the media type in @Produces annotation
 * does not start with 'application/'
 */
public class ProducesMediaTypeValidationRule extends AbstractJavaRule {

	/**
	 * Logger
	 */
	private static Logger log = Logger.getLogger(ProducesMediaTypeValidationRule.class);
	
	/**
	 * Visits all the REST methods in this class and checks the validity of 
	 * media types of @Produces annotation.
	 */
	public Object visit(ASTMethodDeclarator methodNode, Object data) {
		boolean isRestMethod = false;
		boolean isProducesOnMethodExists = false;
		boolean isProducesOnClassExists = false;
		boolean isValidProduces = true;
		boolean isValidDTO = false;
		
		RuleContext ctx = (RuleContext) data;
		String methodName = methodNode.getImage();
		try{
			// checks whether the method is REST or not.
			isRestMethod = PMDUtils.isRestMethod(methodNode);
			if (isRestMethod) {
				// checks if parameter is DTO type
				isValidDTO = PMDUtils.isReturningDTO(methodNode);
				if (isValidDTO) {
					isProducesOnMethodExists = PMDUtils.isMethodAnnotationExists(methodNode, PMDConstants.PRODUCES_ANNOTATION);
					isProducesOnClassExists = PMDUtils.isClassAnnotationExists(methodNode, PMDConstants.PRODUCES_ANNOTATION);
					if (isProducesOnMethodExists) {
						List<String> methodProducesAnnotationValues = PMDUtils.getMethodAnnotationValue(methodNode, PMDConstants.PRODUCES_ANNOTATION);
						isValidProduces = PMDUtils.isMediaTypeStartWithApplication(methodProducesAnnotationValues);					
					}
					else if(!isProducesOnMethodExists && isProducesOnClassExists){
						List<String> classProducesAnnotationValues = PMDUtils.getClassAnnotationValue(methodNode, PMDConstants.PRODUCES_ANNOTATION);
						isValidProduces = PMDUtils.isMediaTypeStartWithApplication(classProducesAnnotationValues);
					}
				}
			}
			
			if(log.isDebugEnabled()){
				StringBuilder builder = new StringBuilder();
				builder.append("source file=").append(ctx.getSourceCodeFilename()).append(",method=").append(methodName);
				builder.append(",isRestMethod=").append(isRestMethod).append(",isValidDTO=").append(isValidDTO).append(",isProducesOnMethodExists=");
				builder.append(isProducesOnMethodExists).append(",isProducesOnClassExists=").append(isProducesOnClassExists).append(",isValidProduces=").append(isValidProduces);
				log.debug(builder.toString());
			}
			
			if (!isValidProduces)
				addViolation(data, methodNode);
		}catch(Exception e){
			log.error("Exception in executing method " + methodName + " of " + ctx.getSourceCodeFilename(), e);
		}
		return super.visit(methodNode, data);
	}	
}
