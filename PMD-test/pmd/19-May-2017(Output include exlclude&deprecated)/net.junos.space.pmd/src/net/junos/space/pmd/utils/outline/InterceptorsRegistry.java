package net.junos.space.pmd.utils.outline;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class InterceptorsRegistry {
	
	private static InterceptorsRegistry instance ;

	public static InterceptorsRegistry getInstance() {
		if(instance == null)
			instance = new InterceptorsRegistry() ;
		
		return instance;
	}
	
	private HashMap<String,ArrayList<String>> interceptorsMap = null ;
	
	public List<String> getOwningClasses(String interceptorName){
		
		if(!isInitialized())
			return null ;
		
		return interceptorsMap.get(interceptorName) ;		
	}
	
	public boolean isInterceptor(String className){
		if(!isInitialized())
			return false ;
		
		return interceptorsMap.containsKey(className) ;
	}
	
	public List<String> getAllInterceptors(){
		if(!isInitialized())
			return null ;
		
		return new ArrayList<String>(interceptorsMap.keySet()) ;
	}
	
	public boolean isInitialized(){		
		if(interceptorsMap != null)
			return true ;
		
		initMap() ;
		
		return interceptorsMap != null ;
	}

	private void initMap() {
		
		BasicOutlineContainer boc = BasicOutlineContainer.getInstance();
		if(!boc.isComplete())
			return ;
		
		List<IClassOutline> allOutlines = boc.getAllOutlines();
		for( IClassOutline o : allOutlines ){
			
			IJavaTypeDeclaration decl = o.getTypeDeclaration();
			List<IJavaAnnotation> annotations = decl.getAnnotations();
			if(annotations == null)
				continue ;
			
			IJavaAnnotation interceptorsAnnotation = null ;
			for( IJavaAnnotation ann : annotations ){
				if(!ann.getName().equals("Interceptors"))
					continue ;
				
				interceptorsAnnotation = ann ;
				break ;
			}
			
			if(interceptorsAnnotation == null)
				continue ;
			
			ArrayList<String> interceptorClasses = extractInterceptorClasses(interceptorsAnnotation);
			
			if(interceptorClasses==null||interceptorClasses.isEmpty())
				continue ;
			
			String qName = o.getQualifiedNameString();
			registerInterceptors(qName,interceptorClasses) ;
		}
	}

	private void registerInterceptors(String qName,	ArrayList<String> interceptorClasses)
	{
		if(interceptorsMap == null)
			interceptorsMap = new HashMap<String, ArrayList<String>>() ;
		
		for(String str : interceptorClasses){
			ArrayList<String> arr = interceptorsMap.get(str);
			if(arr == null){
				arr = new ArrayList<String>() ;
				interceptorsMap.put(str,arr) ;
			}
			arr.add(qName) ;				
		}		
	}

	private ArrayList<String> extractInterceptorClasses(IJavaAnnotation interceptorsAnnotation)
	{
		ArrayList<String> interceptorClasses = new ArrayList<String>() ;		
		IAnnotationMemberValue val = interceptorsAnnotation.getMemberValue("value");
		if(val instanceof MemberValueArray){
			MemberValueArray mva = (MemberValueArray) val ;
			ArrayList<IAnnotationMemberValue> values = mva.getValues();
			for(IAnnotationMemberValue member : values){
				if(!(member instanceof AnnotationClassMemberValue))
					continue ;
				
				AnnotationClassMemberValue acmv = (AnnotationClassMemberValue) member ;
				QualifiedName qName = acmv.getType().getQualifiedName();
				if(qName.getValues().size() == 1){
					interceptorClasses.add(qName.getValues().iterator().next()) ;			
				}
			}
			
		} else if(val instanceof AnnotationClassMemberValue){
			AnnotationClassMemberValue acmv = (AnnotationClassMemberValue) val ;
			QualifiedName qName = acmv.getType().getQualifiedName();
			if(qName.getValues().size() == 1){
				interceptorClasses.add(qName.getValues().iterator().next()) ;			
			}
		}
		
		return interceptorClasses;
	}
	
}
