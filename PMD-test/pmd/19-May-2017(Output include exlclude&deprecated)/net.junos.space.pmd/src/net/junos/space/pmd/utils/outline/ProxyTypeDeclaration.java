package net.junos.space.pmd.utils.outline;

import java.util.Collection;
import java.util.List;

public class ProxyTypeDeclaration implements IJavaTypeDeclaration {
	
	IJavaTypeDeclaration referencedDeclaration ;

	public IJavaTypeDeclaration getReferencedDeclaration() {
		return referencedDeclaration;
	}

	public void setReferencedDeclaration(IJavaTypeDeclaration referencedDeclaration) {
		this.referencedDeclaration = referencedDeclaration;
	}

	@Override
	public boolean isPublic() {
		return referencedDeclaration.isPublic() ;
	}

	@Override
	public boolean isProtected() {
		return referencedDeclaration.isProtected() ;
	}

	@Override
	public boolean isPrivate() {
		return referencedDeclaration.isPrimitive() ;
	}

	@Override
	public boolean isDefault() {
		return referencedDeclaration.isDefault() ;
	}

	@Override
	public void setPublic() {
		referencedDeclaration.setPublic() ;		
	}

	@Override
	public void setProtected() {
		referencedDeclaration.setProtected() ;		
	}

	@Override
	public void setPrivate() {
		referencedDeclaration.setPrivate() ;		
	}

	@Override
	public void setDefault() {
		referencedDeclaration.setDefault() ;		
	}

	@Override
	public boolean isStatic() {
		return referencedDeclaration.isStatic() ;		
	}

	@Override
	public void setStatic(boolean isStatic) {
		referencedDeclaration.setStatic(isStatic) ;		
	}

	@Override
	public boolean isFinal() {		
		return referencedDeclaration.isFinal() ;
	}

	@Override
	public void setFinal(boolean isFinal) {
		referencedDeclaration.setFinal(isFinal) ;		
	}

	@Override
	public String getSimpleName() {
		return referencedDeclaration.getSimpleName() ;
	}

	@Override
	public void setSimpleName(String simpleName) {
		referencedDeclaration.setSimpleName(simpleName) ;		
	}

	@Override
	public QualifiedName getQualifiedName() {
		return referencedDeclaration.getQualifiedName() ;
	}

	@Override
	public void setQualifiedName(QualifiedName qualifiedName) {
		referencedDeclaration.setQualifiedName(qualifiedName) ;		
	}

	@Override
	public String getPackage() {		
		return referencedDeclaration.getPackage() ;
	}

	@Override
	public void setPackage(String packageName) {
		referencedDeclaration.setPackage(packageName) ;		
	}

	@Override
	public IJavaTypeDeclaration getOwner() {
		return referencedDeclaration.getOwner() ;
	}

	@Override
	public void setOwner(IJavaTypeDeclaration owner) {
		referencedDeclaration.setOwner(owner) ;
	}

	@Override
	public int[] getLines() {
		return referencedDeclaration.getLines();
	}

	@Override
	public void setLines(int... lines) {
		referencedDeclaration.setLines(lines) ;		
	}

	@Override
	public List<IJavaAnnotation> getAnnotations() {
		return referencedDeclaration.getAnnotations() ;
	}

	@Override
	public void setAnnotations(List<IJavaAnnotation> annotations) {
		referencedDeclaration.setAnnotations(annotations) ;		
	}

	@Override
	public boolean isAbstract() {
		return referencedDeclaration.isAbstract() ;
	}

	@Override
	public void setAbstract(boolean isAbstract) {
		referencedDeclaration.setAbstract(isAbstract) ;		
	}

	@Override
	public int getGenericParamsCount() {
		return referencedDeclaration.getGenericParamsCount() ;
	}

	@Override
	public void setGenericParamsCount(int count) {
		referencedDeclaration.setGenericParamsCount(count) ;		
	}

	@Override
	public List<String> getGenericParamNames() {
		return referencedDeclaration.getGenericParamNames() ;
	}

	@Override
	public void setGenericParamNames(Collection<String> paramNames) {
		referencedDeclaration.setGenericParamNames(paramNames) ;
	}

	@Override
	public int getOrder(String paramName) {
		return referencedDeclaration.getOrder(paramName);
	}

	@Override
	public boolean isPrimitive() {
		return referencedDeclaration.isPrimitive();
	}

	@Override
	public void setPrimitive(boolean isPrimitive) {
		referencedDeclaration.setPrimitive(isPrimitive) ;		
	}

	@Override
	public boolean isSimilar(IJavaTypeDeclaration other) {
		return referencedDeclaration.isSimilar(other);
	}

	@Override
	public boolean isInterface() {
		return referencedDeclaration.isInterface();
	}

	@Override
	public void setIsInterface(boolean isInterface) {
		referencedDeclaration.setIsInterface(isInterface) ;
		
	}

	@Override
	public IJavaType getSuperClass() {
		return  referencedDeclaration.getSuperClass();
	}

	@Override
	public void setSuperClass(IJavaType superClass) {
		referencedDeclaration.setSuperClass(superClass) ;
		
	}

	@Override
	public List<IJavaType> getInterfaces() {
		return referencedDeclaration.getInterfaces() ;
	}

	@Override
	public List<IJavaTypeDeclaration> getAllInterfaces() {
		return referencedDeclaration.getAllInterfaces() ;
	}

	@Override
	public List<IJavaTypeDeclaration> getAllSuperClasses() {
		return referencedDeclaration.getAllSuperClasses() ;
	}

	@Override
	public void setInterfaces(Collection<IJavaType> interfaces) {
		referencedDeclaration.setInterfaces(interfaces) ;
	}

	@Override
	public List<IJavaTypeDeclaration> getNestedClasses() {
		return referencedDeclaration.getNestedClasses();
	}

	@Override
	public void setNestedClasses(Collection<IJavaTypeDeclaration> nestedClasses) {
		referencedDeclaration.setNestedClasses(nestedClasses) ;
	}

	@Override
	public void addSubclass(IJavaTypeDeclaration subclass) {
		referencedDeclaration.addSubclass(subclass) ;
	}

	@Override
	public List<IJavaTypeDeclaration> getSubclasses() {
		return referencedDeclaration.getSubclasses();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((referencedDeclaration == null) ? 0 : referencedDeclaration
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		
		if(!(obj instanceof IJavaTypeDeclaration))
			return false ;
		
		
		IJavaTypeDeclaration other = (IJavaTypeDeclaration) obj ;
		boolean sameQName = this.getQualifiedName().equals( other.getQualifiedName() ) ;		
		
		return sameQName;
	}

	@Override
	public boolean isEnum() {
		return referencedDeclaration.isEnum() ;
	}

	@Override
	public void setIsEnum(boolean isEnum) {
		referencedDeclaration.setIsEnum(isEnum) ;		
	}

	@Override
	public List<IEnumConstant> getEnumConstants() {
		return referencedDeclaration.getEnumConstants() ;
	}

	@Override
	public void setEnumConstants(List<IEnumConstant> enumConstants) {
		referencedDeclaration.setEnumConstants( enumConstants );		
	}

	@Override
	public boolean hasAnnotation(String annotationName) {
		return referencedDeclaration.hasAnnotation(annotationName);
	}

	@Override
	public List<IJavaTypeDeclaration> getUndefinedInterfaces( IOutlineContainer container) {
		return referencedDeclaration.getUndefinedInterfaces(container) ;
	}

	@Override
	public List<IJavaTypeDeclaration> getUndefinedSuperClasses( IOutlineContainer container) {
		return referencedDeclaration.getUndefinedSuperClasses(container);
	}

}
