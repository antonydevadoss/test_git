package net.junos.space.pmd;

public class ClassContext {
	private String packageName;
	private String className;
	private String projectRoot;
	private String contextRoot;
	private String pathAnnotationValue;
	private String serviceName;
	
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public String getProjectRoot() {
		return projectRoot;
	}
	public void setProjectRoot(String projectRoot) {
		this.projectRoot = projectRoot;
	}
	public String getContextRoot() {
		return contextRoot;
	}
	public void setContextRoot(String contextRoot) {
		this.contextRoot = contextRoot;
	}
	public String getPathAnnotationValue() {
		return pathAnnotationValue;
	}
	public void setPathAnnotationValue(String pathAnnotationValue) {
		this.pathAnnotationValue = pathAnnotationValue;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	
	public ClassContext clone() {
		ClassContext clone = new ClassContext();
		clone.setClassName(getClassName());
		clone.setContextRoot(getContextRoot());
		clone.setPackageName(getPackageName());
		clone.setPathAnnotationValue(getPathAnnotationValue());
		clone.setProjectRoot(getProjectRoot());
		return clone;
	}
}
