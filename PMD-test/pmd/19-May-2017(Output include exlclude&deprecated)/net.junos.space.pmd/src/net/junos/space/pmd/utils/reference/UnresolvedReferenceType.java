package net.junos.space.pmd.utils.reference;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import net.junos.space.pmd.utils.outline.IJavaType;
import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;
import net.junos.space.pmd.utils.outline.QualifiedName;

public class UnresolvedReferenceType implements IJavaType {

	@Override
	public IJavaTypeDeclaration getDeclaration() {
		return null;
	}

	@Override
	public void setDeclaration(IJavaTypeDeclaration declaration) {}

	@Override
	public boolean isArray() {
		return false;
	}

	@Override
	public void setIsArray(boolean isArray) {}

	@Override
	public List<IJavaType> getGenericParameters() {
		return null;
	}

	@Override
	public void setGenericParameters(Collection<IJavaType> params) {}

	@Override
	public int getArrayDepth() {
		return 0;
	}

	@Override
	public void setArrayDepth(int arrayDepth) {}

	@Override
	public String getSimpleName() {
		return "Unresolved Reference Type";
	}

	@Override
	public void setSimpleName(String simpleName) {}

	@Override
	public QualifiedName getQualifiedName() {
		return new QualifiedName("Unresolved Reference Type",
				Arrays.asList(new String[]{"Unresolved Reference Type","Unresolved Reference Type"}));
	}

	@Override
	public void setQualifiedName(QualifiedName QualifiedName) {}

	@Override
	public boolean isSimilar(IJavaType another) {
		return false;
	}

}
