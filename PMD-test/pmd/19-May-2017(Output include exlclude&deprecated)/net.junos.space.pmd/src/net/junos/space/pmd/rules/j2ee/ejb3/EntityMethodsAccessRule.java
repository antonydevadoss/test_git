package net.junos.space.pmd.rules.j2ee.ejb3;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import net.junos.space.pmd.utils.outline.AbstractEntityRule;
import net.junos.space.pmd.utils.outline.EjbEntity;
import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IJavaMethodDeclaration;
import net.sourceforge.pmd.RuleContext;

public class EntityMethodsAccessRule extends AbstractEntityRule {

	private static final String message = "No methods of the entity or embeddedable class may be final.";
	
	@Override
	protected void processEntities(RuleContext ctx) {
		if (AbstractEntityRule.entityRegistry == null) {
			return;
		}
		for (EjbEntity ejbEntity : AbstractEntityRule.entityRegistry.values()) {
			IClassOutline classOutline = getOutlineContainer().getClassOutline(ejbEntity.getQualifiedName());
			List<IJavaMethodDeclaration> methods = classOutline.getMethods();

			for (IJavaMethodDeclaration method : methods) {
				if (method.isFinal()) {
					reportError(ctx,method,message) ;
				}
			}
		}
		super.processEntities(ctx);
	}
	
	protected Set<String> getTargetAnnotations() {
		HashSet<String> set = new HashSet<String>();
		set.add("Entity") ;
		set.add("Embeddable") ;
		return set ;
	}
}
