package net.junos.space.pmd.rules.j2ee;


import org.w3c.dom.Element;
import net.junos.space.pmd.rules.xml.AbstractXMLRule;
import net.junos.space.pmd.rules.xml.SimpleXpathRule;

public class UseACorrectNameForAJDBCEnvironmentReferenceNameRule extends SimpleXpathRule {
	
	

	@Override
	protected String getXpathExpression() {
		return "//resource-ref/res-ref-name";
	}

	public String getMessage() {
		return "Use a correct name for a JDBC environment reference name. All references to JDBC resources must be organized in the jdbc subcontext of the application component's environment.";
	}

	protected String getTargetXMLName() {
		return AbstractXMLRule.XML_NAME_EJB_JAR;
	}

	@Override
	protected boolean isValid(Element element) {		
		String str = element.getTextContent();
		if(str == null)
			return false ;
		
		str = str.toLowerCase() ;
		return str.startsWith("jdbc/");
	}
}
