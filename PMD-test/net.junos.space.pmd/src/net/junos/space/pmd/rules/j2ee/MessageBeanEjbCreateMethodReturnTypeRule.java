package net.junos.space.pmd.rules.j2ee;

import java.util.ArrayList;
import java.util.List;

import net.junos.space.pmd.utils.outline.AbstractOutlineAccessor;
import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IJavaMethodDeclaration;
import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;
import net.sourceforge.pmd.RuleContext;

public class MessageBeanEjbCreateMethodReturnTypeRule extends AbstractOutlineAccessor {

	@Override
	public void doEnd(RuleContext ctx) {
		
		for (IClassOutline outline : getOutlineContainer().getAllOutlines()) {
				
			IJavaTypeDeclaration decl = outline.getTypeDeclaration();
				
			if ( !isMessageDrivenBeanEJB2(decl) )
				continue ;
			
			List<IClassOutline> definedSuperclasses = outline.getDefinedSuperclasses(getOutlineContainer());
			ArrayList<IClassOutline> hierarchy = new ArrayList<IClassOutline>() ;
			hierarchy.add(outline) ;			
			hierarchy.addAll(definedSuperclasses) ;
			
			for(IClassOutline o : hierarchy)
			{
				for (IJavaMethodDeclaration method : o.getMethodsBySimpleName("ejbCreate")) {
					if (method.getReturnType() != null) {
						reportError(ctx, outline, outline.getTypeDeclaration());
					}
				}
			}			
		}
		super.doEnd(ctx);
	}
	
	@Override
	public String getMessage() {
		return "Return void for an ejbCreate method of a Message Bean";
	}
}
