package net.junos.space.pmd.rules.j2ee;

import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;

public class SessionBeanEjbCreateReturnTypeRule extends
			AbstractEjbPostCreateReturnTypeRule {
				
	@Override
	public boolean isProperBean(IJavaTypeDeclaration decl) {
		return isSessionBeanEJB2(decl);
	}
	
	@Override
	public String getMessage() {
		return "Enterprise session beans must declare one or more ejbCreate methods of which the return type must be void." ;
	}
	
	public String getMethodName() {
		return "ejbCreate";
	}
}
