package net.junos.space.pmd.rules.j2ee.ejb3;

import java.util.List;

import net.junos.space.pmd.utils.outline.AbstractEntityRule;
import net.junos.space.pmd.utils.outline.AnnotationClassMemberValue;
import net.junos.space.pmd.utils.outline.EjbEntity;
import net.junos.space.pmd.utils.outline.IAnnotationMemberValue;
import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IJavaAnnotation;
import net.junos.space.pmd.utils.outline.IJavaFieldDeclaration;
import net.junos.space.pmd.utils.outline.IJavaMethodDeclaration;
import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;
import net.sourceforge.pmd.RuleContext;

public class EntityPrimaryKeyEqualsAndHashCodeRule extends AbstractEntityRule {
	private static final String message = "The composite primary key class must define equals and hashCode methods.";

	@Override
	protected void processEntities(RuleContext ctx) {
		if (AbstractEntityRule.entityRegistry == null) {
			return;
		}
		
		for (EjbEntity ejbEntity : AbstractEntityRule.entityRegistry.values()) {
			IClassOutline classOutline = ejbEntity.getOutline() ;
			IJavaTypeDeclaration typeDeclaration = classOutline.getTypeDeclaration();
			IJavaTypeDeclaration keyTypeDeclaration = null;
			List<IJavaAnnotation> annotations = typeDeclaration.getAnnotations();
			if (annotations != null) {
				for (IJavaAnnotation annotation : annotations) {
					if (annotation.getName().equals("IdClass")) {
						IAnnotationMemberValue memberValue = annotation.getMemberValue("value");
						if (memberValue instanceof AnnotationClassMemberValue) {
							AnnotationClassMemberValue classMemberValue = (AnnotationClassMemberValue)memberValue;
							if (classMemberValue.getType() != null && classMemberValue.getType().getDeclaration() != null ) {
								keyTypeDeclaration = classMemberValue.getType().getDeclaration();
								break;
							}
						}
					}
				}
			}
			
			if (keyTypeDeclaration == null) {
				List<IJavaFieldDeclaration> fields = ejbEntity.getPersistentFields();
				for (IJavaFieldDeclaration field : fields) {
					if (field.getAnnotations() != null) {
						for (IJavaAnnotation annotation : field.getAnnotations()) {
							if (annotation.getName().equals("EmbeddedId")) {
								if (field.getType() != null && field.getType().getDeclaration() != null) {
									keyTypeDeclaration = field.getType().getDeclaration();
									break;
								}
							}
						}
					}
				}
			}
			
			
			if (keyTypeDeclaration != null) {
				for (String keyTypeQName : keyTypeDeclaration.getQualifiedName().getValues()) {
					IClassOutline keyOutline = getOutlineContainer().getClassOutline(keyTypeQName);
					if (keyOutline != null && keyOutline.getMethods() != null) {
						List<IJavaMethodDeclaration> equalsMethods = keyOutline.getMethodsBySimpleName("equals");
						List<IJavaMethodDeclaration> hashCodeMethods = keyOutline.getMethodsBySimpleName("hashCode");
						if (equalsMethods == null || hashCodeMethods == null || equalsMethods.isEmpty() || hashCodeMethods.isEmpty()) {
							reportError(ctx,typeDeclaration,message) ;
						}
					}
				}
				
			}
		}
		super.processEntities(ctx);
	}
}
