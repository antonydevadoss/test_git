/*******************************************************************************
 * FILE NAME: MissingSortableFilterableUsageRule.java
 * PURPOSE: This is Rule-25 of document 
 *
 *
 * Revision History: 
 * DATE:           				  AUTHOR:              	CHANGE:  
 * Tue March 22, 2011             Amit Arora	       	Initial Version  
 * 
 * Copyright (c) 2011 Juniper Networks. All rights reserved.
 ******************************************************************************/
package net.junos.space.pmd.rules.dto;

import java.util.ArrayList;import java.util.List;import net.junos.space.pmd.utils.PMDConstants;import net.junos.space.pmd.utils.PMDConstants.HttpMethod;import net.junos.space.pmd.utils.PMDUtils;import net.junos.space.pmd.utils.Project;import net.sourceforge.pmd.RuleContext;import net.sourceforge.pmd.lang.java.ast.ASTMethodDeclarator;import net.sourceforge.pmd.lang.java.rule.AbstractJavaRule;import org.apache.log4j.Logger;

public class MissingSortableFilterableUsageRule extends AbstractJavaRule {
	
	/**
	 * Logger
	 */
	private static Logger log = Logger.getLogger(MissingSortableFilterableUsageRule.class);

	/**
	 * Method executes by :
	 * 1. Check whether method is REST or not
	 * 2. If step 1 is true, check method has @Context annotation as formal parameter
	 * 3. If step 1 is true, check method is returning DTO
	 * 3. If 2 and 3 are true, get the class of DTO
	 * 4. Check the usage of Sortable or Filterable in this DTO class by calling hasSortableFilterableAnnotation
	 * 5. If step 4 is not valid, then report error.
	 */
	public Object visit(ASTMethodDeclarator methodNode, Object data) {
		RuleContext ctx = (RuleContext) data;
		String methodName = methodNode.getImage();
		try {
			boolean isRestMethod = false;
			boolean hasContextAnnotation = false;
			boolean hasGETAnnotation = true; 			
			boolean isReturningDTO = false;
			boolean hasSortableFiterableForReturnDTO = false;

			// checks whether the method is REST or not.
			isRestMethod = PMDUtils.isRestMethod(methodNode);
			if (isRestMethod) {
				
				hasContextAnnotation = PMDUtils.hasAnnotationParameter(methodNode, PMDConstants.CONTEXT_ANNOTATION);				
				//Check if method is annotated with @GET, making toUpperCase() for future
				hasGETAnnotation = PMDUtils.isMethodAnnotationExists(methodNode, HttpMethod.GET.name().toUpperCase());				
				isReturningDTO = PMDUtils.isReturningDTO(methodNode);					
				
				//Should proceed only for GET method, else return with success
				if(!hasGETAnnotation)
					return super.visit(methodNode, data);

				if (hasContextAnnotation && isReturningDTO) {
					List<String> annotationList = new ArrayList<String>();
					annotationList.add(PMDConstants.SORTABLE_ANNOTATION);
					annotationList.add(PMDConstants.FILTERABLE_ANNOTATION);
					Class<?> returningDTOClass = PMDUtils.getReturningDTOClass(methodNode, new Project(PMDConstants.WEB_SVC_PROJECT));
					hasSortableFiterableForReturnDTO = PMDUtils.hasAnnotationOnFieldOrMethod(returningDTOClass, annotationList);
					
				}
			}

			if (log.isDebugEnabled()) {
				StringBuilder builder = new StringBuilder();
				builder.append("source file=").append(ctx.getSourceCodeFilename()).append(",method=").append(methodName);
				builder.append(",isRestMethod=").append(isRestMethod).append(",hasContextAnnotation=").append(hasContextAnnotation).append(
						",isReturningDTO=").append(isReturningDTO)
						.append(",hasSortableFiterableForReturnDTO=").append(hasSortableFiterableForReturnDTO);
				log.debug(builder.toString());
			}

			if (isRestMethod && hasContextAnnotation && isReturningDTO && !hasSortableFiterableForReturnDTO)
				addViolation(data, methodNode);
		} catch (Exception e) {
			log.error("Exception in executing method " + methodName + " of " + ctx.getSourceCodeFilename(), e);
		}
		return super.visit(methodNode, data);
	}
	
}	
