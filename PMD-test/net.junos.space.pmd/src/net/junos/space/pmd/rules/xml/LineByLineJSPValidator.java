package net.junos.space.pmd.rules.xml;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import net.sourceforge.pmd.RuleContext;

public class LineByLineJSPValidator extends AbstractJSPRule {

	public LineByLineJSPValidator() {
		super();
	}

	protected void processSingleJSP(String path, RuleContext ctx) {
		
		BufferedReader reader ;
		try {
			
			int lineNumber = 1 ;
			FileReader fReader = new FileReader(path);
			reader = new BufferedReader( fReader ) ;
			String line = reader.readLine() ;
			while( line != null ){
				
				validateLine( line, lineNumber, path, ctx ) ;
				line = reader.readLine() ;
				lineNumber++ ;
			}
			
			reader.close() ;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}		
		
	}

	protected void validateLine(String line, int lineNumber, String path, RuleContext ctx) {}

}