package net.junos.space.pmd.rules.j2ee.ejb3;

import java.util.List;

import net.junos.space.pmd.utils.outline.AbstractOutlineAccessor;
import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;
import net.sourceforge.pmd.RuleContext;

public class EnterpriseBeanAbstractRule extends AbstractOutlineAccessor {
	
	@Override
	public String getMessage() {
		return "The enterprise bean class must not be abstract.";
	}

	@Override
	public void doEnd(RuleContext ctx) {
		
		List<IClassOutline> allOutlines = getOutlineContainer().getAllOutlines();
		for( IClassOutline outline : allOutlines){
			
			String qualifiedName = outline.getQualifiedNameString() ;
			if(!isEnterpriseBeanEJB3(qualifiedName))
				continue ;
			
			IJavaTypeDeclaration typeDeclaration = outline.getTypeDeclaration();
			if(!typeDeclaration.isAbstract())
				continue ;
			
			reportError(ctx, outline, typeDeclaration) ;			
		}		
	}

}
