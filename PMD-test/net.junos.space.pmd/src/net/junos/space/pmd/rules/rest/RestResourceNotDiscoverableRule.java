/*******************************************************************************
 * FILE NAME: RestResourceNotDiscoverableRule.java
 * PURPOSE:  This is Rule-21 in document 
 *
 *
 * Revision History: 
 * DATE:           				  AUTHOR:              	CHANGE:  
 * Tue Feb 18th Feb, 2011         Amit Arora        	Initial Version  
 * 
 * 
 ******************************************************************************/
package net.junos.space.pmd.rules.rest;

import java.util.List;import net.junos.space.pmd.utils.PMDConstants;import net.junos.space.pmd.utils.PMDUtils;import net.junos.space.pmd.utils.WebXMLReader;import net.sourceforge.pmd.RuleContext;import net.sourceforge.pmd.lang.java.ast.ASTMethodDeclarator;import net.sourceforge.pmd.lang.java.rule.AbstractJavaRule;import org.apache.log4j.Logger;

/**
 * This class verifies that every REST resource should be discoverable
 * by well known URL.
 */
public class RestResourceNotDiscoverableRule extends AbstractJavaRule {

	/**
	 * Logger
	 */
	private static Logger log = Logger.getLogger(RestResourceNotDiscoverableRule.class);

	/**
	 * This method checks:
	 * 	a> Method is REST or not
	 *  b> Read web.xml of websvc project
	 *  c> checks whether context-param resteasy.scan is true or false
	 *  d> get all list of listeners
	 *  e> check whether InfoServiceInterceptor is present or not
	 *  f> check whether InfoServiceInterceptor is present after RestEasyBootStrap listener
	 */
	public Object visit(ASTMethodDeclarator methodNode, Object data) {
		RuleContext ctx = (RuleContext) data;
		String methodName = methodNode.getImage();
		try {
			boolean isRestMethod = PMDUtils.isRestMethod(methodNode);
			boolean isRestEasyScan = false;
			int NOT_FOUND = -1;
			int infoServiceInterceptorPosition = NOT_FOUND;
			int restEasyBootStrapPosition = NOT_FOUND;
			if (isRestMethod) {
				WebXMLReader xmlReader = new WebXMLReader();
				String restEasyScan = xmlReader.getContextParamater(PMDConstants.CONTEXT_PARAM_REST_EASY_SCAN);
				isRestEasyScan = Boolean.valueOf(restEasyScan);
				List<String> listeners = xmlReader.getListeners();
				for (int i = 0; i < listeners.size(); i++) {
					String listener = listeners.get(i);
					if (listener.equals(PMDConstants.LISTENER_BOOT_STRAP)) {
						restEasyBootStrapPosition = i;
					}
					if (listener.equals(PMDConstants.LISTENER_INFO_SERVICE)) {
						infoServiceInterceptorPosition = i;
					}
				}
			}
			
			if(log.isDebugEnabled()){
				StringBuilder builder = new StringBuilder();
				builder.append("source file=").append(ctx.getSourceCodeFilename()).append(",method=").append(methodName);
				builder.append(",isRestMethod=").append(isRestMethod).append(",isRestEasyScan=").append(isRestEasyScan).append(
						",infoServiceInterceptorPosition=").append(infoServiceInterceptorPosition).append(
						",restEasyBootStrapPosition=").append(restEasyBootStrapPosition).append(",infoServiceInterceptorPosition=")
						.append(infoServiceInterceptorPosition);
				log.debug(builder.toString());
			}
			
			if (isRestMethod
					&& (!isRestEasyScan || infoServiceInterceptorPosition == NOT_FOUND || restEasyBootStrapPosition > infoServiceInterceptorPosition))
				addViolation(data, methodNode);
		} catch (Exception e) {
			log.error("Exception in executing method " + methodName + " of " + ctx.getSourceCodeFilename(), e);
		}
		return super.visit(methodNode, data);
	}

}
