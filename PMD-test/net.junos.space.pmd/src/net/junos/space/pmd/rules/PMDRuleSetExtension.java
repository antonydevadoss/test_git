package net.junos.space.pmd.rules;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Platform;
import org.osgi.framework.Bundle;

import net.sourceforge.pmd.RuleSet;
import net.sourceforge.pmd.RuleSetFactory;
import net.sourceforge.pmd.RuleSets;
import net.sourceforge.pmd.eclipse.core.IRuleSetsExtension;

public class PMDRuleSetExtension implements IRuleSetsExtension {
	
	private RuleSets mainRuleSets = null;

	public PMDRuleSetExtension() {
	}

	public void registerRuleSets(Set<RuleSet> registeredRuleSets) {
		if (mainRuleSets == null) {
			createRuleSet();
		}
		
		if (mainRuleSets != null) {
			registeredRuleSets.addAll(Arrays.asList(mainRuleSets.getAllRuleSets()));
		}
	}

	public void registerDefaultRuleSets(Set<RuleSet> defaultRuleSets) {
		if (mainRuleSets == null) {
			createRuleSet();
		}
		
		if (mainRuleSets != null) {
			defaultRuleSets.addAll(Arrays.asList(mainRuleSets.getAllRuleSets()));
		}
	}

	private void createRuleSet() {
		String pluginRootFolder = constructPluginRoot("net.junos.space.pmd");
		if (pluginRootFolder == null) {
			return;
		}
		
		ArrayList<RuleSet> ruleSetList = new ArrayList<RuleSet>() ;
		ArrayList<String> lst = new ArrayList<String>() ;
		
		//build case
		File rootFolder = new File(pluginRootFolder) ;
		addRuleSetFileName( lst, rootFolder, "junos-space-ruleset.xml" ) ;
		addRuleSetFileName( lst, rootFolder, "junos-space-ruleset-eclipse.xml" ) ;
		addRuleSetFileName( lst, rootFolder, "junos-space-ejb2-ruleset.xml" ) ;
		addRuleSetFileName( lst, rootFolder, "junos-space-ejb3-ruleset.xml" ) ;
		
		//developing case
		File rulesetsFolder = new File(rootFolder,"src") ;		
		addRuleSetFileName( lst, rulesetsFolder, "junos-space-ruleset.xml" ) ;
		addRuleSetFileName( lst, rulesetsFolder, "junos-space-ruleset-eclipse.xml" ) ;
		addRuleSetFileName( lst, rulesetsFolder, "junos-space-ejb2-ruleset.xml" ) ;
		addRuleSetFileName( lst, rulesetsFolder, "junos-space-ejb3-ruleset.xml" ) ;

		if(!lst.isEmpty()){
			for( String fileName : lst )
			{
				try{
					RuleSetFactory factory = new RuleSetFactory();
					RuleSets ruleSets = factory.createRuleSets(fileName);
					for(RuleSet rs : ruleSets.getAllRuleSets())
						ruleSetList.add(rs) ;
				}catch(Exception e){
				
				}
			}
		}
			
		mainRuleSets = new RuleSets() ;
		for(RuleSet rs : ruleSetList)
			mainRuleSets.addRuleSet(rs) ;
		
	}
	
	private void addRuleSetFileName(ArrayList<String> lst, File rootFolder,	String fileName)
	{
		File f = new File(rootFolder, fileName) ;
		if(!f.exists())
			return ;
		
		String path = f.getAbsolutePath();
		lst.add(path) ;		
	}

	public static String constructPluginRoot( String pluginName ) {
		
		Bundle bundle = Platform.getBundle(pluginName) ;

		if( bundle == null )
			return null ;
			

		URL url = bundle.getEntry("");
		URL url2 = null;
		try {
			url2 = FileLocator.resolve(url) ;
		} catch (IOException e) {
			e.printStackTrace();
		}
		String docGeneratorPlugInRoot = new java.io.File( url2.getFile() ).getAbsolutePath() ;
		return docGeneratorPlugInRoot;
	}
}
