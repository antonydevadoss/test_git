/*******************************************************************************
 * FILE NAME: InvalidPathParamRule.java
 * PURPOSE: This is Rule-11 of document 
 *
 *
 * Revision History: 
 * DATE:           				  AUTHOR:              	CHANGE:  
 * Tue Feb 23, 2011               Atul Bajpai       	Initial Version  
 * 
 * 
 ******************************************************************************/
package net.junos.space.pmd.rules.rest;

import java.util.ArrayList;import java.util.List;import net.junos.space.pmd.utils.PMDConstants;import net.junos.space.pmd.utils.PMDUtils;import net.sourceforge.pmd.RuleContext;import net.sourceforge.pmd.lang.java.ast.ASTMethodDeclarator;import net.sourceforge.pmd.lang.java.rule.AbstractJavaRule;import org.apache.log4j.Logger;

/**
 * This rule verifies that if REST method is having @Path annotation at method or/and class with 
 * any path param attribute, then there may be @PathParam identifier in the REST method for that
 * attribute.
 */
public class MissingPathParamRule extends AbstractJavaRule {

	/**
	 * Logger
	 */
	private static Logger log = Logger.getLogger(MissingPathParamRule.class);

	/**
	 * This method checks:
	 * 	a> Method is REST
	 *  b> if a> is true, Checks for @Path at method
	 *  c> Checks for @Path at class
	 *  d> If any one of b> or c> is true
	 *  e> Get @Path annotation value for @Path at class or method which one is true.
	 *  f> Get all @PathParam annotation values (modified one with { prepended and } appended )
	 *  g> loop through e> (method and then class) to find out whether they are present in f> list   
	 *  h> if not present, raise warning
	 */
	public Object visit(ASTMethodDeclarator methodNode, Object data) {
		RuleContext ctx = (RuleContext) data;
		String methodName = methodNode.getImage();
		try {
			boolean isRestMethod = false;
			boolean isMethodPathExists = false;
			boolean isClassPathExists = false;
			boolean isPathParamExist = true;
			boolean isValidPathParam = true;

			List<String> lstMethodPathValue = new ArrayList<String>();
			List<String> lstClassPathValue = new ArrayList<String>();

			// checks whether the method is REST or not.
			isRestMethod = PMDUtils.isRestMethod(methodNode);
			if (isRestMethod) {
				String methodPathAnnotationValue = "";
				String classPathAnnotationValue = "";
				List<String> annotationValues = new ArrayList<String>();

				// checks if @Path exists as a method annotation
				isMethodPathExists = PMDUtils.isMethodAnnotationExists(methodNode, PMDConstants.PATH_ANNOTATION);
				// checks if @Path exists as a class annotation
				isClassPathExists = PMDUtils.isClassAnnotationExists(methodNode, PMDConstants.PATH_ANNOTATION);
				if (isMethodPathExists || isClassPathExists) {

					if (isMethodPathExists) {
						methodPathAnnotationValue = PMDUtils.getAnnotationValue(methodNode,PMDUtils.getMethodAnnotation(methodNode,
								PMDConstants.PATH_ANNOTATION));
						lstMethodPathValue.addAll(PMDUtils.getLiteralsseparatedByBraces(methodPathAnnotationValue));

					}
					if (isClassPathExists) {
						classPathAnnotationValue = PMDUtils.getAnnotationValue(methodNode,PMDUtils.getClassAnnotation(methodNode,
								PMDConstants.PATH_ANNOTATION));
						lstClassPathValue.addAll(PMDUtils.getLiteralsseparatedByBraces(classPathAnnotationValue));
					}

					isPathParamExist = PMDUtils.hasAnnotationParameter(methodNode, PMDConstants.PATHPARAM_ANNOTATION);
					if (isPathParamExist) {
						// List of all annotation value used in a method parameter
						annotationValues = PMDUtils.getModifiedParameterAnnotationValue(methodNode,
								PMDConstants.PATHPARAM_ANNOTATION, PMDConstants.CURLYBRACE_OPENING,
								PMDConstants.CURLYBRACE_CLOSING);
					}

				}// End if for isMethodPathExists or isClassPathExists

				for (int i = 0; i < lstMethodPathValue.size(); i++) {
					if (!annotationValues.contains(lstMethodPathValue.get(i))) {
						isValidPathParam = false;
						break;
					}
				}

				if (isValidPathParam) {
					for (int i = 0; i < lstClassPathValue.size(); i++) {
						if (!annotationValues.contains(lstClassPathValue.get(i))) {
							isValidPathParam = false;
							break;
						}
					}
				}
			}
			
			if(log.isDebugEnabled()){
				StringBuilder builder = new StringBuilder();
				builder.append("source file=").append(ctx.getSourceCodeFilename()).append(",method=").append(methodName);
				builder.append(",isRestMethod=").append(isRestMethod).append(",isValidPathParam=").append(isValidPathParam);
				log.debug(builder.toString());
			}
			
			if (isRestMethod && (!lstMethodPathValue.isEmpty() || !lstClassPathValue.isEmpty()) && !isValidPathParam)
				addViolation(data, methodNode);
		} catch (Exception e) {
			log.error("Exception in executing method " + methodName + " of " + ctx.getSourceCodeFilename(), e);
		}
		return super.visit(methodNode, data);
	}
}
