package net.junos.space.pmd.rules.j2ee.ejb3;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import net.junos.space.pmd.utils.outline.AbstractEntityRule;
import net.junos.space.pmd.utils.outline.EjbEntity;
import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IJavaMethodDeclaration;
import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;
import net.sourceforge.pmd.RuleContext;

public class EntityConstructorRule extends AbstractEntityRule {

	
	private static final String message = "The entity class must have a no-arg constructor, which must be public or protected.";

	@Override
	protected void processEntities(RuleContext ctx) {
		if (AbstractEntityRule.entityRegistry == null) {
			return;
		}
		for (EjbEntity ejbEntity : AbstractEntityRule.entityRegistry.values()) {
			IClassOutline classOutline = getOutlineContainer().getClassOutline(ejbEntity.getQualifiedName());
			
			List<IJavaMethodDeclaration> constructors = classOutline.getConstructors();
			if (constructors != null && !constructors.isEmpty()) {
				boolean hasConstructonWithParams = false;
				boolean hasDefaultConstructor = false;
				for (IJavaMethodDeclaration constructor : constructors) {
					if (constructor.getParameters() == null || constructor.getParameters().isEmpty()) {
						
						if (constructor.isPublic() || constructor.isProtected()) {
							hasDefaultConstructor = true;
						}
					} else {
						hasConstructonWithParams = true;
					}
				}
				
				if (!hasDefaultConstructor && hasConstructonWithParams) {					
					IJavaTypeDeclaration tDecl = classOutline.getTypeDeclaration();
					reportError(ctx,tDecl,message) ;
				}
			}
		}
		super.processEntities(ctx);
	}
	
	protected Set<String> getTargetAnnotations() {
		HashSet<String> set = new HashSet<String>();
		set.add("Entity") ;
		set.add("Embeddable") ;
		return set ;
	}
}
