package net.junos.space.pmd.rules.j2ee;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;
import net.junos.space.pmd.utils.outline.QualifiedName;
import net.junos.space.pmd.utils.reference.SimpleMethodReferenceRule;

public class DoNoPrintStaticContentFromServletRule extends SimpleMethodReferenceRule {
	private static final String[] targetClasses = new String[]{
		"java.io.PrintWriter",
	} ;
	
	private static final String[] targetMethods = new String[]{
		"print",
		"println"
	} ;
	
	private static final String message = "Do no print unnecessary static content from a Servlet"; 
	
	public String getMessage(){
		return message ;
	}

	protected List<String> getTargetClasses() {
		return new ArrayList<String>( Arrays.asList(targetClasses) ) ;
	}
	
	protected List<String> getTargetMethods() {
		return new ArrayList<String>( Arrays.asList(targetMethods) ) ;
	}

	protected boolean mustBeChecked(String className) {	
		IClassOutline classOutline = getOutlineContainer().getClassOutline(className);
		if (classOutline != null && classOutline.getTypeDeclaration() != null) {
			IJavaTypeDeclaration typeDeclaration = classOutline.getTypeDeclaration();
			List<IJavaTypeDeclaration> allSuperClasses = typeDeclaration.getAllSuperClasses();
			for (IJavaTypeDeclaration superclass : allSuperClasses) {
				QualifiedName qualifiedName = superclass.getQualifiedName();
				if (qualifiedName == null )
					continue ;
				
				for(String s : qualifiedName.getValues()){
					if( s.equals("javax.servlet.http.HttpServlet"))
						return true;
				}
			}
		}
		return false;
	}
}
