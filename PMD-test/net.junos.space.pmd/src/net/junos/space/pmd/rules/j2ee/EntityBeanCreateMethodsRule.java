package net.junos.space.pmd.rules.j2ee;

import net.junos.space.pmd.utils.outline.AbstractOutlineAccessor;
import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IJavaMethodDeclaration;
import net.sourceforge.pmd.RuleContext;

public class EntityBeanCreateMethodsRule extends AbstractOutlineAccessor {
	
	@Override
	public void doEnd(RuleContext ctx) {
		for (IClassOutline outline : getOutlineContainer().getAllOutlines()) {
			if (isEnterpriseBeanSuperclassEJB2(outline.getTypeDeclaration())) {
				for (IJavaMethodDeclaration method : outline.getMethodsBySimpleName("ejbCreate")) {
					if (method.isFinal() || method.isStatic()) {
						reportError(ctx, outline, method);
					}
				}
				for (IJavaMethodDeclaration method : outline.getMethodsBySimpleName("ejbPostCreate")) {
					if (method.isFinal() || method.isStatic()) {
						reportError(ctx, outline, method);
					}
				}
			}
		}
		super.doEnd(ctx);
	}
	
	@Override
	public String getMessage() {
		return "Do not declare an ejbCreate or ejbPostCreate method as final or static for an Entity Bean";
	}
}
