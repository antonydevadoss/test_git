package net.junos.space.pmd.rules.j2ee.ejb3;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import net.junos.space.pmd.utils.outline.AbstractEntityRule;
import net.junos.space.pmd.utils.outline.EjbEntity;
import net.junos.space.pmd.utils.outline.IJavaFieldDeclaration;
import net.sourceforge.pmd.RuleContext;

public class EntityFieldsAccessRule extends AbstractEntityRule {

	
	private static final String message = "Entity or embeddedable class instance variables must be private, protected, or package visibility. Persistent instance variables must not be final.";
	@Override
	protected void processEntities(RuleContext ctx) {
		if (AbstractEntityRule.entityRegistry == null) {
			return;
		}
		for (EjbEntity ejbEntity : AbstractEntityRule.entityRegistry.values()) {
			if(!ejbEntity.hasFieldAccess())
				continue ;
			
			List<IJavaFieldDeclaration> fields = ejbEntity.getPersistentFields();
			for (IJavaFieldDeclaration field : fields) {
				if ( field.isPublic() && !field.isStatic()  ) {					
					
					reportError(ctx,field,message) ;
				}
			}
			ArrayList<IJavaFieldDeclaration> persistentFields = ejbEntity.getPersistentFields();
			if(persistentFields == null)
				continue ;
			
			for (IJavaFieldDeclaration field : persistentFields) {
				if ( field.isFinal() ) {					
					
					reportError(ctx,field,message) ;
				}
			}
		}
		super.processEntities(ctx);
	}
	
	protected Set<String> getTargetAnnotations() {
		HashSet<String> set = new HashSet<String>();
		set.add("Entity") ;
		set.add("Embeddable") ;
		return set ;
	}
}
