package net.junos.space.pmd.rules.j2ee.ejb3;

import java.util.ArrayList;
import java.util.List;

import net.junos.space.pmd.utils.outline.AbstractOutlineAccessor;
import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IJavaMethodDeclaration;
import net.junos.space.pmd.utils.outline.IJavaType;
import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;
import net.junos.space.pmd.utils.outline.QualifiedName;
import net.sourceforge.pmd.RuleContext;

public class TimedObjectTimeoutMethodRule extends AbstractOutlineAccessor {

	
	@Override
	public String getMessage() {		
		return "If the enterprise bean implements the TimedObject interface, the Timeout annotation can only be used to specify the ejbTimeout method.";
	}

	@Override
	public void doEnd(RuleContext ctx) {
		
		ArrayList<IClassOutline> classesToCheck = new ArrayList<IClassOutline>() ;
		
		for( IClassOutline outline : getOutlineContainer().getAllOutlines() ){
			
			String qualifiedName = outline.getQualifiedNameString();
			if(!isEnterpriseBeanEJB3(qualifiedName))
				continue ;
				
			List<IJavaTypeDeclaration> allInterfaces = outline.getTypeDeclaration().getAllInterfaces();
			if(allInterfaces == null || allInterfaces.isEmpty())
				continue ;
			
			for( IJavaTypeDeclaration iDecl : allInterfaces ){
				
				QualifiedName qName = iDecl.getQualifiedName();
				if(!qName.hasValue("javax.ejb.TimedObject"))
					continue ;
				
				classesToCheck.add(outline) ;
				break ;
			}			
		}
		
		for( IClassOutline outline : classesToCheck ){
			
			List<IJavaMethodDeclaration> methods = outline.getMethods();
			if(methods == null)
				continue ;
			
			for(IJavaMethodDeclaration md : methods){
				if(!md.hasAnnotation("Timeout"))
					continue ;
				
				boolean isInvalid = true ;
				while(true){
					if(!md.getSimpleName().equals("ejbTimeout") )
						break;
					
					List<IJavaType> params = md.getParameters();
					if(params == null || params.size() != 1 )
						break ;
				
					IJavaType paramType = params.get(0);
					QualifiedName qn = paramType.getQualifiedName();
					if( !qn.getSimpleName().equals("Timer") )
						break;
					
					isInvalid = false ;
					break;				
				}
				if(!isInvalid)
					continue ;
				
				reportError(ctx, outline, md) ;
			}
		}		
	}
}

