/*******************************************************************************
 * FILE NAME: AvoidConsumesForNoParameterRule.java
 * PURPOSE: This is Rule-6 of document 
 *
 *
 * Revision History: 
 * DATE:           				  AUTHOR:              	CHANGE:  
 * Tue Feb 09, 2011               Atul Bajpai       	Initial Version  
 * 
 * 
 ******************************************************************************/
package net.junos.space.pmd.rules.rest;

import net.junos.space.pmd.utils.PMDConstants;import net.junos.space.pmd.utils.PMDUtils;import net.sourceforge.pmd.RuleContext;import net.sourceforge.pmd.lang.java.ast.ASTMethodDeclarator;import net.sourceforge.pmd.lang.java.rule.AbstractJavaRule;import org.apache.log4j.Logger;

/**
 * This class provides a warning if @Consumes Annotation is used in a method,
 * while the method is not having any DTO as input parameter.
 */
public class AvoidConsumesForNoParameterRule extends AbstractJavaRule {

	/**
	 * Logger
	 */
	private static Logger log = Logger.getLogger(AvoidConsumesForNoParameterRule.class);
	
	/*
	 * This method checks if @Consumes Annotation is used in a method which has
	 * no Input parameter(DTO).
	 */
	public Object visit(ASTMethodDeclarator methodNode, Object data) {
		RuleContext ctx = (RuleContext) data;
		String methodName = methodNode.getImage();
		try {
			boolean hasNoParameter = false;
			boolean isConsumesAnnotation = false;
			boolean isRest = false;
			
			// Checks if there is no any parameter in method argument
			hasNoParameter = PMDUtils.hasNoParameter(methodNode);
			
			// checks whether the method is REST or not.		
			isRest = PMDUtils.isRestMethod(methodNode);
	
			if (isRest && hasNoParameter) {
				isConsumesAnnotation = PMDUtils.isMethodAnnotationExists(methodNode, PMDConstants.CONSUMES_ANNOTATION);
			}
			
			if(log.isDebugEnabled()){
				StringBuilder builder = new StringBuilder();
				builder.append("source file=").append(ctx.getSourceCodeFilename()).append(",method=").append(methodName);
				builder.append(",isConsumesAnnotation=").append(isConsumesAnnotation);
				log.debug(builder.toString());
			}
			
			if (isConsumesAnnotation)
				addViolation(data, methodNode);
		} catch(Exception e) {
			log.error("Exception in executing method " + methodName + " of " + ctx.getSourceCodeFilename(), e);
		}
		return super.visit(methodNode, data);
	}
}
