package net.junos.space.pmd.rules.j2ee;


import org.w3c.dom.Element;
import net.junos.space.pmd.rules.xml.AbstractXMLRule;
import net.junos.space.pmd.rules.xml.SimpleXpathRule;

public class UseCorrectNameForWebModuleDisplayNameRule extends SimpleXpathRule {	

	@Override
	protected String getXpathExpression() {
		return "//web-app/display-name";
	}

	public String getMessage() {
		return "Use A Correct Name For A Web Module Display Name. The Web module display name within the deployment descriptor is the web module name, written in mixed cases, with a suffix WAR.";
	}

	protected String getTargetXMLName() {
		return AbstractXMLRule.XML_NAME_WEB;
	}

	@Override
	protected boolean isValid(Element element) {
		
		String name = element.getTextContent();
		if (!name.endsWith("WAR")) {
			return false;
		}
		if (!Character.isUpperCase(name.toCharArray()[0])) {
			return false;
		}
		String pName = getProjectName() ;
		if(pName != null){
			String pName_lc = pName.toLowerCase() ;
			String name_lc = name.toLowerCase();
			if( !(name_lc.equals(pName_lc) || name_lc.equals(pName_lc+"war")) ){
				return false ;				
			}
		}
		return true;
	}
}
