package net.junos.space.pmd.utils.outline;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import net.sourceforge.pmd.lang.java.ast.ASTClassOrInterfaceType;
import net.sourceforge.pmd.lang.java.ast.ASTPrimitiveType;
import net.sourceforge.pmd.lang.java.ast.ASTReferenceType;
import net.sourceforge.pmd.lang.java.ast.ASTType;
import net.sourceforge.pmd.lang.java.ast.ASTTypeArgument;
import net.sourceforge.pmd.lang.java.ast.ASTTypeArguments;

public class TypeResolvingService {
	
	public TypeResolvingService() {
		super();
		this.importsMap = new HashMap<String, String>() ; 
	}

	public TypeResolvingService(Map<String, String> importsMap,
			IOutlineContainer outlineContainer) {
		super();
		this.importsMap = importsMap;
		this.outlineContainer = outlineContainer;
	}

	private Map<String,String> importsMap = new LinkedHashMap<String, String>() ;
	
	private IOutlineContainer outlineContainer ;

	private String contextClassQualifiedName;

	public Map<String, String> getImportsMap() {
		return importsMap;
	}

	public void setImportsMap(Map<String, String> importsMap) {
		this.importsMap = importsMap;
	}

	public IOutlineContainer getOutlineContainer() {
		return outlineContainer;
	}

	public void setOutlineContainer(IOutlineContainer outlineContainer) {
		this.outlineContainer = outlineContainer;
	}
	
	public IJavaType extractTypeFromNode(ASTType fd, IDeclaration owner) {		
		
		ASTReferenceType refNode
				= fd.getFirstDescendantOfType(ASTReferenceType.class);
		
		ASTClassOrInterfaceType typeNode
				= fd.getFirstDescendantOfType(ASTClassOrInterfaceType.class) ;
		
		if( typeNode != null ){			
			return obtainType(refNode,owner) ;
		}
		
		ASTPrimitiveType pt = fd.getFirstDescendantOfType(ASTPrimitiveType.class);
		IJavaType result = obtainPrimitiveType(pt, refNode);
		
		return result;
	}

	public IJavaType obtainPrimitiveType(ASTPrimitiveType pt, ASTReferenceType refNode) {
		
		
		String simpleName = pt.getImage() ;
		String qualifiedName = simpleName ;
		
		IJavaTypeDeclaration fieldTypeDeclaration
				= this.outlineContainer.getTypeDeclaration(qualifiedName) ;
		
		if( fieldTypeDeclaration == null ){
			
			fieldTypeDeclaration = new JavaTypeDeclaration() ;
			fieldTypeDeclaration.setSimpleName(simpleName) ;			
			fieldTypeDeclaration.setQualifiedName(new QualifiedName(qualifiedName) ) ;
			this.outlineContainer.putTypeDeclaration(fieldTypeDeclaration) ;
		}
		
		fieldTypeDeclaration.setPrimitive(true) ;	
		
		IJavaType result = new JavaType() ;
		result.setDeclaration(fieldTypeDeclaration) ;
		
		if( refNode != null && refNode.isArray() ) {
			result.setIsArray(true) ;
			result.setArrayDepth( refNode.getArrayDepth() ) ;
		}
		return result;
	}
	
	
	private void extractGenericParams(
			IJavaType fieldType,
			ASTClassOrInterfaceType typeNode,
			IDeclaration owner )
	{
		ASTTypeArguments typeArgsNode = typeNode.getFirstDescendantOfType( ASTTypeArguments.class );
		if( typeArgsNode == null )
			return ;
		
		List<ASTTypeArgument> argNodes = typeArgsNode.findChildrenOfType( ASTTypeArgument.class );
		if(argNodes == null || argNodes.isEmpty())
			return ;
		
		ArrayList<IJavaType> paramTypes = new ArrayList<IJavaType>() ;
		for(ASTTypeArgument arg : argNodes){			
			ASTReferenceType argRefTypeNode
					= arg.getFirstDescendantOfType(ASTReferenceType.class);			
			
			IJavaType argType ;
			if( argRefTypeNode == null ){
				//case with SomeClass<?>
				argType = createObjectType() ;
			}
			else{
				ASTClassOrInterfaceType paramTypeNode
					= arg.getFirstDescendantOfType(ASTClassOrInterfaceType.class) ;
				if(paramTypeNode != null)
					argType = obtainType( argRefTypeNode, owner ) ;
				else{
					ASTPrimitiveType primitiveParamTypeNode
						= arg.getFirstDescendantOfType(ASTPrimitiveType.class) ;
					argType= obtainPrimitiveType(primitiveParamTypeNode, argRefTypeNode) ;
				}
			}			
			paramTypes.add(argType) ;
		}
		fieldType.setGenericParameters(paramTypes) ;		
	}
	
	public IJavaType obtainType( ASTReferenceType refNode,	IDeclaration owner )
	{
		ASTClassOrInterfaceType typeNode
			= refNode.getFirstDescendantOfType(ASTClassOrInterfaceType.class) ;
		
		IJavaType result = obtainType(typeNode, owner, true );
		if( refNode.isArray() ) {
			result.setIsArray(true) ;
			result.setArrayDepth( refNode.getArrayDepth() ) ;
		}
		
		return result;
	}
	
	public IJavaType obtainType(
			ASTClassOrInterfaceType typeNode,
			IDeclaration genericsContext,
			boolean searchAmongNested)
	{
		
		String simpleName = typeNode.getImage() ;		
		IJavaType result = matchGenericParam( simpleName, genericsContext ) ;
		
		if( result == null){
			
			result = obtainType(simpleName, genericsContext, searchAmongNested);
			
			extractGenericParams( result, typeNode, genericsContext ) ;
		}
		return result;
	}

	public IJavaType obtainType(String simpleName, IDeclaration genericsContext, boolean searchAmongNested)
	{
		IJavaTypeDeclaration owner = null;
		if( genericsContext instanceof IJavaTypeDeclaration ){
			owner = (IJavaTypeDeclaration) genericsContext ;
		}
		else {
			owner = genericsContext.getOwner() ;
		}
		
		QualifiedName qualifiedName = null ;
		
		int ind = simpleName.lastIndexOf('.') ;
		if( ind >= 0 && ind + 1 < simpleName.length() ){
			String qName = simpleName ;
			simpleName = simpleName.substring(ind+1) ;
			qualifiedName = new QualifiedName(qName) ;
		}
		
		if( qualifiedName == null && searchAmongNested){
			qualifiedName = findAmongNested( simpleName, owner) ;
		}
		
		if( qualifiedName == null ){
			String qName = importsMap.get(simpleName) ;
			if(qName != null)
				qualifiedName = new QualifiedName(qName) ;
		}
		
		if(qualifiedName == null){
			String qName = JavaLangPackageTypeList.resolveSimpleName(simpleName) ;
			if(qName != null)
				qualifiedName = new QualifiedName(qName) ;
		}
		
		if( qualifiedName == null && this.outlineContainer.isComplete()){
			String possibleName = genericsContext.getPackage() + "." + simpleName ;
			if( outlineContainer.getClassOutline(possibleName) != null ){
				qualifiedName = new QualifiedName(possibleName) ;
			}
			else{
				if(owner != null){
					List<String> vals = owner.getQualifiedName().getValues();
					if(vals.size()==1){
						String ownerName = vals.iterator().next() ;
						IClassOutline ownerOutline = outlineContainer.getClassOutline(ownerName);
						List<String> packageImports = ownerOutline.getPackageImports();
						if(packageImports != null && !packageImports.isEmpty()){
							ArrayList<String> values = new ArrayList<String>() ;
							for(String s : packageImports)
								values.add(s+"."+simpleName) ; 
		
							qualifiedName = new QualifiedName(simpleName, values) ;
						}
					}
				}
			}
		}		
		
		if(qualifiedName == null){			
			qualifiedName = new QualifiedName(simpleName,
					owner.getQualifiedName().getValues().iterator().next() ) ;
		}
		
		IJavaTypeDeclaration typeDeclaration = null;
		
		
		if(qualifiedName.getValues().size()==1){
			String key = qualifiedName.getValues().iterator().next() ;
			typeDeclaration	= this.outlineContainer.getTypeDeclaration(key) ;
		}
		else if(owner != null){
			List<String> vals = owner.getQualifiedName().getValues();
			if(vals.size()==1){
				typeDeclaration	= this.outlineContainer.getTypeDeclaration(
						simpleName,vals.iterator().next() ) ;
			}
		}
			
		if( typeDeclaration == null ){
			
			if(qualifiedName.getValues().size() != 1){
				ProxyTypeDeclaration proxy = new ProxyTypeDeclaration();
				proxy.setReferencedDeclaration(new JavaTypeDeclaration() ) ;
				typeDeclaration = proxy ;				
			}
			else{
				typeDeclaration = new JavaTypeDeclaration() ;
			}
			typeDeclaration.setSimpleName(simpleName) ;			
			typeDeclaration.setQualifiedName( qualifiedName ) ;				
		}			
		typeDeclaration.setPrimitive(false) ;
		this.outlineContainer.putTypeDeclaration(typeDeclaration) ;
		IJavaType result = new JavaType() ;
		result.setDeclaration(typeDeclaration) ;
		
		return result;
	}

	private QualifiedName findAmongNested(String simpleName, IJavaTypeDeclaration owner) {
		
		if( owner == null )
			return null ;
		
		if( owner.getSimpleName().equals(simpleName) )
			return owner.getQualifiedName() ;
		
		List<IJavaTypeDeclaration> nestedClasses = owner.getNestedClasses();
		if( nestedClasses != null ){
			for( IJavaTypeDeclaration child : nestedClasses ){
				if( child.getSimpleName().equals(simpleName) )
					return child.getQualifiedName() ;
			}
		}
		
		IJavaTypeDeclaration ownerClass = owner.getOwner();
		if( ownerClass != null )
			return findAmongNested(simpleName, ownerClass );
		
		
		return null ;
	}

	private GenericParam matchGenericParam(String name, IDeclaration genericsContext ) {
		
		GenericParam result = null ;
		
		IDeclaration jtd = genericsContext ;
l0:		while( jtd != null ){
	
			while(true){
				if(!(jtd instanceof ICanBeGeneric)){				
					break ;
				}			
				ICanBeGeneric generic = (ICanBeGeneric)jtd;				
			
				int order = generic.getOrder(name) ;
				if(order < 0)
					break ;
			
				result = new GenericParam() ;
				result.setOrder(order) ;
				result.setOwnerIsClass() ;
				result.setOwnerQualifiedName( jtd.getQualifiedName() ) ;
				result.setSimpleName(name) ;
				result.setQualifiedName( new QualifiedName(name) ) ;
				break l0;
			}
			jtd = jtd.getOwner() ;
		}
		return result;
	}
	
	private IJavaType createObjectType() {
		
		String objQName = Object.class.getCanonicalName();
		String objSName = Object.class.getSimpleName() ;
		IJavaTypeDeclaration objDecl = getOutlineContainer().getTypeDeclaration( objQName ) ;		
		if( objDecl == null ){
			objDecl = new JavaTypeDeclaration() ;
			objDecl.setQualifiedName(new QualifiedName(objQName)) ;
			objDecl.setSimpleName( Object.class.getSimpleName() ) ;
			objDecl.setAbstract(false) ;
			objDecl.setPublic() ;
			objDecl.setFinal(false) ;
			objDecl.setPrimitive(false) ;
			getOutlineContainer().putTypeDeclaration(objDecl) ;
		}
		
		IJavaType objType = new JavaType() ;
		objType.setDeclaration(objDecl) ;
		objType.setIsArray(false) ;
		return objType;
	}

	public void setContextQualifiedname(String qualifiedName) {
		this.contextClassQualifiedName = qualifiedName ;		
	}

}
