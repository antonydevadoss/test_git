package net.junos.space.pmd.utils.reference;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import net.junos.space.pmd.utils.outline.BasicOutlineContainer;
import net.junos.space.pmd.utils.outline.IClassOutline;
import net.junos.space.pmd.utils.outline.IDeclaration;
import net.junos.space.pmd.utils.outline.IJavaMethodDeclaration;
import net.junos.space.pmd.utils.outline.IJavaTypeDeclaration;



public class BasicReferenceContainer implements IReferenceContainer {

	private static BasicReferenceContainer instance ;
	
	public static BasicReferenceContainer getInstance(){
		if( instance == null )
			instance = new BasicReferenceContainer() ;
		
		return instance ;
	}
	
	private BasicReferenceContainer(){}
	
	
	HashMap<String, IClassReferenceStorage<IReference>> tempClassMap
			= new HashMap<String, IClassReferenceStorage<IReference>>()  ;
	
	HashMap<String, IClassReferenceStorage<SimpleClassReference>> classMap
	= new HashMap<String, IClassReferenceStorage<SimpleClassReference>>()  ;
	
	@Override
	public void register(IDeclaration method, Collection<IReference> referenceSet) {
		
		//qualifiedNmae existence is guaranteed here
		IJavaTypeDeclaration owner = method.getOwner();
		if(owner==null)
			return ;
		
		List<String> values = owner.getQualifiedName().getValues();
		if( values.size()!=1 )
			return ;
		
		String classQualifiedName = values.iterator().next() ;
		IClassOutline outline = BasicOutlineContainer.getInstance().getClassOutline(classQualifiedName );
		if(outline==null)
			return ;
		
		IClassReferenceStorage<IReference> refStorage = tempClassMap.get(classQualifiedName);
		if(refStorage==null){
			refStorage = new ClassReferenceStorage<IReference>( outline ) ;
			tempClassMap.put(classQualifiedName, refStorage) ;
		}
		refStorage.addMethodReferences(method, referenceSet) ;
	}


	@Override
	public IClassReferenceStorage<IReference> getTemporaryReferenceListForClass(
			String classQualifiedName) {
		return tempClassMap.get(classQualifiedName) ;
	}

	@Override
	public Collection<IClassReferenceStorage<IReference>> getAllTemporaryClassStorages() {
		return tempClassMap.values() ;
	}
	
	

	@Override
	public void clear() {
		this.tempClassMap.clear() ;
		this.classMap.clear() ;
	}

	@Override
	public void registerResolvedRefs(IDeclaration method, Collection<SimpleClassReference> referenceSet) {
		
		//qualifiedNmae existence is guaranteed here
		IJavaTypeDeclaration owner = method.getOwner();
		if(owner==null)
			return ;
		
		List<String> values = owner.getQualifiedName().getValues();
		if( values.size() != 1 )
			return ;
		
		String classQualifiedName = values.iterator().next() ;
		
		IClassOutline outline = BasicOutlineContainer.getInstance().getClassOutline(classQualifiedName);
		if(outline==null)
			return ;
		
		IClassReferenceStorage<SimpleClassReference> refStorage = classMap.get(classQualifiedName);
		if(refStorage==null){
			refStorage = new ClassReferenceStorage<SimpleClassReference>( outline ) ;
			classMap.put(classQualifiedName, refStorage) ;
		}
		refStorage.addMethodReferences(method, referenceSet) ;		
	}

	@Override
	public IClassReferenceStorage<SimpleClassReference> getResolvedReferenceListForClass(
			String classQualifiedName) {

		return classMap.get(classQualifiedName) ;
	}

	@Override
	public void clearTemporaryRefs() {
		this.tempClassMap.clear() ;		
	}

	@Override
	public Collection<IClassReferenceStorage<SimpleClassReference>> getAllResolvedClassStorages() {
		return classMap.values() ;
	}

}
