package net.junos.space.pmd.utils.outline;

import java.util.Collection;
import java.util.List;



public class GenericParam implements IGenericParam {
	
	private boolean ownerIsMethod ;
	
	private boolean ownerIsClass ;

	private QualifiedName ownerQualifiedName;
	
	private int order ;
	
	private String name ;
	
	private QualifiedName qName ;
	
	private boolean isArray ;
	
	private int arrayDepth ;
	
	@Override
	public boolean ownerIsClass() {
		return ownerIsClass ;
	}

	@Override
	public void setOwnerIsClass() {
		this.ownerIsClass = true ;
		this.ownerIsMethod = false ;		
	}

	@Override
	public boolean ownerIsMethod() {
		return this.ownerIsMethod ;
	}

	@Override
	public void setOwnerIsMethod() {
		this.ownerIsClass = false ;
		this.ownerIsMethod = true ;		
	}

	@Override
	public QualifiedName getOwnerQualifiedName() {
		return ownerQualifiedName ;
	}

	public void setOwnerQualifiedName(QualifiedName ownerQualifiedName) {
		this.ownerQualifiedName = ownerQualifiedName;
	}

	@Override
	public int getOrder() {		
		return order ;
	}

	@Override
	public void setOrder(int order) {
		this.order = order ;		
	}

	@Override
	public IJavaTypeDeclaration getDeclaration() {
		return null;
	}

	@Override
	public boolean isArray() {
		return isArray;
	}

	@Override
	public void setIsArray(boolean isArray) {
		this.isArray = isArray ;
	}

	@Override
	public List<IJavaType> getGenericParameters() {
		return null;
	}

	@Override
	public void setGenericParameters(Collection<IJavaType> params) {}

	@Override
	public void setDeclaration(IJavaTypeDeclaration declaration) {}

	@Override
	public int getArrayDepth() {
		return arrayDepth ;
	}

	@Override
	public void setArrayDepth(int arrayDepth) {
		this.arrayDepth = arrayDepth ;
	}

	@Override
	public String getSimpleName() {
		return name;
	}

	@Override
	public void setSimpleName(String simpleName) {
		this.name = simpleName;
	}

	@Override
	public QualifiedName getQualifiedName() {
		return qName;
	}

	@Override
	public void setQualifiedName(QualifiedName qName) {
		this.qName = qName ;
		this.name = qName.getSimpleName() ;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + arrayDepth;
		result = prime * result + (isArray ? 1231 : 1237);
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + order;
		result = prime * result + (ownerIsClass ? 1231 : 1237);
		result = prime * result + (ownerIsMethod ? 1231 : 1237);
		result = prime
				* result
				+ ((ownerQualifiedName == null) ? 0 : ownerQualifiedName
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GenericParam other = (GenericParam) obj;
		if (arrayDepth != other.arrayDepth)
			return false;
		if (isArray != other.isArray)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (order != other.order)
			return false;
		if (ownerIsClass != other.ownerIsClass)
			return false;
		if (ownerIsMethod != other.ownerIsMethod)
			return false;
		if (ownerQualifiedName == null) {
			if (other.ownerQualifiedName != null)
				return false;
		} else if (!ownerQualifiedName.equals(other.ownerQualifiedName))
			return false;
		return true;
	}
	
	@Override
	public String toString(){
		return getQualifiedName()!=null? getQualifiedName().toString() : getSimpleName() ;
	}

	@Override
	public boolean isSimilar(IJavaType p2) {		
		return true ;
	}
}
