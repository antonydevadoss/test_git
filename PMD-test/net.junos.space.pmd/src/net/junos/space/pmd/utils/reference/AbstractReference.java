package net.junos.space.pmd.utils.reference;

public class AbstractReference implements IReference {

	public AbstractReference(int beginLine, int endLine) {
		super();
		this.beginLine = beginLine;
		this.endLine = endLine;
	}

	int beginLine ;
	
	int endLine ;
	@Override
	public int getBeginLine() {
		return endLine ;
	}

	@Override
	public int getEndLine() {
		return endLine;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + beginLine;
		result = prime * result + endLine;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractReference other = (AbstractReference) obj;
		if (beginLine != other.beginLine)
			return false;
		if (endLine != other.endLine)
			return false;
		return true;
	}

}
