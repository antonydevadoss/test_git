package net.junos.space.pmd.utils.reference;

public class FieldAccessReference extends AbstractReference {

	public FieldAccessReference(IReference headReference, String fieldName,int beginLine, int endLine) {
		super(beginLine, endLine);
		this.headReference = headReference;
		this.fieldName = fieldName;
	}	
	
	private final IReference headReference ;
	private final String fieldName ;
	public IReference getHeadReference() {
		return headReference;
	}
	public String getFieldName() {
		return fieldName;
	}

}
