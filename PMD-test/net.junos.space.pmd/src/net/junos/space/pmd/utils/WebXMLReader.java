/*******************************************************************************
 * FILE NAME: WebXMLReader.java
 * PURPOSE:   This is reading web.xml present in WEBSVC project by using DOM parsing 
 * and loading listener and context parameters.
 *
 *
 * Revision History: 
 * DATE:           				  AUTHOR:              	CHANGE:  
 * Mar 03, 2011		              Amit Arora	       	Initial Version  
 * 
 * 
 ******************************************************************************/
package net.junos.space.pmd.utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


/**
 * Responsible for reading web.xml from WEBSVC project using DOM. Then it fetches
 * //listener/listener-class and stores them in List using XPATH and also stores all 
 * //context-param/param-name and //contex-param/param-value in Map.
 */
public class WebXMLReader extends XMLReader{
	
	/**
	 * Logger
	 */
	private Logger log = Logger.getLogger(WebXMLReader.class);
	
	/**
	 * Collection of listeners in web.xml
	 */
	private List<String> listeners;

	/**
	 * Map of context parameters present in web.xml
	 */
	private Map<String, String> contextParamMap;
	
	/**
	 * Default constructor calling parseXML
	 */
	public WebXMLReader(){
		parseXML();
	}
	
	/**
	 * Returns all listeners
	 * @return the listeners
	 */
	public List<String> getListeners() {
		return listeners;
	}
	
	/**
	 * Returns context parameter map
	 * @return the contextParamMap
	 */
	public String getContextParamater(String contextParamName) {
		return contextParamMap.get(contextParamName);
	}

	@Override
	protected void parseXML() {
		listeners = new ArrayList<String>();
		contextParamMap = new HashMap<String, String>();
		String webXMLFilePath = getXMLFilePath(PMDConstants.WEB_SVC_PROJECT, PMDConstants.WEB_XML_DIR, PMDConstants.WEB_XML_NAME, false);
		try {			
			Document xmlDocument = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(webXMLFilePath); 
			XPathFactory  factory=XPathFactory.newInstance();
			
			XPath listenerXPath=factory.newXPath();
			NodeList listenerNodes = (NodeList) listenerXPath.evaluate(PMDConstants.LISTENER_XPATH, xmlDocument, XPathConstants.NODESET);
			for(int i = 0; i < listenerNodes.getLength(); i++){
				Element listenerNode = (Element) listenerNodes.item(i);				
				listeners.add(listenerNode.getTextContent().trim());
			}
			
			XPath contextParamXPath=factory.newXPath();			
			NodeList contextParamNodes = (NodeList) contextParamXPath.evaluate(PMDConstants.CONTEXT_PARAM_XPATH, xmlDocument, XPathConstants.NODESET);
			for(int i = 0; i < contextParamNodes.getLength(); i++){
				Element contextParamNode = (Element) contextParamNodes.item(i);
				NodeList contextParamChildren = contextParamNode.getChildNodes();
				String key = null, value = null;
				for(int j = 0; j < contextParamChildren.getLength(); j++){
					if(contextParamChildren.item(j) instanceof Element){
						Element contextParamChild = (Element) contextParamChildren.item(j);
						if(contextParamChild.getNodeName().equals(PMDConstants.CONTEXT_PARAM_NAME)){
							key = contextParamChild.getTextContent().trim();						
						}
						else if(contextParamChild.getNodeName().equals(PMDConstants.CONTEXT_PARAM_VALUE)){
							value = contextParamChild.getTextContent().trim();						
							contextParamMap.put(key, value);
						}
					}
				}				
			}			
		} catch (FileNotFoundException e) {			
			log.error(e.getMessage(), e);
		} catch (XPathExpressionException e) {			
			log.error(e.getMessage(), e);
		} catch (SAXException e) {			
			log.error(e.getMessage(), e);
		} catch (IOException e) {			
			log.error(e.getMessage(), e);
		} catch (ParserConfigurationException e) {
			log.error(e.getMessage(), e);
		}
	}
	

}
